# BlueLapiz Server Plugin

Travis-CL build status: 
[![Travis Status](https://travis-ci.com/SagenKoder/BlueLapizSource.svg?token=qnk2xDtLocZbphj3qxx2&branch=master)](https://travis-ci.com/SagenKoder/BlueLapizSource)

```
The major difference between a thing that migth go wrong and a thing
that cannot possibly go wrong is that when a thing that cannot
possibly go wrong goes wrong it usually turns out to be impossible to
get at or repair

- Douglas Adams
```
