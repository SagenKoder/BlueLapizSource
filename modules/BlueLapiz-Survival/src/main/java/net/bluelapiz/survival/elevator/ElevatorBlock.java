/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.elevator;

import org.bukkit.block.Block;

public class ElevatorBlock {

    public Elevator elevator;
    public Block block;

    public ElevatorBlock(Block block, Elevator elevator) {
        this.block = block;
        this.elevator = elevator;
    }

}
