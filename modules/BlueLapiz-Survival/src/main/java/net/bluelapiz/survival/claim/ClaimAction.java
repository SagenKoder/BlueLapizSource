/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

public enum ClaimAction {

    NONE(),
    USE_DOORS(),
    USE_LEVERS(),
    USE_BUTTONS(),
    USE_PRESSURE_PLATES(),
    BUILD(),
    BREAK(),
    KILL(),
    INTERACT_ENTITY(),
    DAMAGE_ENTITY(),
    INTERACT_INVENTORY_BLOCK(),
    INTERACT_INTERACTABLE_BLOCK(),
    TRAMPLE_FARMLAND(),
    GRANT_OTHERS(),
    DENY_OTHERS(),
    TRANSFER_CLAIM(),
    IGNITE(),
    PLACE_LAVA(),
    PLACE_WATER(),
    SPAWN_ENTITY(),
    FISH_ENTITY(),
    SET_HOME(),
    USE_BED(),
    USE_ELEVATOR(),
    CLAIM_SIDE_BY_SIDE(),
    EXPAND_CLAIM()

}
