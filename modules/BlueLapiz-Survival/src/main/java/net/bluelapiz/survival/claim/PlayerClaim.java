/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 12:03 AM                                              *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import net.bluelapiz.survival.Survival;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.UUID;

public class PlayerClaim extends Claim {

    public PlayerClaim(UUID owner, String name, String world, int xMin, int yMin, int xMax, int yMax) {
        super(owner, name, world, xMin, yMin, xMax, yMax);
    }

    @Override
    public boolean handlePlayerActionAndReturnResult(Player player, ClaimAction action, int x, int z) {
        ClaimRank permissionLevel = getPlayerPermission(player.getUniqueId());
        if (!permissionLevel.can(action)) {
            player.spigot().sendMessage(
                    ChatMessageType.ACTION_BAR,
                    TextComponent.fromLegacyText(
                            Survival.lang().getTranslatedString(player, "action.claim.playerclaim.owned_by")
                                    .replace("<owner>", getOwnerName())));
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "PlayerClaim{" +
                "owner=" + owner +
                ", name='" + name + '\'' +
                ", world='" + world + '\'' +
                ", xMin=" + xMin +
                ", zMin=" + zMin +
                ", xMax=" + xMax +
                ", zMax=" + zMax +
                ", permittedPlayers=" + permittedPlayers +
                ", defaultPermissionLevel=" + defaultPermissionLevel +
                '}';
    }
}
