/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 12/1/18 1:51 PM                                                *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.entity.Player;

import java.util.UUID;

public class ClaimVisualiser {

    private Particle.DustOptions redDust = new Particle.DustOptions(Color.RED, 1);
    private Particle.DustOptions greenDust = new Particle.DustOptions(Color.GREEN, 1);
    private Particle.DustOptions blueDust = new Particle.DustOptions(Color.BLUE, 1);

    ClaimVisualiser() {
    }

    void update() {

        for (UUID uuid : ClaimManager.get().getAllPlayersShowing()) {
            Player p = Bukkit.getPlayer(uuid);
            if (p == null) continue;
            for (Claim c : ClaimManager.get().getAllClaimsInWorld(p.getWorld().getName())) {
                if (c.getPlayerPermission(uuid).isAtLeast(ClaimRank.MANAGE)) render(c, p, greenDust);
                else if (c.getPlayerPermission(uuid).isAtLeast(ClaimRank.BUILD)) render(c, p, blueDust);
                else render(c, p, redDust);
            }
        }
    }

    public void render(Claim c, Player p, Particle.DustOptions dustOptions) {

        int playerX = p.getLocation().getBlockX();
        int playerY = p.getLocation().getBlockY();
        int playerZ = p.getLocation().getBlockZ();


        if (c.distanceFromClaimSqr(playerX, playerZ) > Math.pow(10, 2)) return;

        int xMin = c.getxMin();
        int xMax = c.getxMax() + 1;

        int yMin = playerY - 1;
        int yMax = playerY + 5;

        int zMin = c.getzMin();
        int zMax = c.getzMax() + 1;

        // render the xMin wall
        if (Math.abs(xMin - playerX) <= 8) { // only render this wall if nearby
            for (int y = yMin; y <= yMax; y++) {
                for (int z = Math.max(zMin, playerZ - 5); z <= Math.min(zMax, playerZ + 5); z++) {
                    // renderParticle at xMin, y, z
                    p.spawnParticle(Particle.REDSTONE, xMin, y, z, 0, dustOptions);
                }
            }
        }

        // render the zMin wall
        if (Math.abs(zMin - playerZ) <= 8) { // only render this wall if nearby
            for (int y = yMin; y <= yMax; y++) {
                for (int x = Math.max(xMin, playerX - 6); x <= Math.min(xMax, playerX + 6); x++) {
                    // renderParticle at x, y, zMin
                    p.spawnParticle(Particle.REDSTONE, x, y, zMin, 0, dustOptions);
                }
            }
        }

        // render the xMax wall
        if (Math.abs(xMax - playerX) <= 8) { // only render this wall if nearby
            for (int y = yMin; y <= yMax; y++) {
                for (int z = Math.max(zMin, playerZ - 6); z <= Math.min(zMax, playerZ + 6); z++) {
                    // renderParticle at xMin, y, z
                    p.spawnParticle(Particle.REDSTONE, xMax, y, z, 0, dustOptions);
                }
            }
        }

        // render the zMax wall
        if (Math.abs(zMax - playerZ) <= 8) { // only render this wall if nearby
            for (int y = yMin; y <= yMax; y++) {
                for (int x = Math.max(xMin, playerX - 6); x <= Math.min(xMax, playerX + 6); x++) {
                    // renderParticle at x, y, zMin
                    p.spawnParticle(Particle.REDSTONE, x, y, zMax, 0, dustOptions);
                }
            }
        }
    }

}
