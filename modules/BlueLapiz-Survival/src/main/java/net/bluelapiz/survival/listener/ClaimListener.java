/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 6:09 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.listener;

import net.bluelapiz.survival.claim.Claim;
import net.bluelapiz.survival.claim.ClaimAction;
import net.bluelapiz.survival.claim.ClaimManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.world.StructureGrowEvent;
import org.bukkit.projectiles.ProjectileSource;

import java.util.*;
import java.util.stream.Collectors;

public class ClaimListener implements Listener {

    private static final Set<Material> DOOR_TYPES = new HashSet<>(Arrays.asList(
            // doors
            Material.DARK_OAK_DOOR,
            Material.ACACIA_DOOR,
            Material.BIRCH_DOOR,
            Material.IRON_DOOR,
            Material.JUNGLE_DOOR,
            Material.OAK_DOOR,
            Material.SPRUCE_DOOR,

            // trapdoors
            Material.DARK_OAK_TRAPDOOR,
            Material.ACACIA_TRAPDOOR,
            Material.BIRCH_TRAPDOOR,
            Material.IRON_TRAPDOOR,
            Material.JUNGLE_TRAPDOOR,
            Material.OAK_TRAPDOOR,
            Material.SPRUCE_TRAPDOOR,

            // fence gates
            Material.ACACIA_FENCE_GATE,
            Material.BIRCH_FENCE_GATE,
            Material.DARK_OAK_FENCE_GATE,
            Material.JUNGLE_FENCE_GATE,
            Material.OAK_FENCE_GATE,
            Material.SPRUCE_FENCE_GATE
    ));
    private static final Set<Material> BUTTON_TYPES = new HashSet<>(Arrays.asList(
            Material.ACACIA_BUTTON,
            Material.BIRCH_BUTTON,
            Material.DARK_OAK_BUTTON,
            Material.JUNGLE_BUTTON,
            Material.OAK_BUTTON,
            Material.SPRUCE_BUTTON,
            Material.STONE_BUTTON
    ));
    private static final Set<Material> LEVER_TYPES = new HashSet<>(Arrays.asList(
            Material.LEVER
    ));
    private static final Set<Material> PRESSUREPLATE_TYPES = new HashSet<>(Arrays.asList(
            Material.ACACIA_PRESSURE_PLATE,
            Material.BIRCH_PRESSURE_PLATE,
            Material.DARK_OAK_PRESSURE_PLATE,
            Material.HEAVY_WEIGHTED_PRESSURE_PLATE,
            Material.JUNGLE_PRESSURE_PLATE,
            Material.LIGHT_WEIGHTED_PRESSURE_PLATE,
            Material.OAK_PRESSURE_PLATE,
            Material.SPRUCE_PRESSURE_PLATE,
            Material.STONE_PRESSURE_PLATE
    ));
    private static final Set<Material> INVENTORY_BLOCK_TYPES = new HashSet<>(Arrays.asList(
            Material.CHEST,
            Material.TRAPPED_CHEST,
            Material.ENDER_CHEST,
            Material.ANVIL,
            Material.CHIPPED_ANVIL,
            Material.DAMAGED_ANVIL,
            Material.FURNACE,
            Material.BEACON,
            Material.HOPPER,
            Material.DISPENSER,
            Material.DROPPER,
            Material.JUKEBOX
    ));
    private static final Set<Material> INTERACTABLE_BLOCK_TYPES = new HashSet<>(Arrays.asList(
            Material.CRAFTING_TABLE,
            Material.TNT,
            Material.CAKE,
            Material.ENCHANTING_TABLE,
            Material.DAYLIGHT_DETECTOR,
            Material.NOTE_BLOCK,
            Material.FLOWER_POT,
            Material.REPEATER,
            Material.CHAIN_COMMAND_BLOCK,
            Material.COMMAND_BLOCK_MINECART,
            Material.COMMAND_BLOCK,
            Material.REPEATING_COMMAND_BLOCK,
            Material.COMPARATOR,
            Material.BREWING_STAND,
            Material.CAULDRON
    ));
    private static final Set<EntityType> PROTECTED_ENTITY_TYPES = new HashSet<>(Arrays.asList(
            EntityType.ARMOR_STAND,
            EntityType.BAT,
            EntityType.BOAT,
            EntityType.CHICKEN,
            EntityType.COD,
            EntityType.COW,
            EntityType.DOLPHIN,
            EntityType.DONKEY,
            EntityType.ELDER_GUARDIAN,
            EntityType.ENDER_CRYSTAL,
            EntityType.IRON_GOLEM,
            EntityType.HORSE,
            EntityType.ITEM_FRAME,
            EntityType.LEASH_HITCH,
            EntityType.LLAMA,
            EntityType.MINECART,
            EntityType.MINECART_CHEST,
            EntityType.MINECART_COMMAND,
            EntityType.MINECART_FURNACE,
            EntityType.MINECART_HOPPER,
            EntityType.MINECART_MOB_SPAWNER,
            EntityType.MINECART_TNT,
            EntityType.MULE,
            EntityType.MUSHROOM_COW,
            EntityType.OCELOT,
            EntityType.PAINTING,
            EntityType.PARROT,
            EntityType.PIG,
            EntityType.POLAR_BEAR,
            EntityType.PUFFERFISH,
            EntityType.RABBIT,
            EntityType.SALMON,
            EntityType.SHEEP,
            EntityType.SNOWMAN,
            EntityType.SQUID,
            EntityType.TRIDENT,
            EntityType.TURTLE,
            EntityType.VILLAGER,
            EntityType.WOLF,
            EntityType.MINECART_TNT,
            EntityType.COD,
            EntityType.SALMON,
            EntityType.TROPICAL_FISH
    ));
    private static Set<Material> SPAWN_ENTITY_ITEMS = new HashSet<>(Arrays.asList(
            Material.MINECART,
            Material.CHEST_MINECART,
            Material.COMMAND_BLOCK_MINECART,
            Material.FURNACE_MINECART,
            Material.HOPPER_MINECART,
            Material.TNT_MINECART,
            Material.BIRCH_BOAT,
            Material.ACACIA_BOAT,
            Material.DARK_OAK_BOAT,
            Material.JUNGLE_BOAT,
            Material.OAK_BOAT,
            Material.SPRUCE_BOAT,
            Material.ARMOR_STAND,
            Material.BONE_MEAL,
            Material.BUCKET
    ));

    static {
        INVENTORY_BLOCK_TYPES.addAll(Arrays.stream(Material.values())
                .filter(m -> m.name().contains("SHULKER_BOX"))
                .collect(Collectors.toSet()));
    }

    static {
        SPAWN_ENTITY_ITEMS.addAll(Arrays.stream(Material.values())
                .filter(m -> m.name().contains("EGG"))
                .collect(Collectors.toSet()));
    }

    public static boolean handlePlayerActionAndReturnResult(Player player, ClaimAction claimAction, int x, int z) {
        if (claimAction == ClaimAction.NONE) return false;
        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(player.getWorld().getName(), x, z);
        if (!optClaim.isPresent()) return false;
        Claim claim = optClaim.get();
        return claim.handlePlayerActionAndReturnResult(player, claimAction, x, z);
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.BUILD,
                e.getBlock().getX(),
                e.getBlock().getZ());
        if (denied) {
            e.setCancelled(true);
            e.setBuild(false);
        }

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.BUILD,
                e.getBlock().getX(),
                e.getBlock().getZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onTrampleFarmland(PlayerInteractEvent event) {

        if (event.getAction() == Action.PHYSICAL) {
            Block block = event.getClickedBlock();
            if (block == null) return;
            if (block.getType() == Material.FARMLAND || block.getType() == Material.TURTLE_EGG) {
                boolean denied = handlePlayerActionAndReturnResult(
                        event.getPlayer(),
                        ClaimAction.TRAMPLE_FARMLAND,
                        block.getX(),
                        block.getZ());
                if (denied) {
                    event.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
                    event.setCancelled(true);
                }
            }
        }

    }

    @EventHandler
    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.INTERACT_ENTITY,
                e.getRightClicked().getLocation().getBlockX(),
                e.getRightClicked().getLocation().getBlockZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.INTERACT_ENTITY,
                e.getRightClicked().getLocation().getBlockX(),
                e.getRightClicked().getLocation().getBlockZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onArmourstandManipulate(PlayerArmorStandManipulateEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.INTERACT_ENTITY,
                e.getRightClicked().getLocation().getBlockX(),
                e.getRightClicked().getLocation().getBlockZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {

        if ((e.getRemover() instanceof Player)) {
            Player remover = (Player) e.getRemover();

            boolean denied = handlePlayerActionAndReturnResult(
                    remover,
                    ClaimAction.DAMAGE_ENTITY,
                    e.getEntity().getLocation().getBlockX(),
                    e.getEntity().getLocation().getBlockZ());
            if (denied) {
                e.setCancelled(true);
            }
        }

    }

    @EventHandler
    public void onFrameBrake(HangingBreakEvent e) {

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(
                e.getEntity().getWorld().getName(),
                e.getEntity().getLocation().getBlockX(),
                e.getEntity().getLocation().getBlockZ());
        if (!optClaim.isPresent()) return;

        if (e.getCause().toString().equals("EXPLOSION")) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {

        boolean projectile = false;
        Player remover;
        if ((e.getDamager() instanceof Projectile)) {
            ProjectileSource shooter = ((Projectile) e.getDamager()).getShooter();
            if (shooter instanceof Player) {
                remover = (Player) shooter;
                projectile = true;
            } else {
                return;
            }
        } else if ((e.getDamager() instanceof Player)) {
            remover = (Player) e.getDamager();
        } else {
            return;
        }

        if (PROTECTED_ENTITY_TYPES.contains(e.getEntity().getType())) {

            boolean denied = handlePlayerActionAndReturnResult(
                    remover,
                    ClaimAction.DAMAGE_ENTITY,
                    e.getEntity().getLocation().getBlockX(),
                    e.getEntity().getLocation().getBlockZ());
            if (denied) {
                if (projectile) {
                    e.getDamager().remove();
                }
                e.setCancelled(true);
            }

        }

    }

    @EventHandler
    public void onHangingBreak(HangingPlaceEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.INTERACT_ENTITY,
                e.getEntity().getLocation().getBlockX(),
                e.getEntity().getLocation().getBlockZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onBlockFromTo(BlockFromToEvent e) {
        // ignore dragon eggs
        if (e.getBlock().getType() != Material.LAVA && e.getBlock().getType() != Material.WATER) return;

        Block block = e.getBlock();
        Block toBlock = e.getToBlock();

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(toBlock.getWorld().getName(), toBlock.getX(), toBlock.getZ());
        if (!optClaim.isPresent()) return; // does not flow into a Claim

        Optional<Claim> optClaimFrom = ClaimManager.get().getClaimAt(block.getWorld().getName(), block.getX(), block.getZ());
        if (!optClaimFrom.isPresent() || !optClaim.get().equals(optClaimFrom.get())) { // lava starts in another Claim or outside of claims
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (optClaim.isPresent()) {
            e.blockList().clear();
            return;
        }

        for (Block b : e.blockList()) {
            Optional<Claim> optClaimBlock = ClaimManager.get().getClaimAt(b.getWorld().getName(), b.getX(), b.getZ());
            if (optClaimBlock.isPresent()) {
                e.blockList().clear();
                return;
            }
        }

    }

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(e.getEntity().getWorld().getName(), e.getEntity().getLocation().getBlockX(), e.getEntity().getLocation().getBlockZ());
        if (optClaim.isPresent()) {
            e.blockList().clear();
            return;
        }

        for (Block b : e.blockList()) {
            Optional<Claim> optClaimBlock = ClaimManager.get().getClaimAt(b.getWorld().getName(), b.getX(), b.getZ());
            if (optClaimBlock.isPresent()) {
                e.blockList().clear();
                return;
            }
        }
    }

    @EventHandler
    public void onEntityDamageByExplosion(EntityDamageEvent e) {

        if (e.getEntity() instanceof Player) return; // let explosions harm players

        if (e.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION || e.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) {

            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(
                    e.getEntity().getWorld().getName(),
                    e.getEntity().getLocation().getBlockX(),
                    e.getEntity().getLocation().getBlockZ());
            if (!optClaim.isPresent()) return;

            e.setCancelled(true);

        }

    }

    @EventHandler
    public void onPlayerFish(PlayerFishEvent e) {

        if (e.getState() != PlayerFishEvent.State.CAUGHT_ENTITY) return;

        Entity onHook = e.getCaught();
        if (onHook == null) return;

        if (PROTECTED_ENTITY_TYPES.contains(onHook.getType())) {

            boolean denied = handlePlayerActionAndReturnResult(
                    e.getPlayer(),
                    ClaimAction.FISH_ENTITY,
                    onHook.getLocation().getBlockX(),
                    onHook.getLocation().getBlockZ());
            if (denied) {
                if (e.getHook() != null) e.getHook().remove();
                e.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent e) {

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (!optClaim.isPresent()) return;

        e.setCancelled(true);

    }

    @EventHandler
    public void onBlockSpread(BlockSpreadEvent e) {

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (!optClaim.isPresent()) return;

        if (e.getSource().getType() != Material.FIRE) { // if mycelium++, only deny across borders
            Optional<Claim> optClaimFrom = ClaimManager.get().getClaimAt(e.getSource().getWorld().getName(), e.getSource().getX(), e.getSource().getZ());
            if (!optClaimFrom.isPresent() || !optClaim.get().equals(optClaimFrom.get())) {
                return;
            }
        }

        e.setCancelled(true);

    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        ClaimManager.get().playerLeave(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onPistonExtend(BlockPistonExtendEvent e) {

        Optional<Claim> optClaimPiston = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (optClaimPiston.isPresent()) return;

        BlockFace direction = e.getDirection();

        for (Block b : e.getBlocks()) {
            b = b.getRelative(direction);
            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(b.getWorld().getName(), b.getX(), b.getZ());
            if (!optClaim.isPresent()) continue;
            e.setCancelled(true);
            break;
        }

    }

    @EventHandler
    public void onPistonRetract(BlockPistonRetractEvent e) {

        Optional<Claim> optClaimPiston = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (optClaimPiston.isPresent()) return;

        for (Block b : e.getBlocks()) {
            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(b.getWorld().getName(), b.getX(), b.getZ());
            if (!optClaim.isPresent()) continue;
            e.setCancelled(true);
            break;
        }

    }

    @EventHandler
    public void onDispenser(BlockDispenseEvent e) {

        Optional<Claim> optClaimPiston = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (optClaimPiston.isPresent()) return;

        BlockFace[] blockFaces = new BlockFace[]{BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST};
        for (BlockFace bf : blockFaces) {
            Block next = e.getBlock().getRelative(bf);
            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(next.getWorld().getName(), next.getX(), next.getZ());
            if (!optClaim.isPresent()) continue;
            e.setCancelled(true);
            break;
        }

    }

    @EventHandler
    public void onEntityChangeBlock(EntityChangeBlockEvent e) {

        Optional<Claim> optClaimPiston = ClaimManager.get().getClaimAt(e.getBlock().getWorld().getName(), e.getBlock().getX(), e.getBlock().getZ());
        if (!optClaimPiston.isPresent()) return;

        e.setCancelled(true);

    }

    @EventHandler
    public void onVehicleDestroy(VehicleDestroyEvent e) {

        if (!(e.getAttacker() instanceof Player)) return;

        boolean denied = handlePlayerActionAndReturnResult(
                ((Player) e.getAttacker()),
                ClaimAction.DAMAGE_ENTITY,
                e.getVehicle().getLocation().getBlockX(),
                e.getVehicle().getLocation().getBlockZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onStructureGrow(StructureGrowEvent e) {

        Optional<Claim> optClaimPiston = ClaimManager.get().getClaimAt(e.getWorld().getName(), e.getLocation().getBlockX(), e.getLocation().getBlockZ());
        if (optClaimPiston.isPresent()) return;

        List<BlockState> remove = new ArrayList<>();
        for (BlockState b : e.getBlocks()) {

            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(b.getWorld().getName(), b.getX(), b.getZ());
            if (!optClaim.isPresent()) continue;
            remove.add(b);

        }
        e.getBlocks().removeAll(remove);

    }

    @EventHandler
    public void onPlayerLeashEntity(PlayerLeashEntityEvent e) {

        boolean denied = handlePlayerActionAndReturnResult(
                e.getPlayer(),
                ClaimAction.INTERACT_ENTITY,
                e.getEntity().getLocation().getBlockX(),
                e.getEntity().getLocation().getBlockZ());
        if (denied) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent e) {
        Player player = e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = e.getClickedBlock();
            if (block == null) return;

            ClaimAction claimAction = ClaimAction.NONE;

            if (e.getItem() != null && e.getItem().getType().equals(Material.FLINT_AND_STEEL)) {
                claimAction = ClaimAction.IGNITE;
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.LAVA_BUCKET)) {
                claimAction = ClaimAction.PLACE_LAVA;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.WATER_BUCKET)) {
                claimAction = ClaimAction.PLACE_WATER;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.LAVA)) {
                claimAction = ClaimAction.PLACE_LAVA;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.WATER)) {
                claimAction = ClaimAction.PLACE_WATER;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.TROPICAL_FISH_BUCKET)) {
                claimAction = ClaimAction.PLACE_WATER;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.COD_BUCKET)) {
                claimAction = ClaimAction.PLACE_WATER;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.PUFFERFISH_BUCKET)) {
                claimAction = ClaimAction.PLACE_WATER;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && e.getItem().getType().equals(Material.SALMON_BUCKET)) {
                claimAction = ClaimAction.PLACE_WATER;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getItem() != null && SPAWN_ENTITY_ITEMS.contains(e.getItem().getType())) {
                claimAction = ClaimAction.SPAWN_ENTITY;
                block = block.getRelative(e.getBlockFace());
            } else if (e.getClickedBlock().getType().name().contains("BED")) {
                claimAction = ClaimAction.USE_BED;
            } else if (INVENTORY_BLOCK_TYPES.contains(block.getType())) {
                claimAction = ClaimAction.INTERACT_INVENTORY_BLOCK;
            } else if (INTERACTABLE_BLOCK_TYPES.contains(block.getType())) {
                claimAction = ClaimAction.INTERACT_INTERACTABLE_BLOCK;
            } else if (DOOR_TYPES.contains(block.getType())) {
                claimAction = ClaimAction.USE_DOORS;
            } else if (LEVER_TYPES.contains(block.getType())) {
                claimAction = ClaimAction.USE_LEVERS;
            } else if (BUTTON_TYPES.contains(block.getType())) {
                claimAction = ClaimAction.USE_BUTTONS;
            } else if (PRESSUREPLATE_TYPES.contains(block.getType())) {
                claimAction = ClaimAction.USE_PRESSURE_PLATES;
            }

            boolean denied = handlePlayerActionAndReturnResult(player, claimAction, block.getX(), block.getZ());
            if (denied) {
                e.setCancelled(true);
                e.setUseInteractedBlock(Event.Result.DENY);
            }
        } else if (e.getAction() == Action.PHYSICAL) {

            ClaimAction claimAction = ClaimAction.NONE;

            if (PRESSUREPLATE_TYPES.contains(e.getClickedBlock().getType())) {
                claimAction = ClaimAction.USE_PRESSURE_PLATES;
            }

            boolean denied = handlePlayerActionAndReturnResult(player, claimAction, e.getClickedBlock().getX(), e.getClickedBlock().getZ());
            if (denied) {
                e.setCancelled(true);
                e.setUseInteractedBlock(Event.Result.DENY);
            }

        }
    }

    @EventHandler
    public void onEntitySpawn(EntitySpawnEvent e) {
        if (e.getEntityType() == EntityType.PLAYER) return;

        Optional<Claim> optClaim = ClaimManager.get().getClaimAt(
                e.getLocation().getWorld().toString(),
                e.getLocation().getBlockX(),
                e.getLocation().getBlockZ());
        if (!optClaim.isPresent()) return;
        Claim claim = optClaim.get();

        if (e.getEntity() instanceof Creature) {
            if (e.getEntity() instanceof Monster && !claim.monsterCanSpawn()) {
                e.setCancelled(true);
            } else if (!claim.animalCanSpawn()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onExpandClaim(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK) {

            Optional<Claim> optExpandedClaim = ClaimManager.get().getClaimExpandedBy(p.getUniqueId());
            if (!optExpandedClaim.isPresent()) return;
            ClaimManager.get().removeClaimExpandedBy(p.getUniqueId());
            Claim claim = optExpandedClaim.get();
            e.setCancelled(true);

            if (claim.isInRegion(e.getClickedBlock())) {
                p.sendMessage("§cYou can only expand a claim outward!");
                return;
            }

            int clickedX = e.getClickedBlock().getX();
            int clickedZ = e.getClickedBlock().getZ();

            int xMin = Math.min(claim.getxMin(), clickedX);
            int xMax = Math.max(claim.getxMax(), clickedX);
            int zMin = Math.min(claim.getzMin(), clickedZ);
            int zMax = Math.max(claim.getzMax(), clickedZ);

            int maxClaimLength = 250;
            int maxArea = 10_000;

            int xLength = xMax - xMin + 1;
            int zLength = zMax - zMin + 1;
            int area = xLength * zLength;

            if (xLength > maxClaimLength || zLength > maxClaimLength) {
                p.sendMessage("§cThe claim can not have longer sides than §f" + maxClaimLength + "§c!");
                p.sendMessage("§cYou tried to expand claim to be §f" + xLength + " §cx §f" + zLength + " §c(§f" + area + "§c) blocks.");
                return;
            }

            if (area > maxArea) {
                p.sendMessage("§cThe claim cannot be bigger than §f" + maxArea + " §cblocks in total!");
                p.sendMessage("§cYou tried to expand claim to be §f" + xLength + " §cx §f" + zLength + " §c(§f" + area + "§c) blocks.");
                return;
            }

            int distanceToNextClaim = 50;

            Location ploc = p.getLocation();
            Set<Claim> overlappingClaims = ClaimManager.get().getOverlappingClaims(
                    ploc.getWorld().getName(),
                    xMin - distanceToNextClaim,
                    zMin - distanceToNextClaim,
                    xMax + distanceToNextClaim,
                    zMax + distanceToNextClaim);

            // ignore claims where the player has permissions
            Set<Claim> overlappingIllegalClaims = new HashSet<>();
            for (Claim c : overlappingClaims) {
                if (!c.getPlayerPermission(p.getUniqueId()).can(ClaimAction.CLAIM_SIDE_BY_SIDE)) {
                    overlappingIllegalClaims.add(claim);
                }
            }

            // too close to nearby claims
            if (overlappingIllegalClaims.size() != 0) {
                p.sendMessage("§cThis claim is too close to the following claims (minimum distance is " + distanceToNextClaim + " blocks):");
                for (Claim c : overlappingIllegalClaims) {
                    p.sendMessage("§c    - " + c.getName() + " owned by " + c.getOwnerName());
                }
                return;
            }

            overlappingClaims = ClaimManager.get().getOverlappingClaims(
                    p.getWorld().getName(), xMin, zMin, xMax, zMax);

            if (overlappingClaims.size() > 1) {
                p.sendMessage("§cThis claim will overlap with the following claims:");
                for (Claim c : overlappingClaims) {
                    p.sendMessage("§c    - " + c.getName() + " owned by " + c.getOwnerName());
                }
                return;
            }

            ClaimManager.get().setShowing(p.getUniqueId(), true);
            claim.setNewBorders(xMin, zMin, xMax, zMax);
            claim.setShouldSave(true);

            p.sendMessage("§aYou expanded the claim " + claim.getName() + " to be §f" + xLength + " §ax §f" + zLength + " §a(§f" + area + "§a) blocks!");

        }
    }

}
