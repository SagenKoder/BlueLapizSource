/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.elevator;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.Optional;

public class HiddenElevator extends Elevator {

    @Override
    public String getElevatorName() {
        return "Skjult Heis";
    }


    @Override
    public boolean isElevatorBlock(Block b) {
        if (!b.getType().isSolid() || !isSafeTeleportTo(b.getRelative(BlockFace.UP).getLocation())) return false;

        for (BlockFace blockFace : new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST}) {
            Block closest = b.getRelative(blockFace);
            if (closest.getType().equals(Material.IRON_BLOCK)) {
                Block furthest = closest.getRelative(blockFace);
                if (furthest.getType().equals(Material.REDSTONE_TORCH) || furthest.getType().equals(Material.REDSTONE_WALL_TORCH)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Optional<Block> isPartOfElevator(Block b) {


        if (b.getType().equals(Material.REDSTONE_TORCH) || b.getType().equals(Material.REDSTONE_TORCH)) {

            for (BlockFace blockFace : new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST}) {
                Block relative = b.getRelative(blockFace);
                Block relative2 = b.getRelative(blockFace, 2);
                if (relative.getType().equals(Material.IRON_BLOCK) && relative2.getType().isSolid()) {
                    return Optional.of(relative2);
                }
            }
        } else if (b.getType().equals(Material.IRON_BLOCK)) {

            for (BlockFace blockFace : new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST}) {
                Block forward = b.getRelative(blockFace);
                Block backward = b.getRelative(blockFace, -1);
                if (forward.getType().isSolid() && (backward.getType().equals(Material.REDSTONE_TORCH) || backward.getType().equals(Material.REDSTONE_WALL_TORCH))) {
                    return Optional.of(forward);
                }
            }

        } else if (b.getType().isSolid()) {

            for (BlockFace blockFace : new BlockFace[]{BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST}) {

                Block relative = b.getRelative(blockFace);
                Block relative2 = b.getRelative(blockFace, 2);
                if (relative.getType().equals(Material.IRON_BLOCK) && (relative2.getType().equals(Material.REDSTONE_TORCH) || relative2.getType().equals(Material.REDSTONE_WALL_TORCH))) {
                    return Optional.of(b);
                }

            }
        }

        return Optional.empty();
    }

}
