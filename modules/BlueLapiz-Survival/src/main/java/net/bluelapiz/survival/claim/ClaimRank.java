/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum ClaimRank {

    STRANGER(0,
            ClaimAction.NONE),
    ACCESS(1,
            STRANGER.getAllowedClaimActions(),
            ClaimAction.USE_DOORS,
            ClaimAction.USE_BUTTONS,
            ClaimAction.USE_LEVERS,
            ClaimAction.USE_PRESSURE_PLATES,
            ClaimAction.SET_HOME,
            ClaimAction.USE_BED,
            ClaimAction.USE_ELEVATOR),
    CONTAINER(2,
            ACCESS.getAllowedClaimActions(),
            ClaimAction.INTERACT_ENTITY,
            ClaimAction.INTERACT_INVENTORY_BLOCK,
            ClaimAction.FISH_ENTITY),
    BUILD(3,
            CONTAINER.getAllowedClaimActions(),
            ClaimAction.DAMAGE_ENTITY,
            ClaimAction.BUILD,
            ClaimAction.BREAK,
            ClaimAction.TRAMPLE_FARMLAND,
            ClaimAction.KILL,
            ClaimAction.PLACE_LAVA,
            ClaimAction.PLACE_WATER,
            ClaimAction.SPAWN_ENTITY,
            ClaimAction.IGNITE,
            ClaimAction.CLAIM_SIDE_BY_SIDE),
    MANAGE(4,
            BUILD.getAllowedClaimActions(),
            ClaimAction.DENY_OTHERS,
            ClaimAction.GRANT_OTHERS,
            ClaimAction.EXPAND_CLAIM),
    OWNER(5,
            ClaimAction.values()); // grant owner everything

    private int level;
    private Set<ClaimAction> claimActions;

    ClaimRank(int level, ClaimAction... allowedActions) {
        this.level = level;
        this.claimActions = new HashSet<>(Arrays.asList(allowedActions));
    }

    ClaimRank(int level, Set<ClaimAction> allowedActionsSet, ClaimAction... allowedActions) {
        this.level = level;
        this.claimActions = new HashSet<>(Arrays.asList(allowedActions));
        this.claimActions.addAll(allowedActionsSet);
    }

    public boolean isAtLeast(ClaimRank other) {
        return this.level >= other.level;
    }

    public boolean can(ClaimAction action) {
        return claimActions.contains(action);
    }

    public Set<ClaimAction> getAllowedClaimActions() {
        return claimActions;
    }
}
