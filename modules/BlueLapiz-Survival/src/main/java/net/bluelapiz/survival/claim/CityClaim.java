/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 12:02 AM                                              *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import net.bluelapiz.survival.Survival;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CityClaim extends Claim {

    private List<Claim> subclaims = new ArrayList<>();
    private boolean city;

    public CityClaim(UUID owner, String name, boolean city, String world, int xMin, int yMin, int xMax, int yMax) {
        super(owner, name, world, xMin, yMin, xMax, yMax);
        this.city = city;

        this.getBlueBossbar().setTitle("§3§lThe " + getTypeName() + " of §f§l" + getName());
        this.getGreenBossbar().setTitle("§a§lThe " + getTypeName() + " of §f§l" + getName());
        this.getYellowBossbar().setTitle("§e§lThe " + getTypeName() + " of §f§l" + getName());
        this.getRedBossbar().setTitle("§c§lThe " + getTypeName() + " of §f§l" + getName());
    }

    public boolean isCity() {
        return city;
    }

    public String getTypeName() {
        return city ? "City" : "Settlement";
    }

    public List<Claim> getSubclaims() {
        return subclaims;
    }

    public Claim getSubclaim(int x, int z) {
        for (Claim claim : subclaims) {
            if (claim.isInRegion(x, z))
                return claim;
        }
        // return the whole city if no subclaim
        return this;
    }

    @Override
    public boolean handlePlayerActionAndReturnResult(Player player, ClaimAction action, int x, int z) {
        Claim subClaim = getSubclaim(x, z);
        ClaimRank permissionLevel = subClaim.getPlayerPermission(player.getUniqueId());
        if (!permissionLevel.can(action)) {
            player.spigot().sendMessage(
                    ChatMessageType.ACTION_BAR,
                    TextComponent.fromLegacyText(Survival.lang().getTranslatedString(player, "action.claim.cityclaim.owned_by")
                            .replace("{1}", getTypeName())
                            .replace("{0}", getName())));
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "CityClaim{" +
                "subclaims=" + subclaims +
                ", city=" + city +
                ", owner=" + owner +
                ", name='" + name + '\'' +
                ", world='" + world + '\'' +
                ", xMin=" + xMin +
                ", zMin=" + zMin +
                ", xMax=" + xMax +
                ", zMax=" + zMax +
                ", permittedPlayers=" + permittedPlayers +
                ", defaultPermissionLevel=" + defaultPermissionLevel +
                '}';
    }
}
