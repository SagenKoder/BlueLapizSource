/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import net.bluelapiz.survival.Survival;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ClaimManager {

    private static ClaimManager claimManager;
    BossBar showingClaimsBar = Bukkit.createBossBar("§7Showing claim borders. Disable with §f/claim hide§7.", BarColor.PURPLE, BarStyle.SEGMENTED_20);
    private Map<String, Set<Claim>> worldClaims = new ConcurrentHashMap<>();
    private Map<UUID, Claim> lastPlayerClaims = new ConcurrentHashMap<>();
    private Map<UUID, Claim> playerExpandClaim = new ConcurrentHashMap<>();
    private Set<UUID> showBorders = new HashSet<>();

    private ClaimVisualiser claimVisualiser = new ClaimVisualiser();
    private ClaimManager() {
        // TODO: load all from database
        Bukkit.getScheduler().runTaskTimerAsynchronously(Survival.get(), () -> {
            for (Player o : Bukkit.getOnlinePlayers()) {
                Optional<Claim> currentRegion = getClaimAt(o.getLocation());
                Claim lastRegion = lastPlayerClaims.get(o.getUniqueId());
                if (currentRegion.isPresent()) {
                    if (currentRegion.get() != lastRegion) {
                        if (lastRegion != null) lastRegion.removePlayerFromBossbar(o);
                        currentRegion.get().addPlayerToBossbar(o);
                        lastPlayerClaims.put(o.getUniqueId(), currentRegion.get());
                    }
                } else {
                    if (lastRegion != null) {
                        lastRegion.removePlayerFromBossbar(o);
                        lastPlayerClaims.remove(o.getUniqueId());
                    }
                }

                if (playerExpandClaim.containsKey(o.getUniqueId())) {
                    Claim claim = playerExpandClaim.get(o.getUniqueId());
                    o.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(
                            "§b§lClick anywhere to expand the claim §f" + claim.getName() + "§b§l!"));
                }
            }
        }, 10, 5);

        // update visuals
        Bukkit.getScheduler().runTaskTimerAsynchronously(Survival.get(), claimVisualiser::update, 5, 5);
    }

    public static ClaimManager get() {
        if (claimManager == null) claimManager = new ClaimManager();
        return claimManager;
    }

    public void shutdown() {
        showingClaimsBar.removeAll();
        for (Claim pr : getAllClaims()) {
            for (Player o : Bukkit.getOnlinePlayers()) {
                pr.removePlayerFromBossbar(o);
            }
        }
    }

    public void playerLeave(UUID uuid) {
        lastPlayerClaims.remove(uuid);
        setShowing(uuid, false);
    }

    public Set<Claim> getAllClaimsInWorld(String world) {
        return worldClaims.getOrDefault(world, new HashSet<>());
    }

    public Set<Claim> getAllClaims() {
        HashSet<Claim> claims = new HashSet<>();
        for (Set<Claim> claimInWorld : worldClaims.values()) {
            claims.addAll(claimInWorld);
        }
        return claims;
    }

    void updateBossbarFor(UUID uuid) {
        lastPlayerClaims.remove(uuid);
    }

    public Optional<Claim> getClaimAt(Block block) {
        return getClaimAt(block.getWorld().getName(), block.getX(), block.getZ());
    }

    public Optional<Claim> getClaimAt(Location location) {
        return getClaimAt(location.getWorld().getName(), location.getBlockX(), location.getBlockZ());
    }

    public Optional<Claim> getClaimAt(String world, int x, int z) {
        for (Claim c : getAllClaimsInWorld(world)) {
            if (c.isInRegion(x, z)) return Optional.of(c);
        }
        return Optional.empty();
    }

    public Set<Claim> getOverlappingClaims(String world, int xMin, int zMin, int xMax, int zMax) {
        HashSet<Claim> overlappingClaims = new HashSet<>();
        for (Claim c : getAllClaimsInWorld(world)) {
            if (c.overlaps(xMin, zMin, xMax, zMax))
                overlappingClaims.add(c);
        }
        return overlappingClaims;
    }

    public Set<Claim> getAllClaimsByPlayer(UUID uuid) {
        Set<Claim> playerClaims = new HashSet<>();
        for (Claim claim : getAllClaims()) {
            if (claim.getOwner().equals(uuid)) playerClaims.add(claim);
        }
        return playerClaims;
    }

    public Optional<Claim> getClaimByPlayerAndName(UUID uuid, String name) {
        for (Claim claim : getAllClaims()) {
            if (claim.getOwner().equals(uuid) && claim.getName().equalsIgnoreCase(name)) return Optional.of(claim);
        }
        return Optional.empty();
    }

    public void deleteClaim(Claim protectedRegion) {
        // todo: delete from database
        getAllClaimsInWorld(protectedRegion.getWorld()).remove(protectedRegion);
    }

    public void addClaim(Claim protectedRegion) {
        // todo: add to database
        if (!worldClaims.containsKey(protectedRegion.getWorld()))
            worldClaims.put(protectedRegion.getWorld(), new HashSet<>());
        worldClaims.get(protectedRegion.getWorld()).add(protectedRegion);
    }

    public ClaimVisualiser getClaimVisualiser() {
        return claimVisualiser;
    }

    public Optional<Claim> getClaimExpandedBy(UUID player) {
        if (!playerExpandClaim.containsKey(player)) return Optional.empty();
        return Optional.of(playerExpandClaim.get(player));
    }

    public void removeClaimExpandedBy(UUID player) {
        playerExpandClaim.remove(player);
    }

    public void setClaimExpandedBy(UUID player, Claim claim) {
        playerExpandClaim.put(player, claim);
    }

    public boolean isShowing(UUID uuid) {
        return showBorders.contains(uuid);
    }

    public void setShowing(UUID uuid, boolean showing) {
        if (showing && !isShowing(uuid)) {
            showBorders.add(uuid);
            showingClaimsBar.addPlayer(Bukkit.getPlayer(uuid));
        } else if (!showing && isShowing(uuid)) {
            showBorders.remove(uuid);
            showingClaimsBar.removePlayer(Bukkit.getPlayer(uuid));
        }
    }

    public Set<UUID> getAllPlayersShowing() {
        return showBorders;
    }
}
