/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class TpaCommand extends BLCommand {

    public static final int TIME_OUT = 30_000; // 30 sec

    public static HashMap<UUID, LinkedHashMap<UUID, Long>> tpaList = new HashMap<>();

    public TpaCommand() {
        super("tpa", "tpaccept");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.only_players"));
            return true;
        }
        Player p = (Player) sender;

        switch (label.toLowerCase()) {
            case "tpa":
                if (args.length != 1) {
                    sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpa.wrong_usage"));
                    return true;
                }

                Player r = Bukkit.getPlayer(args[0]);
                if (r == null) {
                    sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.cant_find_player")
                            .replace("<name>", args[0]));
                    return true;
                }

                if (r == p) {
                    sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpa.tp_self"));
                    return true;
                }

                if (!tpaList.containsKey(r.getUniqueId())) {
                    tpaList.put(r.getUniqueId(), new LinkedHashMap<>());
                }

                LinkedHashMap<UUID, Long> playerTpaMap = tpaList.get(r.getUniqueId());
                if (playerTpaMap.containsKey(p.getUniqueId()) && (playerTpaMap.get(p.getUniqueId()) + TIME_OUT > System.currentTimeMillis())) {
                    p.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpa.already_sent"));
                    return true;
                }

                playerTpaMap.put(p.getUniqueId(), System.currentTimeMillis());

                p.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpa.request_sent")
                        .replace("<player>", r.getName()));
                r.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpa.request")
                        .replace("<player>", p.getName()));
                break;
            case "tpaccept":
                if (!tpaList.containsKey(p.getUniqueId())) {
                    sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpaccept.no_requests"));
                    return true;
                }
                if (args.length == 0) {
                    playerTpaMap = tpaList.get(p.getUniqueId());
                    for (Map.Entry<UUID, Long> e : playerTpaMap.entrySet()) {
                        r = Bukkit.getPlayer(e.getKey());
                        if (e.getValue() + TIME_OUT > System.currentTimeMillis() && r != null) {
                            r.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpaccept.other_accepted")
                                    .replace("<player>", p.getName()));
                            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpaccept.you_accepted")
                                    .replace("<player>", r.getName()));
                            r.teleport(p);
                            return true;
                        }
                    }
                    sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpaccept.no_requests"));
                    return true;
                } else if (args.length == 1) {

                } else {
                    sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.tpaccept.wrong_usage"));
                    return true;
                }
                break;
        }


        return true;
    }
}
