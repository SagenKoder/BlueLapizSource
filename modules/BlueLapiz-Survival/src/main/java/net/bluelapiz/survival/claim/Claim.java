/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 6:04 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import net.bluelapiz.common.player.PlayerManager_legacy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Claim {

    protected UUID owner;
    protected String name;
    protected String world;
    protected int xMin;
    protected int zMin;
    protected int xMax;
    protected int zMax;

    protected boolean monsterCanSpawn = true;
    protected boolean animalCanSpawn = true;

    protected boolean shouldSave = false;
    protected HashMap<UUID, ClaimRank> permittedPlayers = new HashMap<>();
    protected ClaimRank defaultPermissionLevel = ClaimRank.STRANGER;
    private BossBar greenBossbar;
    private BossBar blueBossbar;
    private BossBar yellowBossbar;
    private BossBar redBossbar;

    public Claim(UUID owner, String name, String world, int xMin, int zMin, int xMax, int zMax) {
        this.owner = owner;
        this.name = name;
        this.world = world;
        this.xMin = Math.min(xMin, xMax);
        this.zMin = Math.min(zMin, zMax);
        this.xMax = Math.max(xMin, xMax);
        this.zMax = Math.max(zMin, zMax);

        this.greenBossbar = Bukkit.createBossBar("§a§lLand claimed by §f§l" + getOwnerName(), BarColor.GREEN, BarStyle.SOLID);
        this.blueBossbar = Bukkit.createBossBar("§3§lLand claimed by §f§l" + getOwnerName(), BarColor.BLUE, BarStyle.SOLID);
        this.yellowBossbar = Bukkit.createBossBar("§e§lLand claimed by §f§l" + getOwnerName(), BarColor.YELLOW, BarStyle.SOLID);
        this.redBossbar = Bukkit.createBossBar("§c§lLand claimed by §f§l" + getOwnerName(), BarColor.RED, BarStyle.SOLID);
    }

    public ClaimRank getDefaultPermissionLevel() {
        return defaultPermissionLevel;
    }

    public void setDefaultPermissionLevel(ClaimRank defaultPermissionLevel) {
        this.defaultPermissionLevel = defaultPermissionLevel;
    }

    public void givePlayerPermission(UUID uuid, ClaimRank permissionLevel) {
        if (permissionLevel.equals(ClaimRank.STRANGER)) {
            permittedPlayers.remove(uuid);
        }
        if (permissionLevel.equals(ClaimRank.OWNER)) {
            permittedPlayers.remove(uuid);
            owner = uuid;
        }
        permittedPlayers.put(uuid, permissionLevel);

        ClaimManager.get().updateBossbarFor(uuid);
    }

    public ClaimRank getPlayerPermission(UUID uuid) {
        if (uuid.equals(owner)) return ClaimRank.OWNER;
        if (!permittedPlayers.containsKey(uuid)) return defaultPermissionLevel;
        ClaimRank returnLevel = permittedPlayers.get(uuid);
        if (returnLevel.isAtLeast(defaultPermissionLevel))
            return returnLevel;
        else return defaultPermissionLevel;
    }

    public Set<UUID> getAllPlayersWith(ClaimRank... permissionLevel) {
        List<ClaimRank> matchPermissions = Arrays.asList(permissionLevel);
        Set<UUID> returnSet = permittedPlayers.entrySet().stream()
                .filter(e -> matchPermissions.contains(e.getValue()))
                .map(e -> e.getKey())
                .collect(Collectors.toSet());
        if (matchPermissions.contains(ClaimRank.OWNER))
            returnSet.add(owner);
        return returnSet;
    }

    public boolean isInRegion(Location location) {
        return isInRegion(location.getBlockX(), location.getBlockZ());
    }

    public boolean isInRegion(Block block) {
        return isInRegion(block.getX(), block.getZ());
    }

    public boolean isInRegion(double x, double z) {
        return x >= xMin && x <= xMax &&
                z >= zMin && z <= zMax;
    }

    public UUID getOwner() {
        return owner;
    }

    public int calculateArea() {
        // max + 1 because it is inclusive both ends
        return Math.abs((xMax + 1) - xMin) * Math.abs((zMax + 1) - zMin);
    }

    public boolean overlaps(int xMin, int zMin, int xMax, int zMax) {
        if (xMax < this.xMin) return false;
        if (zMax < this.zMin) return false;
        if (xMin > this.xMax) return false;
        return zMin <= this.zMax;
    }

    public String getOwnerName() {
        return PlayerManager_legacy.get().getNameFromUuid(owner);
    }

    public String getName() {
        return name;
    }

    public String getWorld() {
        return world;
    }

    public int getxMin() {
        return xMin;
    }

    public int getzMin() {
        return zMin;
    }

    public int getxMax() {
        return xMax;
    }

    public int getzMax() {
        return zMax;
    }

    public boolean shouldSave() {
        return shouldSave;
    }

    public void setShouldSave(boolean shouldSave) {
        this.shouldSave = shouldSave;
    }

    public BossBar getGreenBossbar() {
        return greenBossbar;
    }

    public BossBar getBlueBossbar() {
        return blueBossbar;
    }

    public BossBar getYellowBossbar() {
        return yellowBossbar;
    }

    public BossBar getRedBossbar() {
        return redBossbar;
    }

    public void addPlayerToBossbar(Player player) {
        ClaimRank permissionLevel = getPlayerPermission(player.getUniqueId());
        if (permissionLevel == ClaimRank.STRANGER || permissionLevel == ClaimRank.ACCESS) {
            redBossbar.addPlayer(player);
            greenBossbar.removePlayer(player);
            blueBossbar.removePlayer(player);
            yellowBossbar.removePlayer(player);
        } else if (permissionLevel == ClaimRank.CONTAINER) {
            redBossbar.removePlayer(player);
            greenBossbar.removePlayer(player);
            blueBossbar.removePlayer(player);
            yellowBossbar.addPlayer(player);
        } else if (permissionLevel == ClaimRank.BUILD) {
            redBossbar.removePlayer(player);
            greenBossbar.removePlayer(player);
            blueBossbar.addPlayer(player);
            yellowBossbar.removePlayer(player);
        } else if (permissionLevel == ClaimRank.MANAGE || permissionLevel == ClaimRank.OWNER) {
            redBossbar.removePlayer(player);
            greenBossbar.addPlayer(player);
            blueBossbar.removePlayer(player);
            yellowBossbar.removePlayer(player);
        }
    }

    public void removePlayerFromBossbar(Player player) {
        redBossbar.removePlayer(player);
        greenBossbar.removePlayer(player);
        blueBossbar.removePlayer(player);
        yellowBossbar.removePlayer(player);
    }

    public abstract boolean handlePlayerActionAndReturnResult(Player player, ClaimAction action, int x, int z);

    public boolean monsterCanSpawn() {
        return monsterCanSpawn;
    }

    public void setMonsterCanSpawn(boolean monsterCanSpawn) {
        this.monsterCanSpawn = monsterCanSpawn;
    }

    public boolean animalCanSpawn() {
        return animalCanSpawn;
    }

    public void setAnimalCanSpawn(boolean animalCanSpawn) {
        this.animalCanSpawn = animalCanSpawn;
    }

    public double distanceFromClaimSqr(double x, double z) {
        double cx = Math.max(Math.min(x, xMax), xMin);
        double cz = Math.max(Math.min(z, zMax), zMin);

        return Math.pow(x - cx, 2) + Math.pow(z - cz, 2);
    }

    public double distanceFromClaim(double x, double z) {
        return Math.sqrt(distanceFromClaimSqr(x, z));
    }

    public void setNewBorders(int xMin, int zMin, int xMax, int zMax) {
        this.xMin = Math.min(xMin, xMax);
        this.xMax = Math.max(xMin, xMax);
        this.zMin = Math.min(zMin, zMax);
        this.zMax = Math.max(zMin, zMax);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Claim claim = (Claim) o;
        return xMin == claim.xMin &&
                zMin == claim.zMin &&
                xMax == claim.xMax &&
                zMax == claim.zMax &&
                Objects.equals(owner, claim.owner) &&
                Objects.equals(name, claim.name) &&
                Objects.equals(world, claim.world);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, name, world, xMin, zMin, xMax, zMax);
    }

    @Override
    public abstract String toString();
}
