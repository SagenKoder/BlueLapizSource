/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.common.player.HomeManager;
import net.bluelapiz.common.player.objects.Home;
import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.LinkedList;

public class ListhomeCommand extends BLCommand {

    public ListhomeCommand() {
        super("listhomes", "homes", "listhome");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.only_players"));
            return true;
        }

        Player p = (Player) sender;

        LinkedList<Home> homes = HomeManager.get("survival").getAllHomesOf(p.getUniqueId());

        if (homes.size() == 0) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.no_homes"));
            return true;
        }

        p.sendMessage(Survival.lang().getTranslatedString(sender, "command.listhome.listtitle"));
        for (Home h : homes) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.listhome.listelement")
                    .replace("<home>", h.getName())
                    .replace("<world>", h.getWorld())
                    .replace("<x>", h.getX() + "")
                    .replace("<y>", h.getY() + "")
                    .replace("<z>", h.getZ() + ""));
        }
        p.sendMessage(Survival.lang().getTranslatedString(sender, "command.listhome.listfooter"));
        return true;
    }
}
