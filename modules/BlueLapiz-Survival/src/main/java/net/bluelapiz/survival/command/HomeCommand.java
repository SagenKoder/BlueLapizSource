/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.common.player.HomeManager;
import net.bluelapiz.common.player.objects.Home;
import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HomeCommand extends BLCommand {

    public HomeCommand() {
        super("home");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.only_players"));
            return true;
        }

        Player p = (Player) sender;

        if (HomeManager.get("survival").getAllHomesOf(p.getUniqueId()).size() == 0) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.no_homes"));
            return true;
        }

        if (args.length == 0 && HomeManager.get("survival").getAllHomesOf(p.getUniqueId()).size() == 1) {
            Home home = HomeManager.get("survival").getAllHomesOf(p.getUniqueId()).getFirst();
            World world = Bukkit.getWorld(home.getWorld());
            if (world == null) {
                p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.world_unloaded")
                        .replace("<world>", home.getWorld()));
                return true;
            }
            Location loc = new Location(world, home.getX() + 0.5, home.getY() + 0.5, home.getZ() + 0.5, home.getYaw(), home.getPitch());
            p.teleport(loc);
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.teleported")
                    .replace("<home>", home.getName()));
            return true;
        } else if (args.length == 0) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.choose_home"));
            p.performCommand("homes");
            return true;
        }

        if (args.length != 1) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.wrong_usage"));
            return true;
        }

        Home home = HomeManager.get("survival").getHomeOf(p.getUniqueId(), args[0]);

        if (home == null) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.no_home")
                    .replace("<name>", args[0]));
            p.performCommand("homes");
        }

        World world = Bukkit.getWorld(home.getWorld());
        if (world == null) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.world_unloaded")
                    .replace("<world>", home.getWorld()));
            return true;
        }
        Location loc = new Location(world, home.getX() + 0.5, home.getY() + 0.5, home.getZ() + 0.5, home.getYaw(), home.getPitch());
        p.teleport(loc);
        p.sendMessage(Survival.lang().getTranslatedString(sender, "command.home.teleported")
                .replace("<home>", home.getName()));
        return true;
    }
}
