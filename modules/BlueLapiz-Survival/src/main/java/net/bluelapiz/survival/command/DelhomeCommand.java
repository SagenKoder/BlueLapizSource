/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.common.player.HomeManager;
import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelhomeCommand extends BLCommand {

    public DelhomeCommand() {
        super("delhome", "deletehome", "removehome", "dhome");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.only_players"));
            return true;
        }

        Player p = (Player) sender;

        if (args.length != 1) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.delhome.wrongusage"));
            return true;
        }

        if (HomeManager.get("survival").getHomeOf(p.getUniqueId(), args[0]) == null) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.delhome.no_home")
                    .replace("<name>", args[0]));
            return true;
        }

        HomeManager.get("survival").deleteHome(p.getUniqueId(), args[0]);
        p.sendMessage(Survival.lang().getTranslatedString(sender, "command.delhome.deleted_home")
                .replace("<name>", args[0]));
        return true;
    }
}
