/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/30/18 6:25 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import net.bluelapiz.survival.claim.ClaimManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

public class RandomTeleportCommand extends BLCommand {

    private Map<UUID, Set<Long>> cooldownMap = new ConcurrentHashMap<>();

    public RandomTeleportCommand() {
        super("rtp", "randomteleport", "wild");
        Bukkit.getScheduler().runTaskTimerAsynchronously(Survival.get(), () -> {
            List<UUID> removeCooldown = new ArrayList<>();
            for (Map.Entry<UUID, Set<Long>> e : cooldownMap.entrySet()) {
                e.getValue().removeIf(t -> t + 20 * 60 * 20 < System.currentTimeMillis());
                if (e.getValue().size() == 0) removeCooldown.add(e.getKey());
            }
            removeCooldown.forEach(cooldownMap::remove);
        }, 10 * 20, 10 * 20);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly players!");
            return true;
        }
        Player p = (Player) sender;

        if (cooldownMap.containsKey(p.getUniqueId()) && cooldownMap.get(p.getUniqueId()).size() >= 3) {
            p.sendMessage("§cYou can only use this command 3 times every 20 minutes!");
            return true;
        }

        // add to cooldown map
        if (!cooldownMap.containsKey(p.getUniqueId())) cooldownMap.put(p.getUniqueId(), new HashSet<>());
        cooldownMap.get(p.getUniqueId()).add(System.currentTimeMillis());

        ThreadLocalRandom random = ThreadLocalRandom.current();

        Block block;
        for (int i = 0; ; i++) {
            if (i > 20) { // max 20 tries...
                p.sendMessage("§cCould not find a good location!");
                return true;
            }
            block = p.getWorld().getHighestBlockAt(random.nextInt(-15000, 15000), random.nextInt(-15000, 15000));
            if (block.getType() == Material.WATER || block.getType() == Material.LAVA) continue;
            if (ClaimManager.get().getClaimAt(block).isPresent()) continue;
            break;
        }
        Location loc = block.getRelative(BlockFace.UP).getLocation().add(0.5, 0.5, 0.5);
        p.teleport(loc);
        p.sendMessage("§aYou were teleported to a random location (§f" + loc.getBlockX() + "§a, §f" + loc.getBlockY() + "§a, §f" + loc.getBlockZ() + "§a)!");

        return true;
    }
}
