/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 1:40 AM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.listener;

import net.bluelapiz.common.player.HomeManager;
import net.bluelapiz.survival.Survival;
import net.bluelapiz.survival.command.TpaCommand;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class PlayerListener implements Listener {

    public static final int RESPAWN_MAX_RADIUS = 100;

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        HomeManager.get("survival").loadHomesOf(e.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        // remove tpa requests to player
        TpaCommand.tpaList.remove(e.getPlayer().getUniqueId());

        HomeManager.get("survival").unloadHomesOf(e.getPlayer().getUniqueId());
    }

    @EventHandler // teleport player to a place nearby
    public void onRespawn(PlayerRespawnEvent e) {
        Survival.log.debug("PLAYER RESPAWN EVENT");

        Player p = e.getPlayer();

        int oldX = p.getLocation().getBlockX();
        int oldY = p.getLocation().getBlockY();
        int oldZ = p.getLocation().getBlockZ();
        boolean isSafeSpawn;
        int counter = 0, x, y, z;
        do {
            double a = Math.random() * 2 * Math.PI;
            double r = RESPAWN_MAX_RADIUS * Math.sqrt(Math.random());
            x = oldX + (int) (r * Math.cos(a));
            y = Math.max(oldY - 20, 5); // start 30 blocks under and find first viable spawn location
            z = oldZ + (int) (r * Math.sin(a));
            Block tmp = p.getWorld().getBlockAt(x, y, z);
            while(!tmp.isPassable() || !tmp.getRelative(BlockFace.UP).isPassable()) tmp = tmp.getRelative(BlockFace.UP);
            isSafeSpawn = isSafeSpawn(tmp);
        } while(counter++ < 20 && !isSafeSpawn);

        if(!isSafeSpawn) {
            // fallback to default spawn
            p.sendMessage("§cCould not find a safe respawn location for you! You died at (§e" + oldX + "§c,§e" + oldY + "§c,§e" + oldX + "§c)!");
        } else {
            e.setRespawnLocation(new Location(p.getWorld(), x + .5, y + .25, z + .5));
            p.sendMessage("§cYou respawned §e" + ((int)p.getLocation().distance(e.getRespawnLocation())) + " §cblocks from where you died!");
        }
    }

    private boolean isSafeSpawn(Block block) {
        return true;
    }

    @EventHandler
    public void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        ItemStack mainHand = p.getInventory().getItemInMainHand();

        ThreadLocalRandom random = ThreadLocalRandom.current();
        List<Material> shovels = Arrays.asList(
                Material.DIAMOND_SHOVEL,
                Material.IRON_SHOVEL,
                Material.GOLDEN_SHOVEL,
                Material.WOODEN_SHOVEL,
                Material.STONE_SHOVEL);

        if (p.getGameMode() != GameMode.SURVIVAL) return;

        if (mainHand != null && shovels.contains(mainHand.getType())) {
            float fortune = mainHand.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) / 100f;
            if (e.getBlock().getType().equals(Material.SAND)) {
                if (random.nextDouble() < 0.04 + fortune * 1.5) {
                    ItemStack itemStack = new ItemStack(Material.GLOWSTONE_DUST, random.nextInt(1, 2));
                    p.getWorld().dropItemNaturally(e.getBlock().getLocation().add(0.5d, 0.5d, 0.5d), itemStack);
                }
            }
            if (e.getBlock().getType().equals(Material.GRAVEL)) {
                if (random.nextDouble() < 0.04 + fortune * 1.5) {
                    ItemStack itemStack = new ItemStack(Material.SLIME_BALL, 1);
                    p.getWorld().dropItemNaturally(e.getBlock().getLocation().add(0.5d, 0.5d, 0.5d), itemStack);
                }
            }
        }
    }
}
