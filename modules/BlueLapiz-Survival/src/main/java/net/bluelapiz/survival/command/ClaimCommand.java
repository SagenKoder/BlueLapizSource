/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:37 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import net.bluelapiz.survival.claim.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClaimCommand extends BLCommand {

    public ClaimCommand() {
        super("claim");
    }

    @SuppressWarnings("Duplicates")
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            Survival.lang().send(sender, "command.only_players");
            return true;
        }
        Player player = (Player) sender;

        if (args.length == 0) {
            sendCommandList(player);
            return true;
        } else if (args[0].equalsIgnoreCase("deny")) {

            if (args.length != 2) {
                player.sendMessage("§cWrong usage! /claim deny <player>");
                return true;
            }

            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(player.getLocation());
            if (!optClaim.isPresent()) {
                Survival.lang().send(player, "command.claim.not_in_claim");
                return true;
            }

            Claim claim = optClaim.get();

            if (!(claim instanceof PlayerClaim)) {
                if (claim instanceof CityClaim) {
                    Survival.lang().send(player, "command.claim.city_claim");
                } else {
                    Survival.lang().send(player, "command.claim.server_claim");
                }
                return true;
            }

            if (!claim.getPlayerPermission(player.getUniqueId()).isAtLeast(ClaimRank.MANAGE)) {
                Survival.lang().send(player, "command.claim.deny.need_manage");
                return true;
            }

            UUID otherUuid;
            Player other = Bukkit.getPlayer(args[1]);
            if (other != null) {
                otherUuid = other.getUniqueId();
            } else {
                otherUuid = PlayerManager_legacy.get().getUuidFromName(args[1]);
            }
            if (otherUuid == null) {
                Survival.lang().send(player, "command.cant_find_player",
                        args[1]);
                return true;
            }

            if (otherUuid.equals(player.getUniqueId())) {
                Survival.lang().send(player, "command.claim.deny.demote_self");
                return true;
            }

            ClaimRank oldRank = claim.getPlayerPermission(otherUuid);
            if (oldRank == ClaimRank.STRANGER) {
                Survival.lang().send(player, "command.claim.deny.not_trusted");
                return true;
            }

            if (oldRank == ClaimRank.MANAGE && claim.getPlayerPermission(player.getUniqueId()) != ClaimRank.OWNER) {
                Survival.lang().send(player, "command.claim.deny.need_owner");
                return true;
            }

            if (oldRank == ClaimRank.OWNER) {
                Survival.lang().send(player, "command.claim.deny.demote_owner");
                return true;
            }

            claim.givePlayerPermission(otherUuid, ClaimRank.STRANGER);
            Survival.lang().send(player, "command.claim.deny.denied_user",
                    PlayerManager_legacy.get().getNameFromUuid(otherUuid));

            return true;

        } else if (args[0].equalsIgnoreCase("grant")) {

            if (args.length != 3) {
                player.sendMessage("§cWrong usage! /claim grant <player> <permission>");
                return true;
            }

            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(player.getLocation());
            if (!optClaim.isPresent()) {
                Survival.lang().send(player, "command.claim.not_in_claim");
                return true;
            }

            Claim claim = optClaim.get();

            if (!(claim instanceof PlayerClaim)) {
                if (claim instanceof CityClaim) {
                    Survival.lang().send(player, "command.claim.city_claim");
                } else {
                    Survival.lang().send(player, "command.claim.server_claim");
                }
                return true;
            }

            if (!claim.getPlayerPermission(player.getUniqueId()).isAtLeast(ClaimRank.MANAGE)) {
                Survival.lang().send(player, "command.claim.grant.need_manage");
                return true;
            }

            UUID otherUuid;
            Player other = Bukkit.getPlayer(args[1]);
            if (other != null) {
                otherUuid = other.getUniqueId();
            } else {
                otherUuid = PlayerManager_legacy.get().getUuidFromName(args[1]);
            }

            if (otherUuid == null) {
                Survival.lang().send(player, "command.cant_find_player",
                        args[1]);
                return true;
            }

            if (otherUuid.equals(player.getUniqueId())) {
                Survival.lang().send(player, "command.claim.grant.self");
                return true;
            }

            ClaimRank newRank = ClaimRank.valueOf(args[2].toUpperCase());

            if (claim.getPlayerPermission(otherUuid) == newRank) {
                Survival.lang().send(player, "command.claim.grant.already_trust");
                return true;
            }

            ClaimRank oldRank = claim.getPlayerPermission(otherUuid);

            if (newRank == ClaimRank.OWNER || oldRank == ClaimRank.OWNER) {
                Survival.lang().send(player, "command.claim.grant.change_owner");
                return true;
            }

            if ((newRank == ClaimRank.MANAGE || oldRank == ClaimRank.MANAGE) && claim.getPlayerPermission(player.getUniqueId()) != ClaimRank.OWNER) {
                Survival.lang().send(player, "command.claim.grant.owner_manager");
                return true;
            }

            if (newRank == ClaimRank.STRANGER) {
                Survival.lang().send(player, "command.claim.grant.use_deny");
                return true;
            }

            claim.givePlayerPermission(otherUuid, newRank);

            Survival.lang().send(player, "command.claim.grant.granted_player",
                    PlayerManager_legacy.get().getNameFromUuid(otherUuid),
                    newRank.name().toLowerCase());
            return true;

        } else if (args[0].equalsIgnoreCase("list")) {

            if (args.length != 1) {
                player.sendMessage("§cWrong usage! /claim list");
                return true;
            }

            Set<Claim> ownedClaims = ClaimManager.get().getAllClaimsInWorld(player.getWorld().getName()).stream()
                    .filter(c -> c.getPlayerPermission(player.getUniqueId()) == ClaimRank.OWNER)
                    .collect(Collectors.toSet());

            Set<Claim> managedClaims = ClaimManager.get().getAllClaimsInWorld(player.getWorld().getName()).stream()
                    .filter(c -> c.getPlayerPermission(player.getUniqueId()) == ClaimRank.MANAGE)
                    .collect(Collectors.toSet());

            if (ownedClaims.size() == 0 && managedClaims.size() == 0) {
                Survival.lang().send(player, "command.claim.list.no_claims");
                return true;
            }

            if (ownedClaims.size() == 0) {
                Survival.lang().send(player, "command.claim.list.no_owned_claims");
            } else {
                Survival.lang().send(player, "command.claim.list.listing_owned");
                for (Claim owned : ownedClaims) {
                    Survival.lang().send(player, "command.claim.list.owned_claim",
                            owned.getName(),
                            owned.getxMin() + "",
                            owned.getzMin() + "");
                }
            }

            if (managedClaims.size() == 0) {
                //Survival.lang().send(player, "command.claim.list.no_managed_claims");
            } else {
                Survival.lang().send(player, "command.claim.list.listing_managed");
                for (Claim managed : managedClaims) {
                    Survival.lang().send(player, "command.claim.list.managed_claim",
                            managed.getName(),
                            managed.getOwnerName(),
                            managed.getxMin() + "",
                            managed.getzMin() + "");
                }
            }

            return true;

        } else if (args[0].equalsIgnoreCase("info")) {

            Claim claim;
            if (args.length == 1) {
                Optional<Claim> optClaim = ClaimManager.get().getClaimAt(player.getLocation());
                if (!optClaim.isPresent()) {
                    Survival.lang().send(player, "command.claim.not_in_claim");
                    return true;
                }
                claim = optClaim.get();
            } else if (args.length == 2) {
                Optional<Claim> optClaim = ClaimManager.get().getClaimByPlayerAndName(player.getUniqueId(), args[1]);
                if (!optClaim.isPresent()) {
                    sender.sendMessage("§cYou do not own any claim named §f" + args[1] + "§c!");
                    return true;
                }
                claim = optClaim.get();
            } else {
                player.sendMessage("§cWrong usage! /claim info [name]");
                return true;
            }

            if (!(claim instanceof PlayerClaim)) {
                if (claim instanceof CityClaim) {
                    Survival.lang().send(player, "command.claim.city_claim");
                } else {
                    Survival.lang().send(player, "command.claim.server_claim");
                }
                return true;
            }

            ClaimManager.get().setShowing(player.getUniqueId(), true);

            Survival.lang().send(player, "command.claim.info.start",
                    claim.getName(),
                    claim.getOwnerName(),
                    claim.calculateArea() + "",
                    claim.getxMin() + "",
                    claim.getzMin() + "");

            Survival.lang().send(player, "command.claim.info.managelist");
            if (claim.getDefaultPermissionLevel().isAtLeast(ClaimRank.MANAGE)) {
                Survival.lang().send(player, "command.claim.info.access_all");
            } else {
                Set<UUID> playersWithRank = claim.getAllPlayersWith(ClaimRank.MANAGE);
                if (playersWithRank.size() == 0) {
                    Survival.lang().send(player, "command.claim.info.access_none");
                } else {
                    Survival.lang().send(player, "command.claim.info.listplayer",
                            String.join(", ", playersWithRank.stream()
                                    .map(uuid -> PlayerManager_legacy.get().getNameFromUuid(uuid))
                                    .collect(Collectors.toSet())));
                }
            }

            Survival.lang().send(player, "command.claim.info.buildlist");
            if (claim.getDefaultPermissionLevel().isAtLeast(ClaimRank.BUILD)) {
                Survival.lang().send(player, "command.claim.info.access_all");
            } else {
                Set<UUID> playersWithRank = claim.getAllPlayersWith(ClaimRank.BUILD);
                if (playersWithRank.size() == 0) {
                    Survival.lang().send(player, "command.claim.info.access_none");
                } else {
                    Survival.lang().send(player, "command.claim.info.listplayer",
                            String.join(", ", playersWithRank.stream()
                                    .map(uuid -> PlayerManager_legacy.get().getNameFromUuid(uuid))
                                    .collect(Collectors.toSet())));
                }
            }

            Survival.lang().send(player, "command.claim.info.containerlist");
            if (claim.getDefaultPermissionLevel().isAtLeast(ClaimRank.CONTAINER)) {
                Survival.lang().send(player, "command.claim.info.access_all");
            } else {
                Set<UUID> playersWithRank = claim.getAllPlayersWith(ClaimRank.CONTAINER);
                if (playersWithRank.size() == 0) {
                    Survival.lang().send(player, "command.claim.info.access_none");
                } else {
                    Survival.lang().send(player, "command.claim.info.listplayer",
                            String.join(", ", playersWithRank.stream()
                                    .map(uuid -> PlayerManager_legacy.get().getNameFromUuid(uuid))
                                    .collect(Collectors.toSet())));
                }
            }

            Survival.lang().send(player, "command.claim.info.accesslist");
            if (claim.getDefaultPermissionLevel().isAtLeast(ClaimRank.ACCESS)) {
                Survival.lang().send(player, "command.claim.info.access_all");
            } else {
                Set<UUID> playersWithRank = claim.getAllPlayersWith(ClaimRank.ACCESS);
                if (playersWithRank.size() == 0) {
                    Survival.lang().send(player, "command.claim.info.access_none");
                } else {
                    Survival.lang().send(player, "command.claim.info.listplayer",
                            String.join(", ", playersWithRank.stream()
                                    .map(uuid -> PlayerManager_legacy.get().getNameFromUuid(uuid))
                                    .collect(Collectors.toSet())));
                }
            }

            Survival.lang().send(player, "command.claim.info.end");

            return true;

        } else if (args[0].equalsIgnoreCase("create")) {
            if (args.length != 2) {
                player.sendMessage("§cWrong usage! /claim create <name>");
                return true;
            }

            if (ClaimManager.get().getAllClaimsByPlayer(player.getUniqueId()).size() >= 8) {
                player.sendMessage("§cYou can only create §f8 §cclaims in total! Please delete one of your old claims to make a new one.");
                player.sendMessage("§7Do §f/claim list §7to view all your claims.");
                return true;
            }

            String claimName = args[1];
            Optional<Claim> claimWithSameName = ClaimManager.get().getClaimByPlayerAndName(player.getUniqueId(), claimName);
            if (claimWithSameName.isPresent()) {
                player.sendMessage("§cYou already have a claim with the name §f" + claimName + "§c!");
                return true;
            }

            ClaimManager.get().setShowing(player.getUniqueId(), true);

            int distanceToNextClaim = 50;

            Location ploc = player.getLocation();
            Set<Claim> overlappingClaims = ClaimManager.get().getOverlappingClaims(
                    player.getWorld().getName(),
                    ploc.getBlockX() - (2 + 50),
                    ploc.getBlockZ() - (2 + 50),
                    ploc.getBlockX() + (2 + 50),
                    ploc.getBlockZ() + (2 + 50));

            // ignore claims where the player has permissions
            Set<Claim> overlappingIllegalClaims = new HashSet<>();
            for (Claim claim : overlappingClaims) {
                if (!claim.getPlayerPermission(player.getUniqueId()).can(ClaimAction.CLAIM_SIDE_BY_SIDE)) {
                    overlappingIllegalClaims.add(claim);
                }
            }

            // too close to nearby claims
            if (overlappingIllegalClaims.size() != 0) {
                player.sendMessage("§cThis claim is too close to the following claims (minimum distance is " + distanceToNextClaim + " blocks):");
                for (Claim claim : overlappingIllegalClaims) {
                    player.sendMessage("§c    - " + claim.getName() + " owned by " + claim.getOwnerName());
                }
                return true;
            }

            overlappingClaims = ClaimManager.get().getOverlappingClaims(player.getWorld().getName(),
                    ploc.getBlockX() - 2,
                    ploc.getBlockZ() - 2,
                    ploc.getBlockX() + 2,
                    ploc.getBlockZ() + 2);

            if (overlappingClaims.size() != 0) {
                player.sendMessage("§cThis claim will overlap with the following claims:");
                for (Claim claim : overlappingClaims) {
                    player.sendMessage("§c    - " + claim.getName() + " owned by " + claim.getOwnerName());
                }
                return true;
            }

            Claim claim = new PlayerClaim(
                    player.getUniqueId(),
                    claimName,
                    ploc.getWorld().getName(),
                    ploc.getBlockX() - 2,
                    ploc.getBlockZ() - 2,
                    ploc.getBlockX() + 2,
                    ploc.getBlockZ() + 2);

            player.sendMessage("§aYou created the claim §f" + claimName + "§a!");
            player.sendMessage("§7Use §f/claim expand §7to expand the claim!");

            return true;

        } else if (args[0].equalsIgnoreCase("expand")) {

            if (args.length != 1) {
                player.sendMessage("§cWrong usage! /claim expand");
                return true;
            }

            Optional<Claim> optClaim = ClaimManager.get().getClaimAt(player.getLocation());
            if (!optClaim.isPresent()) {
                player.sendMessage("§cYou are not standing in any claim!");
                return true;
            }

            Claim claim = optClaim.get();

            if (!claim.getPlayerPermission(player.getUniqueId()).can(ClaimAction.EXPAND_CLAIM)) {
                player.sendMessage("§cYou do not have permission to expand this claim!");
                return true;
            }
            ClaimManager.get().setShowing(player.getUniqueId(), true);
            ClaimManager.get().setClaimExpandedBy(player.getUniqueId(), claim);
            player.sendMessage("§aClick on any block to expand the claim out to that block!");
            return true;

        } else if (args[0].equalsIgnoreCase("show")) {

            if (args.length != 1) {
                player.sendMessage("§cWrong usage! /claim show");
                return true;
            }

            if (ClaimManager.get().isShowing(player.getUniqueId())) {
                player.sendMessage("§cClaims are already visible for you. Have you enabled particles in settings?");
                player.sendMessage("§7You can hide the claims with §f/claim hide§7.");
                return true;
            }

            ClaimManager.get().setShowing(player.getUniqueId(), true);
            player.sendMessage("§aClaims are now visible to you! Remember to enable particles in settings.");
            return true;

        } else if (args[0].equalsIgnoreCase("hide")) {

            if (args.length != 1) {
                player.sendMessage("§cWrong usage! /claim show");
                return true;
            }

            if (!ClaimManager.get().isShowing(player.getUniqueId())) {
                player.sendMessage("§cClaims are already hidden for you!");
                player.sendMessage("§7You can show claims with §f/claim show§7.");
                return true;
            }

            ClaimManager.get().setShowing(player.getUniqueId(), false);
            player.sendMessage("§aClaims are no longer visible!");
            return true;

        } else {
            Survival.lang().send(player, "command.wrong_usage");
            sendCommandList(player);
            return true;
        }

    }

    public void sendCommandList(Player p) {
        Survival.lang().send(p, "command.claim.commandlist");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {

        String lastArg = args[args.length - 1].toLowerCase().trim();

        if (args.length == 1) {
            return Stream.of("create", "expand", "info", "list", "delete", "grant", "deny", "show", "hide")
                    .filter(s -> s.toLowerCase().startsWith(lastArg))
                    .collect(Collectors.toList());
        } else if (args.length == 2 && args[0].equalsIgnoreCase("grant") || args[0].equalsIgnoreCase("deny")) {

            Survival.log.info(args);

            return PlayerManager_legacy.get().getAllCachedNames().stream()
                    .filter(s -> s.toLowerCase().startsWith(lastArg))
                    .collect(Collectors.toList());

        } else if (args.length == 3 && args[0].equalsIgnoreCase("grant")) {
            return Stream.of("access", "container", "build", "manage")
                    .filter(s -> s.toLowerCase().startsWith(lastArg))
                    .collect(Collectors.toList());

        }

        return Collections.emptyList();
    }
}
