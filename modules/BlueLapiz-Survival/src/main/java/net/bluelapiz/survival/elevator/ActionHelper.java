/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.elevator;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

public class ActionHelper {

    public static void playoutElevatorEffect(Player p, Location from, Location to) {
        if (isVanished(p)) return;

        p.getWorld().playSound(from, Sound.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 0.0F);
        p.getWorld().playSound(to, Sound.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 0.0F);

        World world = p.getWorld();

        try {
            world.spawnParticle(Particle.SPELL_INSTANT, from, 1, 2, 1, 1, 100);
            world.spawnParticle(Particle.SPELL_INSTANT, to, 1, 2, 1, 1, 100);

            int particles = 50;
            float radius = 0.3f;

            from.add(0, 1.62, 0); // Eye position
            to.add(0, 1.62, 0);
            Location[] particleEffectLocations = new Location[]{from, to};

            for (Location loc : particleEffectLocations) {
                Location location1 = loc.clone();
                Location location2 = loc.clone();
                Location location3 = loc.clone();

                for (int par = 0; par < particles; par++) {
                    double angle, x, z;
                    angle = 2 * Math.PI * par / particles;
                    x = Math.cos(angle) * radius;
                    z = Math.sin(angle) * radius;
                    location1.add(x, 0, z);
                    location2.add(x, -0.66, z);
                    location3.add(x, -1.33, z);
                    world.spawnParticle(Particle.SPELL_WITCH, location1, 0, 0, 0, 0, 1);
                    world.spawnParticle(Particle.SPELL_WITCH, location2, 0, 0, 0, 0, 1);
                    world.spawnParticle(Particle.SPELL_WITCH, location3, 0, 0, 0, 0, 1);
                    location1.subtract(x, 0, z);
                    location2.subtract(x, -0.66, z);
                    location3.subtract(x, -1.33, z);
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void playoutPlaceElevatorEffect(Player p, Block b) {
        if (isVanished(p)) return;

        Location l = b.getRelative(BlockFace.UP).getLocation();
        l.add(.5, 0, .5);

        p.getWorld().playSound(l, Sound.ENTITY_IRON_GOLEM_ATTACK, 1.0F, 0.0F);

        World world = p.getWorld();

        l.add(0, 0, 0);

        try {
            for (Player o : Bukkit.getOnlinePlayers()) {

                if (o.getWorld() != p.getWorld()) continue;
                if (o.getLocation().distance(p.getLocation()) > 50) continue;

                world.spawnParticle(Particle.VILLAGER_ANGRY, l, 0, 0, 0, 0, 10);

                Location location = l.clone();
                int particles = 50;
                float radius = 0.3f;
                for (int par = 0; par < particles; par++) {
                    double angle, x, z;
                    angle = 2 * Math.PI * par / particles;
                    x = Math.cos(angle) * radius;
                    z = Math.sin(angle) * radius;
                    location.add(x, .2, z);
                    world.spawnParticle(Particle.VILLAGER_HAPPY, location, 0, 0, 0, 0, 1);
                    location.subtract(x, .2, z);
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    private static boolean isVanished(Player player) {
        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
        if (blp == null) return false;
        return blp.isVanished();
    }

}
