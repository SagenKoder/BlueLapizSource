/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.elevator;

import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.Optional;

public class IronElevator extends Elevator {

    @Override
    public String getElevatorName() {
        return "Jern Heis";
    }

    @Override
    public boolean isElevatorBlock(Block b) {
        return b.getType().equals(Material.IRON_BLOCK);
    }

    @Override
    public Optional<Block> isPartOfElevator(Block b) {
        if (isElevatorBlock(b)) return Optional.of(b);

        return Optional.empty();
    }

}
