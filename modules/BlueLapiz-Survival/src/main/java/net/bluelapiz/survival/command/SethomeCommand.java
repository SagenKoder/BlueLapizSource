/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.command;

import net.bluelapiz.common.player.HomeManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Home;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.command.BLCommand;
import net.bluelapiz.survival.Survival;
import net.bluelapiz.survival.claim.Claim;
import net.bluelapiz.survival.claim.ClaimAction;
import net.bluelapiz.survival.claim.ClaimManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Optional;

public class SethomeCommand extends BLCommand {

    public SethomeCommand() {
        super("sethome", "sethomes", "shome", "chome", "nhome", "newhome", "createhome");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(Survival.lang().getTranslatedString(sender, "command.only_players"));
            return true;
        }

        Player p = (Player) sender;

        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
        Rank rank = blPlayer.getRank();

        if (args.length != 1) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.sethome.wrong_usage"));
            return true;
        }

        int numOfHomes = HomeManager.get("survival").getAllHomesOf(p.getUniqueId()).size();

        if (numOfHomes + 1 > maxHomesFor(rank)) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.sethome.max_homes")
                    .replace("<number>", maxHomesFor(rank) + ""));
            return true;
        }

        Home home = HomeManager.get("survival").getHomeOf(p.getUniqueId(), args[0]);
        if (home != null) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.sethome.duplicate")
                    .replace("<home>", home.getName()));
            return true;
        }

        Optional<Claim> optRegion = ClaimManager.get().getClaimAt(p.getLocation());
        if (optRegion.isPresent() && !optRegion.get().getPlayerPermission(p.getUniqueId()).can(ClaimAction.SET_HOME)) {
            p.sendMessage(Survival.lang().getTranslatedString(sender, "command.sethome.claimed")
                    .replace("<owner>", optRegion.get().getOwnerName()));
            return true;
        }

        HomeManager.get("survival").createHome(p.getUniqueId(), args[0],
                p.getWorld().getName(),
                p.getLocation().getBlockX(),
                p.getLocation().getBlockY(),
                p.getLocation().getBlockZ(),
                p.getLocation().getYaw(),
                p.getLocation().getPitch());

        p.sendMessage(Survival.lang().getTranslatedString(sender, "command.sethome.created")
                .replace("<home>", args[0]));
        return true;
    }

    public int maxHomesFor(Rank rank) {
        switch (rank) {
            case PLAYER:
                return 5;
            case VIP:
                return 8;
            default:
                return 40;
        }
    }
}
