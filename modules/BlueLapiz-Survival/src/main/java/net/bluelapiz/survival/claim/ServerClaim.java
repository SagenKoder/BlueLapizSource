/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 6:10 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival.claim;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.survival.Survival;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

import java.util.UUID;

public class ServerClaim extends Claim {

    public ServerClaim(String name, String world, int xMin, int zMin, int xMax, int zMax) {
        super(PlayerManager_legacy.CONSOLE_UUID, name, world, xMin, zMin, xMax, zMax);

        this.getBlueBossbar().setTitle("§3§lThe protected region of §f§l" + getName());
        this.getGreenBossbar().setTitle("§a§lThe protected region of §f§l" + getName());
        this.getYellowBossbar().setTitle("§e§lThe protected region of §f§l" + getName());
        this.getRedBossbar().setTitle("§c§lThe protected region of §f§l" + getName());

        setAnimalCanSpawn(false);
        setMonsterCanSpawn(false);
    }

    @Override
    public boolean handlePlayerActionAndReturnResult(Player player, ClaimAction action, int x, int z) {
        ClaimRank permissionLevel = getPlayerPermission(player.getUniqueId());
        if (!permissionLevel.can(action) || action.equals(ClaimAction.DAMAGE_ENTITY) || action.equals(ClaimAction.KILL)) {
            player.spigot().sendMessage(
                    ChatMessageType.ACTION_BAR,
                    TextComponent.fromLegacyText(Survival.lang().getTranslatedString(player, "action.claim.serverclaim.protected")));
            return true;
        }
        return false;
    }

    @Override
    public UUID getOwner() {
        return super.getOwner();
    }

    @Override
    public String toString() {
        return "ServerClaim{" +
                "owner=" + owner +
                ", name='" + name + '\'' +
                ", world='" + world + '\'' +
                ", xMin=" + xMin +
                ", zMin=" + zMin +
                ", xMax=" + xMax +
                ", zMax=" + zMax +
                ", permittedPlayers=" + permittedPlayers +
                ", defaultPermissionLevel=" + defaultPermissionLevel +
                '}';
    }
}
