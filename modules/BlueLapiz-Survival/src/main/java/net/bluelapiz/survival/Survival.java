/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 1:21 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.survival;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.player.HomeManager;
import net.bluelapiz.core.BLLoggerSpigotImpl;
import net.bluelapiz.core.config.BukkitLanguage;
import net.bluelapiz.survival.claim.Claim;
import net.bluelapiz.survival.claim.ClaimManager;
import net.bluelapiz.survival.claim.ClaimRank;
import net.bluelapiz.survival.claim.ServerClaim;
import net.bluelapiz.survival.command.*;
import net.bluelapiz.survival.elevator.ElevatorListener;
import net.bluelapiz.survival.elevator.LaunchPadListener;
import net.bluelapiz.survival.listener.ClaimListener;
import net.bluelapiz.survival.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.Duration;
import java.time.Instant;

public class Survival extends JavaPlugin {

    public static BLLogger log = new BLLoggerSpigotImpl("BlueLapiz-Survival");
    private static Survival instance;
    private BukkitLanguage language;

    public static Survival get() {
        return instance;
    }

    public static BukkitLanguage lang() {
        return get().language;
    }

    @Override
    public void onDisable() {
        ClaimManager.get().shutdown();
    }

    @Override
    public void onEnable() {
        Instant start = Instant.now();
        instance = this;

        log.debug("Loading language files");
        language = new BukkitLanguage("BLSurvival");

        log.debug("Registering event listeners");
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new ClaimListener(), this);
        Bukkit.getPluginManager().registerEvents(new ElevatorListener(), this);
        Bukkit.getPluginManager().registerEvents(new LaunchPadListener(), this);

        log.debug("Registering commands");
        new TpaCommand();
        new DelhomeCommand();
        new HomeCommand();
        new ListhomeCommand();
        new SethomeCommand();
        new ClaimCommand();
        new SpawnCommand();
        new RandomTeleportCommand();

        log.debug("Loading homes of online players");
        for (Player o : Bukkit.getOnlinePlayers()) {
            HomeManager.get("survival").loadHomesOf(o.getUniqueId());
        }

        log.info("***************** STARTED BlueLapiz-Survival *******************");
        Instant end = Instant.now();
        log.debug("Used " + Duration.between(start, end).toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase());

        // 2a7ab7cd-8b9f-499f-a28d-a4f964b308b7
        Claim spawn = new ServerClaim(
                "Survival Spawn",
                "world",
                -426, 252, -93, 546);
        ClaimManager.get().addClaim(spawn);
        spawn.setDefaultPermissionLevel(ClaimRank.ACCESS);
        //spawn.givePlayerPermission(UUID.fromString("2a7ab7cd-8b9f-499f-a28d-a4f964b308b7"), ClaimRank.BUILD); // Kaj
        //spawn.givePlayerPermission(PlayerManager_legacy.get().getUuidFromName("Sagen97"), ClaimRank.BUILD);
        //spawn.givePlayerPermission(PlayerManager_legacy.get().getUuidFromName("01v"), ClaimRank.BUILD);
        //spawn.givePlayerPermission(UUID.fromString("f32241da-3c4a-4a53-92a9-6fa9a55892c8"), ClaimRank.BUILD);
    }
}
