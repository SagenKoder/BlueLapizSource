/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportHereCommand extends BLCommand {

    public TeleportHereCommand() {
        super("tphere", "tph");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) {
            sender.sendMessage("§Only players can use this!");
            return true;
        }

        if (args.length != 1) {
            sender.sendMessage("§cWrong usage! /tph <player>");
            return true;
        }

        Player r = Bukkit.getPlayer(args[0]);
        if (r == null) {
            sender.sendMessage("§cCould not find player " + args[0] + "!");
            return true;
        }

        ((Player) sender).performCommand("tp " + r.getName() + " " + sender.getName());

        return true;
    }
}
