/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.objects.Rank;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class HatCommand extends BLCommand {

    public HatCommand() {
        super("hat", "hatt");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorizedOrError(sender, Rank.VIP)) return true;

        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly players can do this!");
            return true;
        }

        Player player = (Player) sender;

        ItemStack item = player.getInventory().getItemInMainHand();
        if (item == null || item.getType() == Material.AIR) {
            player.sendMessage("§cYou are not holding any item in your hand!");
            return true;
        }

        player.getInventory().setItemInMainHand(player.getInventory().getHelmet());
        player.getInventory().setHelmet(item);

        player.sendMessage("§aYou look fine with that!, oh ooooh <3");
        return true;
    }
}
