/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.packet.BroadcastMessagePacket;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GamemodeCommand extends BLCommand {

    public GamemodeCommand() {
        super("gamemode", "gm", "gms", "survival", "gmc", "creative", "spec", "spectator", "gma", "adventure");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorizedOrError(sender, Rank.BUILDER)) return true;

        switch (label.toLowerCase()) {
            case "gms":
            case "survival":
                if (!(sender instanceof Player)) {
                    sender.sendMessage("§cConsole cannot do this!");
                    return true;
                }
                ((Player) sender).performCommand("gm s");
                return true;
            case "gmc":
            case "creative":
                if (!(sender instanceof Player)) {
                    sender.sendMessage("§cConsole cannot do this!");
                    return true;
                }
                ((Player) sender).performCommand("gm c");
                return true;
            case "gma":
            case "adventure":
                if (!(sender instanceof Player)) {
                    sender.sendMessage("§cConsole cannot do this!");
                    return true;
                }
                ((Player) sender).performCommand("gm a");
                return true;
            case "spec":
                if (!(sender instanceof Player)) {
                    sender.sendMessage("§cConsole cannot do this!");
                    return true;
                }
                ((Player) sender).performCommand("gm spec");
                return true;
        }

        if (args.length < 1 || args.length > 2) {
            sender.sendMessage("§cWrong usage! /gm <0, 1, 2, 3> [player]");
            return true;
        }

        GameMode gameMode = null;
        switch (args[0].trim().toLowerCase()) {
            case "0":
            case "s":
            case "survival":
                gameMode = GameMode.SURVIVAL;
                break;
            case "1":
            case "c":
            case "creative":
                gameMode = GameMode.CREATIVE;
                break;
            case "2":
            case "a":
            case "adventure":
                gameMode = GameMode.ADVENTURE;
                break;
            case "3":
            case "spec":
            case "spectator":
                gameMode = GameMode.SPECTATOR;
                break;
        }

        if (gameMode == null) {
            sender.sendMessage("§cCould not parse gamemode " + args[0]);
            return true;
        }

        Player r;
        if (args.length == 2) {
            r = Bukkit.getPlayer(args[1]);
            if (r == null) {
                sender.sendMessage("§cCould not find player " + args[1]);
                return true;
            }
            if (!isAuthorized(sender, Rank.ADMIN)) {
                sender.sendMessage("§cYou need to be Admin or higher to change gamemode of other players!");
                return true;
            }
        } else { // args.length == 1
            if (!(sender instanceof Player)) {
                sender.sendMessage("§cConsole has to specify a player to change gamemode on!");
                return true;
            }
            r = (Player) sender;
            if (gameMode == GameMode.CREATIVE) {
                BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(((Player) sender).getUniqueId());
                if (blPlayer.getRank().getLevel() != Rank.BUILDER.getLevel() && !isAuthorized(sender, Rank.ADMIN)) {
                    sender.sendMessage("§cYou need to be Admin or higher to change to gamemode Creative!");
                    return true;
                }
            }
        }

        if (r.getGameMode() == gameMode) {
            if (r == sender) {
                sender.sendMessage("§cYou are already in gamemode " + gameMode.name().toLowerCase() + "!");
            } else {
                sender.sendMessage("§cThe player is already in gamemode " + gameMode.name().toLowerCase() + "!");
            }
            return true;
        }

        r.setGameMode(gameMode);

        String broadcast;
        if (r == sender) {
            broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §achanged gamemode to §f" + gameMode.name().toLowerCase() + "§a!";
        } else {
            broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §achanged gamemode of §f" + r.getName() + " §ato §f" + gameMode.name().toLowerCase() + "§a!";
        }

        PluginMessageChannel.sendPluginMessage(r, new BroadcastMessagePacket(Rank.MOD_JR, broadcast));
        return true;
    }
}
