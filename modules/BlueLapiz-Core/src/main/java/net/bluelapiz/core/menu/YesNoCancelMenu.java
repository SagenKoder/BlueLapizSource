/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.menu;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class YesNoCancelMenu extends BLMenu {

    private ItemStack noItem = new ItemBuilder(Material.RED_STAINED_GLASS_PANE)
            .setName(ChatColor.RED + "" + ChatColor.BOLD + "No").toItemStack();
    private ItemStack cancelItem = new ItemBuilder(Material.WHITE_STAINED_GLASS_PANE)
            .setName(ChatColor.WHITE + "" + ChatColor.BOLD + "Cancel").toItemStack();
    private ItemStack yesItem = new ItemBuilder(Material.GREEN_STAINED_GLASS_PANE)
            .setName(ChatColor.GREEN + "" + ChatColor.BOLD + "Yes").toItemStack();
    public YesNoCancelMenu(String title, JavaPlugin main) {
        super(title, 9, main);
        addOption(noItem);
        addOption(cancelItem, 4);
        addOption(yesItem, 8);
        int i = 0;
    }

    public void setCancelItem(ItemStack is) {
        this.cancelItem = is;
    }

    public void setNoItem(ItemStack is) {
        this.noItem = is;
    }

    public void setYesItem(ItemStack is) {
        this.yesItem = is;
    }

    @Override
    public void onClose(InventoryCloseEvent e) {
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        e.setCancelled(true);
        ItemStack is = e.getCurrentItem();
        if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasDisplayName()) return;
        String disp = is.getItemMeta().getDisplayName();
        if (disp.equals(cancelItem.getItemMeta().getDisplayName())) {
            onFinish(Response.CANCEL);
        } else if (disp.equals(yesItem.getItemMeta().getDisplayName())) {
            onFinish(Response.YES);
        } else if (disp.equals(noItem.getItemMeta().getDisplayName())) {
            onFinish(Response.NO);
        }
    }

    public abstract void onFinish(Response response);

    public enum Response {
        YES,
        NO,
        CANCEL
    }
}
