/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/29/18 12:03 AM                                              *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.server.PluginDisableEvent;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestCommand extends BLCommand implements Listener {

    Map<Long, Map<Block, Material>> fakeDiamonds = new ConcurrentHashMap<>();

    public TestCommand() {
        super("test", "admin");
        Bukkit.getScheduler().runTaskTimer(Core.get(), () -> fakeDiamonds.keySet().stream()
                .filter(k -> (k + 5 * 60 * 1000) < System.currentTimeMillis())
                .forEach(this::resetFakeDiamonds), 10 * 20, 10 * 20);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorizedOrError(sender, Rank.MOD)) return true;

        if (args.length < 1) {
            sendList(sender);
            return true;
        }

        if (args[0].equalsIgnoreCase("spawnFakeDiamonds") && (args.length == 1 || args.length == 2)) {

            if (!(sender instanceof Player)) {
                sender.sendMessage("§cOnly players!");
                return true;
            }
            Player p = (Player) sender;

            int type = new int[]{1, 2, 6, 8}[ThreadLocalRandom.current().nextInt(4)];
            if (args.length == 2) {
                switch (args[1].trim().toLowerCase()) {
                    case "1":
                    case "2":
                    case "6":
                    case "8":
                        type = Integer.parseInt(args[1]);
                        break;
                    default:
                        sender.sendMessage("§cInvalid diamond type: " + args[1]);
                        return true;
                }
            }

            Map<Block, Material> blockMaterialMap = new HashMap<>();

            Block playerBlock = p.getLocation().getBlock();
            Block tmp;
            switch (type) {
                case 1:
                    blockMaterialMap.put(playerBlock, playerBlock.getType());
                    break;
                case 2:
                    blockMaterialMap.put(playerBlock, playerBlock.getType());
                    tmp = playerBlock.getRelative(BlockFace.EAST);
                    blockMaterialMap.put(tmp, tmp.getType());
                    break;
                case 6:
                    blockMaterialMap.put(playerBlock, playerBlock.getType());
                    tmp = playerBlock.getRelative(BlockFace.EAST);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = playerBlock.getRelative(BlockFace.NORTH);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.EAST);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.DOWN);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.SOUTH);
                    blockMaterialMap.put(tmp, tmp.getType());
                    break;
                case 8:
                    blockMaterialMap.put(playerBlock, playerBlock.getType());
                    tmp = playerBlock.getRelative(BlockFace.EAST);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = playerBlock.getRelative(BlockFace.NORTH);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.EAST);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.DOWN);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.SOUTH);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.WEST);
                    blockMaterialMap.put(tmp, tmp.getType());
                    tmp = tmp.getRelative(BlockFace.NORTH);
                    blockMaterialMap.put(tmp, tmp.getType());
            }

            for (Block b : blockMaterialMap.keySet()) {
                b.setType(Material.DIAMOND_ORE);
            }

            fakeDiamonds.put(System.currentTimeMillis(), blockMaterialMap);

            p.sendMessage("§aSpawned in " + blockMaterialMap.size() + " fake diamond ores!");

            return true;
        } else if (args[0].equalsIgnoreCase("sudo") && (args.length > 2)) {

            if (!isAuthorizedOrError(sender, Rank.ADMIN)) return true;

            Player r = Bukkit.getPlayer(args[1]);
            if (r == null) {
                sender.sendMessage("§cCannot find player §f" + args[1] + "§c!");
                return true;
            }

            if (sender instanceof Player) {
                if (PlayerManager_legacy.get().getPlayer(((Player) sender).getUniqueId()).getRank().getLevel() <= PlayerManager_legacy.get().getPlayer(r.getUniqueId()).getRank().getLevel()) {
                    sender.sendMessage("§cYou can only sudo players with lower rank than you!");
                    return true;
                }
            }

            String sudoCommand = String.join(" ", Arrays.copyOfRange(args, 2, args.length));

            if (sudoCommand.charAt(0) == '/') {
                r.performCommand(sudoCommand.substring(1));
                sender.sendMessage("§6Executed the following command as §f" + r.getName() + "§6!");
                sender.sendMessage("§6Command: §f" + sudoCommand);
            } else {
                r.chat(sudoCommand);
                sender.sendMessage("§6Sent the following text as §f" + r.getName() + "§6!");
                sender.sendMessage("§6Text: §f" + sudoCommand);
            }

            return true;

        } else if (args[0].equalsIgnoreCase("runScript") && args.length == 2) {
            if (!isAuthorizedOrError(sender, Rank.FOUNDER)) return true;

            try {
                Path path = Paths.get("../" + args[1]);

                if (path == null) {
                    sender.sendMessage("§cCannot find the script §f" + args[1] + "§c!");
                    return true;
                }

                sender.sendMessage("§bRunning the script §f" + path.toAbsolutePath().toString() + "§b!");

                Bukkit.getScheduler().runTaskAsynchronously(Core.get(), () -> {
                    try {
                        Process p = Runtime.getRuntime().exec(path.toAbsolutePath().toString());
                        Instant instant = Instant.now();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
                        String s;
                        while ((s = reader.readLine()) != null) {
                            Core.log.info("§bScript (pid = " + instant.toString() + "): " + s);
                            sender.sendMessage("§b" + s);
                        }
                        int rc = p.waitFor();
                        sender.sendMessage("§aScript executed with exit code §f" + rc + "§a!");
                    } catch (Exception e) {
                        sender.sendMessage("§cError while executing script!\nError: " + e.getMessage());
                        e.printStackTrace();
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        }

        sendList(sender);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorized(sender, Rank.MOD)) return Collections.emptyList();
        if (args.length == 0) return Collections.emptyList();
        String lastArg = args[args.length - 1].trim().toLowerCase();
        if (args.length == 1) {
            return Stream.of("spawnFakeDiamonds", "sudo", "runScript")
                    .filter(s -> s.toLowerCase().startsWith(lastArg))
                    .collect(Collectors.toList());
        } else if (args.length == 2) {
            switch (args[0]) {
                case "spawnFakeDiamonds":
                    return Stream.of("1", "2", "6", "8")
                            .filter(s -> s.toLowerCase().startsWith(lastArg))
                            .collect(Collectors.toList());
                case "sudo":
                    return Bukkit.getOnlinePlayers().stream()
                            .map(Player::getName)
                            .filter(name -> name.toLowerCase().startsWith(lastArg))
                            .collect(Collectors.toList());
                case "runScript":
                    return Stream.of("stopall.sh", "restartallfast.sh", "restartall.sh", "updateAll.sh")
                            .filter(script -> script.toLowerCase().startsWith(lastArg))
                            .collect(Collectors.toList());
            }
        }
        return Collections.emptyList();
    }

    public void sendList(CommandSender sender) {
        sender.sendMessage("§e/test spawnFakeDiamonds <1 | 2 | 6 | 8>");
        sender.sendMessage("§e/test sudo <player> <chat or command>");
        sender.sendMessage("§e/test runScript <script>");
    }

    public void resetFakeDiamonds(long oreKey) {
        fakeDiamonds.get(oreKey).forEach(Block::setType);
        fakeDiamonds.remove(oreKey);
    }

    @EventHandler
    public void onFakeDiamondsBreak(BlockBreakEvent e) {
        if (fakeDiamonds.size() == 0) return;
        for (BlockFace nearbyFace : Arrays.asList(BlockFace.SELF, BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST, BlockFace.EAST)) {
            Block nearby = e.getBlock().getRelative(nearbyFace);
            if (!nearby.getType().equals(Material.DIAMOND_ORE)) continue;
            fakeDiamonds.entrySet().stream()
                    .filter(fakeBlockEntry ->
                            fakeBlockEntry.getValue().entrySet().stream()
                                    .anyMatch(entry -> entry.getKey().equals(nearby)))
                    .forEach(fakeBlockEntry -> resetFakeDiamonds(fakeBlockEntry.getKey()));
        }
    }

    @EventHandler
    public void onDisable(PluginDisableEvent e) {
        if (e.getPlugin() == Core.get()) {
            fakeDiamonds.keySet().forEach(this::resetFakeDiamonds);
        }
    }
}
