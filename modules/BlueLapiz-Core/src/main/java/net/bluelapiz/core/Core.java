/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 5:10 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core;

import net.bluelapiz.common.maven.MavenLoader;
import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.ornlitetest.ORMTester;
import net.bluelapiz.common.packet.PacketHelper;
import net.bluelapiz.common.packet.RequestEnableGlobalChatOacket;
import net.bluelapiz.common.packet.RequestServerNamePacket;
import net.bluelapiz.common.player.*;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.sql.SQLConfig;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.util.BLExecutor;
import net.bluelapiz.core.alias.AliasManager;
import net.bluelapiz.core.command.*;
import net.bluelapiz.core.config.BukkitLanguage;
import net.bluelapiz.core.config.Configuration;
import net.bluelapiz.core.config.ConfigurationManager;
import net.bluelapiz.core.helper.GlowHelper;
import net.bluelapiz.core.helper.VanishHelper;
import net.bluelapiz.core.listener.PlayerListener;
import net.bluelapiz.core.listener.WorldListener;
import net.bluelapiz.core.path.BeaconFlightManager;
import net.bluelapiz.core.path.BeaconFlightPath;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Core extends JavaPlugin {

    public static boolean USE_GLOBAL_CHAT = false;
    public static boolean HAS_UPDATED_SERVER_NAME = false;
    public static String SERVER_NAME = "unknown";
    public static BLLogger log;
    private static Core instance;
    private ConfigurationManager configurationManager;
    private Scoreboard scoreboard;
    private BukkitLanguage language;

    public static Core get() {
        return instance;
    }

    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    public BukkitLanguage lang() {
        return language;
    }

    @Override
    public void onDisable() {
        Core.log.debug("Saving and stopping all players data");
        for (Player o : Bukkit.getOnlinePlayers()) {
            handlePlayerQuit(o);
        }

        Core.log.debug("Closing MySQL connections");
        SQLManager.getMySQLMan().closeConnection();

        Core.log.debug("Unregistering all scoreboard teams");
        for (Rank r : Rank.values()) {
            Team team = scoreboard.getTeam(r.getName());
            if (team != null) team.unregister();
        }

        Core.log.debug("Stopping VanishHelper");
        VanishHelper.get().stop();

        Core.log.debug("Destroying all beacon flight paths");
        for (BeaconFlightPath path : BeaconFlightManager.get().getAllBeaconFlightPaths()) {
            path.destroy();
        }

        Core.log.debug("Stopping the task executor");
        BLExecutor.get().stopAll();

        configurationManager = null;
    }

    @Override
    public void onEnable() {
        Instant start = Instant.now();

        log = new BLLoggerSpigotImpl(getDescription().getName());
        BLLogger.commonLogger = new BLLoggerSpigotImpl("BlueLapiz-Common");

        Core.log.debug("Loading external dependencies for Common....");
        MavenLoader.setup(getDataFolder());
        MavenLoader.get().loadCommonLibriaries();

        Core.log.debug("Loading external dependencies for Core....");
        MavenLoader.get().load("4.1.31.Final", "io.netty", "netty-all");
        MavenLoader.get().load("2.8.5", "com.google.code.gson", "gson");

        Core.log.debug("Setting up variables...");
        instance = this;
        scoreboard = this.getServer().getScoreboardManager().getMainScoreboard();
        configurationManager = new ConfigurationManager(this);

        Core.log.debug("Loading language files");
        language = new BukkitLanguage("BLCore");

        Core.log.debug("Setting up config file");
        Configuration config = configurationManager.getConfiguration("config");
        if (!config.isSet("server.serverName")) {
            config.set("server.serverName", SERVER_NAME);
        }
        if (!config.isSet("server.globalChat")) {
            config.set("server.globalChat", USE_GLOBAL_CHAT);
        }
        config.forceSave();

        Core.log.debug("Loading global values");
        SERVER_NAME = config.getString("server.serverName");
        USE_GLOBAL_CHAT = config.getBoolean("server.globalChat");
        Core.log.debug("SERVER_NAME = " + SERVER_NAME);
        Core.log.debug("USE_GLOBAL_CHAT = " + USE_GLOBAL_CHAT);

        Core.log.debug("Setting up mysql config");
        // create or update mysql config
        Configuration mysqlConfig = configurationManager.getConfiguration("mysql");
        if (!mysqlConfig.isSet("mysql.host")) {
            mysqlConfig.set("mysql.host", "localhost:3306");
        }
        if (!mysqlConfig.isSet("mysql.user")) {
            mysqlConfig.set("mysql.user", "root");
        }
        if (!mysqlConfig.isSet("mysql.pass")) {
            mysqlConfig.set("mysql.pass", "my-password");
        }
        if (!mysqlConfig.isSet("mysql.database")) {
            mysqlConfig.set("mysql.database", "minecraft");
        }
        mysqlConfig.forceSave();

        Core.log.debug("Setting up MySQL");
        SQLConfig sqlConfig = SQLConfig.builder()
                .database(mysqlConfig.getString("mysql.database"))
                .host(mysqlConfig.getString("mysql.host"))
                .user(mysqlConfig.getString("mysql.user"))
                .pass(mysqlConfig.getString("mysql.pass")).build();
        SQLManager.setup(sqlConfig);

        Core.log.debug("Creating missing mysql tables");
        SQLManager.getTableHelper().createTablesNotExisting();

        Core.log.debug("Setting up PlayerManager_legacy");
        PlayerManager_legacy.get();

        Core.log.debug("Setting up BankManager");
        BankManager.get();

        Core.log.debug("Setting up RawPlayerManager");
        PlayerManager.get();

        Core.log.debug("Setting up BeaconFlightManager");
        BeaconFlightManager.get();

        Core.log.debug("Setting up AliasManager");
        AliasManager.get();

        Core.log.debug("Setting up scoreboard teams");
        // create a team for every rank
        for (Rank r : Rank.values()) {
            Team oldTeam = scoreboard.getTeam(r.getName());
            if (oldTeam != null) {
                oldTeam.unregister();
            }
            Team team = scoreboard.registerNewTeam(r.getName());
            team.setPrefix(r.getHeadPrefix());
            team.setColor(ChatColor.getByChar(r.getColorChar()));
        }

        Core.log.debug("Registering PluginChannels");
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, PacketHelper.BUNGEE_MESSAGES_CHANNEL);
        this.getServer().getMessenger().registerIncomingPluginChannel(this, PacketHelper.BLUELAPIZ_MESSAGES_CHANNEL, new PluginMessageChannel());

        Core.log.debug("Registering event listeners");
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new WorldListener(), this);
        Bukkit.getPluginManager().registerEvents(new TestCommand(), this);

        Core.log.debug("Registering commands");
        new GamemodeCommand();
        new TeleportCommand();
        new TeleportHereCommand();
        new ForceRespawnCommand();
        new SpeedCommand();
        new GlowCommand();
        new HatCommand();
        new FlyCommand();
        new BeaconFlightCommand();
        new ExecCommand();
        new ReloadAliasCommand();
        new TestCommand();

        Core.log.debug("Registering dummy bungeecord commands");
        new DummyEvidenceCommand();
        new DummyPunishCommand();
        new DummyRankCommand();
        new DummyUnpunishCommand();
        new DummyServerCommand();
        new DummyHistoryCommand();
        new DummyDelhistoryCommand();
        new DummyHubCommand();
        new DummyFriendCommand();
        new DummyPrivateMessageCommand();
        new DummyPrivateReplyCommand();
        new DummyStafflistCommand();
        new DummyBExecCommand();

        Core.log.debug("Loading data of online players");
        // load online players data
        for (Player o : Bukkit.getOnlinePlayers()) {
            handlePlayerJoin(o);
        }

        Core.log.debug("Starting schedulers....");
        // update vanish every 3 seconds
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
            for (Player o : Bukkit.getOnlinePlayers()) VanishHelper.get().updateVanish(o);
        }, 60, 60);

        // update loaded player data
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, () -> {
            PlayerManager_legacy.get().saveAndUnloadPlayers(Bukkit.getOnlinePlayers().stream()
                    .map(Player::getUniqueId)
                    .collect(Collectors.toList()));
        }, 40, 40);

        Core.log.info("***************** STARTED BlueLapiz-Core *******************");
        Instant end = Instant.now();
        Core.log.debug("Used " + Duration.between(start, end).toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase());

        Core.log.debug("Starting ORM testing....");
        ORMTester.test(sqlConfig.getUser(), sqlConfig.getPass(), sqlConfig.getDatabase());
    }

    public ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    public void broadcastMessage(Rank rank, String message) {
        for (Player o : Bukkit.getOnlinePlayers()) {
            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(o.getUniqueId());
            if (blPlayer == null) continue;

            if (blPlayer.getRank().isAtLeast(rank)) {
                o.sendMessage(message);
            }
        }
        Bukkit.getConsoleSender().sendMessage(message);
    }

    public void handlePlayerJoin(Player p) {
        try {
            Optional<BLPlayer> optPlayer = PlayerManager.get().createPlayerNow(p.getUniqueId(), p.getName(), 1, TimeUnit.SECONDS);
            if (!optPlayer.isPresent()) {
                p.kickPlayer("§cCould not load your player data! Try again or contact staff. Ref-code: 0x45ab5e");
                Core.log.severe("Could not load playerdata for player " + p.getName() + "(" + p.getUniqueId() + ")!", new Exception());
                return;
            }
            BLPlayer player = optPlayer.get();
            if (!p.getName().equals(p.getName())) player.setName(p.getName());

            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().playerJoin(p.getUniqueId(), p.getName());
            if (blPlayer == null) {
                p.kickPlayer("§cCould not load your player data! Try again or contact staff. Ref-code: 0x45ab5b");
                Core.log.severe("Could not load legacy playerdata for player " + p.getName() + "(" + p.getUniqueId() + ")!", new Exception());
                return;
            }

            FriendManager.get().loadFriendsOf(p.getUniqueId());
            GlowHelper.get().updateGlowFor(p);
            VanishHelper.get().updateVanish(p);

            if (!Core.HAS_UPDATED_SERVER_NAME) {
                Bukkit.getScheduler().runTaskLater(Core.get(), () -> {
                    if (!Core.HAS_UPDATED_SERVER_NAME) {
                        PluginMessageChannel.sendPluginMessage(p, new RequestServerNamePacket());
                        PluginMessageChannel.sendPluginMessage(p, new RequestEnableGlobalChatOacket());
                    }
                }, 10);
            }
        } catch (Exception e) {
            System.err.println("Exception while handling player join of player " + p.getName() + "(" + p.getUniqueId().toString() + ")");
            e.printStackTrace();
            Core.log.severe("Exception while handling player join of player " + p.getName() + "(" + p.getUniqueId() + ")!", e);
        }
    }

    public void handlePlayerQuit(Player p) {
        FriendManager.get().unloadFriends(p.getUniqueId());

        for (BeaconFlightPath path : BeaconFlightManager.get().getAllBeaconFlightPaths()) {
            path.playerQuit(p);
        }

        Core.get().getScoreboard().getTeams().forEach(team -> team.removeEntry(p.getUniqueId().toString()));
    }
}
