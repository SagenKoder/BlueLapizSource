/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.Rank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DummyRankCommand extends BLCommand {

    public DummyRankCommand() {
        super("rank", "setrank");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] strings) {
        if (strings.length == 1) {
            String startOfName = strings[0].toLowerCase().trim();
            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfName))
                    .collect(Collectors.toList());
            return names;
        } else if (strings.length == 2) {
            String startOfRank = strings[1].toLowerCase().trim();
            return Arrays.stream(Rank.values())
                    .map(Rank::name)
                    .filter(n -> n.toLowerCase().startsWith(startOfRank))
                    .collect(Collectors.toList());
        } else {
            return Arrays.asList();
        }
    }
}
