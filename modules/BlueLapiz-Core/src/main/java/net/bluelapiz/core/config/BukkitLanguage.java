/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:27 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.config;

import net.bluelapiz.common.util.Touple;
import net.bluelapiz.core.Core;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.text.MessageFormat;
import java.util.concurrent.ConcurrentHashMap;

public class BukkitLanguage {

    private static final String DEFAULT_LANG = "en";

    private String baseName;
    private ConcurrentHashMap<String, Configuration> languages = new ConcurrentHashMap<>();

    public BukkitLanguage(String baseName) {
        this.baseName = baseName;
    }

    public String getTranslatedString(CommandSender sender, String key) {
        String locale;
        if (sender instanceof Player) {
            locale = ((Player) sender).getLocale();
        } else {
            locale = DEFAULT_LANG;
        }

        return getTranslatedString(locale, key);
    }

    private String getTranslatedString(String locale, String key) {
        locale = locale.split("_")[0];
        String fileName = "lang/" + baseName + "_" + locale;
        if (!languages.containsKey(fileName)) {
            File folder = new File(Core.get().getDataFolder(), "lang");
            folder.mkdir();
            Core.log.info("Loading or creating new translation file for locale: " + locale + "! File: " + fileName + ".yml");
            Configuration config = Core.get().getConfigurationManager().getConfiguration(fileName);
            languages.put(fileName, config);
        }
        Configuration config = languages.get(fileName);
        if (!config.isSet(key)) {
            Core.log.info("MISSING KEY!!! Locale: " + locale + " Key: " + key);
            if (locale.equalsIgnoreCase(DEFAULT_LANG)) {
                config.set(key, "UNKNOWN-KEY_" + key);
            } else {
                config.set(key, getTranslatedString(DEFAULT_LANG, key));
            }
            config.forceSave();
        }
        return config.getString(key);
    }

    /**
     * @param sender
     * @param key
     * @param replaces
     * @deprecated
     */
    public void send_legacy(CommandSender sender, String key, Touple<String, String>... replaces) {
        String msg = getTranslatedString(sender, key);
        for (Touple<String, String> replace : replaces)
            msg = msg.replace(replace.getLeft(), replace.getRight());
        sender.sendMessage(msg);
    }

    public void send(CommandSender sender, String key, Object... replaces) {
        String msg = getTranslatedString(sender, key);
        msg = MessageFormat.format(msg, replaces);
        sender.sendMessage(msg);
    }

}
