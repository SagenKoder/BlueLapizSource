/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 4:46 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.path;

import net.bluelapiz.core.Core;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BeaconFlightManager {

    private static BeaconFlightManager beaconFlightManager;
    // world , List<BeaconFlightManager>
    public HashMap<String, List<BeaconFlightPath>> pathsInWorld = new HashMap<>();

    private BeaconFlightManager() {
        List<BeaconFlightPath> paths = BeaconFlightFileConfiguration.get().loadAllFromFile();
        for (BeaconFlightPath path : paths) {
            addBeaconFlightToWorldAndDontSave(path.getWorldName(), path);
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                for (List<BeaconFlightPath> paths : pathsInWorld.values()) {
                    for (BeaconFlightPath path : paths) {
                        path.update();
                        for (Player o : Bukkit.getOnlinePlayers()) {
                            // ignore if player is on a path
                            for (BeaconFlightPath pathOther : pathsInWorld.getOrDefault(o.getWorld().getName(), new ArrayList<>())) {
                                if (pathOther.getPlayersOnPath().containsKey(o.getUniqueId())) continue;
                            }
                            // ignore if player is on cooldown
                            if (path.isOnCooldown(o)) {
                                continue;
                            }
                            if (o.getWorld().getName().equalsIgnoreCase(path.getWorldName())) {
                                if (path.getPStart().distanceSquared(o.getLocation().toVector()) < 1.0) {
                                    //if(pathStartX == playerX && pathStartY == playerY && pathStartZ == playerZ) {
                                    // must be run sync
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            path.addPlayer(o, false);
                                        }
                                    }.runTask(Core.get());
                                } else if (path.getPEnd().distanceSquared(o.getLocation().toVector()) < 1.0) {
                                    //} else if(pathEndX == playerX && pathEndY == playerY && pathEndZ == playerZ) {
                                    // must be run sync
                                    new BukkitRunnable() {
                                        @Override
                                        public void run() {
                                            path.addPlayer(o, true);
                                        }
                                    }.runTask(Core.get());
                                }
                            }
                        }
                    }
                }
            }
        }.runTaskTimerAsynchronously(Core.get(), 1, 1);
    }

    public static BeaconFlightManager get() {
        if (beaconFlightManager == null)
            beaconFlightManager = new BeaconFlightManager();
        return beaconFlightManager;
    }

    public List<BeaconFlightPath> getBeaconFlightsInWorld(String world) {
        return pathsInWorld.getOrDefault(world.toLowerCase(), new ArrayList<>());
    }

    public BeaconFlightPath getBeaconFlightPath(String world, String name) {
        for (BeaconFlightPath path : getBeaconFlightsInWorld(world)) {
            if (path.getName().equalsIgnoreCase(name)) return path;
        }
        return null;
    }

    public void addBeaconFlightToWorld(String world, BeaconFlightPath path) {
        if (!pathsInWorld.containsKey(world.toLowerCase()))
            pathsInWorld.put(world.toLowerCase(), new ArrayList<>());

        pathsInWorld.get(world.toLowerCase()).add(path);

        BeaconFlightFileConfiguration.get().saveAllToFile(getAllBeaconFlightPaths());
    }

    private void addBeaconFlightToWorldAndDontSave(String world, BeaconFlightPath path) {
        if (!pathsInWorld.containsKey(world.toLowerCase()))
            pathsInWorld.put(world.toLowerCase(), new ArrayList<>());

        pathsInWorld.get(world.toLowerCase()).add(path);
    }

    public ArrayList<BeaconFlightPath> getAllBeaconFlightPaths() {
        ArrayList<BeaconFlightPath> allFlightPaths = new ArrayList<>();

        for (List<BeaconFlightPath> paths : pathsInWorld.values()) {
            allFlightPaths.addAll(paths);
        }

        return allFlightPaths;
    }

    public void remove(BeaconFlightPath path) {
        getBeaconFlightsInWorld(path.getWorldName()).remove(path);
        BeaconFlightFileConfiguration.get().saveAllToFile(getAllBeaconFlightPaths());
    }

}
