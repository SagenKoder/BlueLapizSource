/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.packet.BroadcastMessagePacket;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpeedCommand extends BLCommand {

    public SpeedCommand() {
        super("speed");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!isAuthorized(sender, Rank.BUILDER)) return true;

        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly players can do this silly!");
            return true;
        }

        Player player = (Player) sender;

        if (args.length != 1) {
            sender.sendMessage("§cWrong usage! /speed <1-10>");
            return true;
        }

        int speed;
        try {
            speed = Integer.parseInt(args[0]);
        } catch (Exception e) {
            sender.sendMessage("§cCould not parse number " + args[0]);
            return true;
        }

        player.setFlySpeed(0.1f * speed);

        String broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §aset flyspeed to §f" + speed + "§a!";
        PluginMessageChannel.sendPluginMessage(player, new BroadcastMessagePacket(Rank.BUILDER, broadcast));
        return true;
    }
}
