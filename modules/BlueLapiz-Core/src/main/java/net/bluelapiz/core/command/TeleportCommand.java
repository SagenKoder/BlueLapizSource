/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.packet.BroadcastMessagePacket;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCommand extends BLCommand {

    public TeleportCommand() {
        super("teleport", "tp", "tppos");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§Only players can use this!");
            return true;
        }
        Player player = (Player) sender;

        // perform /tpa if player don't have permission...
        if (!isAuthorized(sender, Rank.MOD)/* && Bukkit.getPluginCommand("tpa") != null*/) {
            player.performCommand("tpa " + String.join(" ", args));
            return true;
        }
        // else just send error
        //else if(!isAuthorizedOrError(sender, Rank.MOD)) return true;

        if (args.length != 1 && args.length != 2 && args.length != 3) {
            player.sendMessage("§cWrong usage!\n/tp [[target player] <destination player> | <x> <y> <z>]");
            return true;
        }

        if (args.length == 1) {
            Player r = Bukkit.getPlayer(args[0]);
            if (r == null) {
                sender.sendMessage("§cCould not find player " + args[0] + "!");
                return true;
            }

            if (r == player) {
                sender.sendMessage("§cCannot tp to yourself!");
                return true;
            }

            player.teleport(r);

            String broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §ateleported to §f" + r.getName() + "§a!";
            PluginMessageChannel.sendPluginMessage(r, new BroadcastMessagePacket(Rank.MOD_JR, broadcast));

            return true;
        }

        if (args.length == 2) {
            Player r1 = Bukkit.getPlayer(args[0]);
            boolean shouldBreak = false;
            if (r1 == null) {
                player.sendMessage("§cCould not find the first player " + args[0] + "!");
                shouldBreak = true;
            }

            Player r2 = Bukkit.getPlayer(args[1]);
            if (r2 == null) {
                player.sendMessage("§cCould not find the second player " + args[1] + "!");
                shouldBreak = true;
            }

            if (r1 == r2) {
                sender.sendMessage("§cBoth players cannot be the same!");
                return true;
            }

            if (shouldBreak) return true;

            r1.teleport(r2);

            String broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §ateleported §f" + r1.getName() + "§a to §f" + r2.getName() + "§a!";
            PluginMessageChannel.sendPluginMessage(player, new BroadcastMessagePacket(Rank.MOD_JR, broadcast));

            return true;
        }

        if (args.length == 3) {

            float x, y, z;
            try {
                x = Float.parseFloat(args[0]);
                y = Float.parseFloat(args[1]);
                z = Float.parseFloat(args[2]);
            } catch (Exception e) {
                player.sendMessage("§cCould not parse numbers! /tp <x> <y> <z>");
                return true;
            }

            player.teleport(new Location(player.getWorld(), x, y, z));

            String broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §ateleported to §f" + x + "§a, §f" + y + "§a, §f" + z + "§a!";
            PluginMessageChannel.sendPluginMessage(player, new BroadcastMessagePacket(Rank.MOD_JR, broadcast));

            return true;
        }

        return false;
    }
}
