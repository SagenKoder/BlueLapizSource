/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.alias.AliasManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class ReloadAliasCommand extends BLCommand {

    public ReloadAliasCommand() {
        super("reloadalias");
        requireRank(Rank.FOUNDER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        AliasManager.get().loadAlias();

        sender.sendMessage("§aSuccessfully reloaded aliases!");

        return true;

    }
}
