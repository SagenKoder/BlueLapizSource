/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:35 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.packet.SendDiscordExceptionPacket;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class BLLoggerSpigotImpl extends BLLogger {

    public BLLoggerSpigotImpl(String pluginName) {
        super(pluginName);
    }

    @Override
    public void info(String... strings) {
        for (String s : strings) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.BLUE + "[" + pluginName + "] " + ChatColor.WHITE + s);
        }
    }

    @Override
    public void info(Object... objects) {
        for (Object s : objects) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.BLUE + "[" + pluginName + "] " + ChatColor.WHITE + s.toString());
        }
    }

    @Override
    public void debug(String... strings) {
        for (String s : strings) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "[" + pluginName + "] " + ChatColor.WHITE + s);
        }
    }

    @Override
    public void debug(Object... objects) {
        for (Object s : objects) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + "[" + pluginName + "] " + ChatColor.WHITE + s.toString());
        }
    }

    @Override
    public void severe(Object object, Throwable e) {
        severe(object.toString(), e);
    }

    @Override
    public void severe(String string, Throwable e) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[" + pluginName + "] " + ChatColor.WHITE + string);
        e.printStackTrace();
        PluginMessageChannel.sendPluginMessage(new SendDiscordExceptionPacket(Core.SERVER_NAME, pluginName, string, e));
    }
}
