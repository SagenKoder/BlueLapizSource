/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 3:15 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.helper;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import org.bukkit.entity.Player;

public class GlowHelper {

    public static GlowHelper glowHelper;

    public static GlowHelper get() {
        if (glowHelper == null) glowHelper = new GlowHelper();
        return glowHelper;
    }

    public void updateGlowFor(Player player) {
        for (Rank r : Rank.values()) Core.get().getScoreboard().getTeam(r.getName()).removeEntry(player.getName());
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
        if (blPlayer != null) {
            Core.get().getScoreboard().getTeam(blPlayer.getRank().getName()).addEntry(player.getName());
            if (blPlayer.getRank().isAtLeast(Rank.VIP) && blPlayer.isGlowing()) {
                player.setGlowing(true);
            } else if (blPlayer.isGlowing()) {
                player.setGlowing(false);
                blPlayer.setGlowing(false);
            } else {
                player.setGlowing(false);
            }
        }
    }
}
