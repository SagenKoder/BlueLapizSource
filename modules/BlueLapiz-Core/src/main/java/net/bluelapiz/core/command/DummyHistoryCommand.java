/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DummyHistoryCommand extends BLCommand {

    public DummyHistoryCommand() {
        super("history", "h");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] strings) {
        if (strings.length == 1) {
            String startOfName = strings[0].toLowerCase();
            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfName))
                    .collect(Collectors.toList());
            return names;
        } else {
            return Arrays.asList();
        }
    }
}
