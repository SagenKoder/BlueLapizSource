/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:26 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.alias;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.util.BLCodeCompiler;
import net.bluelapiz.core.Core;
import org.bukkit.entity.Player;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AliasManager {

    private static AliasManager aliasManager;
    Path scriptFolder;
    Map<String, CommandAlias> aliasCommands = new ConcurrentHashMap<>();

    private AliasManager() {
        scriptFolder = new File(Core.get().getDataFolder(), "commandScripts/").toPath();
        loadAlias();
    }

    public static AliasManager get() {
        if (aliasManager == null) aliasManager = new AliasManager();
        return aliasManager;
    }

    public void loadAlias() {

        aliasCommands.clear();

        if (!Files.exists(scriptFolder) || !Files.isDirectory(scriptFolder)) {
            try {
                Files.createDirectory(scriptFolder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            for (Path scriptPath : Files.newDirectoryStream(scriptFolder)) {
                String fileName = scriptPath.getFileName().toString();
                try {
                    if (fileName.endsWith(".aliasExecutor")) {
                        String name = fileName.split("\\.")[0];
                        String content = String.join("\n", Files.readAllLines(scriptPath));
                        Core.log.info("Found aliascommand " + name);
                        CommandAlias commandAlias = (CommandAlias) BLCodeCompiler.get().compile(content, true).getConstructor().newInstance();

                        aliasCommands.put(name.toLowerCase(), commandAlias);
                    }
                } catch (Exception e) {
                    Core.log.info("Exception while loading command alias '" + fileName + "'!");
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean handleCommand(Player player, String input) {
        String[] split = input.split("\\s+");
        if (aliasCommands.containsKey(split[0].toLowerCase())) {
            try {
                CommandAlias ca = aliasCommands.get(split[0].toLowerCase());
                if (!isAuthorized(player.getUniqueId(), ca.minimumRank())) return false;
                LinkedList<String> args = new LinkedList<>(Arrays.asList(split));
                args.removeFirst();
                ca.handleCommand(player, args.toArray(new String[0]));
            } catch (Exception e) {
                Core.log.info("Exception while executing alias command!");
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    private boolean isAuthorized(UUID uuid, Rank rank) {
        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(uuid);
        return blp.getRank().isAtLeast(rank);
    }

    public interface CommandAlias {

        void handleCommand(Player sender, String[] args);

        Rank minimumRank();

    }

}
