/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.helper.GlowHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GlowCommand extends BLCommand {

    public GlowCommand() {
        super("glow");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorizedOrError(sender, Rank.VIP)) return true;

        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly players can do this!");
            return true;
        }

        Player p = (Player) sender;

        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
        blPlayer.setGlowing(!blPlayer.isGlowing());

        if (blPlayer.isGlowing()) {
            p.sendMessage("§aYou enabled the fashionable glowing! You look fancy <3");
        } else {
            p.sendMessage("§cDid you not like the beautiful glow? I disabled it for you :P");
        }

        GlowHelper.get().updateGlowFor(p);

        return false;
    }
}
