/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 1:42 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.listener;

import net.bluelapiz.common.logging.ChatLogManager;
import net.bluelapiz.common.packet.RequestGlobalChatPacket;
import net.bluelapiz.common.packet.SendDiscordMessagePacket;
import net.bluelapiz.common.player.BLPlayer;
import net.bluelapiz.common.player.PlayerManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.alias.AliasManager;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.*;
import org.bukkit.scoreboard.Team;

import java.awt.*;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PlayerListener implements Listener {

    @EventHandler
    public void onPreLogin(AsyncPlayerPreLoginEvent e) {
        UUID uuid = e.getUniqueId();

        Optional<BLPlayer> optPlayer = PlayerManager.get().createPlayerNow(uuid, e.getName(), 1, TimeUnit.SECONDS);
        if (!optPlayer.isPresent()) {
            e.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            e.setKickMessage("§cCould not load your player data! Try again or contact staff. Ref-code: 0x45scb43");
            for (Player p : Bukkit.getOnlinePlayers()) {
                PluginMessageChannel.sendPluginMessage(p, new SendDiscordMessagePacket(
                        481828303314092044L,
                        Color.RED,
                        "Exception[server=" + Core.SERVER_NAME + ",plugin=BlueLapiz-Core]",
                        "Could not load playerdata for player " + p.getName() + "(" + p.getUniqueId() + ")! Ref-code: 0x45scb43"));
                break;
            }
            return;
        }

        BLPlayer_legacy blp = PlayerManager_legacy.get().playerJoin(uuid, e.getName());

        if (blp == null) {
            e.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            e.setKickMessage("§cCould not load your player data! Try again or contact staff. Ref-code: 0xas45gfsdf");
            for (Player p : Bukkit.getOnlinePlayers()) {
                PluginMessageChannel.sendPluginMessage(p, new SendDiscordMessagePacket(
                        481828303314092044L,
                        Color.RED,
                        "Exception[server=" + Core.SERVER_NAME + ",plugin=BlueLapiz-Core]",
                        "Could not load playerdata for player " + p.getName() + "(" + p.getUniqueId() + ")! Ref-code: 0xas45gfsdf"));
                break;
            }
        }

    }

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Core.get().handlePlayerJoin(e.getPlayer());
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();

        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(player.getUniqueId());

        if (blp == null) {
            BLPlayer_legacy blp2 = PlayerManager_legacy.get().playerJoin(player.getUniqueId(), player.getName());

            if (blp2 == null) {
                player.kickPlayer("§cCould not load your player data! Try again or contact staff. Ref-code: 0xjghjsye63");
                for (Player p : Bukkit.getOnlinePlayers()) {
                    PluginMessageChannel.sendPluginMessage(p, new SendDiscordMessagePacket(
                            481828303314092044L,
                            Color.RED,
                            "Exception[server=" + Core.SERVER_NAME + ",plugin=BlueLapiz-Core]",
                            "Could not load playerdata for player " + p.getName() + "(" + p.getUniqueId() + ")! Ref-code: 0xjghjsye63"));
                    break;
                }
            }
        }
        e.setJoinMessage(null);
    }

    @EventHandler
    public void onItemPickup(EntityPickupItemEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

            if (blPlayer.isVanished()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (blPlayer.isVanished()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (blPlayer.isVanished()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (blPlayer.isVanished()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        if (e.isCancelled()) return;

        e.setCancelled(true);

        String message = e.getMessage();
        Player player = e.getPlayer();

        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
        Rank rank = blp.getRank();

        if (blp.isVanished()) {
            player.sendMessage("§cYou cannot chat while in vanish!");
            return;
        }

        if (Core.USE_GLOBAL_CHAT) {
            PluginMessageChannel.sendPluginMessage(e.getPlayer(),
                    new RequestGlobalChatPacket(e.getPlayer().getUniqueId(), e.getPlayer().getName(), e.getMessage()));
            return;
        }

        String format = "%prefix%player %suffix %message";
        format = format.replace("%prefix", rank.getPrefix());
        format = format.replace("%player", player.getName());
        format = format.replace("%suffix", rank.getSuffix());
        format = format.replace("%color", "&" + rank.getColor());
        format = format.replace("%rank", "&" + rank.getName());
        format = format.replace("%message", message);

        ChatLogManager.get().logMessage(ChatLogManager.ChatType.PUBLIC_CHAT, Core.SERVER_NAME, player.getUniqueId(), null, message);

        if (rank.isAtLeast(Rank.MOD))
            format = ChatColor.translateAlternateColorCodes('&', format);

        Core.get().broadcastMessage(Rank.PLAYER, format);

        // Don't spam!
        // DiscordBot.get().sendChatMessage(DiscordBot.CHANNEL_MINECRAFT_CHAT, player.getName(), message, rank);
    }

    @EventHandler
    public void onCommand(PlayerCommandPreprocessEvent e) {
        String commandName = e.getMessage().split("\\s+")[0].trim().replaceFirst("/", "").toLowerCase();

        Rank rank = PlayerManager_legacy.get().getPlayer(e.getPlayer().getUniqueId()).getRank();

        if (commandName.contains(":") && !rank.isAtLeast(Rank.ADMIN)) {
            e.getPlayer().sendMessage("§cYou need to be at least §e" + Rank.ADMIN.getName() + " §cto prefix commands!");
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);

        Core.get().handlePlayerQuit(e.getPlayer());

        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(e.getPlayer().getUniqueId());
        PlayerManager_legacy.get().playerLeave(e.getPlayer().getUniqueId());
        if (blp == null) return;
        Team team = Core.get().getScoreboard().getTeam(blp.getRank().getName());
        if (team != null)
            team.removeEntry(e.getPlayer().getUniqueId().toString());
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onCommand(AsyncPlayerChatEvent e) {
        if (!e.getMessage().startsWith(".")) return;
        if (AliasManager.get().handleCommand(e.getPlayer(), e.getMessage().substring(1)))
            e.setCancelled(true);
    }

}
