/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.helper;

import org.bukkit.Bukkit;
import org.bukkit.Server;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ServerPropertiesHelper {

    private static ServerPropertiesHelper serverPropertiesHelper;

    public static ServerPropertiesHelper get() {
        if (serverPropertiesHelper == null) serverPropertiesHelper = new ServerPropertiesHelper();
        return serverPropertiesHelper;
    }

    private Object getPropertyManager() throws NoSuchFieldException, IllegalAccessException {
        Server server = Bukkit.getServer();
        Field propertyManagerField = server.getClass().getField("propertyManager");
        return propertyManagerField.get(server);
    }

    public void forceSaveProperties() {
        try {
            Object propertyManager = getPropertyManager();
            Method savePropertiesFileMethod = propertyManager.getClass().getDeclaredMethod("savePropertiesFile");
            savePropertiesFileMethod.invoke(propertyManager);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setProperty(ServerProperty property, Object value) {
        setProperty(property.getPropertyName(), value);
    }

    public void setProperty(String property, Object value) {
        try {
            Object propertyManager = getPropertyManager();
            Method setPropertyMethod = propertyManager.getClass().getDeclaredMethod("setProperty", String.class, Object.class);
            setPropertyMethod.invoke(propertyManager, property, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum ServerProperty {

        SPAWN_PROTECTION("spawn-protection"),
        SERVER_NAME("server-name"),
        FORCE_GAMEMODE("force-gamemode"),
        NETHER("allow-nether"),
        DEFAULT_GAMEMODE("gamemode"),
        QUERY("enable-query"),
        PLAYER_IDLE_TIMEOUT("player-idle-timeout"),
        DIFFICULTY("difficulty"),
        SPAWN_MONSTERS("spawn-monsters"),
        OP_PERMISSION_LEVEL("op-permission-level"),
        RESOURCE_PACK_HASH("resource-pack-hash"),
        RESOURCE_PACK("resource-pack"),
        ANNOUNCE_PLAYER_ACHIEVEMENTS("announce-player-achievements"),
        PVP("pvp"),
        SNOOPER("snooper-enabled"),
        LEVEL_NAME("level-name"),
        LEVEL_TYPE("level-type"),
        LEVEL_SEED("level-seed"),
        HARDCORE("hardcore"),
        COMMAND_BLOCKS("enable-command-blocks"),
        MAX_PLAYERS("max-players"),
        PACKET_COMPRESSION_LIMIT("network-compression-threshold"),
        MAX_WORLD_SIZE("max-world-size"),
        IP("server-ip"),
        PORT("server-port"),
        DEBUG_MODE("debug"),
        SPAWN_NPCS("spawn-npcs"),
        SPAWN_ANIMALS("spawn-animals"),
        FLIGHT("allow-flight"),
        VIEW_DISTANCE("view-distance"),
        WHITE_LIST("white-list"),
        GENERATE_STRUCTURES("generate-structures"),
        MAX_BUILD_HEIGHT("max-build-height"),
        MOTD("motd"),
        REMOTE_CONTROL("enable-rcon");

        private String propertyName;

        ServerProperty(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyName() {
            return propertyName;
        }
    }
}
