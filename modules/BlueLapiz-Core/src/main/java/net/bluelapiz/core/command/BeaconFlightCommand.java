/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 4:31 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.path.BeaconFlightManager;
import net.bluelapiz.core.path.BeaconFlightPath;
import net.bluelapiz.core.path.PathCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BeaconFlightCommand extends BLCommand {

    HashMap<UUID, PathCreator> pathCreators = new HashMap<>();

    public BeaconFlightCommand() {
        super("beaconflight");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorized(sender, Rank.BUILDER)) return true;

        if (!(sender instanceof Player)) {
            Core.get().lang().send(sender, "command.only_players");
            return true;
        }

        Player p = (Player) sender;

        if (args.length == 0) {
            sendHelp(p);
        }

        // list paths
        else if (args.length == 1 && args[0].equalsIgnoreCase("list")) {
            List<BeaconFlightPath> pathsInWorld = BeaconFlightManager.get().getBeaconFlightsInWorld(p.getWorld().getName());
            if (pathsInWorld.size() == 0) {
                Core.get().lang().send(p, "command.beaconflight.no_beaconflights");
                return true;
            }
            Core.get().lang().send(p, "command.beaconflight.list_flights.title", p.getWorld().getName());
            for (BeaconFlightPath path : pathsInWorld) {
                Core.get().lang().send(p, "command.beaconflight.list_flights.node",
                        path.getName(),
                        path.getPath().getPoints().size(),
                        path.getPEnd().getBlockX(), path.getPEnd().getBlockY(), path.getPEnd().getBlockZ(),
                        path.getPStart().getBlockX(), path.getPStart().getBlockY(), path.getPStart().getBlockZ());
            }
            p.sendMessage("§3---");
        } else if (args.length == 2 && args[0].equalsIgnoreCase("list")) {
            BeaconFlightPath path = BeaconFlightManager.get().getBeaconFlightPath(p.getWorld().getName(), args[1]);
            if (path == null) {
                Core.get().lang().send(p, "command.beaconflight.not_exists", args[1]);
                return true;
            }
            Core.get().lang().send(p, "command.beaconflight.list_points.title", path.getName(), p.getWorld().getName());
            for (Vector vector : path.getPath().getPoints()) {
                Core.get().lang().send(p, "command.beaconflight.list_points.node", vector.toString());
            }
            Core.get().lang().send(p, "command.beaconflight.list_points.foot");
        } else if (args.length == 2 && args[0].equalsIgnoreCase("remove")) {
            BeaconFlightPath path = BeaconFlightManager.get().getBeaconFlightPath(p.getWorld().getName(), args[1]);
            if (path == null) {
                Core.get().lang().send(p, "command.beaconflight.not_exists", args[1]);
                return true;
            }
            path.destroy();
            BeaconFlightManager.get().remove(path);
            Core.get().lang().send(p, "command.beaconflight.removed", args[1]);
        } else if (args.length == 2 && args[0].equalsIgnoreCase("ride")) {
            BeaconFlightPath path = BeaconFlightManager.get().getBeaconFlightPath(p.getWorld().getName(), args[1]);
            if (path == null) {
                Core.get().lang().send(p, "command.beaconflight.not_exists", args[1]);
                return true;
            }
            path.addPlayer(p, false);
            Core.get().lang().send(p, "command.beaconflight.started_flight");
            p.sendMessage("§3Started the BeaconFlight <3");
        } else if (args.length == 2 && args[0].equalsIgnoreCase("create")) {
            PathCreator pathCreator = new PathCreator(args[1], p.getWorld().getName());
            pathCreators.put(p.getUniqueId(), pathCreator);
            Core.get().lang().send(p, "command.beaconflight.created_path", args[0], p.getWorld().getName());
            p.performCommand("beaconflight addpoint");
        } else if (args.length == 1 && args[0].equalsIgnoreCase("addpoint")) {
            PathCreator pathCreator = pathCreators.get(p.getUniqueId());
            if (pathCreator == null) {
                p.sendMessage("§cYou do not have any pathcreator! Make a new one with /beaconflight create <name>");
                return true;
            }
            if (!p.getWorld().getName().equalsIgnoreCase(pathCreator.getWorld())) {
                Core.get().lang().send(p, "command.beaconflight.addpoint.not_same_world", pathCreator.getWorld());
                return true;
            }

            pathCreator.addFirst(p.getLocation().toVector());
            Core.get().lang().send(p, "command.beaconflight.addpoint.added_point", p.getLocation().toVector());
        } else if (args.length == 1 && args[0].equalsIgnoreCase("finish")) {
            PathCreator pathCreator = pathCreators.get(p.getUniqueId());
            if (pathCreator == null) {
                p.sendMessage("§cYou do not have any pathcreator! Make a new one with /beaconflight create <name>");
                return true;
            }

            if (pathCreator.getNumberOfPoints() < 3) {
                p.sendMessage("§cYou need to add at least 3 points to create a BeaconFlight!");
                return true;
            }

            BeaconFlightPath path = pathCreator.build();
            BeaconFlightManager.get().addBeaconFlightToWorld(pathCreator.getWorld(), path);
            pathCreators.remove(p.getUniqueId());

            p.sendMessage("§3Added the new BeaconFlight <3<3");
            p.sendMessage("§3The travel duration for the flight is currently " + path.getTravelDurationInTicks());
        } else if (args.length > 3 && args[0].equalsIgnoreCase("title") &&
                (args[1].equalsIgnoreCase("start") || args[1].equalsIgnoreCase("end"))) {
            String message = String.join(" ", Arrays.copyOfRange(args, 2, args.length));

            PathCreator pathCreator = pathCreators.get(p.getUniqueId());
            if (pathCreator == null) {
                p.sendMessage("§cYou do not have any pathcreator! Make a new one with /beaconflight create <name>");
                return true;
            }

            if (args[1].equalsIgnoreCase("start")) {
                pathCreator.setTitleStart(message);
                p.sendMessage("§3Set the title on start to '" + message + "'!");
            } else {
                pathCreator.setTitleEnd(message);
                p.sendMessage("§3Set the title on start to '" + message + "'!");
            }
        } else {
            p.sendMessage("§cWrong usage!");
            sendHelp(p);
        }

        return true;
    }

    public void sendHelp(CommandSender p) {
        p.sendMessage("§3-------< BeaconFlight >-------");
        p.sendMessage("§e/beaconflight §7- Show this help");
        p.sendMessage("§e/beaconflight list §7- List every BeaconFlight in this world");
        p.sendMessage("§e/beaconflight list <name> §7- List every point on a BeaconFlight");
        p.sendMessage("§e/beaconflight remove <name> §7- Removes a BeaconPath");
        p.sendMessage("§e/beaconflight ride <name> §7- Starts the BeaconFlight for you");
        p.sendMessage("§e/beaconflight create <name> §7- Creates a BeaconFlight");
        p.sendMessage("§e/beaconflight title start §7- Sets the title of the start point");
        p.sendMessage("§e/beaconflight title end §7- Sets the title of the end point");
        p.sendMessage("§e/beaconflight addpoint §7- Sets a point");
        p.sendMessage("§e/beaconflight finish §7- Builds the new BeaconFlight");
        p.sendMessage("§3---");
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            String startOfAction = args[0].toLowerCase().trim();
            return Arrays.asList("list", "remove", "ride", "create", "title", "title", "addpoint", "finish").stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
                    .collect(Collectors.toList());
        } else if (args.length == 2 && Arrays.asList("list", "remove", "ride").contains(args[0].toLowerCase())) {
            String startOfAction = args[1].toLowerCase().trim();
            return BeaconFlightManager.get().getBeaconFlightsInWorld(((Player) sender).getWorld().getName()).stream()
                    .map(BeaconFlightPath::getName)
                    .filter(s -> s.startsWith(startOfAction))
                    .collect(Collectors.toList());
        } else if (args.length == 2 && args[0].equalsIgnoreCase("title")) {
            String startOfAction = args[1].toLowerCase().trim();
            return Arrays.asList("start", "end").stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
                    .collect(Collectors.toList());
        } else {
            return Arrays.asList();
        }
    }
}
