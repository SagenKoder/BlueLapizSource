/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.punish.PunishReason;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DummyEvidenceCommand extends BLCommand {

    public DummyEvidenceCommand() {
        super("evidence", "e");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] strings) {
        if (strings.length == 1) {
            String startOfAction = strings[0].toLowerCase().trim();
            return Arrays.asList("list", "add", "delete").stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
                    .collect(Collectors.toList());
        } else if (strings.length == 2 && !strings[0].equalsIgnoreCase("delete")) {
            String startOfName = strings[1].toLowerCase().trim();
            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfName))
                    .collect(Collectors.toList());
            return names;
        } else if (strings.length == 3 && !strings[0].equalsIgnoreCase("delete")) {
            return Arrays.stream(PunishReason.values())
                    .filter(PunishReason::isInPunishCommand)
                    .map(PunishReason::name)
                    .filter(n -> n.toLowerCase().startsWith(strings[2].toLowerCase()))
                    .collect(Collectors.toList());
        } else {
            return Arrays.asList();
        }
    }
}
