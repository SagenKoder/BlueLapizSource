/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:27 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.util.BLCodeCompiler;
import net.bluelapiz.common.util.BLCodeCompilerException;
import net.bluelapiz.common.util.BLCodeExecutionException;
import net.bluelapiz.core.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.lang.reflect.InvocationTargetException;

public class ExecCommand extends BLCommand {

    private final String EXEC_CLASS_BODY = "" +
            "import net.bluelapiz.core.command.ExecCommand;\n" +
            "import org.bukkit.*;\n" +
            "import org.bukkit.entity.*;\n" +
            "import org.bukkit.command.*;\n" +
            "import net.bluelapiz.*;\n" +
            "import net.bluelapiz.core.*;\n" +
            "public class ClassName extends ExecCommand.CommandExecutable {\n" +
            "    @Override public void handle(){\n" +
            "        %code%\n" +
            "    }\n" +
            "}\n";

    public ExecCommand() {
        super("exec");
        requireRank(Rank.FOUNDER);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        sender.sendMessage("§dTrying to compile and run code...");

        String source = EXEC_CLASS_BODY.replace("%code%", String.join(" ", args));

        Class<?> clazz;
        try {
            clazz = BLCodeCompiler.get().compile(source, false);
        } catch (BLCodeCompilerException e) {
            sender.sendMessage("§cException while compiling source class!");
            sender.sendMessage(e.getMessage());
            e.printStackTrace();
            return true;
        }

        if (!CommandExecutable.class.isAssignableFrom(clazz)) {
            sender.sendMessage("§cThe compiled class is an instance of " + CommandExecutable.class.getName() + "!");
            Core.log.info("The compiled class is an instance of " + CommandExecutable.class.getName() + "!");
            return true;
        }

        CommandExecutable executable;
        try {
            Object instance = clazz.getConstructor().newInstance();
            executable = (CommandExecutable) instance;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException | ClassCastException e) {
            e.printStackTrace();
            sender.sendMessage("§cException while creating a new instance of the class!");
            sender.sendMessage(e.getMessage());
            return true;
        }

        executable.setSender(sender);
        executable.setPlugin(Core.get());

        try {
            executable.handle();
        } catch (Exception e) {
            BLCodeExecutionException exception = new BLCodeExecutionException("Exception while running code!", e);
            sender.sendMessage("Exception while running code!");
            sender.sendMessage(e.getMessage());
            exception.printStackTrace();
            return true;
        }

        sender.sendMessage("§aCode compiled and run without exceptions!");
        return true;
    }

    public static abstract class CommandExecutable {
        protected Core plugin;
        protected CommandSender sender;

        protected void setSender(CommandSender sender) {
            this.sender = sender;
        }

        protected void setPlugin(Core plugin) {
            this.plugin = plugin;
        }

        public abstract void handle();
    }
}
