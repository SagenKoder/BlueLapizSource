/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.helper;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;

public class VanishHelper {

    private static VanishHelper vanishHelper;
    private BossBar bossBar = Bukkit.createBossBar("§fYou are in §c§lVanish§7!", BarColor.RED, BarStyle.SEGMENTED_20, BarFlag.DARKEN_SKY);

    public static VanishHelper get() {
        if (vanishHelper == null) vanishHelper = new VanishHelper();
        return vanishHelper;
    }

    public void updateVanish(Player player) {
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
        if (blPlayer == null) return;

        if (blPlayer.isVanished()) {
            bossBar.addPlayer(player);
        } else {
            bossBar.removePlayer(player);
        }

        for (Player o : Bukkit.getOnlinePlayers()) {
            BLPlayer_legacy blOther = PlayerManager_legacy.get().getPlayer(o.getUniqueId());
            if (blOther == null) continue;

            if (blPlayer.isVanished() && !blOther.getRank().isAtLeast(Rank.MOD)) {
                o.hidePlayer(player);
            } else {
                o.showPlayer(player);
            }
        }
    }

    public void stop() {
        bossBar.removeAll();
    }
}
