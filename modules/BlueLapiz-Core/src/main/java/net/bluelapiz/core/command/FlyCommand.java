/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 4:28 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.packet.BroadcastMessagePacket;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand extends BLCommand {

    public FlyCommand() {
        super("fly");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (Core.SERVER_NAME.equalsIgnoreCase("hub"))
            if (!isAuthorizedOrError(sender, Rank.VIP)) return true;
            else if (!isAuthorizedOrError(sender, Rank.MOD_JR)) return true;

        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly players can do this!");
            return true;
        }

        Player player = (Player) sender;

        player.setAllowFlight(!player.getAllowFlight());

        String enabled = player.getAllowFlight() ? "§aenabled" : "§cdisabled";

        String broadcast = "§6§l[!] §7[" + Core.SERVER_NAME + "] §f" + sender.getName() + " §7" + enabled + " §ffly!";
        PluginMessageChannel.sendPluginMessage(player, new BroadcastMessagePacket(Rank.BUILDER, broadcast));

        return true;
    }
}
