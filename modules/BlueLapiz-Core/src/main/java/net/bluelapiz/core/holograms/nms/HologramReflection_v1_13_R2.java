/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.core.holograms.nms;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class HologramReflection_v1_13_R2 implements HologramReflection {

    private String path;
    private String version;

    // Classes
    private Class<?> craftWorld;
    private Class<?> craftPlayer;
    private Class<?> armorStand;
    private Class<?> worldClass;
    private Class<?> nmsEntity;
    private Class<?> packetSpawnEntityClass;
    private Class<?> entityLivingClass;
    private Class<?> chatComponentTextClass;
    private Class<?> iChatBaseComponentClass;
    private Class<?> destroyPacketClass;
    private Class<?> nmsPacket;
    private Class<?> entityPlayer;

    // Constructors
    private Constructor<?> destroyPacketConstructor;
    private Constructor<?> armorStandConstructor;

    // Fields
    private Field playerHandleConnectionField;
    private Field packetSpawnEntityField_a;

    // Methods
    private Method craftWorldGetHandle;
    private Method setCustomNameMetod;
    private Method setCustomNameVisible;
    private Method setGravity;
    private Method setLocation;
    private Method setInvisible;
    private Method craftPlayerGetHandle;
    private Method playerHandleSendPacket;

    public HologramReflection_v1_13_R2() {
        path = Bukkit.getServer().getClass().getPackage().getName();
        version = path.substring(path.lastIndexOf(".") + 1);

        try {
            // Classes
            craftWorld = Class.forName("org.bukkit.craftbukkit." + version + ".CraftWorld");
            craftPlayer = Class.forName("org.bukkit.craftbukkit." + version + ".entity.CraftPlayer");
            armorStand = Class.forName("net.minecraft.server." + version + ".EntityArmorStand");
            worldClass = Class.forName("net.minecraft.server." + version + ".World");
            nmsEntity = Class.forName("net.minecraft.server." + version + ".Entity");
            packetSpawnEntityClass = Class.forName("net.minecraft.server." + version + ".PacketPlayOutSpawnEntityLiving");
            entityLivingClass = Class.forName("net.minecraft.server." + version + ".EntityLiving");
            chatComponentTextClass = Class.forName("net.minecraft.server." + version + ".ChatComponentText");
            iChatBaseComponentClass = Class.forName("net.minecraft.server." + version + ".IChatBaseComponent");
            destroyPacketClass = Class.forName("net.minecraft.server." + version + ".PacketPlayOutEntityDestroy");
            nmsPacket = Class.forName("net.minecraft.server." + version + ".Packet");
            entityPlayer = Class.forName("net.minecraft.server." + version + ".EntityPlayer");

            // Constructors
            armorStandConstructor = armorStand.getConstructor(worldClass);
            destroyPacketConstructor = destroyPacketClass.getConstructor(int[].class);

            // Fields
            playerHandleConnectionField = entityPlayer.getField("playerConnection");
            packetSpawnEntityField_a = packetSpawnEntityClass.getDeclaredField("a");

            // Methods
            craftWorldGetHandle = craftWorld.getMethod("getHandle");
            setCustomNameMetod = armorStand.getMethod("setCustomName", iChatBaseComponentClass);
            setCustomNameVisible = nmsEntity.getMethod("setCustomNameVisible", boolean.class);
            setGravity = armorStand.getMethod("setNoGravity", boolean.class);
            setLocation = armorStand.getMethod("setLocation", double.class, double.class, double.class, float.class, float.class);
            setInvisible = armorStand.getMethod("setInvisible", boolean.class);
            craftPlayerGetHandle = craftPlayer.getMethod("getHandle");
            playerHandleSendPacket = playerHandleConnectionField.getType().getMethod("sendPacket", nmsPacket);
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | NoSuchFieldException ex) {
            System.err.println("Something went wrong while doing reflection! Is the server the correct version?");
            ex.printStackTrace();
        }
    }

    @Override
    public Object getPacket(World w, double x, double y, double z, String text) {
        try {
            Object craftWorldObj = craftWorld.cast(w);
            Object entityObject = armorStandConstructor.newInstance(craftWorldGetHandle.invoke(craftWorldObj));
            Object cbc = iChatBaseComponentClass.cast(chatComponentTextClass.getConstructor(String.class).newInstance(text));
            setCustomNameMetod.invoke(entityObject, cbc);
            setCustomNameVisible.invoke(entityObject, true);
            setGravity.invoke(entityObject, true);
            setLocation.invoke(entityObject, x, y, z, 0.0F, 0.0F);
            setInvisible.invoke(entityObject, true);
            Constructor<?> cw = packetSpawnEntityClass.getConstructor(entityLivingClass);
            Object packetObject = cw.newInstance(entityObject);
            return packetObject;
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Object getDestroyPacket(Object object) throws IllegalAccessException {
        packetSpawnEntityField_a.setAccessible(true);
        return getDestroyPacket((int) packetSpawnEntityField_a.get(object));
    }

    @Override
    public Object getDestroyPacket(int id) {
        try {
            return destroyPacketConstructor.newInstance(new int[]{id});
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void sendPacket(Player p, Object packet) {
        try {
            Object entityPlayer = craftPlayerGetHandle.invoke(craftPlayer.cast(p));
            Object playerConnection = playerHandleConnectionField.get(entityPlayer);
            playerHandleSendPacket.invoke(playerConnection, packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
