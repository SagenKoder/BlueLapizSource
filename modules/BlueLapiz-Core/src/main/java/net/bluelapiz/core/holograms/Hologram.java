/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.holograms;

import net.bluelapiz.core.holograms.nms.HologramReflection;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class Hologram {

    private static final double HOLOGRAM_HEIGHT = 0.23D;

    private static HologramReflection hologramReflections;

    private final List<Object> destroyCache;
    private final List<Object> spawnCache;
    private final List<UUID> players;
    private final List<String> lines;
    private final Location loc;

    public Hologram(Location loc, String... lines) {
        this(loc, Arrays.asList(lines));
    }

    public Hologram(Location loc, List<String> lines) {
        if (hologramReflections == null) {
            String path = Bukkit.getServer().getClass().getPackage().getName();
            String version = path.substring(path.lastIndexOf(".") + 1);
            try {
                Class reflection = Class.forName("net.bluelapiz.core.holograms.nms.HologramReflection_" + version);
                hologramReflections = (HologramReflection) reflection.getConstructors()[0].newInstance();
                System.out.println("Initialised holograms for version " + version);
            } catch (Exception e) {
                System.err.println("*******************************************");
                System.err.println("UNSUPPORTED VERSION " + version + "!!!");
                System.err.println("Cannot initialise Holograms module!");
                e.printStackTrace();
            }
        }
        this.lines = lines;
        this.loc = loc;
        this.players = new ArrayList<>();
        this.spawnCache = new ArrayList<>();
        this.destroyCache = new ArrayList<>();

        // Init
        Location displayLoc = loc.clone().add(0, (HOLOGRAM_HEIGHT * lines.size()) - 1.97D, 0);
        for (int i = 0; i < lines.size(); i++) {
            Object packet = hologramReflections.getPacket(this.loc.getWorld(), displayLoc.getX(), displayLoc.getY(), displayLoc.getZ(), this.lines.get(i));
            this.spawnCache.add(packet);
            try {
                this.destroyCache.add(hologramReflections.getDestroyPacket(packet));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            displayLoc.add(0, HOLOGRAM_HEIGHT * (-1), 0);
        }
    }

    public static Hologram newInstance(Location location, String... lines) {
        return newInstance(location, Arrays.asList(lines));
    }

    public static Hologram newInstance(Location location, List<String> lines) {
        return new Hologram(location, lines);
    }

    public boolean display(Player p) {
        for (int i = 0; i < this.spawnCache.size(); i++) {
            hologramReflections.sendPacket(p, this.spawnCache.get(i));
        }

        this.players.add(p.getUniqueId());
        return true;
    }

    public boolean destroy(Player p) {
        if (this.players.contains(p.getUniqueId())) {
            for (int i = 0; i < this.destroyCache.size(); i++) {
                hologramReflections.sendPacket(p, this.destroyCache.get(i));
            }
            this.players.remove(p.getUniqueId());
            return true;
        }
        return false;
    }
}
