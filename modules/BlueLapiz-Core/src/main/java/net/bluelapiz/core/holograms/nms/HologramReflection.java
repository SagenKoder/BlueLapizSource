/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.core.holograms.nms;

import org.bukkit.World;
import org.bukkit.entity.Player;

public interface HologramReflection {

    Object getPacket(World w, double x, double y, double z, String text);

    Object getDestroyPacket(Object object) throws IllegalAccessException;

    Object getDestroyPacket(int id);

    void sendPacket(Player p, Object packet);
}
