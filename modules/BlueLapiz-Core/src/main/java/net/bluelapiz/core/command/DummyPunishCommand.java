/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:27 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.punish.PunishReason;
import net.bluelapiz.core.Core;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DummyPunishCommand extends BLCommand {

    public DummyPunishCommand() {
        super("punish", "p");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] strings) {
        if (strings.length == 1) {
            String startOfName = strings[0].toLowerCase().trim();
            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
                    .filter(s -> s.toLowerCase().startsWith(startOfName))
                    .collect(Collectors.toList());
            Core.log.info(String.join(" ", strings) + " -> " + names);
            return names;
        } else if (strings.length == 2) {
            String startOfPunish = strings[1].toLowerCase().trim();
            List<String> ret = Arrays.stream(PunishReason.values())
                    .filter(PunishReason::isInPunishCommand)
                    .map(PunishReason::name)
                    .filter(n -> n.toLowerCase().startsWith(startOfPunish))
                    .collect(Collectors.toList());
            Core.log.info(String.join(" ", strings) + " -> " + ret);
            return ret;
        } else {
            Core.log.info(String.join(" ", strings) + " -> " + Arrays.asList());
            return Arrays.asList();
        }
    }
}
