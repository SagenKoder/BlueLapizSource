/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.command;

import net.bluelapiz.common.player.objects.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ForceRespawnCommand extends BLCommand {

    public ForceRespawnCommand() {
        super("forcerespawn");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!isAuthorized(sender, Rank.FOUNDER)) return true;

        for (Player o : Bukkit.getOnlinePlayers()) {
            o.spigot().respawn();
            o.sendMessage("§aYou were forced to respawn by " + sender.getName());
        }

        sender.sendMessage("§aDone :P");
        return true;
    }
}
