/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 4:54 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.path;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import net.bluelapiz.common.util.Touple;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.holograms.Hologram;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.lang.reflect.Method;
import java.util.*;

public class BeaconFlightPath {

    private static Color[] cleanColors = new Color[]{
            Color.fromBGR(255, 255, 255),
            Color.fromBGR(1, 1, 255),
            Color.fromBGR(255, 1, 1),
            Color.fromBGR(1, 255, 1),
            Color.fromBGR(255, 255, 1),
            Color.fromBGR(255, 1, 255),
            Color.fromBGR(1, 255, 255),
            Color.fromBGR(1, 1, 1)
    };

    private static Random random = new Random(System.currentTimeMillis());
    private String name;
    private String worldName;
    private CubicSpline3D<Vector> path;
    private float travelDurationInTicks;
    private Vector p_end;
    private Vector p_start;
    private HashMap<UUID, Touple<Entity, Integer>> playersOnPath = new HashMap<>();
    private HashSet<UUID> playersWrongDirection = new HashSet<>();
    private HashMap<UUID, Long> playersCooldown = new HashMap<>();
    private Hologram hologram_start;
    private Hologram hologram_end;
    private String title_start;
    private String title_end;
    private Method[] moveEntityReflection = null;
    private int ticksPassed = 0;

    BeaconFlightPath(String name, String worldName, List<Vector> path, float speedMultiplier, String title_start, String title_end) {
        this.name = name.toLowerCase();
        this.path = new CubicSpline3D<>(Vector.class, Double.TYPE);
        for (Vector v : path) {
            this.path.addPoint(v);
        }
        this.path.calcSpline();
        this.p_end = path.get(0);
        this.p_start = path.get(path.size() - 1);
        this.worldName = worldName;
        this.travelDurationInTicks = (int) (this.path.getHeuristicDistance() * speedMultiplier);

        this.title_start = title_start;
        this.title_end = title_end;

        this.hologram_start = Hologram.newInstance(new Location(Bukkit.getWorld(worldName), p_start.getX(), p_start.getY() + 1.5, p_start.getZ()),
                ChatColor.GOLD + "" + ChatColor.BOLD + "Beacon Flight", ChatColor.AQUA + title_start);
        this.hologram_end = Hologram.newInstance(new Location(Bukkit.getWorld(worldName), p_end.getX(), p_end.getY() + 1.5, p_end.getZ()),
                ChatColor.GOLD + "" + ChatColor.BOLD + "Beacon Flight", ChatColor.AQUA + title_end);
    }

    static String serialize(BeaconFlightPath path) {
        StringBuilder sb = new StringBuilder();

        sb.append(path.name).append("##")
                .append(path.worldName).append("##")
                .append((int) path.travelDurationInTicks).append("##")
                .append(path.title_start).append("##")
                .append(path.title_end).append("##");

        boolean first = true;
        for (Vector v : path.getPath().getPoints()) {
            if (!first) sb.append("@");
            sb.append(v.getX()).append(",").append(v.getY()).append(v.getZ());
            first = false;
        }

        Core.log.info(sb.toString());
        return sb.toString();
    }

    static BeaconFlightPath deserialize(String path) {
        try {
            String[] split = path.split("##");
            String name = split[0];
            String worldname = split[1];
            float travelDurationInTicks = Integer.parseInt(split[2]);
            String title_start = split[3];
            String title_end = split[4];

            String pointsString = split[5];
            String[] pointsStringSplit = pointsString.split("@");
            LinkedList<Vector> points = new LinkedList<>();
            for (String point : pointsStringSplit) {
                String[] pointSegments = point.split(",");
                points.addLast(new Vector(
                        Double.parseDouble(pointSegments[0]),
                        Double.parseDouble(pointSegments[1]),
                        Double.parseDouble(pointSegments[2])
                ));
            }
            BeaconFlightPath beaconFlightPath = new BeaconFlightPath(name, worldname, points, 1.0f, title_start, title_end);
            beaconFlightPath.setTravelDurationInTicks(travelDurationInTicks);
            return beaconFlightPath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private Entity createArmourStand(Vector pos, Player p) {
        LivingEntity entity = (LivingEntity) Bukkit.getWorld(worldName).spawnEntity(new Location(
                Bukkit.getWorld(worldName), pos.getX(), pos.getY(), pos.getZ()), EntityType.ARMOR_STAND);

        entity.addPassenger(p);
        entity.setInvulnerable(true);
        entity.setAI(false);
        entity.setGliding(false);
        entity.setGravity(false);

        ArmorStand as = (ArmorStand) entity;
        as.setVisible(false);
        as.setSmall(true);
        as.setMarker(true);

        return entity;
    }

    boolean isOnCooldown(Player player) {
        return playersCooldown.containsKey(player.getUniqueId());
    }

    public void addPlayer(Player player, boolean otherDirection) {
        if (Bukkit.getWorld(worldName) == null) {
            Core.log.info("Cannot find the world!");
            player.sendMessage("§cError occured. Could not find the world " + worldName + "!");
            return;
        }

        // ignore players already added
        if (playersOnPath.containsKey(player.getUniqueId())) return;

        Vector start = (otherDirection ? p_end : p_start);

        player.setSneaking(false);

        Entity entity = createArmourStand(start, player);

        playersOnPath.put(player.getUniqueId(), new Touple<>(entity, (int) (otherDirection ? travelDurationInTicks : 0)));
        if (otherDirection) {
            playersWrongDirection.add(player.getUniqueId());
        } else {
            playersWrongDirection.remove(player.getUniqueId());
        }
        playersCooldown.put(player.getUniqueId(), System.currentTimeMillis());
    }

    public void playerQuit(Player player) {
        removePlayer(player.getUniqueId());
        hologram_start.destroy(player);
        hologram_end.destroy(player);
        playersCooldown.remove(player.getUniqueId());
    }

    public void destroy() {
        for (Player o : Bukkit.getOnlinePlayers()) {
            playerQuit(o);
        }
    }

    private void removePlayer(UUID... uuids) {
        for (UUID uuid : uuids) {
            if (!playersOnPath.containsKey(uuid)) continue;
            Entity entity = playersOnPath.get(uuid).getLeft(); // delete the vehicle
            Vector end = playersWrongDirection.contains(uuid) ? p_start : p_end;
            moveEntity(entity, end.getX(), end.getY(), end.getZ());
            entity.remove();
            playersOnPath.remove(uuid);
            playersWrongDirection.remove(uuid);
            Player p = Bukkit.getPlayer(uuid);
            if (p != null) {
                p.setVelocity(new Vector());
                p.setGravity(true);
                try {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            p.teleport(new Location(p.getWorld(), end.getX(), end.getY(), end.getZ(), p.getLocation().getYaw(), p.getLocation().getPitch()));
                        }
                    }.runTaskLater(Core.get(), 1);
                } catch (Exception e) {
                }
            }
            playersCooldown.put(uuid, System.currentTimeMillis());
        }
    }

    // package private
    void update() {
        World world = Bukkit.getWorld(worldName);
        if (world == null) {
            Core.log.info("Cannot find world " + worldName + "!!");
            return;
        }
        ArrayList<UUID> remove = new ArrayList<>();
        for (Map.Entry<UUID, Touple<Entity, Integer>> e : playersOnPath.entrySet()) {
            boolean otherDirection = playersWrongDirection.contains(e.getKey());
            Vector end = otherDirection ? p_start : p_end;
            Player p = Bukkit.getPlayer(e.getKey());
            e.getValue().setRight(e.getValue().getRight() + (otherDirection ? -1 : 1));
            if (p == null || p.getWorld() != world || e.getValue().getRight() >= travelDurationInTicks || e.getValue().getRight() < 0) {
                remove.add(e.getKey());
                continue;
            }
            Vector point = (e.getValue().getRight() == travelDurationInTicks) ? end : path.getPoint(1 - (e.getValue().getRight() / travelDurationInTicks));
            if (point == null) continue;
            if (p.getVehicle() == null) {
                e.getValue().getLeft().addPassenger(p);
                //e.getValue().setLeft(createArmourStand(point, p));
            }
            moveEntity(e.getValue().getLeft(), point.getX(), point.getY() + 0.3f, point.getZ(), p.getLocation().getYaw(), p.getLocation().getPitch());
        }
        for (UUID u : remove) removePlayer(u);

        ticksPassed++;

        if (ticksPassed % 2 == 0)
            renderParticles();

        if (ticksPassed % 20 == 0) {
            // update holograms every second
            for (Player o : Bukkit.getOnlinePlayers()) {
                if (o.getWorld().getName().equalsIgnoreCase(worldName)) {
                    if (o.getLocation().toVector().distanceSquared(p_start) < Math.pow(100, 2)) {
                        hologram_start.display(o);
                    }
                    if (o.getLocation().toVector().distanceSquared(p_end) < Math.pow(100, 2)) {
                        hologram_end.display(o);
                    }
                }
            }

            // check for cooldown every second
            HashSet<UUID> removeUuids = new HashSet<>();
            for (Map.Entry<UUID, Long> entry : playersCooldown.entrySet()) {
                Player p = Bukkit.getPlayer(entry.getKey());
                if (p == null) {
                    removeUuids.add(entry.getKey());
                    continue;
                }
                // minimum 2 seconds cooldown
                if (entry.getValue() + 1_000 > System.currentTimeMillis()) continue;
                int pathStartX = p_start.getBlockX();
                int pathStartY = p_start.getBlockY();
                int pathStartZ = p_start.getBlockZ();
                int pathEndX = p_end.getBlockX();
                int pathEndY = p_end.getBlockY();
                int pathEndZ = p_end.getBlockZ();
                int playerX = p.getLocation().getBlockX();
                int playerY = p.getLocation().getBlockY();
                int playerZ = p.getLocation().getBlockZ();
                if (!(p.getWorld().getName().equalsIgnoreCase(worldName)
                        && ((pathStartX == playerX && pathStartY == playerY && pathStartZ == playerZ) ||
                        (pathEndX == playerX && pathEndY == playerY && pathEndZ == playerZ)))) {
                    removeUuids.add(entry.getKey());
                }
            }
            for (UUID uuid : removeUuids) playersCooldown.remove(uuid);
        }
    }

    private void renderParticles() {
        World world = Bukkit.getWorld(worldName);
        if (world == null) return;

        if (playersOnPath.size() > 0) {
            for (float i = 0; i < 1; i += 1.0f / getTravelDurationInTicks()) {
                if (random.nextFloat() > 0.98f) {
                    Vector point = path.getPoint(i);
                    world.spawnParticle(Particle.REDSTONE, point.getX(), point.getY(), point.getZ(), 0,
                            new Particle.DustOptions(cleanColors[0], 4));
                }
            }
        } else {
            for (float i = 0.00f; i <= 1; i += 1.0f / getTravelDurationInTicks()) {
                if (random.nextFloat() > 0.95f) {
                    Vector point = path.getPoint(i);
                    world.spawnParticle(Particle.REDSTONE, point.getX() + Math.random() * 0.5f, point.getY() + Math.random() * 0.5f, point.getZ() + Math.random() * 0.5f, 0,
                            new Particle.DustOptions(cleanColors[random.nextInt(cleanColors.length)], 1));
                }
            }
        }
    }

    private Method[] getMoveEntityReflection() {
        if (moveEntityReflection == null) {
            try {
                Method getHandle = Class.forName(Bukkit.getServer().getClass().getPackage().getName() + ".entity.CraftEntity").getDeclaredMethod("getHandle");
                moveEntityReflection = new Method[]{getHandle, getHandle.getReturnType().getDeclaredMethod("setPositionRotation", double.class, double.class, double.class, float.class, float.class)};
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return moveEntityReflection;
    }

    private void moveEntity(Entity entity, double x, double y, double z) {
        moveEntity(entity, x, y, z, entity.getLocation().getYaw(), entity.getLocation().getPitch());
    }

    private void moveEntity(Entity entity, double x, double y, double z, float yaw, float pitch) {
        try {
            getMoveEntityReflection()[1].invoke(moveEntityReflection[0].invoke(entity), x, y, z, yaw, pitch);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setShifting(boolean shifting) {
        for (UUID u : playersOnPath.keySet()) {
            Player p = Bukkit.getPlayer(u);
            if (p == null) continue;

            for (Player o : Bukkit.getOnlinePlayers()) {
                if (p.getWorld().getName().equals(o.getWorld().getName())) {
                    try {
                        PacketContainer packet = new PacketContainer(PacketType.Play.Server.ENTITY_METADATA);
                        packet.getIntegers().write(0, p.getEntityId());

                        WrappedDataWatcher watcher = new WrappedDataWatcher();
                        WrappedDataWatcher.Serializer serializer = WrappedDataWatcher.Registry.get(Byte.class);
                        watcher.setEntity(p);
                        if (shifting)
                            watcher.setObject(0, serializer, (byte) (0x02));
                        else
                            watcher.setObject(0, serializer, (byte) (0x00));
                        packet.getWatchableCollectionModifier().write(0, watcher.getWatchableObjects());

                        ProtocolLibrary.getProtocolManager().sendServerPacket(o, packet);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public String getName() {
        return name;
    }

    String getWorldName() {
        return worldName;
    }

    public CubicSpline3D<Vector> getPath() {
        return path;
    }

    public float getTravelDurationInTicks() {
        return travelDurationInTicks;
    }

    private void setTravelDurationInTicks(float travelDurationInTicks) {
        this.travelDurationInTicks = travelDurationInTicks;
    }

    public Vector getPEnd() {
        return p_end;
    }

    public Vector getPStart() {
        return p_start;
    }

    HashMap<UUID, Touple<Entity, Integer>> getPlayersOnPath() {
        return playersOnPath;
    }
}
