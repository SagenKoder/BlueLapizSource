/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 3:15 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.helper;

import net.bluelapiz.core.Core;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class ParticleBorder {

    private static final int MAX_VIEW_DISTANCE = 10;
    private static final int MAX_VIEW_SIDE = 8;

    private int xMin;
    private int xMax;
    private int zMin;
    private int zMax;

    public ParticleBorder(int xMin, int xMax, int zMin, int zMax) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.zMin = zMin;
        this.zMax = zMax;
    }

    public void renderAsync(ParticleEffect e, float particlesPerBlock, boolean randomOffset, Player... players) {
        new BukkitRunnable() {
            @Override
            public void run() {
                render(e, particlesPerBlock, randomOffset, players);
            }
        }.runTaskAsynchronously(Core.get());
    }

    public void render(ParticleEffect e, float particlesPerBlock, boolean randomOffset, Player... players) {
        for (Player p : players) renderFor(e, particlesPerBlock, randomOffset, p);
    }

    private void renderFor(ParticleEffect e, float particlesPerBlock, boolean randomOffset, Player p) {

        float increment = 1f / particlesPerBlock;
        float particleOffset = randomOffset ? increment / 2f : 0f;

        int playerX = p.getLocation().getBlockX();
        int playerY = p.getLocation().getBlockY();
        int playerZ = p.getLocation().getBlockZ();

        int xMin = this.xMin;
        int xMax = this.xMax + 1;

        int yMin = playerY - 2;
        int yMax = playerY + 6;

        int zMin = this.zMin;
        int zMax = this.zMax + 1;

        boolean nearbyXMin = Math.abs(xMin - playerX) <= MAX_VIEW_DISTANCE;
        boolean nearbyXMax = Math.abs(xMax - playerX) <= MAX_VIEW_DISTANCE;

        if (nearbyXMin || nearbyXMax) {
            for (float y = yMin; y <= yMax; y += increment) {
                for (float z = Math.max(zMin, playerZ - MAX_VIEW_SIDE); z <= Math.min(zMax, playerZ + MAX_VIEW_SIDE); z += increment) {
                    boolean border = z == Math.max(zMin, playerZ - MAX_VIEW_SIDE) || z == Math.min(zMax, playerZ + MAX_VIEW_SIDE);
                    if (!(border && randomOffset) && nearbyXMin)
                        e.display(0f, particleOffset, particleOffset, 0.0f, 1, new Location(p.getWorld(), xMin, y, z), p);
                    if (!(border && randomOffset) && nearbyXMax)
                        e.display(0f, particleOffset, particleOffset, 0.0f, 1, new Location(p.getWorld(), xMax, y, z), p);
                }
            }
        }

        boolean nearbyZMin = Math.abs(zMin - playerZ) <= MAX_VIEW_DISTANCE;
        boolean nearbyZMax = Math.abs(zMax - playerZ) <= MAX_VIEW_DISTANCE;

        if (nearbyZMin || nearbyZMax) {
            for (float y = yMin; y <= yMax; y += increment) {
                for (float x = Math.max(xMin, playerX - MAX_VIEW_SIDE); x <= Math.min(xMax, playerX + MAX_VIEW_SIDE); x += increment) {
                    boolean border = x == Math.max(xMin, playerX - MAX_VIEW_SIDE) || x == Math.min(xMax, playerX + MAX_VIEW_SIDE);
                    if (!border && nearbyZMin)
                        e.display(particleOffset, particleOffset, 0f, 0.0f, 1, new Location(p.getWorld(), x, y, zMin), p);
                    if (!border && nearbyZMax)
                        e.display(particleOffset, particleOffset, 0f, 0.0f, 1, new Location(p.getWorld(), x, y, zMax), p);
                }
            }
        }
    }

    public void renderWall() {

    }

    public int getxMin() {
        return xMin;
    }

    public int getxMax() {
        return xMax;
    }

    public int getzMin() {
        return zMin;
    }

    public int getzMax() {
        return zMax;
    }
}
