/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:39 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.core.pms;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.bluelapiz.common.packet.*;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.core.Core;
import net.bluelapiz.core.config.Configuration;
import net.bluelapiz.core.helper.GlowHelper;
import net.bluelapiz.core.helper.VanishHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class PluginMessageChannel implements PluginMessageListener {

    public static void sendPluginMessage(Packet packet) {
        for (Player o : Bukkit.getOnlinePlayers()) {
            sendPluginMessage(o, packet);
            return;
        }
    }

    public static void sendPluginMessage(Player player, Packet packet) {

        Core.log.debug("PACKET SENT (player=" + player.getName() + "): " + packet.toString());

        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(PacketHelper.PLUGIN_SUBCHANNEL);
        out.writeUTF(PacketHelper.getPacket(packet));
        player.sendPluginMessage(Core.get(), PacketHelper.BUNGEE_MESSAGES_CHANNEL, out.toByteArray());
    }

    @Override
    public synchronized void onPluginMessageReceived(String channel, Player player, byte[] packetBytes) {

        if (!channel.equalsIgnoreCase(PacketHelper.BLUELAPIZ_MESSAGES_CHANNEL)) return;

        ByteArrayDataInput in = ByteStreams.newDataInput(packetBytes);
        String subchannel = in.readUTF();
        if (!subchannel.equals(PacketHelper.PLUGIN_SUBCHANNEL)) return;

        Packet packet = PacketHelper.getPacket(in.readUTF());

        Core.log.debug("PACKET RECEIVED(player=" + player.getName() + "): " + packet.toString());

        if (packet != null) {
            if (packet instanceof PlayerRankChangePacket) {
                PlayerRankChangePacket prcp = (PlayerRankChangePacket) packet;
                Player p = Bukkit.getPlayer(prcp.getUuid());
                BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(prcp.getUuid());
                if (blPlayer != null) {
                    blPlayer.setRank(prcp.getRank());
                }
                if (p != null) {
                    GlowHelper.get().updateGlowFor(p);
                }
            } else if (packet instanceof GlobalChatPacket) {
                if (Core.USE_GLOBAL_CHAT) {
                    GlobalChatPacket gcp = (GlobalChatPacket) packet;
                    Bukkit.broadcastMessage(gcp.getFormattedMessage());
                }
            } else if (packet instanceof SetGlobalChatEnabledPacket) {
                SetGlobalChatEnabledPacket sgcep = (SetGlobalChatEnabledPacket) packet;
                Core.USE_GLOBAL_CHAT = sgcep.isSetEnabled();
                Core.log.debug("Recieved UseGlobalChat from proxy! New setting is " + sgcep.isSetEnabled());

                Configuration config = Core.get().getConfigurationManager().getConfiguration("config");
                config.set("server.globalChat", sgcep.isSetEnabled());
                config.forceSave();
            } else if (packet instanceof ServerNamePacket) {
                ServerNamePacket snp = (ServerNamePacket) packet;
                Core.SERVER_NAME = snp.getServerName();
                //ServerPropertiesHelper.get().setProperty(ServerPropertiesHelper.ServerProperty.SERVER_NAME, snp.getServerName());
                Core.HAS_UPDATED_SERVER_NAME = true;
                Core.log.debug("Recieved server name from proxy! New name is " + snp.getServerName());

                Configuration config = Core.get().getConfigurationManager().getConfiguration("config");
                config.set("server.serverName", snp.getServerName());
                config.forceSave();
            } else if (packet instanceof SetVanishEnabledPacket) {
                SetVanishEnabledPacket svep = (SetVanishEnabledPacket) packet;
                BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(svep.getUuid());
                if (blPlayer == null) return;
                blPlayer.setVanished(svep.isEnableVanish());

                Player bukkitPlayer = Bukkit.getPlayer(svep.getUuid());
                if (bukkitPlayer != null) {
                    if (bukkitPlayer != null) {
                        for (Player o : Bukkit.getOnlinePlayers())
                            VanishHelper.get().updateVanish(o);
                    }
                }
            } else if (packet instanceof UpdatePunishmentPacket) {
                UpdatePunishmentPacket pp = (UpdatePunishmentPacket) packet;
                BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(pp.getPunishResult().getUuid());
                if (blPlayer == null) return;
                blPlayer.setPunishedBy(pp.getPunishResult().getBy_uuid());
                blPlayer.setPunishType(pp.getPunishResult().getType());
                blPlayer.setPunishReason(pp.getPunishResult().getReason());
                blPlayer.setPunishNumber(pp.getPunishResult().getNumber());
                blPlayer.setPunishDescription(pp.getPunishResult().getDesc());
                blPlayer.setPunishStart(pp.getPunishResult().getStart());
                blPlayer.setPunishDuration(pp.getPunishResult().getDuration());
            }
        }
    }
}
