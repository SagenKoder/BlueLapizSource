/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.utils.HastebinUtils;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.PunishHelper;
import net.bluelapiz.common.punish.PunishReason;
import net.bluelapiz.common.punish.PunishType;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HistoryCommand extends BLCommand {

    private static final String sql = "SELECT \n" +
            "\tpunish_log.id, \n" +
            "\tpunish_log.type, \n" +
            "\tpunish_log.reason, \n" +
            "\tpunish_log.number, \n" +
            "\tpunish_log.punish_desc,\n" +
            "\tpunish_log.start,\n" +
            "\tpunish_log.duration,\n" +
            "\tplayer.name\n" +
            "FROM punish_log, player\n" +
            "WHERE \n" +
            "\tpunish_log.uuid=%uuid AND\n" +
            "\tplayer.uuid = punish_log.by_uuid AND\n" +
            "\tstart > %start\n" +
            "ORDER BY punish_log.start DESC;\n";

    public HistoryCommand() {
        super("history", "h");
    }


    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD_JR)) return;

        if (strings.length != 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /history <player>"));
            return;
        }

        PlayerManager_legacy.get().getOrLoadPlayer(strings[0], new BLPlayerCallback() {
            @Override
            public void success(BLPlayer_legacy player) {

                try {
                    LinkedList<Map<String, Object>> result = SQLManager.getSQLHelper_legacy().selectSync(sql
                            .replace("%uuid", SQLData.ofUuid(player.getUuid()).toString())
                            .replace("%start", SQLData.ofInteger(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(120)).toString())
                    );
                    if (result.size() > 5) {
                        commandSender.sendMessage(TextComponent.fromLegacyText("§3**** Result is too big, creating a paste! ****"));
                        boolean first = true;
                        StringBuilder sb = new StringBuilder("List of every punishment from player " + player.getName() + " (" + result.size() + " in total)!\n\n");
                        for (Map<String, Object> row : result) {
                            if (!first) sb.append("\n");
                            first = false;

                            int id = (Integer) row.get("id");
                            PunishType punishType = PunishType.valueOf((String) row.get("type"));
                            PunishReason punishReason = PunishReason.valueOf((String) row.get("reason"));
                            int number = (Integer) row.get("number");
                            String desc = (String) row.get("punish_desc");
                            long start = BLPlayer_legacy.bigIntegerToLong(row.get("start"));
                            long duration = BLPlayer_legacy.bigIntegerToLong(row.get("duration"));
                            String staffname = (String) row.get("name");

                            sb.append("Date: " + PunishHelper.get().formatDate(start) + "\n");
                            sb.append("Reason: " + punishReason.name() + " " + PunishHelper.get().getStringNumber(number) + "\n");
                            sb.append("Type: " + punishType.name() + ((duration > 0L) ? " Duration: " + PunishHelper.get().formatTime(duration) : "") + "\n");
                            if (punishReason == PunishReason.CUSTOM)
                                sb.append("Custom reason: ").append(desc).append("\n");
                            sb.append("By: ").append(staffname).append(" Punish ID: ").append(id).append("\n");
                        }

                        String url = HastebinUtils.post(sb.toString());
                        commandSender.sendMessage(TextComponent.fromLegacyText("§aCreated the paste link: " + url));
                        commandSender.sendMessage(TextComponent.fromLegacyText("§3****"));
                    } else if (result.size() > 0) {
                        commandSender.sendMessage(TextComponent.fromLegacyText("§3**** Listing punishments for " + player.getName() + " ****"));
                        boolean first = true;
                        for (Map<String, Object> row : result) {

                            if (!first) commandSender.sendMessage();
                            first = false;

                            int id = (Integer) row.get("id");
                            PunishType punishType = PunishType.valueOf((String) row.get("type"));
                            PunishReason punishReason = PunishReason.valueOf((String) row.get("reason"));
                            int number = (Integer) row.get("number");
                            String desc = (String) row.get("punish_desc");
                            long start = BLPlayer_legacy.bigIntegerToLong(row.get("start"));
                            long duration = BLPlayer_legacy.bigIntegerToLong(row.get("duration"));
                            String staffname = (String) row.get("name");

                            commandSender.sendMessage(TextComponent.fromLegacyText("§cDate: §f" + PunishHelper.get().formatDate(start)));
                            commandSender.sendMessage(TextComponent.fromLegacyText("§cReason: §f" + punishReason.name() + " " + PunishHelper.get().getStringNumber(number)));
                            commandSender.sendMessage(TextComponent.fromLegacyText("§cType: §f" + punishType.name() + ((duration > 0L) ? " §cDuration: §f" + PunishHelper.get().formatTime(duration) : "")));
                            if (punishReason == PunishReason.CUSTOM)
                                commandSender.sendMessage(TextComponent.fromLegacyText("§cCustom reason: §f" + desc));
                            commandSender.sendMessage(TextComponent.fromLegacyText("§cBy: §f" + staffname + " §cPunish ID: §f" + id));
                        }
                        commandSender.sendMessage(TextComponent.fromLegacyText("§3****"));
                    } else {
                        commandSender.sendMessage(TextComponent.fromLegacyText("§aNo punishments listed!"));
                    }

                    commandSender.sendMessage(TextComponent.fromLegacyText("§3**** Current Punishment ****"));
                    PunishType punishType = PunishHelper.get().getActivePunishType(player);
                    if (punishType == PunishType.NO_PUNISH) {
                        commandSender.sendMessage(TextComponent.fromLegacyText("§aThis player has no active punishments!"));
                    } else {
                        commandSender.sendMessage(TextComponent.fromLegacyText("§aPrinting players punish message....\n" + PunishHelper.get().getActivePunishDescription(player)));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not find the player in the cache!"));
            }
        });
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfName = strings[0].toLowerCase();
//            return PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//        } else {
//            return Collections.emptyList();
//        }
//    }
}
