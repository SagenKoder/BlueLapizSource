/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.logging.ChatLogManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.awt.*;
import java.util.Arrays;

public class StaffHelpCommand extends BLCommand {

    public StaffHelpCommand() {
        super("staffhelp", "sh");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (strings.length < 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /sh <message>"));
            return;
        }

        String message = String.join(" ", Arrays.copyOfRange(strings, 0, strings.length));
        String format;
        Color color;
        if (commandSender instanceof ProxiedPlayer) {
            BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(((ProxiedPlayer) commandSender).getUniqueId());
            Rank rank = blp.getRank();
            color = rank.getDiscordColor();
            format = "§c§l[?] §7[%server] §f%player §7> §f%message";

            format = format.replace("%prefix", rank.getColor());
            format = format.replace("%player", commandSender.getName());
            format = format.replace("", "");
            //"§6[SS " + rank.getPrefix() + "" + commandSender.getName() + "§6] ";
        } else {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cMust be a player to do this!"));
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        String server;
        if (player.getServer() == null || player.getServer().getInfo() == null) {
            server = "Bungee";
        } else {
            server = player.getServer().getInfo().getName();
        }
        ChatLogManager.get().logMessage(ChatLogManager.ChatType.STAFF_HELP, server, player.getUniqueId(), null, message);

        BungeeMessages.broadcastTo(Rank.MOD_JR, format
                .replace("%message", message)
                .replace("%server", ((ProxiedPlayer) commandSender).getServer().getInfo().getName()));

        DiscordBot.get().sendMessage(DiscordBot.CHANNEL_INGAME_STAFF_CHAT, color, "[Staff Help - " + ((ProxiedPlayer) commandSender).getServer().getInfo().getName() + " - " + commandSender.getName() + " - %time]", message);
    }
}
