/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 6:42 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.pms.PluginMessageChannel;
import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.packet.UpdatePunishmentPacket;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.*;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;
import java.util.UUID;

public class PunishCommand extends BLCommand {

    public PunishCommand() {
        super("punish", "p");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD_JR)) return;

        if (strings.length != 2) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /p <player> <reason>"));
            return;
        }

        if (Arrays.stream(PunishReason.values())
                .filter(PunishReason::isInPunishCommand)
                .map(PunishReason::name)
                .noneMatch(n -> n.equalsIgnoreCase(strings[1]))) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /p <player> <reason>\nCannot find the punish reason " + strings[1] + "!"));
            return;
        }

        String senderName = commandSender.getName();
        UUID senderUuid;
        if (commandSender instanceof ProxiedPlayer) {
            senderUuid = ((ProxiedPlayer) commandSender).getUniqueId();
        } else {
            senderUuid = PlayerManager_legacy.CONSOLE_UUID;
        }

        PunishReason punishReason = PunishReason.valueOf(strings[1].toUpperCase());

        Rank senderRank = (commandSender instanceof ProxiedPlayer) ? PlayerManager_legacy.get().getPlayer(((ProxiedPlayer) commandSender).getUniqueId()).getRank() : Rank.FOUNDER;

        PlayerManager_legacy.get().getOrLoadPlayer(strings[0], new BLPlayerCallback() {
            @Override
            public void success(BLPlayer_legacy player) {

                if (player.getRank().isAtLeast(senderRank)) {
                    commandSender.sendMessage(TextComponent.fromLegacyText("§cYou can only punish players with lower rank than you!"));
                    return;
                }

                PunishmentBuilder.getBuilderFor(player).setPunishedBy(senderUuid).setReason(punishReason).execute(new PunishCallback() {
                    @Override
                    public boolean beforePunish(BLPlayer_legacy player, Punishment result) {
                        // old type is less important than the new one, allow

                        if (result.getType() == PunishType.KICK) return true;

                        if (PunishHelper.get().getActivePunishType(player).getPriority() < result.getType().getPriority()) {
                            commandSender.sendMessage(TextComponent.fromLegacyText("A"));
                            return true;
                        }

                        // if same type, prioritise the permanent or biggest time
                        if (PunishHelper.get().getActivePunishType(player).getPriority() == result.getType().getPriority()) {
                            if ((player.getPunishDuration() <= result.getDuration()) || result.getType().isPermanent()) {
                                return true;
                            } else {
                                commandSender.sendMessage(TextComponent.fromLegacyText("§cThe player has a bigger punishment than what you tried to give him."));
                                return false;
                            }
                        } else { // else just deny
                            commandSender.sendMessage(TextComponent.fromLegacyText("§cThe player has a bigger punishment than what you tried to give him."));
                            return false;
                        }
                    }

                    @Override
                    public void onPunish(BLPlayer_legacy player, Punishment punishResult) {
                        if (punishResult.getType() != PunishType.NO_PUNISH && punishResult.getType() != PunishType.KICK)
                            PluginMessageChannel.sendPluginMessage(new UpdatePunishmentPacket(punishResult));
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPlayer §f" + player.getName() + " §cwas punished by §f" + senderName);
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunishReason: §f" + punishResult.getReason().name() + " §f" + PunishHelper.get().getStringNumber(punishResult.getNumber()));
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunishType: §f" + punishResult.getType().name());
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunish ID: §f" + punishResult.getPunishId());
                        if (punishResult.getDuration() > 12_000) { // 12 seconds
                            BungeeMessages.broadcastTo(Rank.MOD_JR, "§cDuration: §f" + PunishHelper.get().formatTime(punishResult.getDuration()));
                        }

                        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(player.getUuid());
                        if (proxiedPlayer != null) {
                            proxiedPlayer.disconnect(TextComponent.fromLegacyText(punishResult.getType().getDesciption()
                                    .replace("%reason", punishResult.getReason().getMessage())
                                    .replace("%startTime", PunishHelper.get().formatDate(punishResult.getStart()))
                                    .replace("%endTime", PunishHelper.get().formatTime(punishResult.getDuration()))
                                    .replace("%number", PunishHelper.get().getStringNumber(punishResult.getNumber()))));
                        }

                        DiscordBot.get().sendMessage(
                                DiscordBot.CHANNEL_INGAME_PUNISH,
                                player.getRank().getDiscordColor(),
                                "[Minecraft Punish - %time]]",
                                "Player: " + player.getName() + "\n" +
                                        "Reason: " + punishReason + "\n" +
                                        "Number: " + PunishHelper.get().getStringNumber(punishResult.getNumber()) + "\n" +
                                        "By: " + senderName);
                    }
                });
            }

            @Override
            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not find players name in cache!"));
            }
        });
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfName = strings[0].toLowerCase().trim();
//            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//            Bungee.log.info(names);
//            return names;
//        } else if (strings.length == 2) {
//            String startOfPunish = strings[1].toLowerCase().trim();
//            List<String> ret = Arrays.stream(PunishReason.values())
//                    .filter(PunishReason::isInPunishCommand)
//                    .map(PunishReason::name)
//                    .filter(n -> n.toLowerCase().startsWith(startOfPunish))
//                    .collect(Collectors.toList());
//            Bungee.log.info(ret);
//            return ret;
//        } else {
//            return Collections.emptyList();
//        }
//    }
}
