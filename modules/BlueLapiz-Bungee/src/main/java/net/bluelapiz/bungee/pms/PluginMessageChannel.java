/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 6:41 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.pms;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.bluelapiz.bungee.Bungee;
import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.logging.ChatLogManager;
import net.bluelapiz.common.packet.*;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PluginMessageChannel implements Listener {

    public static boolean sendPluginMessage(ServerInfo server, Packet packet, boolean force) {
        return sendPluginMessage(server, PacketHelper.BLUELAPIZ_MESSAGES_CHANNEL, packet, force);
    }

    public static boolean sendPluginMessage(ServerInfo server, Packet packet) {
        return sendPluginMessage(server, PacketHelper.BLUELAPIZ_MESSAGES_CHANNEL, packet);
    }

    public static boolean sendPluginMessage(ServerInfo server, String channel, Packet packet) {
        return sendPluginMessage(server, channel, packet, false);
    }

    public static void sendPluginMessage(Packet packet) {
        for (ServerInfo serverInfo : ProxyServer.getInstance().getServers().values()) {
            PluginMessageChannel.sendPluginMessage(serverInfo, packet);
        }
    }

    public static void sendPluginMessage(Packet packet, boolean force) {
        for (ServerInfo serverInfo : ProxyServer.getInstance().getServers().values()) {
            PluginMessageChannel.sendPluginMessage(serverInfo, packet, force);
        }
    }

    public static boolean sendPluginMessage(ServerInfo server, String channel, Packet packet, boolean force) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(PacketHelper.PLUGIN_SUBCHANNEL);
        out.writeUTF(PacketHelper.getPacket(packet));
        return server.sendData(channel, out.toByteArray(), force);
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        if (!e.getTag().equalsIgnoreCase(PacketHelper.BUNGEE_MESSAGES_CHANNEL)) return;

        ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());
        String subchannel = in.readUTF();
        if (!subchannel.equals(PacketHelper.PLUGIN_SUBCHANNEL)) return;

        Packet packet = PacketHelper.getPacket(in.readUTF());

        e.setCancelled(true);

        if (packet instanceof PlayerRankChangePacket) {
            // todo
        } else if (packet instanceof BroadcastMessagePacket) {
            BroadcastMessagePacket bmp = (BroadcastMessagePacket) packet;
            BungeeMessages.broadcastTo(bmp.getMinimumRank(), bmp.getMessage());
        } else if (packet instanceof RequestGlobalChatPacket) {
            if (!(e.getReceiver() instanceof ProxiedPlayer)) return;

            RequestGlobalChatPacket rgcp = (RequestGlobalChatPacket) packet;

            String message = rgcp.getOriginalMessage();
            ProxiedPlayer player = (ProxiedPlayer) e.getReceiver();

            BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(rgcp.getPlayerUuid());

            Rank rank = blp.getRank();

            String format = "§7[%server] %prefix%player %suffix %message";
            format = ChatColor.translateAlternateColorCodes('&', format);
            format = ChatColor.translateAlternateColorCodes('&', format);
            format = format.replace("%prefix", rank.getPrefix());
            format = format.replace("%player", rgcp.getPlayerName());
            format = format.replace("%suffix", rank.getSuffix());
            format = format.replace("%color", "&" + rank.getColor());
            format = format.replace("%rank", "&" + rank.getName());
            format = format.replace("%message", message);
            format = format.replace("%server", player.getServer().getInfo().getName());

            String server;
            if (player.getServer() == null || player.getServer().getInfo() == null) {
                server = "Bungee";
            } else {
                server = player.getServer().getInfo().getName();
            }

            ChatLogManager.get().logMessage(ChatLogManager.ChatType.PUBLIC_CHAT, server, player.getUniqueId(), null, message);

            if (rank.isAtLeast(Rank.MOD))
                format = ChatColor.translateAlternateColorCodes('&', format);

            PluginMessageChannel.sendPluginMessage(
                    new GlobalChatPacket(format, rgcp.getOriginalMessage(), rgcp.getPlayerUuid(), player.getServer().getInfo().getName()));
        } else if (packet instanceof SendDiscordMessagePacket) {
            SendDiscordMessagePacket sdmp = (SendDiscordMessagePacket) packet;
            DiscordBot.get().sendMessage(sdmp.getChannel(), sdmp.getColor(), sdmp.getTitle(), sdmp.getMessage());
        } else if (packet instanceof RequestServerNamePacket) {
            RequestServerNamePacket rsnp = (RequestServerNamePacket) packet;
            if (e.getReceiver() instanceof ProxiedPlayer) {
                ServerInfo si = ((ProxiedPlayer) e.getReceiver()).getServer().getInfo();
                sendPluginMessage(si, new ServerNamePacket(si.getName()));
            }
        } else if (packet instanceof RequestSendPlayerPacket) {
            RequestSendPlayerPacket rspp = (RequestSendPlayerPacket) packet;
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(rspp.getPlayer());
            ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(rspp.getServer());

            if (proxiedPlayer == null) {
                Bungee.log.severe("Could not find player " + rspp.getPlayer(), new Exception());
                return;
            }
            if (serverInfo == null) {
                Bungee.log.severe("Could not find server " + rspp.getServer(), new Exception());
                return;
            }

            proxiedPlayer.connect(serverInfo);
        } else if (packet instanceof RequestEnableGlobalChatOacket) {
            PluginMessageChannel.sendPluginMessage(new SetGlobalChatEnabledPacket(Bungee.ENABLE_GLOBAL_CHAT), true);
        } else if (packet instanceof SendDiscordExceptionPacket) {
            SendDiscordExceptionPacket sdep = (SendDiscordExceptionPacket) packet;
            DiscordBot.get().sendError(sdep.getServer(), sdep.getPlugin(), sdep.getDescription(), sdep.getEx(), sdep.getCurrentTime());
        }
    }
}
