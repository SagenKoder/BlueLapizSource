/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class SendCommand extends BLCommand {

    public SendCommand() {
        super("send");
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfAction = strings[0].toLowerCase().trim();
//            return ProxyServer.get().getPlayers().stream()
//                    .map(ProxiedPlayer::getName)
//                    .filter(n -> n.toLowerCase().startsWith(startOfAction))
//                    .collect(Collectors.toSet());
//        }
//        if(strings.length == 2) {
//            String startOfAction = strings[1].toLowerCase().trim();
//            return ProxyServer.get().getServers().keySet().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
//                    .collect(Collectors.toList());
//        }
//        return Collections.emptyList();
//    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.ADMIN)) return;
        if (strings.length != 2) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! Use /send <player> <server>"));
            return;
        }

        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(strings[0]);
        if (player == null) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cCannot find player " + strings[0] + "!"));
            return;
        }

        ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(strings[1]);
        if (serverInfo == null) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cCannot find server " + strings[1] + "!"));
            return;
        }

        player.connect(serverInfo);
        player.sendMessage(TextComponent.fromLegacyText("§f" + commandSender.getName() + " §aforced you to join the server §f" + serverInfo.getName() + "!"));
        commandSender.sendMessage(TextComponent.fromLegacyText("§aYou forced §f" + player.getName() + " §ato join the server §f" + serverInfo.getName() + "!"));
    }
}
