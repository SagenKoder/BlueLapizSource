/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 6:42 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ServerCommand extends BLCommand {

    public ServerCommand() {
        super("server");
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfAction = strings[0].toLowerCase().trim();
//            Bungee.log.info(ProxyServer.get().getServers());
//            Bungee.log.info(ProxyServer.get().getServers().keySet());
//            Bungee.log.info(startOfAction);
//            return ProxyServer.get().getServers().keySet().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
//                    .collect(Collectors.toList());
//        }
//        return Collections.emptyList();
//    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD)) return;
        if (strings.length != 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! Use /server <server>"));
            return;
        }

        if ((!(commandSender instanceof ProxiedPlayer))) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cOnly players can do this!"));
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo(strings[0]);
        if (serverInfo == null) {
            player.sendMessage(TextComponent.fromLegacyText("§cCannot find server " + strings[0] + "!"));
            return;
        }

        player.connect(serverInfo);
        player.sendMessage(TextComponent.fromLegacyText("§aYou connected to " + serverInfo.getName() + "!"));
    }
}
