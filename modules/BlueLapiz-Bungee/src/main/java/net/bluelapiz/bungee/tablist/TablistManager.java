/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.tablist;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.protocol.packet.PlayerListItem;

import java.util.HashMap;
import java.util.UUID;

public class TablistManager {

    private HashMap<Integer, UUID> tabSlot = new HashMap<>();

    protected TablistManager() {
        for (int i = 1; i < 81; i++) {
            tabSlot.put(i, UUID.randomUUID());
        }
    }

    protected void clearTab(ProxiedPlayer player) {
        PlayerListItem packet = new PlayerListItem();
        packet.setAction(PlayerListItem.Action.REMOVE_PLAYER);
        PlayerListItem.Item[] items = new PlayerListItem.Item[tabSlot.size()];

        Integer i = 0;
        for (Integer slotID : tabSlot.keySet()) {
            UUID uuid = tabSlot.get(slotID);
            PlayerListItem.Item item = items[i++] = new PlayerListItem.Item();
            item.setUuid(uuid);
        }

        packet.setItems(items);
        player.unsafe().sendPacket(packet);
    }

    protected void setBlankTab(ProxiedPlayer player) {
        // Clear the old tab from the player.
        clearTab(player);
        PlayerListItem packet = new PlayerListItem();
        packet.setAction(PlayerListItem.Action.ADD_PLAYER);
        PlayerListItem.Item[] items = new PlayerListItem.Item[tabSlot.size()];

        Integer i = 0;
        for (Integer slotID : tabSlot.keySet()) {
            UUID uuid = tabSlot.get(slotID);
            PlayerListItem.Item item = items[i++] = new PlayerListItem.Item();

            item.setUuid(uuid);
            item.setUsername(((i < 10) ? "#0" + i : "#" + i));
            item.setDisplayName(legacyTextToJson("", '§'));
            item.setPing(5000);

            String[][] props = new String[1][];
            props[0] = new String[]{
                    "textures",
                    "eyJ0aW1lc3RhbXAiOjE1MzA2NDE0NDM5MzgsInByb2ZpbGVJZCI6IjcyZDVlYTc1MzRmYzRhZmRiNGVkYmYxYTNjMzcyNDFmIiwicHJvZmlsZU5hbWUiOiJDZWNlbGxpYSIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGUyMzVkN2U3MzRlNjIyMjNhMzljYWRhYjU3MmZlMjNmNjZjZDA3ZTEyNjA0NzRjMTQ0OTc4YjQxY2ZlOGFiYSIsIm1ldGFkYXRhIjp7Im1vZGVsIjoic2xpbSJ9fX19",
                    "I/3aZ54wqb6YV+DgUxHsOglbBNRfzA4UJTPU4mOoSwBegEZ9s2PcFkWKofxtFavJrvjz5brb8y4CUgM2bDdyqoz1lvzlZJPW8VpGIcYBOp+GoF3NF4gShs6saxDEUDce9Hq82qtWpDY5ZaQef132khIOOeQbNJ2r+xiiwQgnxc5NKLUgwm/HELtUwys2mwRGN9bpux2qe3cOeqefqaMH4Fqo2ptHkv50oiLo8VyBMb2DW0QaGpktJk6WLvSTdICLKZCbZ48siZEMe1DlSRd3RktIFj3Mcb6BMtuqducy8Ddm2SFDt62dLzKPZFJ8MK7BLahpWwHL6Q9VvBclYCztrY8EqudK+jzTYRJ7Oa4l58GLU7zfvcB7+QGE5Lo5MLZJC6O/ilOXbEZrA4aUp28MEKLqfXUWxICeD69DY+CVA/rD7O0LnK682ES9EjnTDDORH4A+iH/EWs8DjaEyMVVvD5PlSAN9TL3GQj3eWAbogwXNHH3zvka8CXJSljxvYWFt2Y2soGatAxemPkHP9kHkAnlrJBv03YzJ8Aei1geSze0xs96GAMy6v5EBtE41zMRjowzidWZSxNqZEMwIbreTOOkbO1VzIDZ+Lg3Qa9Cw4+AgbDq7/ESU16t/rwLEdBFa2879+A2Cm2g9VLZtLCBMukNzQS7XjL+8fYMOh09ySWE="
            };
            item.setProperties(props);
        }

        packet.setItems(items);
        player.unsafe().sendPacket(packet);
    }

    protected void setTabColumn(ProxiedPlayer player, Integer column, HashMap<Integer, String> slots) {
        HashMap<UUID, String> slotUUIDsToChange = new HashMap<>();

        for (int i = 2; i <= 20; i++) {
            int slotId = getSlotID(column, i);
            if (!tabSlot.containsKey(slotId)) continue;

            if (slots.containsKey(i))
                slotUUIDsToChange.put(tabSlot.get(slotId), slots.get(i));
            else
                slotUUIDsToChange.put(tabSlot.get(slotId), "");
        }

        PlayerListItem packet = new PlayerListItem();
        packet.setAction(PlayerListItem.Action.UPDATE_DISPLAY_NAME);
        PlayerListItem.Item[] items = new PlayerListItem.Item[slotUUIDsToChange.size()];

        Integer i = 0;
        for (UUID uuid : slotUUIDsToChange.keySet()) {
            PlayerListItem.Item item = items[i++] = new PlayerListItem.Item();
            item.setUuid(uuid);
            item.setDisplayName(legacyTextToJson(slotUUIDsToChange.get(uuid), '§'));
        }
        packet.setItems(items);
        player.unsafe().sendPacket(packet);
    }

    protected void setTabSlot(ProxiedPlayer player, Integer col, Integer row, String text) {
        PlayerListItem packet = new PlayerListItem();
        packet.setAction(PlayerListItem.Action.UPDATE_DISPLAY_NAME);
        PlayerListItem.Item[] items = new PlayerListItem.Item[1];
        PlayerListItem.Item item = items[0] = new PlayerListItem.Item();
        if (tabSlot.containsKey(getSlotID(col, row))) {
            item.setUuid(tabSlot.get(getSlotID(col, row)));
            item.setDisplayName(legacyTextToJson(text, '§'));
        }
        packet.setItems(items);
        player.unsafe().sendPacket(packet);
    }

    protected void setTabPingColumn(ProxiedPlayer player, Integer column, HashMap<Integer, Integer> slots) {
        HashMap<UUID, Integer> slotUUIDsToChange = new HashMap<>();

        for (int i = 2; i <= 20; i++) {
            int slotId = getSlotID(column, i);
            if (!tabSlot.containsKey(slotId)) continue;

            if (slots.containsKey(i))
                slotUUIDsToChange.put(tabSlot.get(slotId), slots.get(i));
            else
                slotUUIDsToChange.put(tabSlot.get(slotId), 5000);
        }

        PlayerListItem packet = new PlayerListItem();
        packet.setAction(PlayerListItem.Action.UPDATE_LATENCY);
        PlayerListItem.Item[] items = new PlayerListItem.Item[slotUUIDsToChange.size()];

        Integer i = 0;
        for (UUID uuid : slotUUIDsToChange.keySet()) {
            PlayerListItem.Item item = items[i++] = new PlayerListItem.Item();
            item.setUuid(uuid);
            item.setPing(slotUUIDsToChange.get(uuid));
        }
        packet.setItems(items);
        player.unsafe().sendPacket(packet);
    }

    protected void setTabGamemodeColumn(ProxiedPlayer player, Integer column, HashMap<Integer, GameMode> slots) {
        HashMap<UUID, Integer> slotUUIDsToChange = new HashMap<>();

        for (int i = 2; i <= 20; i++) {
            int slotId = getSlotID(column, i);
            if (!tabSlot.containsKey(slotId)) continue;

            if (slots.containsKey(i))
                slotUUIDsToChange.put(tabSlot.get(slotId), slots.get(i).getId());
            else
                slotUUIDsToChange.put(tabSlot.get(slotId), GameMode.CREATIVE.getId());
        }

        PlayerListItem packet = new PlayerListItem();
        packet.setAction(PlayerListItem.Action.UPDATE_GAMEMODE);
        PlayerListItem.Item[] items = new PlayerListItem.Item[slotUUIDsToChange.size()];

        Integer i = 0;
        for (UUID uuid : slotUUIDsToChange.keySet()) {
            PlayerListItem.Item item = items[i++] = new PlayerListItem.Item();
            item.setUuid(uuid);
            item.setGamemode(slotUUIDsToChange.get(uuid));
        }
        packet.setItems(items);
        player.unsafe().sendPacket(packet);
    }

    public Integer getSlotID(int row, int slot) {
        if (row == 1) {
            if (slot > 0 && slot < 21) {
                return slot;
            } else {
                return 1;
            }
        } else {
            if (slot > 0 && slot < 21) {
                return (20 * (row - 1)) + slot;
            } else {
                return (20 * (row - 1)) + 1;
            }
        }
    }

    private String legacyTextToJson(String legacyText, char alternateColorChar) {
        String emptyJsonText = "{\"text\":\"\"}";
        if (legacyText == null) {
            return null;
        }
        if (legacyText.isEmpty()) {
            return emptyJsonText;
        }
        StringBuilder builder = new StringBuilder(legacyText.length() + 10);
        builder.append("{\"text\":\"");
        for (int i = 0; i < legacyText.length(); ++i) {
            char c = legacyText.charAt(i);
            if (i + 1 < legacyText.length() && (c == ChatColor.COLOR_CHAR || (c == alternateColorChar && "0123456789AaBbCcDdEeFfKkLlMmNnOoRr".indexOf(legacyText.charAt(i + 1)) > -1))) {
                builder.append(ChatColor.COLOR_CHAR);
                c = legacyText.charAt(++i);
            }
            if (c == '"') {
                builder.append("\\\"");
            } else if (c == '\\') {
                builder.append("\\\\");
            } else {
                builder.append(c);
            }
        }
        builder.append("\"}");
        return new String(builder);
    }

    public enum GameMode {
        CREATIVE(1), SPECTATOR(3);

        private int id;

        GameMode(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

}
