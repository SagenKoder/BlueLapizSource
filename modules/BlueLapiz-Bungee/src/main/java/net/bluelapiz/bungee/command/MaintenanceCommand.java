/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.Bungee;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MaintenanceCommand extends BLCommand {

    public MaintenanceCommand() {
        super("maintenance");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.ADMIN)) return;

        Bungee.MAINTENANCE_MODE = !Bungee.MAINTENANCE_MODE;

        if (Bungee.MAINTENANCE_MODE) {
            for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
                BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
                if (!blp.getRank().isAtLeast(Rank.BUILDER)) {
                    p.disconnect(TextComponent.fromLegacyText("§cThe server was put in Maintenance Mode by " + commandSender.getName() + "!"));
                }
            }
            ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText("§aThe server was put in Maintenance Mode by " + commandSender.getName() + "!"));
        } else {
            ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText("§aThe server is not longer in Maintenance Mode!"));
        }
    }
}
