/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.BLPlayer;
import net.bluelapiz.common.player.PlayerManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public abstract class BLCommand extends Command /*implements TabExecutor */ {

    public BLCommand(String name) {
        super(name, null, "BlueLapiz:" + name);
    }

    public BLCommand(String name, String... aliases) {
        super(name, null, addStringToArray(aliases, "BlueLapiz:" + name));
    }

    private static String[] addStringToArray(String[] strings, String toAdd) {
        ArrayList<String> stringList = new ArrayList<>(Arrays.asList(strings));
        stringList.add(toAdd);
        return stringList.toArray(new String[0]);
    }

    public boolean isAuthorized(UUID uuid, Rank rank) {
        try {
            Optional<BLPlayer> optPlayer = PlayerManager.get().fetchPlayer(uuid).get(100, TimeUnit.MILLISECONDS);
            return optPlayer.map(blPlayer -> blPlayer.getRank().isAtLeast(rank)).orElse(false);
        } catch (Exception e) {
            DiscordBot.get().sendMessage(
                    DiscordBot.CHANNEL_INGAME_ERROR,
                    Color.RED,
                    "Exception[server=Bungeecord,plugin=BlueLapiz-Bungee]",
                    "Could not fetch player_data from " + uuid + ". Stacktrace in bungee log.");
            e.printStackTrace();
        }

        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(uuid);
        return blp.getRank().isAtLeast(rank);
    }

    public boolean isAuthorized(CommandSender sender, Rank rank) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            return isAuthorized(p.getUniqueId(), rank);
        }
        return true;
    }

    public boolean isAuthorized(ProxiedPlayer player, Rank rank) {
        return isAuthorized(player.getUniqueId(), rank);
    }

    public boolean isAuthorizedOrError(CommandSender sender, Rank rank) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            return isAuthorizedOrError(p, rank);
        }
        return true;
    }

    public boolean isAuthorizedOrError(ProxiedPlayer player, Rank rank) {
        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(player.getUniqueId());

        if (blp.getRank().isAtLeast(rank)) {
            return true;
        } else {
            player.sendMessage(TextComponent.fromLegacyText("§cYou need to be at least §e%needrank §cto do this!"
                    .replace("%yourrank", rank.getName().toLowerCase())
                    .replace("%needrank", rank.getName().toLowerCase())));
            return false;
        }
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        return ProxyServer.get().getPlayers().stream()
//                .map(ProxiedPlayer::getName)
//                .filter(s -> s.toLowerCase().startsWith(strings[strings.length-1].toLowerCase()))
//                .collect(Collectors.toList());
//    }
}
