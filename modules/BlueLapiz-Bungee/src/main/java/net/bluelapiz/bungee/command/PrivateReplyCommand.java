/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.util.Touple;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;

public class PrivateReplyCommand extends BLCommand {

    public static final HashMap<String, Touple<String, Long>> lastMessages = new HashMap<>();

    public PrivateReplyCommand() {
        super("reply", "r");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cOnly players can do this!"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        if (strings.length < 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /r <message>"));
            return;
        }

        if (!lastMessages.containsKey(player.getName()) || lastMessages.get(player.getName()).getRight() + 300_000 < System.currentTimeMillis()) {
            player.sendMessage(TextComponent.fromLegacyText("§cYou have not conversed with anyone in the last 5 minutes!"));
            return;
        }

        ProxiedPlayer other = ProxyServer.getInstance().getPlayer(lastMessages.get(player.getName()).getLeft());
        if (other == null) {
            player.sendMessage(TextComponent.fromLegacyText("§cThe last player you spoke with is offline!"));
            return;
        }

        ProxyServer.getInstance().getPluginManager().dispatchCommand(player, "m " + other.getName() + " " + String.join(" ", strings));
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        // return default tabcomplete for player names on server
//        return super.onTabComplete(commandSender, strings);
//    }
}
