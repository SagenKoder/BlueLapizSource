/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.Bungee;
import net.bluelapiz.bungee.pms.PluginMessageChannel;
import net.bluelapiz.common.packet.SetGlobalChatEnabledPacket;
import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

public class EnableGlobalChatCommand extends BLCommand {

    public EnableGlobalChatCommand() {
        super("enableglobalchat", "globalchat");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.FOUNDER)) return;

        Bungee.ENABLE_GLOBAL_CHAT = !Bungee.ENABLE_GLOBAL_CHAT;
        if (Bungee.ENABLE_GLOBAL_CHAT) {
            ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText("§6" + commandSender.getName() + " enabled Global Chat for all servers!"));
        } else {
            ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText("§6" + commandSender.getName() + " disabled Global Chat for all servers!"));
        }
        PluginMessageChannel.sendPluginMessage(new SetGlobalChatEnabledPacket(Bungee.ENABLE_GLOBAL_CHAT), true);
    }
}
