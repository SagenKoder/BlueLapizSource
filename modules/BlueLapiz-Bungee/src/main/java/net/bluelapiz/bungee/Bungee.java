/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 12:42 AM                                              *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee;

import net.bluelapiz.bungee.command.*;
import net.bluelapiz.bungee.listener.PlayerListener;
import net.bluelapiz.bungee.pms.PluginMessageChannel;
import net.bluelapiz.bungee.tablist.BLTablistManager;
import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.maven.MavenLoader;
import net.bluelapiz.common.packet.PacketHelper;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.sql.SQLConfig;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.util.BLExecutor;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Bungee extends Plugin {

    public static boolean ENABLE_GLOBAL_CHAT = true;
    public static boolean MAINTENANCE_MODE = false;

    public static BLLogger log;

    private static Bungee instance;

    public Bungee() {
        instance = this;
    }

    public static Bungee getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        Instant start = Instant.now();

        Bungee.log = new BLLoggerBungeeImpl(getDescription().getName());
        BLLogger.commonLogger = new BLLoggerBungeeImpl("BlueLapiz-Common");

        Bungee.log.debug("Loading external dependencies for Common....");
        MavenLoader.setup(getDataFolder());
        MavenLoader.get().loadCommonLibriaries();

        Bungee.log.debug("Loading external dependencies for Bungee....");
        MavenLoader.get().load("2.10.1", "com.discord4j", "Discord4J");
        MavenLoader.get().load("1.2.3", "ch.qos.logback", "logback-classic");
        MavenLoader.get().load("1.2.3", "ch.qos.logback", "logback-core");

        Bungee.log.debug("Setting up configuration");
        Configuration configuration;
        try {
            File configFile = new File(getDataFolder(), "config.yml");
            if (!configFile.exists()) {
                configFile.getParentFile().mkdirs();
                configFile.createNewFile();
            }
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            if (!configuration.contains("MySQL.database")) {
                configuration.set("MySQL.database", "minecraft");
            }
            if (!configuration.contains("MySQL.host")) {
                configuration.set("MySQL.host", "localhost:3306");
            }
            if (!configuration.contains("MySQL.user")) {
                configuration.set("MySQL.user", "minecraft");
            }
            if (!configuration.contains("MySQL.pass")) {
                configuration.set("MySQL.pass", "password");
            }
            if (!configuration.contains("Discord.name")) {
                configuration.set("Discord.name", "BlueLapiz<3");
            }
            if (!configuration.contains("Discord.image")) {
                configuration.set("Discord.image", "http://bluelapiz.net/BlueLapiz-logo.png");
            }
            if (!configuration.contains("Discord.token")) {
                configuration.set("Discord.token", "token");
            }
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, configFile);
        } catch (Exception e) {
            e.printStackTrace();
            ProxyServer.getInstance().stop("Could not load configuration files!");
            return;
        }

        log.debug("Setting up DiscordBot settings");
        DiscordBot.setup(
                configuration.getString("Discord.name"),
                configuration.getString("Discord.token"),
                configuration.getString("Discord.image"));

        log.debug("Setting up MySQL");
        SQLManager.setup(SQLConfig.builder()
                .database(configuration.getString("MySQL.database"))
                .host(configuration.getString("MySQL.host"))
                .user(configuration.getString("MySQL.user"))
                .pass(configuration.getString("MySQL.pass")).build());

        log.info("***************** LOADED BlueLapiz-Bungee *******************");
        Instant end = Instant.now();
        Bungee.log.debug("Used " + Duration.between(start, end).toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase());
    }

    @Override
    public void onEnable() {
        Instant start = Instant.now();

        //new Thread(() -> {
        //    Bungee.log.debug("******************* STARTING SPRING BOOT APPLICATION *********************");
        //    SpringSecurityH2DemoApplication.startSpringBoot();
        //}).start();

        Bungee.log.debug("Registering Listeners");
        getProxy().getPluginManager().registerListener(this, new PlayerListener());
        getProxy().getPluginManager().registerListener(this, new PluginMessageChannel());

        Bungee.log.debug("Registering Commands");
        getProxy().getPluginManager().registerCommand(this, new StaffChatCommand());
        getProxy().getPluginManager().registerCommand(this, new StaffHelpCommand());
        getProxy().getPluginManager().registerCommand(this, new SetRankCommand());
        getProxy().getPluginManager().registerCommand(this, new BroadcastCommand());
        getProxy().getPluginManager().registerCommand(this, new EnableGlobalChatCommand());
        getProxy().getPluginManager().registerCommand(this, new PrivateMessageCommand());
        getProxy().getPluginManager().registerCommand(this, new PrivateReplyCommand());
        getProxy().getPluginManager().registerCommand(this, new MaintenanceCommand());
        getProxy().getPluginManager().registerCommand(this, new VanishCommand());
        getProxy().getPluginManager().registerCommand(this, new PunishCommand());
        getProxy().getPluginManager().registerCommand(this, new UnpunishCommand());
        getProxy().getPluginManager().registerCommand(this, new DelHistoryCommand());
        getProxy().getPluginManager().registerCommand(this, new HistoryCommand());
        getProxy().getPluginManager().registerCommand(this, new KickCommand());
        getProxy().getPluginManager().registerCommand(this, new EvidenceCommand());
        getProxy().getPluginManager().registerCommand(this, new FriendCommand());
        getProxy().getPluginManager().registerCommand(this, new HubCommand());
        getProxy().getPluginManager().registerCommand(this, new SendCommand());
        getProxy().getPluginManager().registerCommand(this, new ServerCommand());
        getProxy().getPluginManager().registerCommand(this, new StafflistCommand());
        getProxy().getPluginManager().registerCommand(this, new InfoCommand());
        getProxy().getPluginManager().registerCommand(this, new RawCommand());

        Bungee.log.debug("Registering PluginChannels");
        getProxy().registerChannel(PacketHelper.BLUELAPIZ_MESSAGES_CHANNEL);

        try {
            Bungee.log.debug("Setting up DiscordBot");
            DiscordBot.get().registerListener(event -> {
                if (event.getChannel().getLongID() == DiscordBot.CHANNEL_INGAME_STAFF_CHAT) {
                    String format = "§6§l[!] §7[discord] §f" + event.getAuthor().getName() + " §7> §a" + event.getMessage();
                    BungeeMessages.broadcastTo(Rank.MOD, format);
                } else if (event.getChannel().getLongID() == DiscordBot.CHANNEL_INGAME_BROADCAST) {
                    String format = "§3§lBlueLapiz Info > §f" + ChatColor.translateAlternateColorCodes('&', event.getMessage().getContent());
                    ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText(format));
                }
            });
        } catch (Exception e) {}

        Bungee.log.debug("Starting schedulers");
        ProxyServer.getInstance().getScheduler().schedule(this, () ->
                        ProxyServer.getInstance().getScheduler().runAsync(Bungee.this, () -> BLTablistManager.get().updateTablist()),
                2, 1, TimeUnit.SECONDS);

        ProxyServer.getInstance().getScheduler().schedule(this, () ->
                        PlayerManager_legacy.get().saveAndUnloadPlayers(ProxyServer.getInstance().getPlayers().stream()
                                .map(ProxiedPlayer::getUniqueId)
                                .collect(Collectors.toList())),
                2, 2, TimeUnit.SECONDS);

        Bungee.log.info("***************** STARTED BlueLapiz-Bungee *******************");
        Instant end = Instant.now();
        Bungee.log.debug("Used " + Duration.between(start, end).toString()
                .substring(2)
                .replaceAll("(\\d[HMS])(?!$)", "$1 ")
                .toLowerCase());
    }

    @Override
    public void onDisable() {
        Bungee.log.debug("Shutting down the DiscordBot");
        DiscordBot.get().disconnect();

        Bungee.log.debug("Stopping all tasks");
        BLExecutor.get().stopAll();

        Bungee.log.debug("Shutting down MySQL");
        SQLManager.getMySQLMan().closeConnection();
    }
}

