/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.utils;

import net.bluelapiz.common.player.FriendManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class BungeeMessages {

    public static void broadcastTo(Rank rank, String message) {
        broadcastTo(rank, TextComponent.fromLegacyText(message));
    }

    public static void broadcastTo(Rank rank, BaseComponent[] message) {
        for (ProxiedPlayer o : ProxyServer.getInstance().getPlayers()) {
            try {
                BLPlayer_legacy blpOnline = PlayerManager_legacy.get().getPlayer(o.getUniqueId());
                if (blpOnline.getRank().isAtLeast(rank)) {
                    o.sendMessage(message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void broadcastJoin(BLPlayer_legacy player) {

        boolean vanish = player.isVanished();
        Rank rank = player.getRank();

        for (UUID uuid : FriendManager.get().getAllFriendsOf(player.getUuid())) {

            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(uuid);
            if (proxiedPlayer == null) continue;

            if (proxiedPlayer.getUniqueId().equals(player.getUuid())) continue;

            BLPlayer_legacy blOther = PlayerManager_legacy.get().getPlayer(proxiedPlayer.getUniqueId());
            proxiedPlayer.sendMessage(TextComponent.fromLegacyText("§e§lFriend > §7Your friend " + rank.getColor() + player.getName() + " §7is now §aonline§7!"));
        }
    }

    public static void broadcastQuit(BLPlayer_legacy player) {

        boolean vanish = player.isVanished();
        Rank rank = player.getRank();

        for (UUID uuid : FriendManager.get().getAllFriendsOf(player.getUuid())) {

            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(uuid);
            if (proxiedPlayer == null) continue;

            if (proxiedPlayer.getUniqueId().equals(player.getUuid())) continue;

            BLPlayer_legacy blOther = PlayerManager_legacy.get().getPlayer(proxiedPlayer.getUniqueId());

            if (!vanish || blOther.getRank().isAtLeast(Rank.MOD_JR)) {
                proxiedPlayer.sendMessage(TextComponent.fromLegacyText("§e§lFriend > §7Your friend " + rank.getColor() + player.getName() + " §7is now §coffline§7!"));
            }
        }
    }

}
