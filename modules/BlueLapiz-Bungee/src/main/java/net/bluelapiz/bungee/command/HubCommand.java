/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class HubCommand extends BLCommand {

    public HubCommand() {
        super("hub", "lobby");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cOnly players can do this!"));
            return;
        }

        ProxiedPlayer p = (ProxiedPlayer) commandSender;

        ServerInfo serverInfo = ProxyServer.getInstance().getServerInfo("hub");
        if (serverInfo == null) {
            p.sendMessage(TextComponent.fromLegacyText("§cCannot locate the hub server!"));
            return;
        }

        p.connect(serverInfo);
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        return Collections.emptyList();
//    }
}
