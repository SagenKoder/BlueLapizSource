/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.pms.PluginMessageChannel;
import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.packet.UpdatePunishmentPacket;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.PunishHelper;
import net.bluelapiz.common.punish.PunishReason;
import net.bluelapiz.common.punish.PunishType;
import net.bluelapiz.common.punish.Punishment;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

public class UnpunishCommand extends BLCommand {

    public UnpunishCommand() {
        super("unpunish", "up");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.ADMIN)) return;

        if (strings.length != 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /unpunish <player>"));
            return;
        }

        PlayerManager_legacy.get().getOrLoadPlayer(strings[0], new BLPlayerCallback() {
            @Override
            public void success(BLPlayer_legacy player) {
                PunishType type = PunishHelper.get().getActivePunishType(player);
                PunishReason reason = player.getPunishReason();
                long start = player.getPunishStart();
                long duration = player.getPunishDuration();
                long end = start + duration;
                long left = end - System.currentTimeMillis();
                int number = player.getPunishNumber();

                if (type == PunishType.NO_PUNISH) {
                    commandSender.sendMessage(TextComponent.fromLegacyText("§cThis player is not punished!"));
                    return;
                }

                PunishHelper.get().clearPunishmentFrom(player);

                BungeeMessages.broadcastTo(Rank.MOD_JR, "§aPlayer §f" + player.getName() + " §awas unpunished by §f" + commandSender.getName());
                BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunishReason: §f" + reason.name() + " §f" + PunishHelper.get().getStringNumber(number));
                BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunishType: §f" + type.name());
                if (end > 0 && !type.isPermanent()) {
                    BungeeMessages.broadcastTo(Rank.MOD_JR, "§cTime left: §f" + PunishHelper.get().formatTime(left));
                }

                PluginMessageChannel.sendPluginMessage(new UpdatePunishmentPacket(new Punishment(player.getUuid())));
            }

            @Override
            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not find the player " + strings[0] + "!"));
            }
        });
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfName = strings[0].toLowerCase().trim();
//            return PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//        } else {
//            return Collections.emptyList();
//        }
//    }
}
