/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.pms.PluginMessageChannel;
import net.bluelapiz.common.packet.Packet;
import net.bluelapiz.common.packet.PlayerRankChangePacket;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class SetRankCommand extends BLCommand {

    private static final List<UUID> founder_uuids = Arrays.asList(
            UUID.fromString("71c5ac8f-761e-4771-bb24-e614cc9f286d"), // Sagen97
            UUID.fromString("a7a68af0-9110-4880-a2ed-2f1fff0199d6"), // MrDrunki
            UUID.fromString("2a7ab7cd-8b9f-499f-a28d-a4f964b308b7"), // Grimseidblacky
            UUID.fromString("f32241da-3c4a-4a53-92a9-6fa9a55892c8")  // XAzmon
    );

    public SetRankCommand() {
        super("rank", "setrank");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        // if player and not founder, check permissions
        if ((sender instanceof ProxiedPlayer) && !founder_uuids.contains(((ProxiedPlayer) sender).getUniqueId())) {
            if (!isAuthorizedOrError(sender, Rank.ADMIN)) return;
        }

        if (args.length != 2) {
            sender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /rank <player> <rank>"));
            return;
        }

        PlayerManager_legacy.get().getOrLoadPlayer(args[0], new BLPlayerCallback() {
            @Override
            public void success(BLPlayer_legacy blp) {
                Rank rank = Rank.getRankOrNull(args[1]);
                if (rank == null) {
                    sender.sendMessage(TextComponent.fromLegacyText("§cCannot find rank " + args[1]));
                    return;
                }

                blp.setRank(rank);

                ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText("§6%newrankcolor%player§f was ranked to §f%newrankcolor%newrank§f by %sender"
                        .replace("%player", blp.getName())
                        .replace("%newrankcolor", rank.getColor())
                        .replace("%newrankprefix", rank.getPrefix())
                        .replace("%newrank", rank.getName().toLowerCase())
                        .replace("%sender", sender.getName())));

                DiscordBot.get().sendRankLog(DiscordBot.CHANNEL_INGAME_RANK_LOG, blp.getName(), sender.getName(), rank);

                Packet packet = new PlayerRankChangePacket(blp.getUuid(), blp.getRank());

                PluginMessageChannel.sendPluginMessage(packet);
            }

            @Override
            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                sender.sendMessage(TextComponent.fromLegacyText("§cCannot find player " + args[0]));
            }
        });
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfName = strings[0].toLowerCase().trim();
//            return PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//        } else if (strings.length == 2) {
//            String startOfRank = strings[1].toLowerCase().trim();
//            return Arrays.stream(Rank.values())
//                    .map(Rank::name)
//                    .filter(n -> n.toLowerCase().startsWith(startOfRank))
//                    .collect(Collectors.toList());
//        } else {
//            return Collections.emptyList();
//        }
//    }
}
