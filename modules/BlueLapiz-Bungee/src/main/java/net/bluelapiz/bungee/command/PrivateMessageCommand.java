/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.logging.ChatLogManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.util.Touple;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;
import java.util.UUID;

public class PrivateMessageCommand extends BLCommand {

    public PrivateMessageCommand() {
        super("message", "msg", "tell", "whisper", "m", "pm");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

        if (strings.length < 2) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /m <player> <message>"));
            return;
        }

        ProxiedPlayer reciever = ProxyServer.getInstance().getPlayer(strings[0]);
        if (reciever == null) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not find player " + strings[0] + "!"));
            return;
        }

        if (commandSender == reciever) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cYou cannot send a message to your self!"));
            return;
        }

        String message = String.join(" ", Arrays.copyOfRange(strings, 1, strings.length));

        UUID uuid = ((ProxiedPlayer) commandSender).getUniqueId();
        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(uuid);
        String colorSender = blp.getRank().getColor();

        BLPlayer_legacy blpReciever = PlayerManager_legacy.get().getPlayer((reciever).getUniqueId());
        String colorReciever = blpReciever.getRank().getColor();

        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        String server;
        if (player.getServer() == null || player.getServer().getInfo() == null) {
            server = "Bungee";
        } else {
            server = player.getServer().getInfo().getName();
        }
        ChatLogManager.get().logMessage(ChatLogManager.ChatType.PRIVATE_MESSAGE, server, player.getUniqueId(), reciever.getUniqueId(), message);

        String messageToReciever = colorSender + commandSender.getName() + " §f-> §cme §6> §7" + message;
        reciever.sendMessage(TextComponent.fromLegacyText(messageToReciever));

        String messageToSender = "§cme §f-> " + colorReciever + reciever.getName() + " §6> §7" + message;
        commandSender.sendMessage(TextComponent.fromLegacyText(messageToSender));

        PrivateReplyCommand.lastMessages.put(commandSender.getName(), new Touple<>(reciever.getName(), System.currentTimeMillis()));
        PrivateReplyCommand.lastMessages.put(reciever.getName(), new Touple<>(commandSender.getName(), System.currentTimeMillis()));
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfName = strings[0].toLowerCase().trim();
//            return PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//        }
//
//        // return default tabcomplete for player names on server
//        return super.onTabComplete(commandSender, strings);
//    }
}
