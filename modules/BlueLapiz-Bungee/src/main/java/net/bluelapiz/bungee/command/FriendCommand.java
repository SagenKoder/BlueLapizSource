/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.FriendManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class FriendCommand extends BLCommand {

    public static final ConcurrentHashMap<UUID, HashMap<UUID, Long>> friendRequests = new ConcurrentHashMap<>();

    public FriendCommand() {
        super("friend", "f", "friends");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cOnly players can use this!"));
            return;
        }
        ProxiedPlayer p = (ProxiedPlayer) commandSender;

        if (strings.length < 1) {
            sendHelp(p);
            return;
        }

        if (strings[0].equalsIgnoreCase("add")) {

            if (strings.length != 2) {
                p.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /f add <player>"));
                return;
            }

            ProxiedPlayer r = ProxyServer.getInstance().getPlayer(strings[1]);

            if (r == null) {
                p.sendMessage(TextComponent.fromLegacyText("§cThe player is not online!"));
                return;
            }

            if (r == p) {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cYou cannot add yourself!"));
                return;
            }

            if (friendRequests.getOrDefault(p.getUniqueId(), new HashMap<>()).containsKey(r.getUniqueId()) && friendRequests.get(p.getUniqueId()).get(r.getUniqueId()) + 60_000 > System.currentTimeMillis()) {

                p.sendMessage(TextComponent.fromLegacyText("§aYou accepted the friend request from §f" + r.getName() + "§a!"));
                r.sendMessage(TextComponent.fromLegacyText(p.getName() + "§a accepted your friend request!"));

                FriendManager.get().createFriends(p.getUniqueId(), r.getUniqueId());

                return;
            }

            if (!friendRequests.containsKey(r.getUniqueId())) {
                friendRequests.put(r.getUniqueId(), new HashMap<>());
            }

            if (friendRequests.get(r.getUniqueId()).containsKey(p.getUniqueId()) && friendRequests.get(r.getUniqueId()).get(p.getUniqueId()) + 60_000 > System.currentTimeMillis()) {
                p.sendMessage(TextComponent.fromLegacyText("§cYou have already send a friend request to this player!"));
                return;
            }

            friendRequests.get(r.getUniqueId()).put(p.getUniqueId(), System.currentTimeMillis());

            p.sendMessage(TextComponent.fromLegacyText("§aYou sent a friend request to §f" + r.getName() + "§a!"));
            r.sendMessage(TextComponent.fromLegacyText("§e§lFriend > §aYou received a friend request from §f" + p.getName() + "§a! You have 60 seconds to accept with §f/f add " + p.getName() + " §a before the request expires!"));

        } else if (strings[0].equalsIgnoreCase("remove")) {

            if (strings.length != 2) {
                p.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /f remove <player>"));
                return;
            }

            ProxiedPlayer r = ProxyServer.getInstance().getPlayer(strings[1]);
            UUID uuid = r != null ? r.getUniqueId() : PlayerManager_legacy.get().getUuidFromName(strings[1]);

            if (uuid == null) {
                p.sendMessage(TextComponent.fromLegacyText("§cCannot find player online or in cache!"));
                return;
            }

            if (!FriendManager.get().isFriends(p.getUniqueId(), uuid)) {
                p.sendMessage(TextComponent.fromLegacyText("§cYou are not friends with this player!"));
                return;
            }

            FriendManager.get().deleteFriends(p.getUniqueId(), uuid);
            p.sendMessage(TextComponent.fromLegacyText("§aYou removed " + (r != null ? r.getName() : strings[0]) + " §afrom your friends!"));

        } else if (strings[0].equalsIgnoreCase("list")) {

            if (strings.length != 1) {
                p.sendMessage(TextComponent.fromLegacyText("§cWrong usage!"));
                sendHelp(p);
                return;
            }

            List<UUID> friends = FriendManager.get().getAllFriendsOf(p.getUniqueId());

            if (friends.size() == 0) {
                p.sendMessage(TextComponent.fromLegacyText("§3You do not currently have any friends on BlueLapiz! But no worries, you can add friends with /f add <player>!"));
                return;
            }

            p.sendMessage(TextComponent.fromLegacyText("§3*** Listing your friends ****"));
            for (UUID u : friends) {
                ProxiedPlayer proxiedFriend = ProxyServer.getInstance().getPlayer(u);
                BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(u);
                String name = PlayerManager_legacy.get().getNameFromUuid(u);
                p.sendMessage(TextComponent.fromLegacyText((blPlayer != null ? blPlayer.getRank().getPrefix() : "§7§o") + name + (proxiedFriend != null ? " §a§o[" + proxiedFriend.getServer().getInfo().getName() + "]" : "")));
            }
            p.sendMessage(TextComponent.fromLegacyText("§3***"));

        } else {
            p.sendMessage(TextComponent.fromLegacyText("§cWrong usage!"));
            sendHelp(p);
        }
    }

    private void sendHelp(ProxiedPlayer p) {
        p.sendMessage(TextComponent.fromLegacyText("§6/f add <player> §7Add a player as friend"));
        p.sendMessage(TextComponent.fromLegacyText("§6/f remove <player> §7Removed a player from friends"));
        p.sendMessage(TextComponent.fromLegacyText("§6/f list §7Lists all your friends!"));
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfAction = strings[0].toLowerCase().trim();
//            return Stream.of("list", "add", "remove")
//                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
//                    .collect(Collectors.toList());
//        } else if(strings.length == 2 && !strings[0].equalsIgnoreCase("remove")) {
//            String startOfName = strings[1].toLowerCase().trim();
//            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//            return names;
//        } else {
//            return Collections.emptyList();
//        }
//    }
}
