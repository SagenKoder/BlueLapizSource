/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.listener;

import net.bluelapiz.bungee.Bungee;
import net.bluelapiz.bungee.command.FriendCommand;
import net.bluelapiz.bungee.tablist.BLTablistManager;
import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.player.BLPlayer;
import net.bluelapiz.common.player.FriendManager;
import net.bluelapiz.common.player.PlayerManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.PunishHelper;
import net.bluelapiz.common.punish.PunishType;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class PlayerListener implements Listener {

    public void handlePlayerJoin() {

    }

    @EventHandler
    public void onLogin(LoginEvent e) {
        UUID uuid = e.getConnection().getUniqueId();
        String name = e.getConnection().getName();

        Optional<BLPlayer> optPlayer = PlayerManager.get().createPlayerNow(uuid, name, 1, TimeUnit.SECONDS);
        if (!optPlayer.isPresent()) {
            e.setCancelled(true);
            e.setCancelReason(TextComponent.fromLegacyText("§cCould not load your player data! Try again or contact staff. Ref-code: 0x45ki21"));
            DiscordBot.get().sendError("Bungeecord", Bungee.getInstance().getDescription().getName(),
                    "Could not load playerdata for player " + name + "(" + uuid + ")!", new Exception());
            return;
        }

        BLPlayer_legacy blp = PlayerManager_legacy.get().playerJoin(uuid, name);
        if (blp == null) {
            e.setCancelled(true);
            e.setCancelReason(TextComponent.fromLegacyText("§cCould not find your data. Please wait some seconds before trying again.\nIf this continues -> Contact the developer!"));
            DiscordBot.get().sendError("Bungeecord", Bungee.getInstance().getDescription().getName(),
                    "Could not load legacy playerdata for player " + name + "(" + uuid + ")!", new Exception());
            return;
        }

        PunishType punishType = PunishHelper.get().getActivePunishType(blp);
        if (punishType == PunishType.BAN || punishType == PunishType.PERMBAN) {
            e.setCancelled(true);
            e.setCancelReason(TextComponent.fromLegacyText(
                    PunishHelper.get().getActivePunishDescription(blp)));
            return;
        }

        if (Bungee.MAINTENANCE_MODE && !blp.getRank().isAtLeast(Rank.BUILDER)) {
            e.setCancelled(true);
            e.setCancelReason(TextComponent.fromLegacyText("§cThe server is currently in Maintenance Mode!\nPlease come back later :)"));
        }
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {

        if (PlayerManager_legacy.get().getPlayer(e.getPlayer().getUniqueId()) == null) {
            e.getPlayer().disconnect(TextComponent.fromLegacyText("§cCould not find your data. Please wait some seconds before trying again.\nIf this continues -> Contact the developer!"));
            return;
        }

        // Stop spamming <3
        // DiscordBot.get().sendMessage(DiscordBot.CHANNEL_MINECRAFT_CHAT, Color.GREEN, "[Player Join - %date]", e.getPlayer().getName() + " joined the server!");

        ProxiedPlayer p = e.getPlayer();

        try {
            ConcurrentMap<String, Long> stats = PlayerManager.get().fetchStatsFrom(e.getPlayer().getUniqueId()).get(1, TimeUnit.SECONDS);
            ConcurrentMap<String, Long> updates = new ConcurrentHashMap<>();
            if (!stats.containsKey("first_join")) {
                updates.put("first_join", System.currentTimeMillis());
            }
            updates.put("last_join", System.currentTimeMillis());
            PlayerManager.get().setStatsInDatabase(p.getUniqueId(), updates);
        } catch (Exception ex) {
            DiscordBot.get().sendError("Bungeecord", Bungee.getInstance().getDescription().getName(),
                    "Could not fetch stats from player " + p.getName() + "(" + p.getUniqueId() + ")!", ex);
            ex.printStackTrace();
        }

        try {
            ConcurrentMap<String, String> options = PlayerManager.get().fetchOptionsFrom(e.getPlayer().getUniqueId()).get(1, TimeUnit.SECONDS);
            ConcurrentMap<String, String> updates = new ConcurrentHashMap<>();
            if (!options.containsKey("vanish")) {
                updates.put("vanish", "no");
            }
            if (!options.containsKey("glow")) {
                updates.put("glow", "no");
            }
            if (updates.size() > 0) {
                PlayerManager.get().setOptionsInDatabase(p.getUniqueId(), updates);
            }
        } catch (Exception ex) {
            DiscordBot.get().sendError("Bungeecord", Bungee.getInstance().getDescription().getName(),
                    "Could not fetch options from player " + p.getName() + "(" + p.getUniqueId() + ")!", ex);
            ex.printStackTrace();
        }

        FriendManager.get().loadFriendsOf(e.getPlayer().getUniqueId());

        BLTablistManager.get().setTablistFor(e.getPlayer());

        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(e.getPlayer().getUniqueId());
        if (!blPlayer.isVanished())
            BungeeMessages.broadcastJoin(blPlayer);
    }

    @EventHandler
    public void onConnect(ServerConnectEvent e) {
        e.getPlayer().setTabHeader(
                new ComponentBuilder("§aWelcome to §9§lBlue§f§lLapiz.net\n\n§7Connected to: §d" + e.getTarget().getName()).create(),
                new ComponentBuilder("\n§aForum: §fbluelapiz.net\n§aTeamSpeak: §fbluelapiz.net\n§aDiscord: §fdisc.bluelapiz.net").create());
    }

    @EventHandler
    public void onQuit(PlayerDisconnectEvent e) {
        // Stop spamming <3
        // DiscordBot.get().sendMessage(DiscordBot.CHANNEL_MINECRAFT_CHAT, Color.RED, "[Player Leave - %date]", e.getPlayer().getName() + " left the server!");

        ProxyServer.getInstance().getScheduler().schedule(Bungee.getInstance(),
                () -> BLTablistManager.get().updateTablist(), 1, TimeUnit.SECONDS);

        // clean up when they leave
        FriendCommand.friendRequests.remove(e.getPlayer().getUniqueId());

        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(e.getPlayer().getUniqueId());
        if (!blPlayer.isVanished())
            BungeeMessages.broadcastQuit(blPlayer);

        FriendManager.get().unloadFriends(e.getPlayer().getUniqueId());
        PlayerManager_legacy.get().playerLeave(e.getPlayer().getUniqueId());
    }
}
