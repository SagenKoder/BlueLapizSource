/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.*;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;

public class KickCommand extends BLCommand {

    public KickCommand() {
        super("kick");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD_JR)) return;

        if (strings.length < 2) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /kick <player> <reason>"));
            return;
        }

        ProxiedPlayer other = ProxyServer.getInstance().getPlayer(strings[0]);
        if (other == null) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not find player " + strings[0]));
            return;
        }

        BLPlayer_legacy blOther = PlayerManager_legacy.get().getPlayer(other.getUniqueId());
        if (blOther == null) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not find playerdata for " + other.getName()));
            return;
        }

        Rank senderRank = (commandSender instanceof ProxiedPlayer) ? PlayerManager_legacy.get().getPlayer(((ProxiedPlayer) commandSender).getUniqueId()).getRank() : Rank.FOUNDER;
        if (blOther.getRank().isAtLeast(senderRank)) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cYou can only kick players with lower rank than you!"));
            return;
        }

        String message = String.join(" ", Arrays.copyOfRange(strings, 1, strings.length));

        PunishmentBuilder.getBuilderFor(blOther)
                .setReason(PunishReason.CUSTOM)
                .setPunishedBy((commandSender instanceof ProxiedPlayer) ? ((ProxiedPlayer) commandSender).getUniqueId() : PlayerManager_legacy.CONSOLE_UUID)
                .setCustomDesc(message)
                .setCustomDuration(0L)
                .setCustomType(PunishType.KICK)
                .execute(new PunishCallback() {
                    @Override
                    public boolean beforePunish(BLPlayer_legacy player, Punishment result) {
                        return true;
                    }

                    @Override
                    public void onPunish(BLPlayer_legacy player, Punishment punishResult) {
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPlayer §f" + player.getName() + " §cwas punished by §f" + commandSender.getName());
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunishReason: §f" + punishResult.getDesc());
                        BungeeMessages.broadcastTo(Rank.MOD_JR, "§cPunishType: §f" + punishResult.getType().name());
                        if (punishResult.getDuration() > 12_000) { // 12 seconds
                            BungeeMessages.broadcastTo(Rank.MOD_JR, "§cDuration: §f" + PunishHelper.get().formatTime(punishResult.getDuration()));
                        }

                        ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(player.getUuid());
                        if (proxiedPlayer != null) {
                            proxiedPlayer.disconnect(TextComponent.fromLegacyText(punishResult.getType().getDesciption()
                                    .replace("%reason", punishResult.getDesc())
                                    .replace("%startTime", PunishHelper.get().formatDate(punishResult.getStart()))
                                    .replace("%endTime", PunishHelper.get().formatTime(punishResult.getDuration()))
                                    .replace("%number", "")));// ignore number for custom events
                        }
                    }
                });
    }
}
