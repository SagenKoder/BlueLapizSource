/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 12:59 PM                                              *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

public class BLLoggerBungeeImpl extends BLLogger {

    public BLLoggerBungeeImpl(String pluginName) {
        super(pluginName);
    }

    @Override
    public void info(String... strings) {
        for (String s : strings) {
            ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(
                    ChatColor.BLUE + "[" + pluginName + "] " + ChatColor.WHITE + s));
        }
    }

    @Override
    public void info(Object... objects) {
        for (Object s : objects) {
            ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(
                    ChatColor.BLUE + "[" + pluginName + "] " + ChatColor.WHITE + s.toString()));
        }
    }

    @Override
    public void debug(String... strings) {
        for (String s : strings) {
            ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(
                    ChatColor.GOLD + "[" + pluginName + "] " + ChatColor.WHITE + s));
        }
    }

    @Override
    public void debug(Object... objects) {
        for (Object s : objects) {
            ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(
                    ChatColor.GOLD + "[" + pluginName + "] " + ChatColor.WHITE + s.toString()));
        }
    }

    @Override
    public void severe(Object object, Throwable e) {
        severe(object.toString(), e);
    }

    @Override
    public void severe(String string, Throwable e) {
        ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(
                ChatColor.RED + "[" + pluginName + "] " + ChatColor.WHITE + string));
        e.printStackTrace();

        DiscordBot.get().sendError("BungeeCord", pluginName, string, e);
    }
}
