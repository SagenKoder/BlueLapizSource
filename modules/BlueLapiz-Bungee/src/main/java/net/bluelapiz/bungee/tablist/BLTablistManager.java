/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.tablist;

import net.bluelapiz.common.player.BLPlayer;
import net.bluelapiz.common.player.FriendManager;
import net.bluelapiz.common.player.PlayerManager;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class BLTablistManager extends TablistManager {

    public static final int MAX_NAME_LENGTH = 12;
    public static final int MAX_SERVER_LENGTH = 5;

    private static BLTablistManager tablistManager;
    int i = 3;
    int row = 3;
    boolean partial;

    public static BLTablistManager get() {
        if (tablistManager == null) tablistManager = new BLTablistManager();
        return tablistManager;
    }

    @Override
    protected void setBlankTab(ProxiedPlayer player) {
        super.setBlankTab(player);
    }

    public void updateTablist() {
        new Thread(() -> {
            for (ProxiedPlayer o : ProxyServer.getInstance().getPlayers()) {
                updateTablistFor(o);
            }
        }).start();
    }

    public void setTablistFor(ProxiedPlayer player) {
        setBlankTab(player);

        setTabSlot(player, 1, 1, " §c§lStaff Online");
        setTabSlot(player, 2, 1, " §a§lPlayers");
        setTabSlot(player, 3, 1, " §a§lPlayers");
        setTabSlot(player, 4, 1, " §e§lFriends");

        if (FriendManager.get().getAllFriendsOf(player.getUniqueId()).size() == 0) {
            setTabSlot(player, 4, 3, "§7Add friends");
            setTabSlot(player, 4, 4, "§7with /friend!");
        }

        updateTablistFor(player);
    }

    public void updateTablistFor(ProxiedPlayer player) {

        boolean evenSecond = (System.currentTimeMillis() / 4000) % 2 == 0;

        try {
            Optional<BLPlayer> optPlayer = PlayerManager.get().fetchPlayer(player.getUniqueId()).get(80, TimeUnit.MILLISECONDS);
        } catch (Exception e) {

        }

        BLPlayer_legacy blp = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
        final Rank playerRank;
        if (blp != null)
            playerRank = blp.getRank();
        else
            playerRank = Rank.PLAYER;

        setTabSlot(player, 1, 1, " §c§lStaff Online");
        setTabSlot(player, 4, 1, " §e§lFriends");

        if (evenSecond) {
            setTabSlot(player, 2, 1, " §d§l" + player.getServer().getInfo().getName());
            setTabSlot(player, 3, 1, " §d§l" + player.getServer().getInfo().getName());
        } else {
            setTabSlot(player, 2, 1, " §a§lGlobal");
            setTabSlot(player, 3, 1, " §a§lGlobal");
        }

        //*** Set staff column
        HashMap<Integer, String> staffColumn = new HashMap<>();
        HashMap<Integer, Integer> staffPingColumn = new HashMap<>();
        row = 3;
        ProxyServer.getInstance().getPlayers().stream().filter(p -> {
            BLPlayer_legacy a = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
            return a.getRank().isAtLeast(Rank.BUILDER) && (!a.isVanished() || playerRank.isAtLeast(Rank.MOD_JR));
        }).sorted((pa, pb) -> {
            BLPlayer_legacy a = PlayerManager_legacy.get().getPlayer(pa.getUniqueId());
            BLPlayer_legacy b = PlayerManager_legacy.get().getPlayer(pb.getUniqueId());
            return Integer.compare(b.getRank().getLevel(), a.getRank().getLevel());
        }).forEach(p -> {
            if (p.getServer() == null) return;
            String serverName = p.getServer().getInfo().getName().trim();
            serverName = serverName.substring(0, 1).toUpperCase() + serverName.substring(1);
            BLPlayer_legacy a = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
            staffColumn.put(row, a.getRank().getColor() + (a.isVanished() ? "§o" : "") + p.getName().substring(0, Math.min(p.getName().length(), MAX_NAME_LENGTH)) + "§a§o " + serverName.substring(0, Math.min(serverName.length(), MAX_SERVER_LENGTH)) + (a.isVanished() ? " §7§o[§a§oV§7§o]" : ""));
            staffPingColumn.put(row++, p.getPing());
        });
        setTabColumn(player, 1, staffColumn);
        setTabPingColumn(player, 1, staffPingColumn);

        //*** Set friend column
        HashMap<Integer, String> friendColumn = new HashMap<>();
        HashMap<Integer, Integer> friendPingColumn = new HashMap<>();
        row = 3;
        FriendManager.get().getAllFriendsOf(player.getUniqueId()).stream().sorted((u1, u2) -> {
            ProxiedPlayer p1 = ProxyServer.getInstance().getPlayer(u1);
            ProxiedPlayer p2 = ProxyServer.getInstance().getPlayer(u2);
            if ((p1 == null) == (p2 == null)) return 0;
            if (p1 == null) return 1;
            else return -1;
        }).forEach(u -> {
            String name = PlayerManager_legacy.get().getNameFromUuid(u);
            ProxiedPlayer proxiedPlayer = ProxyServer.getInstance().getPlayer(u);
            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(u);

            if (proxiedPlayer == null) {
                friendColumn.put(row, "§7§o" + name);
                friendPingColumn.put(row, 5000);
            } else {
                String serverName = proxiedPlayer.getServer().getInfo().getName().trim();
                serverName = serverName.substring(0, 1).toUpperCase() + serverName.substring(1);
                friendColumn.put(row, blPlayer.getRank().getColor() + name.substring(0, Math.min(name.length(), MAX_NAME_LENGTH)) + "§a§o " + serverName.substring(0, Math.min(serverName.length(), MAX_SERVER_LENGTH)));
                friendPingColumn.put(row, proxiedPlayer.getPing());
            }
            row++;
        });
        setTabColumn(player, 4, friendColumn);
        setTabPingColumn(player, 4, friendPingColumn);

        // *** Set player columns
        if (player.getServer() == null || player.getServer().getInfo() == null) return;
        // start at third row
        i = 3;
        partial = false;
        HashMap<Integer, String> tabPlayersOne = new HashMap<>();
        HashMap<Integer, Integer> tabPingOne = new HashMap<>();
        HashMap<Integer, String> tabPlayersTwo = new HashMap<>();
        HashMap<Integer, Integer> tabPingTwo = new HashMap<>();

        Collection<ProxiedPlayer> players = (!evenSecond) ? ProxyServer.getInstance().getPlayers() : player.getServer().getInfo().getPlayers();

        players.stream().filter(p -> {
            BLPlayer_legacy a = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
            return !a.getRank().isAtLeast(Rank.MOD_JR) && (!a.isVanished() || playerRank.isAtLeast(Rank.MOD));
        }).sorted((pa, pb) -> {
            BLPlayer_legacy a = PlayerManager_legacy.get().getPlayer(pa.getUniqueId());
            BLPlayer_legacy b = PlayerManager_legacy.get().getPlayer(pb.getUniqueId());
            return Integer.compare(b.getRank().getLevel(), a.getRank().getLevel());
        }).forEach(o -> {
            BLPlayer_legacy a = PlayerManager_legacy.get().getPlayer(o.getUniqueId());
            Rank rank = a.getRank();
            String name = rank.getColor() + (a.isVanished() ? "§o" : "") + o.getName() + (a.isVanished() ? " §7§o[§a§oV§7§o]" : "");

            // ignore excessive players
            if (i > 20) return;

            if (!partial) {
                tabPlayersOne.put(i, name);
                tabPingOne.put(i, o.getPing());
            } else {
                tabPlayersTwo.put(i, name);
                tabPingTwo.put(i++, o.getPing());
            }
            partial = !partial;
        });
        setTabColumn(player, 2, tabPlayersOne);
        setTabColumn(player, 3, tabPlayersTwo);
        setTabPingColumn(player, 2, tabPingOne);
        setTabPingColumn(player, 3, tabPingTwo);
    }

}
