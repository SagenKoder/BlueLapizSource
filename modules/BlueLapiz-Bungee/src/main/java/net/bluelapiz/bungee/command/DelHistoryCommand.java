/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 6:41 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.Bungee;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

public class DelHistoryCommand extends BLCommand {

    public DelHistoryCommand() {
        super("delhistory", "dh");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.ADMIN)) return;

        if (strings.length != 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /delhistory <id>"));
            return;
        }

        int id;
        try {
            id = Integer.parseInt(strings[0]);
        } catch (Exception e) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cCannot parse id " + strings[0]));
            return;
        }

        SQLManager.getSQLHelper().delete(
                "punish_log",
                SQLData.ofKeyword("id").equalsOther(SQLData.ofInteger(id))
        ).whenComplete((r, ex) -> {
            if (ex != null) {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cError during deletion! Error: " + ex.getMessage()));
                Bungee.log.severe("Exception while deleting history!", ex);
                ex.printStackTrace();
            } else {
                commandSender.sendMessage(TextComponent.fromLegacyText("§aDeleted " + r + " rows!"));
            }
        });
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        return Collections.emptyList();
//    }
}
