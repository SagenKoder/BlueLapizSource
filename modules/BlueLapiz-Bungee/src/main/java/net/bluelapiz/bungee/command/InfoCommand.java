/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/28/18 12:42 AM                                              *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.punish.PunishHelper;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class InfoCommand extends BLCommand {

    public InfoCommand() {
        super("info");
    }

    @Override
    public void execute(CommandSender sender, String[] strings) {
        if (strings.length != 1) {
            sender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Wrong usage! /info <player>"));
            return;
        }
        String playerName = strings[0];
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(playerName);
        if (player != null) {
            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
            sendPlayerInfo(blPlayer, sender);
        } else {
            sender.sendMessage(TextComponent.fromLegacyText(ChatColor.GRAY + "Searching database for offline player...."));
            UUID uuid = PlayerManager_legacy.get().getUuidFromName(playerName);
            if (uuid == null) {
                sender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Cannot find player " + playerName + " in database!"));
                return;
            }
            PlayerManager_legacy.get().getOrLoadPlayer(uuid, new BLPlayerCallback() {
                @Override
                public void success(BLPlayer_legacy player) {
                    sendPlayerInfo(player, sender);
                }

                @Override
                public void failure(SQLExceptionType exceptionType, String errorMessage) {
                    sender.sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Cannot find player " + playerName + " in database!"));
                }
            });
        }
    }

    public void sendPlayerInfo(BLPlayer_legacy blPlayer, CommandSender sender) {
        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.AQUA + "***************** INFO ABOUT " + blPlayer.getName() + " ********************"));
        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.GRAY + "Online status: " + (ProxyServer.getInstance().getPlayer(blPlayer.getUuid()) != null ? ChatColor.GREEN + "ONLINE" : ChatColor.RED + "OFFLINE")));
        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.GRAY + "Balance: " + ChatColor.YELLOW + blPlayer.getBalance()));
        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.GRAY + "Town: " + ChatColor.YELLOW + "none"));
        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.GRAY + "Joined: " + ChatColor.YELLOW + PunishHelper.get().formatDate(blPlayer.getFirstLogin())));
        sender.sendMessage(TextComponent.fromLegacyText(ChatColor.AQUA + "***"));
    }
}
