/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.bungee.pms.PluginMessageChannel;
import net.bluelapiz.bungee.utils.BungeeMessages;
import net.bluelapiz.common.packet.SetVanishEnabledPacket;
import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class VanishCommand extends BLCommand {

    public VanishCommand() {
        super("vanish");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD)) return;

        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cOnly players can do this!"));
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(player.getUniqueId());
        if (blPlayer == null) return;

        blPlayer.setVanished(!blPlayer.isVanished());

        String broadcast;
        if (blPlayer.isVanished()) {
            broadcast = "§6§l[!] §7[" + player.getServer().getInfo().getName() + "] §f" + player.getName() + " §a enabled vanish!";
            BungeeMessages.broadcastQuit(blPlayer);
        } else {
            broadcast = "§6§l[!] §7[" + player.getServer().getInfo().getName() + "] §f" + player.getName() + " §a disabled vanish!";
            BungeeMessages.broadcastJoin(blPlayer);
        }

        BungeeMessages.broadcastTo(Rank.MOD, broadcast);

        PluginMessageChannel.sendPluginMessage(new SetVanishEnabledPacket(player.getUniqueId(), blPlayer.isVanished()));
    }
}
