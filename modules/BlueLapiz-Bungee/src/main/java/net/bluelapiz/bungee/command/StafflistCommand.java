/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.sql.callback.SQLSelectCallback;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.*;

public class StafflistCommand extends BLCommand {

    public StafflistCommand() {
        super("stafflist", "sl");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

        commandSender.sendMessage(TextComponent.fromLegacyText("§3Fetching data...."));

        SQLManager.getSQLHelper_legacy().select("SELECT uuid, name, rank, temp_rank FROM player " +
                "WHERE rank >= " + Rank.BUILDER.getLevel() + " " +
                "OR (temp_rank >= " + Rank.BUILDER.getLevel() + " AND temp_rank_until > " + System.currentTimeMillis() + ")", new SQLSelectCallback() {
            @Override
            public void success(LinkedList<Map<String, Object>> result) {
                HashMap<Rank, List<RankPlayer>> rankedPlayers = new HashMap<>();
                for (Map<String, Object> row : result) {
                    UUID uuid = BLPlayer_legacy.stringToUUID((String) row.get("uuid"));
                    String name = (String) row.get("name");
                    Rank rank = Rank.getRank(Math.max((Integer) row.get("rank"), (Integer) row.get("temp_rank")));

                    if (!rankedPlayers.containsKey(rank)) {
                        rankedPlayers.put(rank, new ArrayList<>());
                    }
                    rankedPlayers.get(rank).add(new RankPlayer(uuid, name, rank));
                }

                commandSender.sendMessage(TextComponent.fromLegacyText("§3-------------- Staff Members ----------------"));
                rankedPlayers.keySet().stream()
                        .sorted(Comparator.comparingInt(Rank::getLevel).reversed())
                        .forEach(rank -> {
                            commandSender.sendMessage(TextComponent.fromLegacyText("\n" + rank.getColor() + "§l[" + rank.getName() + "]"));
                            StringBuilder sb = new StringBuilder();
                            rankedPlayers.get(rank).stream()
                                    .sorted((a, b) -> {
                                        ProxiedPlayer p1 = ProxyServer.getInstance().getPlayer(a.getUuid());
                                        ProxiedPlayer p2 = ProxyServer.getInstance().getPlayer(b.getUuid());
                                        if ((p1 == null) == (p2 == null)) return 0;
                                        if (p1 == null) return 1;
                                        else return -1;
                                    }).forEach(rankPlayer -> {
                                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(rankPlayer.getUuid());
                                if (p != null) {
                                    sb.append(rank.getColor()).append(p.getName());
                                } else {
                                    sb.append("§7").append(rankPlayer.getName());
                                }
                                sb.append(", ");
                            });
                            commandSender.sendMessage(TextComponent.fromLegacyText(sb.substring(0, sb.length() - 2)));
                        });
                commandSender.sendMessage(TextComponent.fromLegacyText("§3---------------"));
            }

            @Override
            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                commandSender.sendMessage(TextComponent.fromLegacyText("§cCould not load data! Error: " + errorMessage));
            }
        });

    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        return Collections.emptyList();
//    }

    private class RankPlayer {
        private UUID uuid;
        private String name;
        private Rank rank;

        RankPlayer(UUID uuid, String name, Rank rank) {
            this.uuid = uuid;
            this.name = name;
            this.rank = rank;
        }

        public UUID getUuid() {
            return uuid;
        }

        public void setUuid(UUID uuid) {
            this.uuid = uuid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Rank getRank() {
            return rank;
        }

        public void setRank(Rank rank) {
            this.rank = rank;
        }
    }
}
