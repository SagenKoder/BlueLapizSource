/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.logging.ChatLogManager;
import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;

public class BroadcastCommand extends BLCommand {

    public BroadcastCommand() {
        super("broadcast", "br");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD_SR)) return;

        if (strings.length < 1) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /ss <message>"));
            return;
        }

        String message = String.join(" ", Arrays.copyOfRange(strings, 0, strings.length));

        if (commandSender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) commandSender;
            String server;
            if (player.getServer() == null || player.getServer().getInfo() == null) {
                server = "Bungee";
            } else {
                server = player.getServer().getInfo().getName();
            }
            ChatLogManager.get().logMessage(ChatLogManager.ChatType.BROADCAST, server, player.getUniqueId(), null, message);
        }

        ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText("§3§lBlueLapiz Info > §f" + ChatColor.translateAlternateColorCodes('&', message)));
    }
}
