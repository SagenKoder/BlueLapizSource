/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.*;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.bluelapiz.common.sql.callback.SQLUpdateCallback;
import net.bluelapiz.discord.DiscordBot;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Arrays;
import java.util.LinkedList;

public class EvidenceCommand extends BLCommand {

    public EvidenceCommand() {
        super("evidence", "e");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {
        if (!isAuthorizedOrError(commandSender, Rank.MOD_JR)) return;

        if (strings.length < 2) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /evidence <list|add|delete> <player|id> <reason>"));
            return;
        }

        if (!(commandSender instanceof ProxiedPlayer)) {
            commandSender.sendMessage(TextComponent.fromLegacyText("§cOnly players can do this!"));
            return;
        }

        ProxiedPlayer p = (ProxiedPlayer) commandSender;

        switch (strings[0].toLowerCase()) {
            case "list":
                commandSender.sendMessage(TextComponent.fromLegacyText("§aTrying to get all evidence from player...."));
                EvidenceHelper.get().getAllEvidenceFrom(strings[1], new EvidenceCallback() {
                    @Override
                    public void success(LinkedList<Evidence> evidence) {
                        if (evidence.size() == 0) {
                            commandSender.sendMessage(TextComponent.fromLegacyText("§aThis player has no evidence attached!"));
                            return;
                        }

                        commandSender.sendMessage(TextComponent.fromLegacyText("§3**** Listing evidence on player ****"));
                        boolean first = true;
                        for (Evidence e : evidence) {
                            if (!first) commandSender.sendMessage();
                            first = false;
                            commandSender.sendMessage(TextComponent.fromLegacyText("§aDate: §f" + PunishHelper.get().formatDate(e.getTime())));
                            commandSender.sendMessage(TextComponent.fromLegacyText("§aReason: §f" + e.getPunishReason().name()));
                            commandSender.sendMessage(TextComponent.fromLegacyText("§aBy: §f" + e.getByName() + " §aId: §f" + e.getId()));
                            commandSender.sendMessage(TextComponent.fromLegacyText("§aEvidence: §f" + e.getEvidence()));
                        }
                        commandSender.sendMessage(TextComponent.fromLegacyText("§3****"));
                    }

                    @Override
                    public void error(SQLExceptionType exceptionType, String message) {
                        commandSender.sendMessage(TextComponent.fromLegacyText("§c" + message));
                    }
                });
                break;
            case "add":
                if (strings.length < 4) {
                    commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /evidence add <player> <reason> <evidence>"));
                    return;
                }

                if (Arrays.stream(PunishReason.values())
                        .filter(PunishReason::isInPunishCommand)
                        .map(PunishReason::name)
                        .noneMatch(n -> n.equalsIgnoreCase(strings[2]))) {
                    commandSender.sendMessage(TextComponent.fromLegacyText("§cWrong usage! Cannot find the punish reason " + strings[2] + "!"));
                    return;
                }

                PunishReason punishReason = PunishReason.valueOf(strings[2].toUpperCase());

                String message = String.join(" ", Arrays.copyOfRange(strings, 3, strings.length));

                PlayerManager_legacy.get().getOrLoadPlayer(strings[1], new BLPlayerCallback() {
                    @Override
                    public void success(BLPlayer_legacy player) {
                        EvidenceHelper.get().insertEvidence(player.getUuid(), p.getUniqueId(), punishReason, message, new SQLUpdateCallback() {
                            @Override
                            public void success(int rowsAffected) {
                                p.sendMessage(TextComponent.fromLegacyText("§aInserted " + rowsAffected + " rows!"));
                                DiscordBot.get().sendMessage(
                                        DiscordBot.CHANNEL_INGAME_EVIDENCE,
                                        player.getRank().getDiscordColor(),
                                        "[Minecraft Evidence - %time]]",
                                        "Player: " + player.getName() + "\n" +
                                                "Reason: " + punishReason + "\n" +
                                                "Message: " + message + "\n" +
                                                "By: " + p.getName()
                                );
                            }

                            @Override
                            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                                p.sendMessage(TextComponent.fromLegacyText("§c" + errorMessage));
                            }
                        });
                    }

                    @Override
                    public void failure(SQLExceptionType exceptionType, String errorMessage) {
                        p.sendMessage(TextComponent.fromLegacyText("§c" + errorMessage));
                    }
                });
                break;
            case "delete":
                int id;
                try {
                    id = Integer.parseInt(strings[1]);
                } catch (Exception e) {
                    p.sendMessage(TextComponent.fromLegacyText("§cCould not parse number " + strings[1]));
                    return;
                }

                EvidenceHelper.get().deleteEvidence(id, new SQLUpdateCallback() {
                    @Override
                    public void success(int rowsAffected) {
                        p.sendMessage(TextComponent.fromLegacyText("§aDeleted " + rowsAffected + " rows!"));
                    }

                    @Override
                    public void failure(SQLExceptionType exceptionType, String errorMessage) {
                        p.sendMessage(TextComponent.fromLegacyText("§c" + errorMessage));
                    }
                });
                break;

            default:
                p.sendMessage(TextComponent.fromLegacyText("§cWrong usage! /evidence <list|add|delete> <player> <reason> <evidence>"));
        }
    }

//    @Override
//    public Iterable<String> onTabComplete(CommandSender commandSender, String[] strings) {
//        if(strings.length == 1) {
//            String startOfAction = strings[0].toLowerCase().trim();
//            return Stream.of("list", "add", "delete")
//                    .filter(s -> s.toLowerCase().startsWith(startOfAction))
//                    .collect(Collectors.toList());
//        } else if(strings.length == 2 && !strings[0].equalsIgnoreCase("delete")) {
//            String startOfName = strings[1].toLowerCase().trim();
//            List<String> names = PlayerManager_legacy.get().getAllCachedNames().stream()
//                    .filter(s -> s.toLowerCase().startsWith(startOfName))
//                    .collect(Collectors.toList());
//            return names;
//        } else if (strings.length == 3 && !strings[0].equalsIgnoreCase("delete")) {
//            return Arrays.stream(PunishReason.values())
//                    .filter(PunishReason::isInPunishCommand)
//                    .map(PunishReason::name)
//                    .filter(n -> n.toLowerCase().startsWith(strings[2].toLowerCase()))
//                    .collect(Collectors.toList());
//        } else {
//            return Collections.emptyList();
//        }
//    }
}
