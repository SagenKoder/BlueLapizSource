/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.bungee.command;

import net.bluelapiz.common.player.objects.Rank;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

public class RawCommand extends BLCommand {

    public RawCommand() {
        super("raw");
    }

    @Override
    public void execute(CommandSender commandSender, String[] strings) {

        if (!isAuthorizedOrError(commandSender, Rank.ADMIN)) return;

        ProxyServer.getInstance().broadcast(TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', String.join(" ", strings))));

    }
}
