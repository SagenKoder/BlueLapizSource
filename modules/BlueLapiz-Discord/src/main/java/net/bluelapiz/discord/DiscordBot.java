/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.discord;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.player.objects.Rank;

import java.awt.*;
import java.io.PrintWriter;
import java.io.StringWriter;

public class DiscordBot extends BufferdDiscordBotConnector {

    public static final long CHANNEL_INGAME_STAFF_CHAT = 479239752545992704L;
    public static final long CHANNEL_INGAME_BROADCAST = 479239793759485952L;
    public static final long CHANNEL_INGAME_RANK_LOG = 479238963958382593L;
    public static final long CHANNEL_INGAME_EVIDENCE = 479239839582126080L;
    public static final long CHANNEL_INGAME_PUNISH = 479254727725547533L;
    public static final long CHANNEL_INGAME_ERROR = 481828303314092044L;

    public static BLLogger log;

    private static String NAME;
    private static String TOKEN;
    private static String IMAGE;

    private static DiscordBot instance;

    private DiscordBot() {
        super(NAME, TOKEN, IMAGE);
    }

    public static void setup(String name, String token, String image) {
        NAME = name;
        TOKEN = token;
        IMAGE = image;
    }

    public static DiscordBot get() {
        if (instance == null) instance = new DiscordBot();
        return instance;
    }

    public void sendChatMessage(long channel, String player, String message, Rank rank) {
        String title = "[Minecraft Chat - %time]]";
        String msg = player + " \u00BB " + message;
        super.sendMessage(channel, rank.getDiscordColor(), title, msg);
    }

    public void sendRankLog(long channel, String playerRanked, String rankedBy, Rank newRank) {
        String title = "[Rank - " + newRank + " - %time]";
        String msg = "Player: " + playerRanked + "\nRanked by: " + rankedBy + "\nRank: " + newRank.getName();
        super.sendMessage(channel, newRank.getDiscordColor(), title, msg);
    }

    public void sendError(String server, String plugin, String description, Throwable exception, String time) {
        String title = "Exception[Server=" + server + ",plugin=" + plugin + ",time=" + time + "]";
        String message = mentionRole("Founder") + "\nMessage: " + description + "\n";

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        message += sw.toString();

        super.sendMessage(CHANNEL_INGAME_ERROR, Color.RED, title, message);
    }

    public void sendError(String server, String plugin, String description, Throwable exception) {
        sendError(server, plugin, description, exception, "%time");
    }
}
