/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.discord;

import net.bluelapiz.common.BLLogger;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.RequestBuffer;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class DiscordBotConnector implements IListener<MessageReceivedEvent> {

    private IDiscordClient discordClient;
    private Set<DiscordListener> listeners = new HashSet<>();

    private Map<String, String> mentionableRoles;

    public DiscordBotConnector(String name, String token, String imageUrl) {
        try {
            this.discordClient = new ClientBuilder().withToken(token).build();
            this.discordClient.getDispatcher().registerListener(this);
            this.discordClient.login();

            long startTime = System.currentTimeMillis();
            while (!discordClient.isReady()) {
                try {
                    Thread.sleep(50);
                } catch (Exception e) {
                } // ignore
                if (System.currentTimeMillis() - startTime > 5000) {
                    BLLogger.commonLogger.severe("COULD NOT START DISCORD!!!!!", new Exception());
                    return;
                }
            }

            try {
                this.discordClient.changeAvatar(Image.forUrl("png", imageUrl));
            } catch (Exception e) {
            }
            try {
                this.discordClient.changeUsername(name);
            } catch (Exception e) {
            }
            try {
                this.discordClient.changePresence(StatusType.ONLINE, ActivityType.WATCHING, "The server logs.");
            } catch (Exception e) {
            }

            mentionableRoles = discordClient.getRoles().stream().collect(Collectors.toMap(IRole::getName, IRole::mention));
        } catch (Exception e) {
            BLLogger.commonLogger.severe("COULD NOT START DISCORD!!!!!", e);
        }
    }

    public String mentionRole(String role) {
        return mentionableRoles.get(role);
    }

    public boolean isConnected() {
        return this.discordClient != null && this.discordClient.isLoggedIn();
    }

    public void disconnect() {
        if (this.discordClient != null && discordClient.isLoggedIn()) {
            discordClient.logout();
            discordClient = null;
        }
    }

    public void registerListener(DiscordListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public void handle(MessageReceivedEvent event) {
        listeners.forEach(l -> l.onEvent(event));
    }

    public boolean sendMessage(long channel, Color color, String title, String message) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            String time = formatter.format(calendar.getTime());

            EmbedBuilder builder = new EmbedBuilder();
            builder.withColor(color);
            builder.withAuthorName(title.replace("%time", time));
            builder.withDescription(message.replace("%time", time));

            IChannel c = discordClient.getChannelByID(channel);
            RequestBuffer.request((() -> {
                c.sendMessage("", builder.build(), false);
            }));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
