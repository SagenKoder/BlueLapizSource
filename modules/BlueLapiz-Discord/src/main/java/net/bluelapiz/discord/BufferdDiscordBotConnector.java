package net.bluelapiz.discord;

import java.awt.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class BufferdDiscordBotConnector extends DiscordBotConnector {

    ExecutorService executorService;

    public BufferdDiscordBotConnector(String name, String token, String imageUrl) {
        super(name, token, imageUrl);
        executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void disconnect() {
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(20, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
            }
        } catch (InterruptedException ex) {
            System.out.println("** Could not shutdown the discord bot. The following messages (if any) could not be sent: **");
            executorService.shutdownNow().forEach(System.out::println);
            System.out.println("**");
            Thread.currentThread().interrupt();
        }
        super.disconnect();
    }

    @Override
    public boolean sendMessage(long channel, Color color, String title, String message) {
        executorService.execute(new MessageExecutioner(channel, color, title, message));
        return true;
    }

    private class MessageExecutioner implements Runnable {
        long channel;
        Color color;
        String title;
        String message;

        int tries = 0;

        public MessageExecutioner(long channel, Color color, String title, String message) {
            this.channel = channel;
            this.color = color;
            this.title = title;
            this.message = message;
        }

        @Override
        public void run() {
            boolean success;
            if (!(success = BufferdDiscordBotConnector.super.sendMessage(channel, color, title, message)) && tries++ < 5) {
                try {
                    Thread.sleep(500);
                } catch (Exception ignored) {
                }
            }
            if (!success) {
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                System.out.println("Could not send discord message after 5 tries! Message: " + this.toString());
                System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
        }

        @Override
        public String toString() {
            return "MessageExecutioner{" +
                    "channel=" + channel +
                    ", color=" + color +
                    ", title='" + title + '\'' +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}
