/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.discord;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public interface DiscordListener {

    void onEvent(MessageReceivedEvent event);

}
