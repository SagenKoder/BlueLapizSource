/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 6:44 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.hub;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.packet.PacketHelper;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.BLLoggerSpigotImpl;
import net.bluelapiz.core.Core;
import net.bluelapiz.hub.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

public class Hub extends JavaPlugin {

    public static BLLogger log = new BLLoggerSpigotImpl("BlueLapiz-Hub");
    private static Hub instance;

    public static Hub getInstance() {
        return instance;
    }

    @Override
    public void onDisable() {
    }

    @Override
    public void onEnable() {
        instance = this;

        Hub.log.debug("Disabling collision from all rank teams");
        for (Rank r : Rank.values()) {
            Team team = Core.get().getScoreboard().getTeam(r.getName());
            team.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        }

        Hub.log.debug("Registering PluginChannels");
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, PacketHelper.BUNGEE_MESSAGES_CHANNEL);

        Hub.log.debug("Registering Listeners");
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
    }
}
