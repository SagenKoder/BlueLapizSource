/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.hub;

import net.bluelapiz.common.packet.RequestSendPlayerPacket;
import net.bluelapiz.core.menu.BLMenu;
import net.bluelapiz.core.menu.ItemBuilder;
import net.bluelapiz.core.pms.PluginMessageChannel;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public class ServerSelectorGUI extends BLMenu {

    private static final String SURVIVAL = "§a§lSurvival";
    private static final String MURDER = "§a§lMurder";
    private static final String BUILD = "§8§lBuild";

    private static ServerSelectorGUI serverSelecorGUI;

    private ServerSelectorGUI() {
        super("§a§lSelect a server!", 9, Hub.getInstance());
        init();
    }

    public static ServerSelectorGUI get() {
        if (serverSelecorGUI == null) serverSelecorGUI = new ServerSelectorGUI();
        return serverSelecorGUI;
    }

    private void init() {
        addOption(new ItemBuilder(Material.GRASS)
                .setName(SURVIVAL)
                .setLore("§3Click me to go to survival!").toItemStack(), 1);
        addOption(new ItemBuilder(Material.IRON_AXE)
                .setName(MURDER)
                .addEnchant(Enchantment.DAMAGE_ALL, 1, false)
                .setLore("§3Click me to go to Murder minigame!").toItemStack(), 4);
        addOption(new ItemBuilder(Material.BEDROCK)
                .setName(BUILD)
                .setLore("§3Click me to go to the build server!").toItemStack(), 7);
    }

    @Override
    public void onClose(InventoryCloseEvent e) {
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player)) return;
        Player p = (Player) e.getWhoClicked();
        e.setCancelled(true);
        ItemStack is = e.getCurrentItem();
        if (is == null || !is.hasItemMeta() || !is.getItemMeta().hasDisplayName()) return;
        String disp = is.getItemMeta().getDisplayName();

        switch (disp) {
            case SURVIVAL:
                p.sendMessage("§aTeleporting to Survival...");
                PluginMessageChannel.sendPluginMessage(p, new RequestSendPlayerPacket(p.getUniqueId(), "survival"));
                break;
            case MURDER:
                p.sendMessage("§cThis server does not yet exist!");
                break;
            case BUILD:
                p.sendMessage("§aTeleporting to Build...");
                PluginMessageChannel.sendPluginMessage(p, new RequestSendPlayerPacket(p.getUniqueId(), "build"));
        }

        p.closeInventory();
    }
}
