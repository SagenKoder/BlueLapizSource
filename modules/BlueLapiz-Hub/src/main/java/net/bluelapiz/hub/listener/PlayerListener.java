/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.hub.listener;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.core.menu.BLMenu;
import net.bluelapiz.core.menu.ItemBuilder;
import net.bluelapiz.hub.Hub;
import net.bluelapiz.hub.ServerSelectorGUI;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;

public class PlayerListener implements Listener {

    private static final ItemStack COMPASS = new ItemBuilder(Material.COMPASS)
            .setName("§6§lSelect server")
            .addEnchant(Enchantment.DURABILITY, 0, false)
            .setLore("§3Rightclick with this compass", "§3to open the server selector!").toItemStack();

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        BLMenu.checkForMenuClose(Hub.getInstance(), e);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());
        if (blPlayer == null) return;

        if (blPlayer.getRank().isAtLeast(Rank.VIP)) {
            p.setAllowFlight(true);
        } else {
            p.setAllowFlight(false);
        }

        p.getInventory().clear();
        p.getInventory().setItem(4, COMPASS.clone());

        p.teleport(p.getWorld().getSpawnLocation());
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        ItemStack i = e.getItemDrop().getItemStack();
        if (COMPASS.isSimilar(i) && i.hasItemMeta() && i.getItemMeta().getDisplayName().equals(COMPASS.getItemMeta().getDisplayName())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventory(InventoryClickEvent e) {
        ItemStack i = e.getCurrentItem();
        if (i != null && COMPASS.isSimilar(i) && i.hasItemMeta() && i.getItemMeta().getDisplayName().equals(COMPASS.getItemMeta().getDisplayName())) {
            e.setCancelled(true);
        }

        i = e.getCursor();
        if (i != null && COMPASS.isSimilar(i) && i.hasItemMeta() && i.getItemMeta().getDisplayName().equals(COMPASS.getItemMeta().getDisplayName())) {
            e.setCancelled(true);
        }

        if (e.getWhoClicked() instanceof Player)
            ((Player) e.getWhoClicked()).updateInventory();

        BLMenu menu = BLMenu.checkForMenuClick(Hub.getInstance(), e, true);
        if (menu != null) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
            e.setCancelled(true);
        }

        ItemStack i = e.getItem();
        if (COMPASS.isSimilar(i) && i.hasItemMeta() && i.getItemMeta().getDisplayName().equals(COMPASS.getItemMeta().getDisplayName())) {
            e.setCancelled(true);

            ServerSelectorGUI.get().show(e.getPlayer());
        }
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractAtEntityEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onArmourstandManipulate(PlayerArmorStandManipulateEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onEntityBreak(HangingBreakByEntityEvent e) {
        if (e.getRemover() instanceof Player) {
            Player p = (Player) e.getRemover();

            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

            if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onEntityDamage(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Player) {
            Player p = (Player) e.getDamager();

            BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

            if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onHangingBreak(HangingPlaceEvent e) {
        Player p = e.getPlayer();
        BLPlayer_legacy blPlayer = PlayerManager_legacy.get().getPlayer(p.getUniqueId());

        if (!blPlayer.getRank().isAtLeast(Rank.FOUNDER)) {
            e.setCancelled(true);
        }
    }
}
