package net.bluelapiz.common.ornlitetest;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import net.bluelapiz.common.punish.PunishReason;
import net.bluelapiz.common.punish.PunishType;

import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class ORMTester {

    public static void test(String sqlUser, String sqlPass, String database) {
        String databaseUrl = "jdbc:mysql://localhost:3306/" + database;
        try {
            System.out.println("Connecting to " + databaseUrl);
            ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl, sqlUser, sqlPass);

            System.out.println("Creating daos");
            Dao<ORMPlayer, Long> playerDao = DaoManager.createDao(connectionSource, ORMPlayer.class);
            Dao<ORMPunishment, Long> punishmentDao = DaoManager.createDao(connectionSource, ORMPunishment.class);

            System.out.println("Creating tables if not exists");
            if(!playerDao.isTableExists())
                TableUtils.createTable(connectionSource, ORMPlayer.class);
            if(!punishmentDao.isTableExists())
                TableUtils.createTable(connectionSource, ORMPunishment.class);

            System.out.println("Creating ORMPlayers for testing");
            ORMPlayer ormPlayer1 = new ORMPlayer(UUID.randomUUID(), Long.toHexString(ThreadLocalRandom.current().nextInt()));
            ORMPlayer ormPlayer2 = new ORMPlayer(UUID.randomUUID(), Long.toHexString(ThreadLocalRandom.current().nextInt()));

            System.out.println("Inserting players into database");
            playerDao.create(ormPlayer1);
            playerDao.create(ormPlayer2);

            System.out.println("Creating and inserting punishment");
            ORMPunishment ormPunishment = new ORMPunishment(
                    ormPlayer1, ormPlayer2,
                    PunishReason.values()[ThreadLocalRandom.current().nextInt(PunishReason.values().length)],
                    PunishType.values()[ThreadLocalRandom.current().nextInt(PunishType.values().length)],
                    new Date(), new Date(System.currentTimeMillis() + 1000*60*60)); // 1 hour
            punishmentDao.create(ormPunishment);

            ormPlayer1.setActivePunishment(ormPunishment);
            playerDao.update(ormPlayer1);
            playerDao.refresh(ormPlayer1);

            System.out.println("Querying for player by uuid");
            System.out.println(playerDao.queryForEq("uuid", ormPlayer1.getUuid()));

            System.out.println("Querying for punishment");
            System.out.println(punishmentDao.queryForId(ormPunishment.getId()));

            playerDao.refresh(ormPlayer1);
            if(ormPlayer1.getActivePunishment() != null) punishmentDao.refresh(ormPlayer1.getActivePunishment());
            System.out.println("Punishment of player1 " + ormPlayer1.getActivePunishment());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
