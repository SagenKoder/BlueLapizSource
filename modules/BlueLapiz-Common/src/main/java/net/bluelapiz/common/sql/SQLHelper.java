/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.sql;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * Helper class for basic mysql queries.
 */
public interface SQLHelper {

    CompletableFuture<LinkedList<Map<String, Object>>> select(String sql);

    CompletableFuture<Integer> update(String sql);

    CompletableFuture<Integer> update(String table, SQLData booleanExpressions, Map<String, SQLData> values);

    CompletableFuture<LinkedList<Map<String, Object>>> selectAllRowsFrom(String table);

    CompletableFuture<Map<String, Object>> selectOneRowFrom(String table);

    CompletableFuture<LinkedList<Map<String, Object>>> select(String table, List<String> columns, SQLData booleanExpression);

    CompletableFuture<LinkedList<Map<String, Object>>> select(String table, List<String> columns, SQLData booleanExpression, int limit, SQLOrder order, String... orderBy);

    CompletableFuture<Integer> delete(String table, SQLData booleanExpression);

    CompletableFuture<Long> countRows(String table, SQLData booleanExpression);

    CompletableFuture<List<Integer>> insert(String table, Map<String, SQLData> values);

    CompletableFuture<List<Integer>> insert(String table, Map<String, SQLData> values, boolean replace);

    CompletableFuture<List<Integer>> insertMultiple(String table, List<String> columns, List<List<SQLData>> rows, boolean replace);

    CompletableFuture<Integer> updateMultipleRowsAddNumber(String table, String keyToChange, String columnToChange, Map<String, Integer> valuesToAdd);

    CompletableFuture<Integer> updateMultipleRowsSetValue(String table, String keyToChange, String columnToChange, Map<String, SQLData> valuesToSet);

    CompletableFuture<List<String>> getTables();
}
