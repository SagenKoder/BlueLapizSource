/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.common.player;

import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.util.Touple;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class PlayerManager extends RawPlayerManager {

    private static final long CACHE_TIME_TO_LIVE = 60_000; // 60 sekunder

    private static PlayerManager playerManager;
    private ConcurrentHashMap<UUID, Touple<BLPlayer, Long>> playerCache = new ConcurrentHashMap<>();
    private ConcurrentHashMap<UUID, Touple<ConcurrentMap<String, Long>, Long>> statsCache = new ConcurrentHashMap<>();
    private ConcurrentHashMap<UUID, Touple<ConcurrentMap<String, String>, Long>> optionsCache = new ConcurrentHashMap<>();

    public static PlayerManager get() {
        if (playerManager == null) playerManager = new PlayerManager();
        return playerManager;
    }

    private Optional<BLPlayer> getCachedPlayer(UUID uuid) {
        if (!playerCache.containsKey(uuid)) return Optional.empty();

        Touple<BLPlayer, Long> cachedValue = playerCache.get(uuid);

        if (cachedValue == null) return Optional.empty();

        if (cachedValue.getRight() + CACHE_TIME_TO_LIVE < System.currentTimeMillis()) {
            playerCache.remove(uuid); // has become invalid
            return Optional.empty();
        }

        return Optional.of(cachedValue.getLeft());
    }

    private void updateCachedPlayer(UUID uuid, BLPlayer player) {
        this.playerCache.put(uuid, new Touple<>(player, System.currentTimeMillis()));
    }


    private Optional<ConcurrentMap<String, Long>> getCachedStats(UUID uuid) {
        if (!statsCache.containsKey(uuid)) return Optional.empty();

        Touple<ConcurrentMap<String, Long>, Long> cachedValue = statsCache.get(uuid);

        if (cachedValue == null) return Optional.empty();

        if (cachedValue.getRight() + CACHE_TIME_TO_LIVE < System.currentTimeMillis()) {
            statsCache.remove(uuid); // has become invalid
            return Optional.empty();
        }

        return Optional.of(cachedValue.getLeft());
    }

    private void updateCachedStats(UUID uuid, ConcurrentMap<String, Long> player) {
        this.statsCache.put(uuid, new Touple<>(player, System.currentTimeMillis()));
    }


    private Optional<ConcurrentMap<String, String>> getCachedOptions(UUID uuid) {
        if (!optionsCache.containsKey(uuid)) return Optional.empty();

        Touple<ConcurrentMap<String, String>, Long> cachedValue = optionsCache.get(uuid);

        if (cachedValue == null) return Optional.empty();

        if (cachedValue.getRight() + CACHE_TIME_TO_LIVE < System.currentTimeMillis()) {
            optionsCache.remove(uuid); // has become invalid
            return Optional.empty();
        }

        return Optional.of(cachedValue.getLeft());
    }

    private void updateCachedOptions(UUID uuid, ConcurrentMap<String, String> player) {
        this.optionsCache.put(uuid, new Touple<>(player, System.currentTimeMillis()));
    }

    /**
     * Fetches the player from the database. Returns Optional.empty() if the player is not in the database.
     *
     * @param uuid The players uuid
     */
    @Override
    public CompletableFuture<Optional<BLPlayer>> fetchPlayer(UUID uuid) {
        Optional<BLPlayer> cachedValue = getCachedPlayer(uuid);
        if (cachedValue.isPresent()) {
            return CompletableFuture.completedFuture(cachedValue);
        }

        return super.fetchPlayer(uuid).thenApply((val) -> {
            val.ifPresent(p -> updateCachedPlayer(uuid, p));
            if (!val.isPresent()) {
                return getCachedPlayer(uuid);
            }
            return val;
        });
    }

    /**
     * Fetches the player from the database or creates a new Player if not existing.
     */
    @Override
    public CompletableFuture<BLPlayer> createPlayer(UUID uuid, String name) {
        Optional<BLPlayer> cachedValue = getCachedPlayer(uuid);
        return cachedValue.map(CompletableFuture::completedFuture).orElseGet(() -> super.createPlayer(uuid, name).thenApply((val) -> {
            if (val != null) updateCachedPlayer(uuid, val);
            else {
                Optional<BLPlayer> secondCachedValue = getCachedPlayer(uuid);
                if (secondCachedValue.isPresent()) return secondCachedValue.get();
            }
            return val;
        }));

    }

    /**
     * Fetches a set of stats from a player. A stat will be ignored if it does not exist in the database.
     *
     * @param uuid
     * @return
     */
    @Override
    public CompletableFuture<ConcurrentMap<String, Long>> fetchStatsFrom(UUID uuid) {
        Optional<ConcurrentMap<String, Long>> cachedValue = getCachedStats(uuid);
        return cachedValue.map(CompletableFuture::completedFuture).orElseGet(() -> super.fetchStatsFrom(uuid).thenApply((val) -> {
            if (val != null) updateCachedStats(uuid, val);
            else {
                Optional<ConcurrentMap<String, Long>> secondCachedValue = getCachedStats(uuid);
                if (secondCachedValue.isPresent()) return secondCachedValue.get();
            }
            return val;
        }));

    }

    /**
     * Fetches a set of options for a player. An option will be ignored if it does not exist in the database.
     *
     * @param uuid
     * @return
     */
    @Override
    public CompletableFuture<ConcurrentMap<String, String>> fetchOptionsFrom(UUID uuid) {
        Optional<ConcurrentMap<String, String>> cachedValue = getCachedOptions(uuid);
        return cachedValue.map(CompletableFuture::completedFuture).orElseGet(() -> super.fetchOptionsFrom(uuid).thenApply((val) -> {
            if (val != null) updateCachedOptions(uuid, val);
            else {
                Optional<ConcurrentMap<String, String>> secondCachedValue = getCachedOptions(uuid);
                if (secondCachedValue.isPresent()) return secondCachedValue.get();
            }
            return val;
        }));

    }

    /**
     * Updates a set of stats to the given value.
     *
     * @param uuid
     * @param newValues
     * @return
     */
    @Override
    public CompletableFuture<Integer> setStatsInDatabase(UUID uuid, ConcurrentMap<String, Long> newValues) {
        return super.setStatsInDatabase(uuid, newValues).thenApply((val) -> {
            Optional<ConcurrentMap<String, Long>> cachedValue = getCachedStats(uuid);
            cachedValue.ifPresent(stats -> stats.putAll(newValues));
            return val;
        });
    }

    /**
     * Updates a set of options for a player.
     *
     * @param uuid
     * @param newValues
     * @return
     */
    @Override
    public CompletableFuture<Integer> setOptionsInDatabase(UUID uuid, ConcurrentMap<String, String> newValues) {
        return super.setOptionsInDatabase(uuid, newValues).thenApply((val) -> {
            Optional<ConcurrentMap<String, String>> cachedValue = getCachedOptions(uuid);
            cachedValue.ifPresent(options -> options.putAll(newValues));
            return val;
        });
    }

    /**
     * Updates the rank of a given player in the database.
     *
     * @param uuid
     * @param newRank
     * @return
     */
    @Override
    CompletableFuture<Boolean> setRankInDatabase(UUID uuid, Rank newRank) {
        return super.setRankInDatabase(uuid, newRank).thenApply((val) -> {
            getCachedPlayer(uuid).ifPresent(p -> p.rank = newRank);
            return val;
        });
    }

    /**
     * Updates the rank of a given player in the database.
     *
     * @param uuid
     * @param newRank
     * @return
     */
    @Override
    CompletableFuture<Boolean> setTempRankInDatabase(UUID uuid, Rank newRank, Date expires) {
        return super.setTempRankInDatabase(uuid, newRank, expires).thenApply((val) -> {
            getCachedPlayer(uuid).ifPresent(p -> {
                p.tmpRank = newRank;
                p.rankExpires = expires;
            });
            return val;
        });
    }

    /**
     * Updates the name of a given player in the database.
     *
     * @param uuid
     * @param newName
     * @return
     */
    @Override
    CompletableFuture<Boolean> setNameInDatabase(UUID uuid, String newName) {
        return super.setNameInDatabase(uuid, newName).thenApply((val) -> {
            getCachedPlayer(uuid).ifPresent(p -> p.name = newName);
            return val;
        });
    }

}
