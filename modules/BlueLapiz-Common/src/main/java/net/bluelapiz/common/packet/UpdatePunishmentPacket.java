/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import net.bluelapiz.common.punish.Punishment;

public class UpdatePunishmentPacket extends Packet {

    Punishment punishResult;

    public UpdatePunishmentPacket(Punishment punishResult) {
        this.punishResult = punishResult;
    }

    public Punishment getPunishResult() {
        return punishResult;
    }

    @Override
    public String toString() {
        return "UpdatePunishmentPacket{" +
                "punishResult=" + punishResult +
                '}';
    }
}
