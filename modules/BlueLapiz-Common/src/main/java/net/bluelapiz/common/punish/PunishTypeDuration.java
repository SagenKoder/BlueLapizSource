/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

public class PunishTypeDuration {

    private long time;
    private PunishType type;

    public PunishTypeDuration(PunishType type) {
        this(type, 0L);
    }

    public PunishTypeDuration(PunishType type, long time) {
        this.time = time;
        this.type = type;
    }

    public long getTime() {
        return time;
    }

    public PunishType getType() {
        return type;
    }
}
