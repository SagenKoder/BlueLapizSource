/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 12/7/18 1:33 PM                                                *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player;

import net.bluelapiz.common.sql.SQLManager;

public class BankManager {
    private static BankManager bankManager;

    private BankManager() {
        SQLManager.getTableHelper().addTable("transactions",
                "CREATE TABLE %table (\n" +
                        "    transaction_id INT NOT NULL AUTO_INCREMENT,\n" +
                        "    transaction_timestamp TIMESTAMP(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),\n" +
                        "    uuid CHAR(32) NOT NULL,\n" +
                        "    amount BIGINT NOT NULL,\n" +
                        "    balance BIGINT NOT NULL,\n" +
                        "    description VARCHAR(126) NOT NULL,\n" +
                        "    UNIQUE KEY(transaction_id),\n" +
                        "    PRIMARY KEY(transaction_timestamp, uuid)" +
                        ");");
        SQLManager.getTableHelper().createTablesNotExisting();
    }

    public static BankManager get() {
        if (bankManager == null) bankManager = new BankManager();
        return bankManager;
    }

}
