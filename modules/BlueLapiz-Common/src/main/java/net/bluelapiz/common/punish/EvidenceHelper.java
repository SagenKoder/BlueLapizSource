/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import net.bluelapiz.common.player.PlayerManager_legacy;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.bluelapiz.common.sql.callback.SQLUpdateCallback;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class EvidenceHelper {

    private static EvidenceHelper evidenceHelper;

    private EvidenceHelper() {
        SQLManager.getTableHelper().addTable("evidence",
                "CREATE TABLE %table (" +
                        "   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                        "   uuid CHAR(32) NOT NULL," +
                        "   by_uuid CHAR(32) NOT NULL," +
                        "   time BIGINT UNSIGNED NOT NULL," +
                        "   reason ENUM('NO_REASON', 'ADVERTISING', 'BAD_BEHAVIOR_IN_CHAT', 'BAD_BEHAVIOR_IN_GAME', 'BUG_ABUSE', 'CROSS_TEAMING', 'GRIEF', 'HACK', 'IMPERSONATING', 'INAPPROPRIATE_NAME', 'INAPPROPRIATE_SKIN', 'MAP_ABUSE', 'PUNISHMENT_BYPASS', 'RACISM', 'REPORT_ABUSE', 'SPAM', 'SPAWN_KILLING', 'TARGETING', 'X_RAY', 'CUSTOM') NOT NULL," +
                        "   evidence VARCHAR(200) NOT NULL" +
                        ");");
        SQLManager.getTableHelper().createTablesNotExisting();
    }

    public static EvidenceHelper get() {
        if (evidenceHelper == null) evidenceHelper = new EvidenceHelper();
        return evidenceHelper;
    }

    public void getAllEvidenceFrom(String name, EvidenceCallback callback) {
        PlayerManager_legacy.get().getOrLoadPlayer(name, new BLPlayerCallback() {
            @Override
            public void success(BLPlayer_legacy player) {
                try {
                    LinkedList<Map<String, Object>> result = SQLManager.getSQLHelper_legacy().selectSync(
                            "SELECT evidence.*, player.name FROM evidence, player WHERE evidence.uuid = " + SQLData.ofUuid(player.getUuid()) + " AND time > " + SQLData.ofInteger(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(120)) + " AND evidence.by_uuid=player.uuid ORDER BY time DESC;");

                    LinkedList<Evidence> evidenceList = new LinkedList<>();

                    for (Map<String, Object> row : result) {
                        Evidence evidence = new Evidence(
                                (int) row.get("id"),
                                BLPlayer_legacy.bigIntegerToLong(row.get("time")),
                                BLPlayer_legacy.stringToUUID((String) row.get("uuid")),
                                BLPlayer_legacy.stringToUUID((String) row.get("by_uuid")),
                                (String) row.get("name"),
                                PunishReason.valueOf((String) row.get("reason")),
                                (String) row.get("evidence"));
                        evidenceList.push(evidence);
                    }

                    callback.success(evidenceList);

                } catch (Exception e) {
                    callback.error(SQLExceptionType.ERROR, "MySQL Error! " + e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                callback.error(SQLExceptionType.ERROR, "Could find player in cache!");
            }
        });
    }

    public void insertEvidence(UUID uuid, UUID by_uuid, PunishReason punishReason, String evidence, SQLUpdateCallback callback) {

        SQLManager.getSQLHelper_legacy().insert("evidence", new HashMap<String, SQLData>() {{
            put("time", SQLData.ofInteger(System.currentTimeMillis()));
            put("uuid", SQLData.ofUuid(uuid));
            put("by_uuid", SQLData.ofUuid(by_uuid));
            put("reason", SQLData.ofString(punishReason.name()));
            put("evidence", SQLData.ofString(evidence));
        }}, callback);

    }

    public void deleteEvidence(int id, SQLUpdateCallback callback) {

        SQLManager.getSQLHelper_legacy().delete("evidence", new String[]{"id = " + SQLData.ofInteger(id)}, null, callback);

    }
}
