/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.util;

public class BLCodeCompilerException extends Exception {

    public BLCodeCompilerException(String message) {
        super(message);
    }

    public BLCodeCompilerException(String message, Throwable cause) {
        super(message, cause);
    }

    public BLCodeCompilerException(Throwable cause) {
        super(cause);
    }

    protected BLCodeCompilerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
