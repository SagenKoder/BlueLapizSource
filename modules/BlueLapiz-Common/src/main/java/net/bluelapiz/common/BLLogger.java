/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:24 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common;

public abstract class BLLogger {

    public static BLLogger commonLogger;

    protected String pluginName;

    protected BLLogger(String pluginName) {
        this.pluginName = pluginName;
    }

    public abstract void info(String... strings);

    public abstract void info(Object... objects);

    public abstract void debug(String... strings);

    public abstract void debug(Object... objects);

    public abstract void severe(Object object, Throwable e);

    public abstract void severe(String string, Throwable e);

}
