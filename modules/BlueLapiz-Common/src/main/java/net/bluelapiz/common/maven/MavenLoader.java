package net.bluelapiz.common.maven;

import com.google.common.collect.Maps;
import net.bluelapiz.common.BLLogger;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public final class MavenLoader {

    private static MavenLoader instance;

    private static Method method;
    private static URLClassLoader classLoader = ((URLClassLoader) ClassLoader.getSystemClassLoader());

    private static boolean working = true;

    static {
        try {
            method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
        } catch (NoSuchMethodException e) {
            BLLogger.commonLogger.debug("Failed to initialize URLClassLoader, Dependencies will not be loaded!");
            e.printStackTrace();

            working = false;
        }
    }

    private final Map<String, MavenDependency> dependencies = Maps.newHashMap();
    private File dependencyFolder;

    private MavenLoader(File dataFolder) {
        instance = this;
        if (!working) return;

        dependencyFolder = new File(dataFolder, "Dependencies");
        if (!dependencyFolder.exists()) dependencyFolder.mkdirs();
    }

    public static void setup(File dataFolder) {
        instance = new MavenLoader(dataFolder);
    }

    public static MavenLoader get() {
        return instance;
    }

    public void loadCommonLibriaries() {
        load("5.1", "com.j256.ormlite", "ormlite-core");
        load("5.1", "com.j256.ormlite", "ormlite-jdbc");
        load("1.7.25", "org.slf4j", "slf4j-simple");
        load("3.2.0", "com.zaxxer", "HikariCP");
        load("1.1.1", "com.googlecode.json-simple", "json-simple");
    }

    public void load(String version, String groupId, String artifactId) {
        load(new MavenDependency(version, groupId, artifactId));
    }

    public void load(MavenDependency dependency) {
        load(dependency, () -> {
        });
    }

    public void load(MavenDependency dependency, Runnable whenDone) {
        MavenUrl.download(dependency, new File(dependencyFolder, dependency.getGroupId()), (jar, pom) -> {

            if (!jar.exists() || !pom.exists())
                BLLogger.commonLogger.debug("POM File Downloaded -> " + pom.exists() + " Jar File Downloaded -> " + jar.exists());
            else {
                loadJar(dependency, jar);
                loadChildren(dependency, pom, whenDone);
            }
        });
    }

    public Optional<MavenDependency> get(String pomName) {
        return Optional.ofNullable(dependencies.get(pomName.toLowerCase()));
    }

    private void loadChildren(MavenDependency dependency, File pomFile, Runnable whenDone) {
        BLLogger.commonLogger.debug("Loading child dependencies of " + dependency.getPomName());
        List<MavenDependency> children = MavenXml.readDependencies(pomFile);
        if (children.isEmpty()) {
            whenDone.run();
            return;
        }

        final int[] loaded = {0};

        children.forEach(child -> {
            child.setParent(dependency);

            load(child, () -> {
                if (++loaded[0] == children.size()) {
                    BLLogger.commonLogger.debug("Finished loading children from " + dependency.getPomName());
                    whenDone.run();
                }
            });
        });
    }

    private void loadJar(MavenDependency dependency, File jarFile) {
        try {
            method.invoke(classLoader, jarFile.toURI().toURL());
            BLLogger.commonLogger.debug("Added " + jarFile.getName() + " to ClassLoader");
            dependencies.put(dependency.getPomName().toLowerCase(), dependency);
        } catch (Exception e) {
            BLLogger.commonLogger.severe("Failed to load Jar File " + jarFile.getName(), e);
            e.printStackTrace();
        }
    }

}
