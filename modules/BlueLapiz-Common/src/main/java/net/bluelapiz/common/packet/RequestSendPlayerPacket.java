/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import java.util.UUID;

public class RequestSendPlayerPacket extends Packet {

    private UUID player;
    private String server;

    public RequestSendPlayerPacket(UUID player, String server) {
        this.player = player;
        this.server = server;
    }

    public UUID getPlayer() {
        return player;
    }

    public String getServer() {
        return server;
    }

    @Override
    public String toString() {
        return "RequestSendPlayerPacket{" +
                "player=" + player +
                ", server='" + server + '\'' +
                '}';
    }
}
