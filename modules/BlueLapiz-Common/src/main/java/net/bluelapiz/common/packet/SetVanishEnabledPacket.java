/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import java.util.UUID;

public class SetVanishEnabledPacket extends Packet {

    private UUID uuid;
    private boolean enableVanish;

    public SetVanishEnabledPacket(UUID uuid, boolean enableVanish) {
        this.uuid = uuid;
        this.enableVanish = enableVanish;
    }

    public UUID getUuid() {
        return uuid;
    }

    public boolean isEnableVanish() {
        return enableVanish;
    }

    @Override
    public String toString() {
        return "SetVanishEnabledPacket{" +
                "uuid=" + uuid +
                ", enableVanish=" + enableVanish +
                '}';
    }
}
