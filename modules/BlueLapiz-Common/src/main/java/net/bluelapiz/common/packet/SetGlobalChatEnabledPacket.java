/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

public class SetGlobalChatEnabledPacket extends Packet {

    private boolean setEnabled;

    SetGlobalChatEnabledPacket() {
    }

    public SetGlobalChatEnabledPacket(boolean setEnabled) {
        this.setEnabled = setEnabled;
    }

    public boolean isSetEnabled() {
        return setEnabled;
    }

    void setEnabled(boolean enabled) {
        this.setEnabled = enabled;
    }

    @Override
    public String toString() {
        return "SetGlobalChatEnabledPacket{" +
                "setEnabled=" + setEnabled +
                '}';
    }
}
