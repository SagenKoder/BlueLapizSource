/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.common.packet;

import com.google.common.io.ByteArrayDataInput;

import java.awt.*;
import java.util.UUID;

public class PacketHandler {

    public static Packet handle(UUID player, String from, ByteArrayDataInput data) {

        byte packetId = data.readByte();

        try {
            switch (packetId) {
                case 0x00: // RequestServerNamePacket
                    RequestServerNamePacket rsnp = new RequestServerNamePacket();
                    return rsnp;

                case 0x01: // ServerNamePacket
                    ServerNamePacket snp = new ServerNamePacket();
                    snp.setServerName(data.readUTF());
                    return snp;

                case 0x02: // SetGlobalChatEnabledPacket
                    SetGlobalChatEnabledPacket sgcenp = new SetGlobalChatEnabledPacket();
                    sgcenp.setEnabled(data.readBoolean());
                    return sgcenp;

                case 0x03: // SendDiscordMessagePacket
                    SendDiscordMessagePacket sdmp = new SendDiscordMessagePacket();
                    sdmp.setChannel(data.readLong());
                    sdmp.setColor(Color.decode(data.readUTF()));
                    sdmp.setTitle(data.readUTF());
                    sdmp.setMessage(data.readUTF());
                    return sdmp;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
