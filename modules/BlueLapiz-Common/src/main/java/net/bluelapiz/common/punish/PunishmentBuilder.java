/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:09 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.sql.callback.SQLSelectCallback;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class PunishmentBuilder {

    private BLPlayer_legacy player;
    private UUID by_uuid;
    private PunishReason reason;
    // only used if PunishReason.CUSTOM
    private PunishType type;
    private String desc;
    private long duration;
    private PunishmentBuilder(BLPlayer_legacy player) {
        this.player = player;
    }

    public static PunishmentBuilder getBuilderFor(BLPlayer_legacy player) {
        return new PunishmentBuilder(player);
    }

    public PunishmentBuilder setPunishedBy(UUID by_uuid) {
        this.by_uuid = by_uuid;
        return this;
    }

    public PunishmentBuilder setReason(PunishReason reason) {
        this.reason = reason;
        return this;
    }


    public PunishmentBuilder setCustomType(PunishType type) {
        this.type = type;
        return this;
    }

    public PunishmentBuilder setCustomDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public PunishmentBuilder setCustomDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public void execute(PunishCallback callback) {
        if (callback == null)
            throw new IllegalArgumentException("Callback cannot be null!");

        if (player == null || by_uuid == null || reason == null)
            throw new IllegalStateException("Player, by_uuid and reason has to be set!");

        if (reason == PunishReason.NO_REASON)
            throw new IllegalStateException("Cannot punish a player for no reason!");

        if (reason == PunishReason.CUSTOM) {
            if (type == null || desc == null || duration < 0)
                throw new IllegalStateException("Cannot punish a player for a custom reason without specifying a valid type, description and duration!");
        }

        if (reason == PunishReason.CUSTOM) {
            punish(player, by_uuid, type, reason, 0, desc, System.currentTimeMillis(), duration, callback);
        } else {
            String sql = "SELECT COUNT(*) AS count FROM punish_log WHERE uuid = " + SQLData.ofUuid(player.getUuid()) + " AND reason = " + SQLData.ofString(reason.name()) + " AND start > " + SQLData.ofInteger(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(120));

            SQLManager.getSQLHelper_legacy().select(sql, new SQLSelectCallback() {
                @Override
                public void success(LinkedList<Map<String, Object>> result) {
                    int number = (int) ((long) result.getFirst().get("count"));
                    PunishTypeDuration punishTypeDuration = reason.getPunishTypeDuration(number);
                    punish(player, by_uuid, punishTypeDuration.getType(), reason, number, desc, System.currentTimeMillis(), punishTypeDuration.getTime(), callback);
                }

                @Override
                public void failure(SQLExceptionType exceptionType, String errorMessage) {
                    BLLogger.commonLogger.severe("Exception trying to get number of punishments for player!\nError: " + errorMessage, new Exception());
                }
            });
        }
    }

    private void punish(BLPlayer_legacy player, UUID by_uuid, PunishType type, PunishReason reason, int number, String desc, long start, long duration, PunishCallback callback) {

        boolean punish = callback.beforePunish(player, new Punishment(player.getUuid(), by_uuid, type, reason, number, desc, start, duration, 0L));
        if (!punish) return;

        if (type != PunishType.NO_PUNISH && type != PunishType.KICK) {
            player.setPunishedBy(by_uuid);
            player.setPunishType(type);
            player.setPunishReason(reason);
            player.setPunishNumber(number);
            player.setPunishDescription(desc);
            player.setPunishStart(start);
            player.setPunishDuration(duration);
        }

        SQLManager.getSQLHelper().insert("punish_log", new HashMap<String, SQLData>() {{
            put("uuid", SQLData.ofUuid(player.getUuid()));
            put("by_uuid", SQLData.ofUuid(by_uuid));
            put("type", SQLData.ofString(type.name()));
            put("reason", SQLData.ofString(reason.name()));
            put("number", SQLData.ofInteger(number));
            put("punish_desc", SQLData.ofString(desc));
            put("start", SQLData.ofInteger(start));
            put("duration", SQLData.ofInteger(duration));
        }}, true).whenComplete((keys, ex) -> {
            if (ex != null) {
                BLLogger.commonLogger.severe("Failed to insert punish_log\nError: " + ex.getMessage(), ex);
            } else {
                BLLogger.commonLogger.info("Successfully inserted punish_log!");
                callback.onPunish(player, new Punishment(player.getUuid(), by_uuid, type, reason, number, desc, start, duration, keys.get(0)));
            }
        });
    }
}
