/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

public class ServerNamePacket extends Packet {

    String serverName;

    ServerNamePacket() {
    }

    public ServerNamePacket(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return serverName;
    }

    void setServerName(String name) {
        this.serverName = name;
    }

    @Override
    public String toString() {
        return "ServerNamePacket{" +
                "serverName='" + serverName + '\'' +
                '}';
    }
}
