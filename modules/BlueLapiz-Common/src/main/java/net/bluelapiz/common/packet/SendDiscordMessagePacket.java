/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import java.awt.*;

public class SendDiscordMessagePacket extends Packet {

    private long channel;
    private Color color;
    private String title;
    private String message;

    SendDiscordMessagePacket() {
    }

    public SendDiscordMessagePacket(long channel, Color color, String title, String message) {
        this.channel = channel;
        this.color = color;
        this.title = title;
        this.message = message;
    }

    public long getChannel() {
        return channel;
    }

    void setChannel(long channel) {
        this.channel = channel;
    }

    public Color getColor() {
        return color;
    }

    void setColor(Color color) {
        this.color = color;
    }

    public String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "SendDiscordMessagePacket{" +
                "channel=" + channel +
                ", color=" + color +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
