/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.LongAdder;

public class BLThreadFactory implements ThreadFactory {
    private final LongAdder counter = new LongAdder();

    private String prefix;

    public BLThreadFactory(String prefix) {
        this.prefix = prefix;
    }

    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r, prefix + counter.toString());
        counter.increment();
        return thread;
    }
}
