/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import java.util.UUID;

public class Evidence {

    private int id;
    private long time;
    private UUID uuid;
    private UUID by_uuid;
    private String by_name;
    private PunishReason punishReason;
    private String evidence;

    public Evidence(int id, long time, UUID uuid, UUID by_uuid, String by_name, PunishReason punishReason, String evidence) {
        this.id = id;
        this.time = time;
        this.uuid = uuid;
        this.by_uuid = by_uuid;
        this.by_name = by_name;
        this.punishReason = punishReason;
        this.evidence = evidence;
    }

    public int getId() {
        return id;
    }

    public long getTime() {
        return time;
    }

    public UUID getUuid() {
        return uuid;
    }

    public UUID getByUuid() {
        return by_uuid;
    }

    public String getByName() {
        return by_name;
    }

    public PunishReason getPunishReason() {
        return punishReason;
    }

    public String getEvidence() {
        return evidence;
    }
}
