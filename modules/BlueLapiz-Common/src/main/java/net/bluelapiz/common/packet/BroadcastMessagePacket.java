/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import net.bluelapiz.common.player.objects.Rank;

public class BroadcastMessagePacket extends Packet {

    private Rank minimumRank;
    private String message;

    public BroadcastMessagePacket(String message) {
        this(Rank.PLAYER, message);
    }

    public BroadcastMessagePacket(Rank minimumRank, String message) {
        this.minimumRank = minimumRank;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Rank getMinimumRank() {
        return minimumRank;
    }

    @Override
    public String toString() {
        return "BroadcastMessagePacket{" +
                "minimumRank=" + minimumRank +
                ", message='" + message + '\'' +
                '}';
    }
}
