/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.common.logging;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLManager;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public class ChatLogManager {

    private static ChatLogManager chatLogManager;

    private ChatLogManager() {
        SQLManager.getTableHelper().addTable("chat_log",
                "CREATE TABLE %table (" +
                        "sent_time TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP," +
                        "type ENUM('PUBLIC_CHAT', 'PRIVATE_MESSAGE', 'STAFF_CHAT', 'STAFF_HELP', 'BROADCAST', 'COMMAND')," +
                        "from_player CHAR(32) NOT NULL," +
                        "to_player CHAR(32) NULL," +
                        "server VARCHAR(32) NOT NULL," +
                        "message VARCHAR(256)," +
                        "PRIMARY KEY(sent_time, from_player)" +
                        ");"
        );
        SQLManager.getTableHelper().createTablesNotExisting();
    }

    public static ChatLogManager get() {
        if (chatLogManager == null) chatLogManager = new ChatLogManager();
        return chatLogManager;
    }

    public void logMessage(ChatType type, String server, UUID fromPlayer, UUID toPlayer, String message) {

        Objects.requireNonNull(server);
        Objects.requireNonNull(fromPlayer);
        Objects.requireNonNull(message);

        SQLManager.getSQLHelper().insert("chat_log", new HashMap<String, SQLData>() {{
            put("type", SQLData.ofString(type.toString()));
            put("from_player", SQLData.ofUuid(fromPlayer));
            put("to_player", toPlayer == null ? SQLData.ofNull() : SQLData.ofUuid(toPlayer));
            put("server", SQLData.ofString(server));
            put("message", SQLData.ofString(message));
        }}).whenComplete((r, e) -> {
            if (e != null) {
                String err = "Exception while inserting into chat log!\n";
                err += "ChatType: " + type + "\n";
                err += "Server: " + server + "\n";
                err += "fromPlayer: " + fromPlayer + "\n";
                err += "toPlayer: " + toPlayer + "\n";
                err += "message: " + message + "\n";
                err += "Error message: " + e.getMessage();
                BLLogger.commonLogger.severe(err, e);
                e.printStackTrace();
            }
        });

    }

    public enum ChatType {
        PUBLIC_CHAT,
        PRIVATE_MESSAGE,
        STAFF_CHAT,
        STAFF_HELP,
        BROADCAST,
        COMMAND
    }

}
