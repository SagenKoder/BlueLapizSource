/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import net.bluelapiz.common.player.objects.Rank;

import java.util.UUID;

public class PlayerRankChangePacket extends Packet {

    private UUID uuid;
    private Rank rank;

    public PlayerRankChangePacket(UUID uuid, Rank rank) {
        this.uuid = uuid;
        this.rank = rank;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Rank getRank() {
        return rank;
    }

    @Override
    public String toString() {
        return "PlayerRankChangePacket{" +
                "uuid=" + uuid +
                ", rank=" + rank +
                '}';
    }
}
