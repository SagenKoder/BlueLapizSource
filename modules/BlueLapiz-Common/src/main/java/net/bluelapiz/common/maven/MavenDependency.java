package net.bluelapiz.common.maven;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public final class MavenDependency {

    private final String version, groupId, artifactId;
    private final MavenOptions options;

    private MavenDependency parent = null;

    public MavenDependency(String version, String groupId, String artifactId, String customRepo, boolean alwaysUpdate) {
        this.version = version;
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.options = new MavenOptions(MavenUrl.fixUrl(customRepo), alwaysUpdate);
    }

    public MavenDependency(String version, String groupId, String artifactId) {
        this(version, groupId, artifactId, "", false);
    }

    public void setParent(MavenDependency parent) {
        this.parent = parent;
    }

    public boolean hasParent() {
        return getParent() != null;
    }

    public int getParentDepth() {
        int depth = 0;

        MavenDependency parent = getParent();
        while (parent != null) {
            parent = parent.getParent();
            depth++;
        }

        return depth;
    }

    public String getJarName() {
        return getArtifactId() + "-" + getVersion() + ".jar";
    }

    public String getPomName() {
        return getArtifactId() + "-" + getVersion() + ".pom";
    }
}
