/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.sql.callback;

import net.bluelapiz.common.sql.SQLExceptionType;

import java.util.LinkedList;
import java.util.Map;

/**
 * @deprecated
 */
public interface SQLSelectCallback {

    void success(LinkedList<Map<String, Object>> result);

    void failure(SQLExceptionType exceptionType, String errorMessage);

}
