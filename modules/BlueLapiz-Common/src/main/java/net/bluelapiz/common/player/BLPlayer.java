/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.common.player;

import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.punish.Punishment;

import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class BLPlayer implements Comparable {

    UUID uuid;
    String name;
    Rank rank;
    Rank tmpRank;
    Date rankExpires;
    Punishment punishment;

    public BLPlayer(UUID uuid, String name, Rank rank, Rank tmpRank, Date rankExpires) {
        this.uuid = uuid;
        this.name = name;
        this.rank = rank;

        // tmp rank is valid
        if (rankExpires != null && tmpRank != null && rankExpires.after(new Date())) {
            this.rankExpires = rankExpires;
            this.tmpRank = tmpRank;
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        PlayerManager.get().setNameInDatabase(uuid, name);
    }

    public Rank getRank() {
        if (tmpRank != null && rankExpires != null) {
            if (rankExpires.before(new Date())) {
                rankExpires = null;
                tmpRank = null;
                PlayerManager.get().setTempRankInDatabase(uuid, null, new Date(0));
            } else {
                return tmpRank;
            }
        }
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
        PlayerManager.get().setRankInDatabase(uuid, rank);
    }

    public Optional<Punishment> getPunishment() {
        if (punishment == null) return Optional.empty();
        return Optional.of(punishment);
    }

    public void setTempRank(Rank rank, Date expires) {
        this.tmpRank = rank;
        this.rankExpires = expires;
        PlayerManager.get().setTempRankInDatabase(uuid, rank, expires);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BLPlayer blPlayer = (BLPlayer) o;

        if (!uuid.equals(blPlayer.uuid)) return false;
        return rank == blPlayer.rank;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, name, rank);
    }

    @Override
    public int compareTo(Object o) {
        Objects.requireNonNull(o);
        if (!(o instanceof BLPlayer)) return Integer.compare(o.hashCode(), this.hashCode());
        return this.uuid.compareTo(((BLPlayer) o).uuid);
    }
}
