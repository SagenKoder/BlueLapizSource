/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import java.util.UUID;

public class GlobalChatPacket extends Packet {

    private String formattedMessage;
    private String originalMessage;
    private String fromServer;
    private UUID uuid;

    public GlobalChatPacket(String formattedMessage, String originalMessage, UUID uuid, String fromServer) {
        this.formattedMessage = formattedMessage;
        this.originalMessage = originalMessage;
        this.uuid = uuid;
        this.fromServer = fromServer;
    }

    public String getFormattedMessage() {
        return formattedMessage;
    }

    public String getOriginalMessage() {
        return originalMessage;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public String toString() {
        return "GlobalChatPacket{" +
                "formattedMessage='" + formattedMessage + '\'' +
                ", originalMessage='" + originalMessage + '\'' +
                ", fromServer='" + fromServer + '\'' +
                ", uuid=" + uuid +
                '}';
    }
}
