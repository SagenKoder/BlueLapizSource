/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import java.io.Serializable;
import java.util.UUID;

public class Punishment implements Serializable {

    UUID uuid;
    UUID by_uuid = null;
    PunishType type = PunishType.NO_PUNISH;
    PunishReason reason = PunishReason.NO_REASON;
    int number = 0;
    String desc = null;
    long start = 0L;
    long duration = 0L;
    long punishId = 0L;

    public Punishment(UUID uuid) {
        this.uuid = uuid;
    }

    public Punishment(UUID uuid, UUID by_uuid, PunishType type, PunishReason reason, int number, String desc, long start, long duration, long punishId) {
        this.uuid = uuid;
        this.by_uuid = by_uuid;
        this.type = type;
        this.reason = reason;
        this.number = number;
        this.desc = desc;
        this.start = start;
        this.duration = duration;
        this.punishId = punishId;
    }

    public UUID getUuid() {
        return uuid;
    }

    public UUID getBy_uuid() {
        return by_uuid;
    }

    public PunishType getType() {
        return type;
    }

    public PunishReason getReason() {
        return reason;
    }

    public int getNumber() {
        return number;
    }

    public String getDesc() {
        return desc;
    }

    public long getStart() {
        return start;
    }

    public long getDuration() {
        return duration;
    }

    public long getPunishId() {
        return punishId;
    }
}
