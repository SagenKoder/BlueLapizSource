/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.common.sql;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.util.BLExecutor;

import java.sql.*;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Helper class for basic mysql queries.
 */
@SuppressWarnings({"unused", "Duplicates"})
public class SQLHelperBasicImpl implements SQLHelper {

    SQLHelperBasicImpl() {
    }

    @Override
    public CompletableFuture<LinkedList<Map<String, Object>>> select(String sql) {
        BLLogger.commonLogger.debug("SQL SELECT: " + sql);
        Objects.requireNonNull(sql, "sql cannot be null!");
        return CompletableFuture.supplyAsync(() -> {
            LinkedList<Map<String, Object>> rows = new LinkedList<>();
            try (Connection conn = SQLManager.getMySQLMan().getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql);
                 ResultSet rs = ps.executeQuery()) {
                return getDataFromResultSet(rs);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Exception while executing select statement! Error: " + e.getMessage());
            }
        }, BLExecutor.get().getMySQLExecutor());
    }

    @Override
    public CompletableFuture<Integer> update(String sql) {
        BLLogger.commonLogger.debug("SQL UPDATE: " + sql);
        Objects.requireNonNull(sql, "sql cannot be null!");
        return CompletableFuture.supplyAsync(() -> {
            int rowsAffected = 0;
            try (Connection conn = SQLManager.getMySQLMan().getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql)) {
                rowsAffected = ps.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Exception while executing update statement! Error: " + e.getMessage());
            }
            return rowsAffected;
        }, BLExecutor.get().getMySQLExecutor());
    }

    @Override
    public CompletableFuture<Integer> update(String table, SQLData booleanExpression, Map<String, SQLData> values) {
        Objects.requireNonNull(table, "table cannot be null!");
        Objects.requireNonNull(booleanExpression, "booleanExpression cannot be null! Use SQLData.ofBoolean(true) as expression if you want it to always be true!");
        Objects.requireNonNull(values, "values cannot be null!");
        return CompletableFuture.supplyAsync(() -> {

            if (values.size() < 1) {
                throw new IllegalArgumentException("No new values is set!");
            }

            StringBuilder sql = new StringBuilder("UPDATE " + table + " SET ");

            // add update clause
            boolean first = true;
            for (Map.Entry<String, SQLData> e : values.entrySet()) {
                if (!first) sql.append(", ");
                first = false;
                sql.append(e.getKey()).append(" = ").append(e.getValue());
            }

            // add WHERE clause
            if (booleanExpression.getDataType() != SQLDataType.BOOLEAN_EXPRESSION)
                throw new IllegalArgumentException("Where clause has to be a boolean expression!");
            sql.append(" WHERE  ");
            sql.append(booleanExpression.toString());

            sql.append(";");
            return sql.toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose(this::update);
    }

    @Override
    public CompletableFuture<LinkedList<Map<String, Object>>> selectAllRowsFrom(String table) {
        return select(table, null, SQLData.ofBoolean(true), -1, SQLOrder.NONE);
    }

    @Override
    public CompletableFuture<Map<String, Object>> selectOneRowFrom(String table) {
        return select(table, null, SQLData.ofBoolean(true), 1, SQLOrder.NONE)
                .thenApply(LinkedList::getFirst);
    }

    @Override
    public CompletableFuture<LinkedList<Map<String, Object>>> select(String table, List<String> columns, SQLData booleanExpression) {
        return select(table, columns, booleanExpression, 0, SQLOrder.NONE);
    }

    @Override
    public CompletableFuture<LinkedList<Map<String, Object>>> select(String table, List<String> columns, SQLData booleanExpression, int limit, SQLOrder order, String... orderBy) {
        Objects.requireNonNull(table, "table cannot be null!");
        Objects.requireNonNull(columns, "columns cannot be null!");
        Objects.requireNonNull(booleanExpression, "booleanExpression cannot be null! Use SQLData.ofBoolean(true) as expression if you want it to always be true!");
        if (order != null && order != SQLOrder.NONE)
            Objects.requireNonNull(orderBy, "orderBy cannot be null if order is set!");
        return CompletableFuture.supplyAsync(() -> {

            // build initial sql
            List<String> col = columns;
            if (col.size() == 0) {
                col = Collections.singletonList("*");
            }
            StringBuilder sql = new StringBuilder("SELECT " + String.join(", ", col) + " FROM " + table + "");

            // add WHERE clause
            if (booleanExpression.getDataType() != SQLDataType.BOOLEAN_EXPRESSION)
                throw new IllegalArgumentException("Where clause has to be a boolean expression!");
            sql.append(" WHERE  ");
            sql.append(booleanExpression.toString());

            // add LIMIT clause
            if (limit > 0) {
                sql.append(" LIMIT ").append(limit);
            }

            // add ORDER BY clause
            if (order != SQLOrder.NONE && orderBy.length > 0) {
                String orderSql = " ORDER BY " + String.join(", ", orderBy) + (order == SQLOrder.ASC ? " ASC" : " DESC");
                sql.append(orderSql);
            }
            sql.append(";");

            return sql.toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose(this::select);
    }

    @Override
    public CompletableFuture<Integer> delete(String table, SQLData booleanExpression) {
        Objects.requireNonNull(table, "table cannot be null!");
        Objects.requireNonNull(booleanExpression, "booleanExpression cannot be null! Use SQLData.ofBoolean(true) as expression if you want it to always be true!");
        return CompletableFuture.supplyAsync(() -> {
            StringBuilder sql = new StringBuilder("DELETE FROM " + table);

            // add WHERE clause
            if (booleanExpression.getDataType() != SQLDataType.BOOLEAN_EXPRESSION)
                throw new IllegalArgumentException("Where clause has to be a boolean expression!");
            sql.append(" WHERE  ");
            sql.append(booleanExpression.toString());

            return sql.append(";").toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose(this::update);
    }

    @Override
    public CompletableFuture<Long> countRows(String table, SQLData booleanExpression) {
        Objects.requireNonNull(table, "table cannot be null!");
        Objects.requireNonNull(booleanExpression, "booleanExpression cannot be null! Use SQLData.ofBoolean(true) as expression if you want it to always be true!");
        return CompletableFuture.supplyAsync(() -> {

            StringBuilder sql = new StringBuilder("SELECT COUNT(*) AS COUNT FROM " + table);

            // where clause
            if (booleanExpression != null) {
                if (booleanExpression.getDataType() != SQLDataType.BOOLEAN_EXPRESSION)
                    throw new IllegalArgumentException("Where clause has to be a boolean expression!");
                sql.append(" WHERE  ");
                sql.append(booleanExpression.toString());
            }
            sql.append(";");
            return sql.toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose(this::select).thenApply(v -> {
            if (v.size() == 0) {
                throw new RuntimeException("No result returned in count statuement!");
            }
            return (Long) v.getFirst().get("COUNT");
        });
    }

    @Override
    public CompletableFuture<List<Integer>> insert(String table, Map<String, SQLData> values) {
        return insert(table, values, true);
    }

    @Override
    public CompletableFuture<List<Integer>> insert(String table, Map<String, SQLData> values, boolean replace) {
        Objects.requireNonNull(table, "table cannot be null!");
        Objects.requireNonNull(values, "values cannot be null!");
        return CompletableFuture.supplyAsync(() -> {
            if (values.size() < 1) {
                throw new IllegalArgumentException("No where clause set!");
            }

            StringBuilder sql;

            if (replace) sql = new StringBuilder("REPLACE INTO " + table);
            else sql = new StringBuilder("INSERT INTO " + table);

            StringBuilder columns = new StringBuilder("(");
            StringBuilder vals = new StringBuilder("(");
            boolean first = true;
            for (Map.Entry<String, SQLData> e : values.entrySet()) {
                if (!first) {
                    columns.append(", ");
                    vals.append(", ");
                }
                first = false;
                columns.append(e.getKey());
                vals.append(e.getValue());
            }
            columns.append(")");
            vals.append(")");
            sql.append(columns).append(" VALUES ").append(vals).append(";");
            return sql.toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose((String sql) -> CompletableFuture.supplyAsync(() -> {
            try (Connection conn = SQLManager.getMySQLMan().getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                List<Integer> keys = new ArrayList<>();
                while (rs.next()) {
                    keys.add(rs.getInt(1));
                }
                return keys;
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Exception while executing update statement! Error: " + e.getMessage());
            }
        }));
    }

    public CompletableFuture<List<Integer>> insertMultiple(String table, List<String> columns, List<List<SQLData>> rows, boolean replace) {
        Objects.requireNonNull(table, "table cannot be null!");
        Objects.requireNonNull(columns, "values cannot be null!");
        Objects.requireNonNull(rows, "values cannot be null!");

        return CompletableFuture.supplyAsync(() -> {
            if (columns.size() < 1) {
                throw new IllegalArgumentException("No columns set!");
            }

            if (rows.size() < 1) {
                throw new IllegalArgumentException("No rows set!");
            }

            StringBuilder sql;

            if (replace) sql = new StringBuilder("REPLACE INTO " + table);
            else sql = new StringBuilder("INSERT INTO " + table);

            StringBuilder col = new StringBuilder("(");
            boolean first = true;
            for (String column : columns) {
                if (!first) {
                    col.append(", ");
                }
                first = false;
                col.append(column);
            }
            col.append(")");
            sql.append(col).append(" VALUES ");

            boolean firstRow = true;
            for (List<SQLData> row : rows) {
                if (!firstRow) sql.append(", ");
                firstRow = false;
                if (row.size() != columns.size())
                    throw new IllegalArgumentException("The number of columns and number of data in each row is not equal!");
                sql.append("(");
                first = true;
                for (SQLData colData : row) {
                    if (!first) sql.append(", ");
                    first = false;
                    sql.append(colData);
                }
                sql.append(")");
            }
            return sql.append(";").toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose((String sql) -> CompletableFuture.supplyAsync(() -> {
            try (Connection conn = SQLManager.getMySQLMan().getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                List<Integer> keys = new ArrayList<>();
                while (rs.next()) {
                    keys.add(rs.getInt(1));
                }
                return keys;
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Exception while executing update statement! Error: " + e.getMessage());
            }
        }));
    }

    @Override
    public CompletableFuture<Integer> updateMultipleRowsAddNumber(String table, String keyToChange, String columnToChange, Map<String, Integer> valuesToAdd) {
        Objects.requireNonNull(table, "Table cannot be null!");
        Objects.requireNonNull(keyToChange, "keyToChange cannot be null!");
        Objects.requireNonNull(columnToChange, "columnToChange cannot be null!");
        Objects.requireNonNull(valuesToAdd, "valuesToAdd cannot be null!");
        return CompletableFuture.supplyAsync(() -> {
            if (valuesToAdd.size() == 0) {
                throw new IllegalArgumentException("No new values is set!");
            }

            StringBuilder sql = new StringBuilder("UPDATE " + table + " SET " + columnToChange + " = " + columnToChange + " + (CASE " + keyToChange);

            for (Map.Entry<String, Integer> e : valuesToAdd.entrySet()) {
                sql.append(" WHEN " + e.getKey() + " THEN " + e.getValue());
            }

            sql.append(" END) WHERE " + keyToChange + " IN (" + String.join(", ", valuesToAdd.keySet()) + ");");
            return sql.toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose(this::update);
    }

    @Override
    public CompletableFuture<Integer> updateMultipleRowsSetValue(String table, String keyToChange, String columnToChange, Map<String, SQLData> valuesToSet) {
        Objects.requireNonNull(table, "Table cannot be null!");
        Objects.requireNonNull(keyToChange, "keyToChange cannot be null!");
        Objects.requireNonNull(columnToChange, "columnToChange cannot be null!");
        Objects.requireNonNull(valuesToSet, "valuesToSet cannot be null!");
        return CompletableFuture.supplyAsync(() -> {
            if (valuesToSet.size() == 0) {
                throw new IllegalArgumentException("No new values is set!");
            }

            StringBuilder sql = new StringBuilder("UPDATE " + table + " SET " + columnToChange + " = (CASE " + keyToChange);

            for (Map.Entry<String, SQLData> e : valuesToSet.entrySet()) {
                sql.append(" WHEN " + e.getKey() + " THEN " + e.getValue());
            }

            sql.append(" END) WHERE " + keyToChange + " IN (" + String.join(", ", valuesToSet.keySet()) + ");");
            return sql.toString();
        }, BLExecutor.get().getMySQLExecutor()).thenCompose(this::update);
    }

    @Override
    public CompletableFuture<List<String>> getTables() {
        return CompletableFuture.supplyAsync(() -> {
            try (Connection conn = SQLManager.getMySQLMan().getConnection()) {
                DatabaseMetaData dbm = conn.getMetaData();
                ResultSet rs = dbm.getTables(conn.getCatalog().toLowerCase(), null, "%", new String[]{"TABLE"});
                ArrayList<String> list = new ArrayList<>();
                while (rs != null && rs.next()) {
                    list.add(rs.getString("TABLE_NAME").toLowerCase());
                }
                rs.close();
                return list;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }, BLExecutor.get().getMySQLExecutor());
    }

    private LinkedList<Map<String, Object>> getDataFromResultSet(ResultSet rs) throws SQLException {
        Objects.requireNonNull(rs, "Cannot get data from a null ResultSet!");
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        LinkedList<Map<String, Object>> rows = new LinkedList<>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<>();
            for (int i = 1; i <= columns; i++) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            rows.push(row);
        }
        BLLogger.commonLogger.debug("SQL Returned set with " + rows.size() + " rows");
        return rows;
    }
}
