package net.bluelapiz.common.ornlitetest;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Data
@NoArgsConstructor
@ToString
@DatabaseTable(tableName = "orm_player")
public class ORMPlayer {

    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField(uniqueIndex = true, dataType = DataType.UUID, canBeNull = false)
    private UUID uuid;

    @DatabaseField(canBeNull = false)
    private String name;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @DatabaseField(foreign = true)
    private ORMPunishment activePunishment = null;

    public ORMPlayer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }
}
