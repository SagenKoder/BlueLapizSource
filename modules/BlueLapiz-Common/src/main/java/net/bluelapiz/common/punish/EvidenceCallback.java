/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import net.bluelapiz.common.sql.SQLExceptionType;

import java.util.LinkedList;

public interface EvidenceCallback {

    void success(LinkedList<Evidence> evidence);

    void error(SQLExceptionType exceptionType, String message);

}
