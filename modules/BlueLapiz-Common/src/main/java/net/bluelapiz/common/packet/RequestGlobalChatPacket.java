/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.packet;

import java.util.UUID;

public class RequestGlobalChatPacket extends Packet {

    UUID playerUuid;
    String playerName;
    String originalMessage;

    public RequestGlobalChatPacket(UUID playerUuid, String playerName, String originalMessage) {
        this.playerUuid = playerUuid;
        this.playerName = playerName;
        this.originalMessage = originalMessage;
    }

    public UUID getPlayerUuid() {
        return playerUuid;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getOriginalMessage() {
        return originalMessage;
    }

    @Override
    public String toString() {
        return "RequestGlobalChatPacket{" +
                "playerUuid=" + playerUuid +
                ", playerName='" + playerName + '\'' +
                ", originalMessage='" + originalMessage + '\'' +
                '}';
    }
}
