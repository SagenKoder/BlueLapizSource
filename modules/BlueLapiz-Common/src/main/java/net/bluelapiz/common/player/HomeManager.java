/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:09 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.player.objects.Home;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class HomeManager {

    private static ConcurrentHashMap<String, HomeManager> homeManagers = new ConcurrentHashMap<>();
    private String server;
    private HashMap<UUID, LinkedList<Home>> playerHomes = new HashMap<>();
    public HomeManager(String server) {
        this.server = server;
    }

    public static HomeManager get(String server) {
        if (homeManagers.size() == 0) {
            SQLManager.getTableHelper().addTable("home",
                    "CREATE TABLE %table (" +
                            "   server VARCHAR(20) NOT NULL," +
                            "   uuid CHAR(32) NOT NULL," +
                            "   name VARCHAR(20) NOT NULL," +
                            "   world VARCHAR(20) NOT NULL," +
                            "   x MEDIUMINT NOT NULL," +
                            "   y TINYINT UNSIGNED NOT NULL," +
                            "   z MEDIUMINT NOT NULL," +
                            "   yaw FLOAT(8, 4) NOT NULL," +
                            "   pitch FLOAT(8, 4) NOT NULL," +
                            "   time BIGINT UNSIGNED NOT NULL," +
                            "   PRIMARY KEY(server, uuid, name)" +
                            ")");
            SQLManager.getTableHelper().createTablesNotExisting();
        }
        if (!homeManagers.containsKey(server.toLowerCase()))
            homeManagers.put(server.toLowerCase(), new HomeManager(server));
        return homeManagers.get(server);
    }

    public void loadHomesOf(UUID uuid) {
        SQLManager.getSQLHelper().select("home", Collections.singletonList("*"),
                SQLData.ofKeyword("server").equalsOther(SQLData.ofString(server))
                        .and(SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid)))
        ).thenAccept(v -> {
            LinkedList<Home> homes = new LinkedList<>();
            for (Map<String, Object> row : v) {
                Home home = new Home(uuid, row);
                homes.add(home);
            }
            playerHomes.put(uuid, homes);
        }).exceptionally(e -> {
            BLLogger.commonLogger.severe("Could not load homes of player " + uuid + "!\nError: " + e.getMessage(), e);
            e.printStackTrace();
            return null;
        });
    }

    public void unloadHomesOf(UUID uuid) {
        playerHomes.remove(uuid);
    }

    public Home getHomeOf(UUID uuid, String home) {
        if (getAllHomesOf(uuid).stream()
                .filter(h -> h.getName().equalsIgnoreCase(home)).count() == 0) return null;
        return playerHomes.getOrDefault(uuid, new LinkedList<>()).stream()
                .filter(h -> h.getName().equalsIgnoreCase(home))
                .findFirst().orElseGet(null);
    }

    public LinkedList<Home> getAllHomesOf(UUID uuid) {
        if (!playerHomes.containsKey(uuid)) playerHomes.put(uuid, new LinkedList<>());
        return playerHomes.get(uuid);
    }

    public void createHome(UUID uuid, String name, String world, int x, int y, int z, float yaw, float pitch) {
        Home home = new Home(System.currentTimeMillis(), uuid, name, world, x, y, z, yaw, pitch);

        SQLManager.getSQLHelper().insert("home", new HashMap<String, SQLData>() {{
            put("server", SQLData.ofString(server));
            put("uuid", SQLData.ofUuid(uuid));
            put("name", SQLData.ofString(name));
            put("world", SQLData.ofString(world));
            put("x", SQLData.ofInteger(x));
            put("y", SQLData.ofInteger(y));
            put("z", SQLData.ofInteger(z));
            put("yaw", SQLData.ofDouble(yaw));
            put("pitch", SQLData.ofDouble(pitch));
            put("time", SQLData.ofInteger(home.getCreated()));
        }}).thenAccept(v -> {
            if (!playerHomes.containsKey(uuid)) playerHomes.put(uuid, new LinkedList<>());
            playerHomes.get(uuid).add(home);
            BLLogger.commonLogger.debug("Created home " + name + " for player " + uuid.toString());
        }).exceptionally(e -> {
            BLLogger.commonLogger.severe("Exception trying to create home for player + " + uuid + "!\nError: " + e.getMessage(), e);
            e.printStackTrace();
            return null;
        });
    }

    public void deleteHome(UUID uuid, String name) {
        Home home = null;
        for (Home h : getAllHomesOf(uuid))
            if (h.getName().equalsIgnoreCase(name)) home = h;
        if (home != null)
            playerHomes.get(uuid).remove(home);

        SQLManager.getSQLHelper().delete("home",
                SQLData.ofKeyword("server").equalsOther(SQLData.ofString(server))
                        .and(SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid)))
                        .and(SQLData.ofKeyword("name").equalsOther(SQLData.ofString(name)))
        ).thenAccept(v -> BLLogger.commonLogger.info("Deleted " + v + " from home for player " + uuid)
        ).exceptionally(e -> {
            BLLogger.commonLogger.severe("Error trying to delete a home for player " + uuid + "!\nError: " + e.getMessage(), e);
            e.printStackTrace();
            return null;
        });
    }

}
