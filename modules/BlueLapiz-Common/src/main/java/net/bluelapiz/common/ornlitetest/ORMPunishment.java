package net.bluelapiz.common.ornlitetest;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import net.bluelapiz.common.punish.PunishReason;
import net.bluelapiz.common.punish.PunishType;

import java.util.Date;

@Data
@NoArgsConstructor
@ToString
@DatabaseTable(tableName = "orm_punishment")
public class ORMPunishment {

    @DatabaseField(generatedId = true)
    long id;

    @DatabaseField(canBeNull = false, foreign = true, columnDefinition = "integer references player(id) on delete cascade")
    ORMPlayer player;

    @DatabaseField(canBeNull = false, foreign = true, columnDefinition = "integer references punisher(id) on delete cascade")
    ORMPlayer punisher;

    @DatabaseField(canBeNull = false, dataType = DataType.ENUM_STRING)
    PunishReason reason;

    @DatabaseField(canBeNull = false, dataType = DataType.ENUM_STRING)
    PunishType action;

    @DatabaseField(canBeNull = false, dataType = DataType.DATE_LONG)
    Date timeStart;

    @DatabaseField(canBeNull = false, dataType = DataType.DATE_LONG)
    Date timeEnd;

    public ORMPunishment(ORMPlayer player, ORMPlayer punisher, PunishReason reason, PunishType action, Date timeStart, Date timeEnd) {
        this.player = player;
        this.punisher = punisher;
        this.reason = reason;
        this.action = action;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
    }
}
