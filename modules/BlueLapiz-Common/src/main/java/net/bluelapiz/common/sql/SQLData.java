/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.sql;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class SQLData {

    private String data;
    private SQLDataType dataType;

    private SQLData(String data, SQLDataType dataType) {
        Objects.requireNonNull(data);
        Objects.requireNonNull(dataType);
        this.data = data;
        this.dataType = dataType;
    }

    public static UUID stringToUuid(String uuid) {
        if (uuid == null) return null;
        return UUID.fromString(uuid.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }

    public static long bigIntegerToLong(Object object) {
        return ((BigInteger) object).longValue();
    }

    public static Timestamp dateToTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    public static SQLData ofTimestamp(Date date) {
        return SQLData.ofString(dateToTimestamp(date).toString());
    }

    public static SQLData ofKeyword(String keyword) {
        Objects.requireNonNull(keyword);
        return new SQLData(keyword, SQLDataType.KEYWORD);
    }

    public static SQLData ofNull() {
        return new SQLData("NULL", SQLDataType.STRING);
    }

    public static SQLData ofString(String string) {
        if (string == null) return ofNull();
        string = string.replace("'", "''");
        return new SQLData("'" + string + "'", SQLDataType.STRING);
    }

    public static SQLData ofInteger(long data) {
        return new SQLData(String.valueOf(data), SQLDataType.NUMERAL);
    }

    public static SQLData ofDouble(double data) {
        return new SQLData(String.valueOf(data), SQLDataType.NUMERAL);
    }

    public static SQLData ofUuid(UUID uuid) {
        Objects.requireNonNull(uuid);
        return new SQLData("'" + uuid.toString().replace("-", "") + "'", SQLDataType.STRING);
    }

    public static SQLData ofUuid(String uuid) {
        Objects.requireNonNull(uuid);
        return new SQLData("'" + uuid.replace("-", "") + "'", SQLDataType.STRING);
    }

    public static SQLData ofBoolean(boolean bool) {
        return new SQLData(bool ? "TRUE" : "FALSE", SQLDataType.BOOLEAN_EXPRESSION);
    }

    @Override
    public String toString() {
        return data;
    }

    public SQLDataType getDataType() {
        return dataType;
    }

    public SQLData equalsOther(SQLData other) {
        Objects.requireNonNull(other);
        return new SQLData(this.toString() + " = " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }

    public SQLData lessThanOther(SQLData other) {
        Objects.requireNonNull(other);
        return new SQLData(this.toString() + " < " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }

    public SQLData lessThanOrEqualsOther(SQLData other) {
        Objects.requireNonNull(other);
        return new SQLData(this.toString() + " <= " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }

    public SQLData greaterThanOther(SQLData other) {
        Objects.requireNonNull(other);
        return new SQLData(this.toString() + " > " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }

    public SQLData greaterThanOrEqualsOther(SQLData other) {
        Objects.requireNonNull(other);
        return new SQLData(this.toString() + " >= " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }

    public SQLData or(SQLData other) {
        Objects.requireNonNull(other);
        if (this.dataType != SQLDataType.BOOLEAN_EXPRESSION)
            throw new IllegalArgumentException("Can only OR boolean expressions!");
        if (other.dataType != SQLDataType.BOOLEAN_EXPRESSION)
            throw new IllegalArgumentException("Can only OR boolean expressions!");
        return new SQLData(this.toString() + " OR " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }

    public SQLData and(SQLData other) {
        Objects.requireNonNull(other);
        if (this.dataType != SQLDataType.BOOLEAN_EXPRESSION)
            throw new IllegalArgumentException("Can only AND boolean expressions!");
        if (other.dataType != SQLDataType.BOOLEAN_EXPRESSION)
            throw new IllegalArgumentException("Can only AND boolean expressions!");
        return new SQLData(this.toString() + " AND " + other.toString(), SQLDataType.BOOLEAN_EXPRESSION);
    }
}
