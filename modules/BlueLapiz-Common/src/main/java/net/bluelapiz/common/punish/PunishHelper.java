/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.sql.SQLManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PunishHelper {

    public static PunishHelper punishHelper;

    private PunishHelper() {
        SQLManager.getTableHelper().addTable("punish_log",
                "CREATE TABLE %table (\n" +
                        "   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY," +
                        "   uuid CHAR(32) NOT NULL,\n" +
                        "   by_uuid CHAR(32) NOT NULL,\n" +
                        "   type ENUM('NO_PUNISH','KICK','BAN','MUTE','PERMMUTE','PERMBAN') NOT NULL DEFAULT 'NO_PUNISH',\n" +
                        "   reason ENUM('NO_REASON', 'ADVERTISING', 'BAD_BEHAVIOR_IN_CHAT', 'BAD_BEHAVIOR_IN_GAME', 'BUG_ABUSE', 'CROSS_TEAMING', 'GRIEF', 'HACK', 'IMPERSONATING', 'INAPPROPRIATE_NAME', 'INAPPROPRIATE_SKIN', 'MAP_ABUSE', 'PUNISHMENT_BYPASS', 'RACISM', 'REPORT_ABUSE', 'SPAM', 'SPAWN_KILLING', 'TARGETING', 'X_RAY', 'CUSTOM') NOT NULL DEFAULT 'NO_REASON',\n" +
                        "   number INT(3) NOT NULL DEFAULT 0,\n" +
                        "   punish_desc VARCHAR(200) DEFAULT NULL,\n" +
                        "   start BIGINT UNSIGNED NOT NULL,\n" +
                        "   duration BIGINT UNSIGNED NOT NULL DEFAULT 0\n" +
                        ");");
        SQLManager.getTableHelper().createTablesNotExisting();
    }

    public static PunishHelper get() {
        if (punishHelper == null) punishHelper = new PunishHelper();
        return punishHelper;
    }

    public PunishType getActivePunishType(BLPlayer_legacy blPlayer) {
        if (blPlayer.getPunishType() != PunishType.NO_PUNISH &&
                ((blPlayer.getPunishStart() + blPlayer.getPunishDuration() > System.currentTimeMillis() || blPlayer.getPunishType().isPermanent()))) {
            return blPlayer.getPunishType();
        }
        if (blPlayer.getPunishType() != PunishType.NO_PUNISH || blPlayer.getPunishDuration() != 0L || blPlayer.getPunishDescription() != null) {
            // remove all punish data from player
            clearPunishmentFrom(blPlayer);
        }
        return PunishType.NO_PUNISH;
    }

    public void clearPunishmentFrom(BLPlayer_legacy player) {
        player.setPunishType(PunishType.NO_PUNISH);
        player.setPunishDescription(null);
        player.setPunishReason(PunishReason.NO_REASON);
        player.setPunishStart(0L);
        player.setPunishDuration(0L);
    }

    public String getActivePunishDescription(BLPlayer_legacy blPlayer) {
        PunishType punishType = getActivePunishType(blPlayer);
        if (punishType == PunishType.NO_PUNISH) {
            return "You do not have any active punishment! :D";
        }

        String message = punishType.getDesciption();

        PunishReason punishReason = blPlayer.getPunishReason();

        if (punishReason == PunishReason.CUSTOM) {
            message = message.replace("%number", "");
        }
        if (punishReason == PunishReason.CUSTOM && blPlayer.getPunishDescription() != null) {
            message = message.replace("%reason", blPlayer.getPunishDescription());
        }

        long punishStart = blPlayer.getPunishStart();
        long punishEnd = punishStart + blPlayer.getPunishDuration();

        message = message.replace("%reason", punishReason.getMessage());
        message = message.replace("%number", getStringNumber(blPlayer.getPunishNumber()));
        message = message.replace("%startTime", formatDate(blPlayer.getPunishStart() + blPlayer.getPunishDuration()));
        message = message.replace("%endTime", formatTime(System.currentTimeMillis() - punishEnd));

        return message;
    }

    public String getStringNumber(int num) {
        switch (num) {
            case 0:
                return "[First Time]";
            case 1:
                return "[Second Time]";
            case 2:
                return "[Third Time]";
            case 3:
                return "[Fourth Time]";
            default:
                return "[" + (num) + ". Time]";
        }
    }

    public String formatDate(long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatter.format(calendar.getTime());
    }

    public String formatTime(long time) {
        long seconds = time / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        return (days + "d, " + hours % 24 + "h, " + minutes % 60 + "m, " + seconds % 60 + "s").replace("-", "");
    }
}
