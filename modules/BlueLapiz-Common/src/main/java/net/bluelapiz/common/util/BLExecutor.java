/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:10 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.util;

import net.bluelapiz.common.BLLogger;

import java.util.List;
import java.util.concurrent.*;

public class BLExecutor {

    private static BLExecutor executor;
    private ExecutorService multiThreadExecutor;
    private ExecutorService mysqlThreadExecutor;
    private ScheduledExecutorService scheduledExecutor;
    private BLExecutor() {
        multiThreadExecutor = Executors.newFixedThreadPool(6, new BLThreadFactory("BL-Multi-Thread-"));
        mysqlThreadExecutor = Executors.newFixedThreadPool(4, new BLThreadFactory("BL-MySQL-Thread-"));
        scheduledExecutor = Executors.newScheduledThreadPool(3, new BLThreadFactory("BL-Scheduled-Thread-"));
    }

    public static BLExecutor get() {
        if (executor == null) executor = new BLExecutor();
        return executor;
    }

    public void schedule(Runnable task, int time, TimeUnit timeUnit) {
        scheduledExecutor.schedule(task, time, timeUnit);
    }

    public void scheduleRepeating(Runnable task, int initialDelay, int period, TimeUnit timeUnit) {
        scheduledExecutor.scheduleAtFixedRate(task, initialDelay, period, timeUnit);
    }

    public Future run(Runnable task) {
        return multiThreadExecutor.submit(task);
    }

    public <T> Future<T> run(Callable<T> task) {
        return multiThreadExecutor.submit(task);
    }

    public ExecutorService getMultiThreadExecutor() {
        return multiThreadExecutor;
    }

    public ScheduledExecutorService getScheduledExecutor() {
        return scheduledExecutor;
    }

    public ExecutorService getMySQLExecutor() {
        return mysqlThreadExecutor;
    }

    public void stopAll() {
        stop(mysqlThreadExecutor);
        stop(multiThreadExecutor);
        stop(scheduledExecutor);
    }

    public void stop(ExecutorService executorService) {
        try {
            executorService.shutdown();
            executorService.awaitTermination(5, TimeUnit.SECONDS);
            List<Runnable> cancelledTasks = executorService.shutdownNow();
            if (cancelledTasks.size() > 0) {
                BLLogger.commonLogger.severe("Exception during shutdown! Could not complete all tasks on the " + executorService.toString() + "!", new Exception());
            }
        } catch (Exception e) {
            BLLogger.commonLogger.severe("Exception while shutting down executorService! " + e.getMessage(), e);
            e.printStackTrace();
        }
    }

}
