/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.sql;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.sql.callback.SQLCountCallback;
import net.bluelapiz.common.sql.callback.SQLSelectCallback;
import net.bluelapiz.common.sql.callback.SQLUpdateCallback;

import java.sql.*;
import java.util.*;

/**
 * @deprecated Helper class for basic mysql queries.
 */
@SuppressWarnings({"unused", "Duplicates"})
public class SQLHelper_legacy {

    SQLHelper_legacy() {
    }

    /**
     * @param sql
     * @return
     * @throws SQLException
     * @deprecated Execute an query in this thread
     */
    public LinkedList<Map<String, Object>> selectSync(String sql) throws SQLException {
        BLLogger.commonLogger.debug("LEGACY_SQL SELECT: " + sql);
        LinkedList<Map<String, Object>> rows = new LinkedList<>();
        //CoreManager.getSkyLandsCore().printSkyLandsDebug(sql, DebugLevel.INFO);
        try (Connection conn = SQLManager.getMySQLMan().getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            rows = getDataFromResultSet(rs);
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rows;
    }

    /**
     * @param sql
     * @return
     * @throws SQLException
     * @deprecated Executes an update in this thread.
     */
    public int updateSync(String sql) throws SQLException {
        BLLogger.commonLogger.debug("LEGACY_SQL UPDATE: " + sql);
        int rowsAffected = 0;
        //CoreManager.getSkyLandsCore().printSkyLandsDebug(sql, DebugLevel.INFO);
        try (Connection conn = SQLManager.getMySQLMan().getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {
            rowsAffected = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rowsAffected;
    }

    /**
     * @param sql
     * @param callback
     * @deprecated
     */
    public void select(String sql, SQLSelectCallback callback) {
        new Thread(() ->
        {
            try {
                callback.success(selectSync(sql));
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error while executing querry!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param sql
     * @param callback
     * @deprecated
     */
    public void update(String sql, SQLUpdateCallback callback) {
        new Thread(() ->
        {
            try {
                callback.success(updateSync(sql));
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error while executing querry!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param table    The table to update from
     * @param callback The callback function
     * @deprecated Updates rows in the database async. Returns the result to the callback when ready.
     */
    public void selectAllRowsFrom(String table, SQLSelectCallback callback) {
        select(table, null, null, SQLBoolOperator.AND, callback, -1, SQLOrder.NONE);
    }

    /**
     * @param table    The table to update from
     * @param callback The callback function
     * @deprecated Updates one row in the database async. Returns the result to the callback when ready.
     */
    public void selectOneRowFrom(String table, SQLSelectCallback callback) {
        select(table, null, null, SQLBoolOperator.AND, callback, 1, SQLOrder.NONE);
    }

    /**
     * @param table    The table to update from
     * @param checks   The values to check for stored in a map of column and data
     * @param values   The new values to insert stored in a map of column and data
     * @param callback The callback function
     * @deprecated Updates one or more rows in the database async. Returns the result to the callback when ready.
     */
    public void update(final String table, String[] checks, SQLBoolOperator type, final Map<String, SQLData> values, SQLUpdateCallback callback) {
        new Thread(() ->
        {
            try {
                String booleanOperator = (type == null ? " AND " : type.getString());

                // no new updates
                if (values.size() < 1) {
                    callback.failure(SQLExceptionType.ERROR, "No new values set!");
                    return;
                }

                StringBuilder sql = new StringBuilder("UPDATE " + table + " SET ");

                // add update clause
                boolean first = true;
                for (Map.Entry<String, SQLData> e : values.entrySet()) {
                    if (!first) sql.append(", ");
                    first = false;
                    sql.append(e.getKey()).append(" = ").append(e.getValue());
                }

                // add WHERE clause
                if (checks == null || checks.length > 0) {
                    StringBuilder arguments = new StringBuilder(" WHERE  ");
                    arguments.append(String.join(booleanOperator, checks));
                    sql.append(arguments);
                }
                sql.append(";");

                int affectedRows = updateSync(sql.toString());
                if (affectedRows == 0) {
                    callback.failure(SQLExceptionType.ERROR, "Error while updating sql!");
                    return;
                }
                callback.success(affectedRows);
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error while updating sql!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param table
     * @param checks
     * @param type
     * @param values
     * @param callback
     * @deprecated
     */
    public void updateSync(final String table, String[] checks, SQLBoolOperator type, final Map<String, SQLData> values, SQLUpdateCallback callback) {
        try {
            String booleanOperator = (type == null ? " AND " : type.getString());

            // no new updates
            if (values.size() < 1) {
                callback.failure(SQLExceptionType.ERROR, "No new values set!");
                return;
            }

            StringBuilder sql = new StringBuilder("UPDATE " + table + " SET ");

            // add update clause
            boolean first = true;
            for (Map.Entry<String, SQLData> e : values.entrySet()) {
                if (!first) sql.append(", ");
                first = false;
                sql.append(e.getKey()).append(" = ").append(e.getValue());
            }

            // add WHERE clause
            if (checks == null || checks.length > 0) {
                StringBuilder arguments = new StringBuilder(" WHERE  ");
                arguments.append(String.join(booleanOperator, checks));
                sql.append(arguments);
            }
            sql.append(";");

            int affectedRows = updateSync(sql.toString());
            if (affectedRows == 0) {
                callback.failure(SQLExceptionType.ERROR, "Error while updating sql!");
                return;
            }
            callback.success(affectedRows);
        } catch (SQLException e) {
            callback.failure(SQLExceptionType.ERROR, "Error while updating sql!\nError: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * @param table
     * @param columns
     * @param checks
     * @param type
     * @param callback
     * @deprecated
     */
    public void select(final String table, final List<String> columns, String[] checks, SQLBoolOperator type, final SQLSelectCallback callback) {
        select(table, columns, checks, type, callback, 0, SQLOrder.NONE);
    }

    /**
     * Select data from the database async. Returns the result to the callback when ready.
     *
     * @param table    The table to select from
     * @param columns  The columns to select
     * @param checks   The values to check for stored in a map og column and data
     * @param callback The callback function
     * @param limit    The maximum number of results to return
     * @param order    If the result should be ordered or not
     * @param orderBy  If ordering, the tables to order by
     */
    public void select(final String table, final List<String> columns, String[] checks, SQLBoolOperator type, final SQLSelectCallback callback, int limit, SQLOrder order, String... orderBy) {
        new Thread(() ->
        {
            try {
                String booleanOperator = (type == null ? " AND " : type.getString());

                // build initial sql
                List<String> col = columns;
                if (col == null || col.size() == 0) {
                    col = Arrays.asList("*");
                }
                StringBuilder sql = new StringBuilder("SELECT " + String.join(", ", col) + " FROM " + table + "");

                // add WHERE clause
                if (checks != null && checks.length > 0) {
                    StringBuilder arguments = new StringBuilder(" WHERE ");
                    arguments.append(String.join(booleanOperator, checks));
                    sql.append(arguments);
                }

                // add LIMIT clause
                if (limit > 0) {
                    sql.append(" LIMIT " + limit);
                }

                // add ORDER BY clause
                if (order != SQLOrder.NONE && orderBy.length > 0) {
                    String orderSql = " ORDER BY " + String.join(", ", orderBy) + (order == SQLOrder.ASC ? " ASC" : " DESC");
                    sql.append(orderSql);
                }
                sql.append(";");

                // Fetch all data
                LinkedList<Map<String, Object>> result = selectSync(sql.toString());
                if (result == null) {
                    callback.failure(SQLExceptionType.ERROR, "MySQL returned NULL!");
                    return;
                }
                if (result.size() == 0) {
                    callback.failure(SQLExceptionType.NO_RESULT, "No results from query!");
                    return;
                }

                callback.success(result);
                return;
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Could not query result from database!\nError: " + e.getMessage());
                e.printStackTrace();
                return;
            }
        }).run();
    }

    /**
     * @param table
     * @param columns
     * @param checks
     * @param type
     * @param callback
     * @deprecated
     */
    public void selectSync(final String table, final List<String> columns, String[] checks, SQLBoolOperator type, final SQLSelectCallback callback) {
        selectSync(table, columns, checks, type, callback, 0, SQLOrder.NONE);
    }

    /**
     * @param table
     * @param columns
     * @param checks
     * @param type
     * @param callback
     * @param limit
     * @param order
     * @param orderBy
     * @deprecated
     */
    public void selectSync(final String table, final List<String> columns, String[] checks, SQLBoolOperator type, final SQLSelectCallback callback, int limit, SQLOrder order, String... orderBy) {
        try {
            String booleanOperator = (type == null ? " AND " : type.getString());

            // build initial sql
            List<String> col = columns;
            if (col == null || col.size() == 0) {
                col = Arrays.asList("*");
            }
            StringBuilder sql = new StringBuilder("SELECT " + String.join(", ", col) + " FROM " + table + "");

            // add WHERE clause
            if (checks != null && checks.length > 0) {
                StringBuilder arguments = new StringBuilder(" WHERE ");
                arguments.append(String.join(booleanOperator, checks));
                sql.append(arguments);
            }

            // add LIMIT clause
            if (limit > 0) {
                sql.append(" LIMIT " + limit);
            }

            // add ORDER BY clause
            if (order != SQLOrder.NONE && orderBy.length > 0) {
                String orderSql = " ORDER BY " + String.join(", ", orderBy) + (order == SQLOrder.ASC ? " ASC" : " DESC");
                sql.append(orderSql);
            }
            sql.append(";");

            // Fetch all data
            LinkedList<Map<String, Object>> result = selectSync(sql.toString());
            if (result == null) {
                callback.failure(SQLExceptionType.ERROR, "MySQL returned NULL!");
                return;
            }
            if (result.size() == 0) {
                callback.failure(SQLExceptionType.NO_RESULT, "No results from query!");
                return;
            }

            callback.success(result);
            return;
        } catch (SQLException e) {
            callback.failure(SQLExceptionType.ERROR, "Could not query result from database!\nError: " + e.getMessage());
            e.printStackTrace();
            return;
        }
    }

    /**
     * @param table    The table to delete from
     * @param checks   The values to check for (The WHERE clause)
     * @param callback The callback function
     * @deprecated Delete from the database async. Calls the callback function with the result when done
     */
    public void delete(final String table, String[] checks, SQLBoolOperator type, final SQLUpdateCallback callback) {
        new Thread(() ->
        {
            try {
                String booleanOperator = (type == null ? " AND " : type.getString());

                if (checks.length < 1) {
                    callback.failure(SQLExceptionType.ERROR, "No where clause set!");
                    return;
                }
                StringBuilder sql = new StringBuilder("DELETE FROM " + table);

                StringBuilder arguments = new StringBuilder(" WHERE ");
                arguments.append(String.join(booleanOperator, checks));
                sql.append(arguments);
                sql.append(";");

                int affectedRows = updateSync(sql.toString());

                callback.success(affectedRows);
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error with mysql update!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param table
     * @param checks
     * @param type
     * @param callback
     * @deprecated
     */
    public void countRows(final String table, String[] checks, SQLBoolOperator type, SQLCountCallback callback) {
        new Thread(() ->
        {
            try {
                String booleanOperator = (type == null ? " AND " : type.getString());

                StringBuilder sql = new StringBuilder("SELECT COUNT(*) AS COUNT FROM " + table);

                if (checks.length > 0) {
                    StringBuilder arguments = new StringBuilder(" WHERE ");
                    arguments.append(String.join(booleanOperator, checks));
                    sql.append(arguments);
                }
                sql.append(";");

                LinkedList<Map<String, Object>> result = selectSync(sql.toString());

                if (result.size() == 0) {
                    callback.failure(SQLExceptionType.ERROR, "Error with mysql select count(*)! No result returned!");
                    return;
                }

                callback.success((Long) (result.getFirst()).get("COUNT"));
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error with mysql update!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param table    The table to insert into
     * @param values   The values to insert stored in a Map of column and data
     * @param callback The callback function
     * @deprecated Insert data to the database async. Calls back the callback function when done with the result.
     */
    public void insert(final String table, final Map<String, SQLData> values, final SQLUpdateCallback callback) {
        new Thread(() ->
        {
            try {
                if (values.size() < 1) {
                    callback.failure(SQLExceptionType.ERROR, "No values clause set!");
                    return;
                }
                StringBuilder sql = new StringBuilder("REPLACE INTO " + table);

                StringBuilder columns = new StringBuilder("(");
                StringBuilder vals = new StringBuilder("(");
                boolean first = true;
                for (Map.Entry<String, SQLData> e : values.entrySet()) {
                    if (!first) {
                        columns.append(", ");
                        vals.append(", ");
                    }
                    first = false;

                    columns.append(e.getKey());
                    vals.append(e.getValue());
                }
                columns.append(")");
                vals.append(")");

                sql.append(columns + " VALUES " + vals + ";");

                int affectedRows = updateSync(sql.toString());

                callback.success(affectedRows);
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error with mysql update!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param table
     * @param keyToChange
     * @param columnToChange
     * @param valuesToAdd
     * @param callback
     * @deprecated
     */
    public void updateMultipleRowsAddNumber(String table, String keyToChange, String columnToChange, Map<String, Integer> valuesToAdd, final SQLUpdateCallback callback) {
        new Thread(() ->
        {
            try {
                if (valuesToAdd.size() == 0) {
                    callback.failure(SQLExceptionType.ERROR, "No new values set!");
                    return;
                }

                StringBuilder sql = new StringBuilder("UPDATE " + table + " SET " + columnToChange + " = " + columnToChange + " + (CASE " + keyToChange);

                for (Map.Entry<String, Integer> e : valuesToAdd.entrySet()) {
                    sql.append(" WHEN " + e.getKey() + " THEN " + e.getValue());
                }

                sql.append(" END) WHERE " + keyToChange + " IN (" + String.join(", ", valuesToAdd.keySet()) + ");");

                int affectedRows = updateSync(sql.toString());

                callback.success(affectedRows);
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error with mysql update!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param table
     * @param keyToChange
     * @param columnToChange
     * @param valuesToSet
     * @param callback
     * @deprecated
     */
    public void updateMultipleRowsSetValue(String table, String keyToChange, String columnToChange, Map<String, SQLData> valuesToSet, final SQLUpdateCallback callback) {
        new Thread(() ->
        {
            try {
                if (valuesToSet.size() == 0) {
                    callback.failure(SQLExceptionType.ERROR, "No new values set!");
                    return;
                }

                StringBuilder sql = new StringBuilder("UPDATE " + table + " SET " + columnToChange + " = (CASE " + keyToChange);

                for (Map.Entry<String, SQLData> e : valuesToSet.entrySet()) {
                    sql.append(" WHEN " + e.getKey() + " THEN " + e.getValue());
                }

                sql.append(" END) WHERE " + keyToChange + " IN (" + String.join(", ", valuesToSet.keySet()) + ");");

                int affectedRows = updateSync(sql.toString());

                callback.success(affectedRows);
            } catch (SQLException e) {
                callback.failure(SQLExceptionType.ERROR, "Error with mysql update!\nError: " + e.getMessage());
                e.printStackTrace();
            }
        }).run();
    }

    /**
     * @param rs The ResultSet
     * @return The values in a list of maps
     * @throws SQLException
     * @deprecated Copy data from ResultSet to a LinkedList of Maps
     */
    private LinkedList<Map<String, Object>> getDataFromResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        int columns = md.getColumnCount();
        LinkedList<Map<String, Object>> rows = new LinkedList<>();
        while (rs.next()) {
            Map<String, Object> row = new HashMap<>();
            for (int i = 1; i <= columns; i++) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            rows.push(row);
        }
        return rows;
    }

    /**
     * @return The list of tables
     * @deprecated Querries the server for the list of tables in the database
     */
    public List<String> getTables() {
        try (Connection conn = SQLManager.getMySQLMan().getConnection()) {
            DatabaseMetaData dbm = conn.getMetaData();
            ResultSet rs = dbm.getTables(conn.getCatalog().toLowerCase(), null, "%", new String[]{"TABLE"});
            ArrayList<String> list = new ArrayList<>();
            while (rs != null && rs.next()) {
                list.add(rs.getString("TABLE_NAME").toLowerCase());
            }
            rs.close();
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}
