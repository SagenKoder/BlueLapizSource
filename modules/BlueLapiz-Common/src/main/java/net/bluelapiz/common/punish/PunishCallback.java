/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import net.bluelapiz.common.player.objects.BLPlayer_legacy;

public interface PunishCallback {

    boolean beforePunish(BLPlayer_legacy player, Punishment result);

    void onPunish(BLPlayer_legacy player, Punishment result);

}
