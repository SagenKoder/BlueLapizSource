/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.sql;

public enum SQLBoolOperator {

    AND(" AND "),
    OR(" OR ");

    private String string;

    SQLBoolOperator(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
