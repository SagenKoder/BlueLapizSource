/*-----------------------------------------------------------------------------
 - Copyright (C) BlueLapiz.net - All Rights Reserved                          -
 - Unauthorized copying of this file, via any medium is strictly prohibited   -
 - Proprietary and confidential                                               -
 - Written by Alexander Sagen <alexmsagen@gmail.com>                          -
 -----------------------------------------------------------------------------*/

package net.bluelapiz.common.packet;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SendDiscordExceptionPacket extends Packet {

    private String server;
    private String plugin;
    private String description;
    private Throwable ex;
    private String currentTime;

    public SendDiscordExceptionPacket(String server, String plugin, String description, Throwable ex) {
        this.server = server;
        this.plugin = plugin;
        this.description = description;
        this.ex = ex;
        this.currentTime = generateCurrentTime();
    }

    public String getServer() {
        return server;
    }

    public String getPlugin() {
        return plugin;
    }

    public String getDescription() {
        return description;
    }

    public Throwable getEx() {
        return ex;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    private String generateCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        return formatter.format(calendar.getTime());
    }

    @Override
    public String toString() {
        return "SendDiscordExceptionPacket{" +
                "server='" + server + '\'' +
                ", plugin='" + plugin + '\'' +
                ", description='" + description + '\'' +
                ", ex=" + ex +
                ", currentTime='" + currentTime + '\'' +
                '}';
    }
}
