/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 12/7/18 1:19 PM                                                *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player;

import net.bluelapiz.common.player.objects.Rank;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.sql.SQLOrder;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class RawPlayerManager {

    protected RawPlayerManager() {
        SQLManager.getTableHelper().addTable("player_data",
                "CREATE TABLE %table (\n" +
                        "    uuid CHAR(32) NOT NULL,\n" +
                        "    name VARCHAR(16) NOT NULL,\n" +
                        "    rank INT(3) NOT NULL DEFAULT 100,\n" +
                        "    rank_tmp INT(3) DEFAULT NULL,\n" +
                        "    rank_expires DATETIME NOT NULL DEFAULT 0,\n" +
                        "    PRIMARY KEY(uuid)\n" +
                        ");");
        SQLManager.getTableHelper().addTable("player_stat",
                "CREATE TABLE %table (\n" +
                        "    uuid CHAR(32) NOT NULL,\n" +
                        "    stat_name VARCHAR(32) NOT NULL,\n" +
                        "    stat_value BIGINT NOT NULL,\n" +
                        "    PRIMARY KEY(uuid, stat_name)\n" +
                        ");");
        SQLManager.getTableHelper().addTable("player_option",
                "CREATE TABLE %table (\n" +
                        "    uuid CHAR(32) NOT NULL,\n" +
                        "    option_name VARCHAR(32) NOT NULL,\n" +
                        "    option_value VARCHAR(64) NOT NULL,\n" +
                        "    PRIMARY KEY(uuid, option_name)\n" +
                        ");");
        SQLManager.getTableHelper().createTablesNotExisting();
    }

    /**
     * Fetches the player from the database. Returns Optional.empty() if the player is not in the database.
     *
     * @param uuid The players uuid
     */
    public CompletableFuture<Optional<BLPlayer>> fetchPlayer(UUID uuid) {
        return SQLManager.getSQLHelper()
                .select(
                        "player_data",
                        Arrays.asList("uuid", "name", "rank", "rank_tmp", "rank_until"),
                        SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid)),
                        1,
                        SQLOrder.NONE
                ).thenApply(l -> {
                    if (l == null || l.size() != 1) {
                        return Optional.empty();
                    }
                    Map<String, Object> d = l.getFirst();
                    UUID u = SQLData.stringToUuid((String) d.get("uuid"));
                    if (!u.equals(uuid)) {
                        System.err.println("Weird exception! Fetched player does not have the same uuid as the query! Queried uuid: " + uuid + " Returned uuid: " + u + " !");
                        return Optional.empty();
                    }
                    String name = (String) d.get("name");
                    Rank rank = Rank.getRank((Integer) d.get("rank"));
                    Rank tmpRank = d.get("rank_tmp") == null ? null : Rank.getRank((Integer) d.get("rank_tmp"));
                    Date rankExpires = (Date) d.get("rank_expires");
                    BLPlayer p = new BLPlayer(u, name, rank, tmpRank, rankExpires);
                    return Optional.of(p);
                });
    }

    /**
     * Fetches the player from the database. Returns Optional.empty() if the player is not in the database.
     *
     * @param uuid The players uuid
     */
    public Optional<BLPlayer> fetchPlayerNow(UUID uuid, int timeout, TimeUnit timeUnit) {
        try {
            return fetchPlayer(uuid).get(timeout, timeUnit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * Fetches the player from the database or creates a new Player if not existing.
     */
    public CompletableFuture<BLPlayer> createPlayer(UUID uuid, String name) {
        return fetchPlayer(uuid).thenApply(opt -> {
            if (opt.isPresent()) return opt.get();
            try {
                return SQLManager.getSQLHelper().insert("player_data", new HashMap<String, SQLData>() {{
                    put("uuid", SQLData.ofUuid(uuid));
                    put("name", SQLData.ofString(name));
                }}, true).thenApply(r -> {
                    Optional<BLPlayer> created;
                    try {
                        created = fetchPlayer(uuid).get(1, TimeUnit.SECONDS);
                    } catch (Exception e) {
                        throw new RuntimeException("Error while fetching newly created player!", e);
                    }
                    if (!created.isPresent()) {
                        throw new RuntimeException("Could not fetch newly created player! Returned Optional.empty()! Player: " + name + "(" + uuid.toString());
                    }
                    return created.get();
                }).get(1, TimeUnit.SECONDS);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * Fetches the player from the database or creates a new Player if not existing.
     */
    public Optional<BLPlayer> createPlayerNow(UUID uuid, String name, int timeout, TimeUnit timeUnit) {
        try {
            return Optional.of(createPlayer(uuid, name).get(timeout, timeUnit));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    /**
     * Fetches a set of stats from a player. A stat will be ignored if it does not exist in the database.
     *
     * @param uuid
     * @return
     */
    public CompletableFuture<ConcurrentMap<String, Long>> fetchStatsFrom(UUID uuid) {
        return SQLManager.getSQLHelper().select("player_stat",
                Arrays.asList("stat_name", "stat_value"),
                SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid))
        ).thenApply(rows -> {
            ConcurrentMap<String, Long> map = new ConcurrentHashMap<>();
            for (Map<String, Object> row : rows) {
                map.put((String) row.get("stat_name"), (Long) row.get("stat_value"));
            }
            return map;
        });
    }

    /**
     * Fetches a set of options for a player. An option will be ignored if it does not exist in the database.
     *
     * @param uuid
     * @return
     */
    public CompletableFuture<ConcurrentMap<String, String>> fetchOptionsFrom(UUID uuid) {
        return SQLManager.getSQLHelper().select("player_option",
                Arrays.asList("option_name", "option_value"),
                SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid))
        ).thenApply(rows -> {
            ConcurrentMap<String, String> map = new ConcurrentHashMap<>();
            for (Map<String, Object> row : rows) {
                map.put((String) row.get("option_name"), (String) row.get("option_value"));
            }
            return map;
        });
    }

    /**
     * Updates a stat to a given value for a player.
     *
     * @param uuid
     * @param stat
     * @param value
     * @return
     */
    public CompletableFuture<Integer> setStatInDatabase(UUID uuid, String stat, Long value) {
        return setStatsInDatabase(uuid, new ConcurrentHashMap<String, Long>() {{
            put(stat, value);
        }});
    }

    /**
     * Updates a set of stats to the given value.
     *
     * @param uuid
     * @param newValues
     * @return
     */
    public CompletableFuture<Integer> setStatsInDatabase(UUID uuid, ConcurrentMap<String, Long> newValues) {
        SQLData uuidData = SQLData.ofUuid(uuid);
        List<String> columns = Arrays.asList("uuid", "stat_name", "stat_value");

        List<List<SQLData>> rows = new ArrayList<>();

        for (Map.Entry<String, Long> entry : newValues.entrySet()) {
            rows.add(Arrays.asList(
                    uuidData,
                    SQLData.ofString(entry.getKey()),
                    SQLData.ofInteger(entry.getValue())
            ));
        }

        return SQLManager.getSQLHelper().insertMultiple("player_stat", columns, rows, true)
                .thenApply(List::size);
    }

    /**
     * Updates an option to a given value for a player.
     *
     * @param uuid
     * @param option
     * @param value
     * @return
     */
    public CompletableFuture<Integer> setOptionInDatabase(UUID uuid, String option, String value) {
        return setOptionsInDatabase(uuid, new ConcurrentHashMap<String, String>() {{
            put(option, value);
        }});
    }

    /**
     * Updates a set of options for a player.
     *
     * @param uuid
     * @param newValues
     * @return
     */
    public CompletableFuture<Integer> setOptionsInDatabase(UUID uuid, ConcurrentMap<String, String> newValues) {
        SQLData uuidData = SQLData.ofUuid(uuid);
        List<String> columns = Arrays.asList("uuid", "option_name", "option_value");

        List<List<SQLData>> rows = new ArrayList<>();

        for (Map.Entry<String, String> entry : newValues.entrySet()) {
            rows.add(Arrays.asList(
                    uuidData,
                    SQLData.ofString(entry.getKey()),
                    SQLData.ofString(entry.getValue())
            ));
        }

        return SQLManager.getSQLHelper().insertMultiple("player_option", columns, rows, true)
                .thenApply(List::size);
    }

    /**
     * Updates the rank of a given player in the database.
     *
     * @param uuid
     * @param newRank
     * @return
     */
    CompletableFuture<Boolean> setRankInDatabase(UUID uuid, Rank newRank) {
        return SQLManager.getSQLHelper().update("player_data",
                SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid)),
                new HashMap<String, SQLData>() {{
                    put("rank", SQLData.ofInteger(newRank.getLevel()));
                }}
        ).thenApply(n -> n >= 1);
    }

    /**
     * Updates the rank of a given player in the database.
     *
     * @param uuid
     * @param newRank
     * @return
     */
    CompletableFuture<Boolean> setTempRankInDatabase(UUID uuid, Rank newRank, Date expires) {
        return SQLManager.getSQLHelper().update("player_data",
                SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid)),
                new HashMap<String, SQLData>() {{
                    put("rank_tmp", newRank == null ? SQLData.ofNull() : SQLData.ofInteger(newRank.getLevel()));
                    put("rank_expires", expires == null ? SQLData.ofInteger(0) : SQLData.ofTimestamp(expires));
                }}
        ).thenApply(n -> n >= 1);
    }

    /**
     * Updates the name of a given player in the database.
     *
     * @param uuid
     * @param newName
     * @return
     */
    CompletableFuture<Boolean> setNameInDatabase(UUID uuid, String newName) {
        return SQLManager.getSQLHelper().update("player_data",
                SQLData.ofKeyword("uuid").equalsOther(SQLData.ofUuid(uuid)),
                new HashMap<String, SQLData>() {{
                    put("name", SQLData.ofString(newName));
                }}
        ).thenApply(n -> n >= 1);
    }
}
