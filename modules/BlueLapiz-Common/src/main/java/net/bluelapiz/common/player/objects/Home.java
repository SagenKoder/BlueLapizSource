/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player.objects;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Home {

    private long created;
    private UUID player;
    private String name;
    private String world;
    private int x, y, z;
    private float yaw, pitch;

    public Home(UUID uuid, Map<String, Object> row) {
        this.created = BLPlayer_legacy.bigIntegerToLong(row.get("time"));
        this.player = uuid;
        this.name = (String) row.get("name");
        this.world = (String) row.get("world");
        this.x = (int) row.get("x");
        this.y = (int) row.get("y");
        this.z = (int) row.get("z");
        this.yaw = (float) row.get("yaw");
        this.pitch = (float) row.get("pitch");
    }
}
