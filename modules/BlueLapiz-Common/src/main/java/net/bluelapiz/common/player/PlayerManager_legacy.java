/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:30 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.player.objects.BLPlayer_legacy;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLExceptionType;
import net.bluelapiz.common.sql.SQLManager;
import net.bluelapiz.common.sql.callback.BLPlayerCallback;
import net.bluelapiz.common.sql.callback.SQLSelectCallback;
import net.bluelapiz.common.sql.callback.SQLUpdateCallback;
import net.bluelapiz.common.util.UUIDFetcher;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
public class PlayerManager_legacy {

    public static final UUID CONSOLE_UUID = UUID.fromString("47695336-e6f4-4d9c-99c9-d4f429e24abe");

    private static PlayerManager_legacy playerManager;
    private ConcurrentHashMap<UUID, BLPlayer_legacy> playersMap = new ConcurrentHashMap<>();
    private TreeMap<String, UUID> playerNameCache = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    private PlayerManager_legacy() {
        SQLManager.getTableHelper().addTable("player",
                "CREATE TABLE %table (\n" +
                        "   uuid CHAR(32) NOT NULL,\n" +
                        "   name VARCHAR(16) NOT NULL,\n" +
                        "   rank INT(3) NOT NULL DEFAULT 100,\n" +
                        "   temp_rank INT(3) NOT NULL DEFAULT 100,\n" +
                        "   temp_rank_until BIGINT UNSIGNED NOT NULL DEFAULT 0,\n" +
                        "   balance BIGINT UNSIGNED NOT NULL DEFAULT 0,\n" +
                        "   coin BIGINT UNSIGNED NOT NULL DEFAULT 0,\n" +
                        "   vanish TINYINT(1) NOT NULL DEFAULT 0,\n" +
                        "   glow TINYINT(1) NOT NULL DEFAULT 0,\n" +
                        "   first_login BIGINT UNSIGNED NOT NULL,\n" +
                        "   punish_type ENUM('NO_PUNISH', 'KICK', 'BAN', 'MUTE', 'PERMMUTE', 'PERMBAN') NOT NULL DEFAULT 'NO_PUNISH',\n" +
                        "   punish_reason ENUM('NO_REASON', 'ADVERTISING', 'BAD_BEHAVIOR_IN_CHAT', 'BAD_BEHAVIOR_IN_GAME', 'BUG_ABUSE', 'CROSS_TEAMING', 'GRIEF', 'HACK', 'IMPERSONATING', 'INAPPROPRIATE_NAME', 'INAPPROPRIATE_SKIN', 'MAP_ABUSE', 'PUNISHMENT_BYPASS', 'RACISM', 'REPORT_ABUSE', 'SPAM', 'SPAWN_KILLING', 'TARGETING', 'X_RAY', 'CUSTOM') NOT NULL DEFAULT 'NO_REASON',\n" +
                        "   punish_number INT(3) NOT NULL DEFAULT 0,\n" +
                        "   punish_desc VARCHAR(200) DEFAULT NULL,\n" +
                        "   punish_by CHAR(32) DEFAULT NULL,\n" +
                        "   punish_start BIGINT UNSIGNED NOT NULL DEFAULT 0,\n" +
                        "   punish_duration BIGINT UNSIGNED NOT NULL DEFAULT 0,\n" +
                        "   PRIMARY KEY(uuid)\n" +
                        ");");
        SQLManager.getTableHelper().createTablesNotExisting();

        try {
            LinkedList<Map<String, Object>> result = SQLManager.getSQLHelper().select("SELECT uuid, name FROM player;").get();
            List<String> names = new ArrayList<>();
            for (Map<String, Object> row : result) {
                UUID uuid = BLPlayer_legacy.stringToUUID((String) row.get("uuid"));
                String name = (String) row.get("name");
                names.add(name);
                playerNameCache.put(name, uuid);
            }

            try {
                // Validate names through mojang....
                UUIDFetcher uuidFetcher = new UUIDFetcher(names);
                Map<String, UUID> mojangNameUuids = uuidFetcher.call();
                if (mojangNameUuids != null) {
                    names.clear();
                    for (Map.Entry<String, UUID> entry : playerNameCache.entrySet()) {
                        if (mojangNameUuids.get(entry.getKey()) != null && !mojangNameUuids.get(entry.getKey()).equals(entry.getValue())) {
                            if (playersMap.entrySet().stream().anyMatch(u -> u.getKey().equals(entry.getValue()))) {
                                playersMap.get(entry.getValue()).setName(entry.getKey());
                            } else {
                                names.add(entry.getKey());
                            }
                        }
                    }
                    // remove unknown names
                    for (String remove : names) playerNameCache.remove(remove);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            playerNameCache.put("Console", CONSOLE_UUID); // add the console

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public static PlayerManager_legacy get() {
        if (playerManager == null) playerManager = new PlayerManager_legacy();
        return playerManager;
    }

    @Deprecated
    public String getNameFromUuid(UUID uuid) {
        if (!playerNameCache.containsValue(uuid)) {
            // SPAM BLLogger.commonLogger.debug("Cannot find uuid " + uuid + " in cache! Returning 'unknown'");
            return "Unknown";
        } else {
            for (Map.Entry<String, UUID> e : playerNameCache.entrySet()) {
                if (e.getValue().equals(uuid)) return e.getKey();
            }
        }
        return "unknown";
    }

    @Deprecated
    public UUID getUuidFromName(String name) {
        return playerNameCache.get(name);
    }

    @Deprecated
    private BLPlayer_legacy loadOrCreatePlayerSync(UUID uuid, String name) {
        playerNameCache.put(name, uuid);

        BLPlayer_legacy oldPlayer = get(uuid);
        if (oldPlayer != null) {
            BLLogger.commonLogger.debug("loadOrCreatePlayerSync - oldPlayer != null");
            if (!oldPlayer.getName().equals(name))
                oldPlayer.setName(name);
            return oldPlayer;
        }

        SQLManager.getSQLHelper_legacy().selectSync("player",
                null,
                new String[]{
                        "uuid = " + SQLData.ofUuid(uuid)
                },
                null,
                new SQLSelectCallback() {
                    @Override
                    public void success(LinkedList<Map<String, Object>> result) {

                        BLPlayer_legacy blp = BLPlayer_legacy.builderFromSqlData(result.getFirst()).build();
                        if (!blp.getName().equals(name)) blp.setName(name);
                        playersMap.put(blp.getUuid(), blp);
                        BLLogger.commonLogger.debug("Found player " + blp.getName());
                    }

                    @Override
                    public void failure(SQLExceptionType exceptionType, String errorMessage) {
                        BLLogger.commonLogger.debug("loadOrCreatePlayerSync - callback.failure");
                        if (exceptionType != SQLExceptionType.NO_RESULT) {
                            BLLogger.commonLogger.debug("loadOrCreatePlayerSync - callback.failure - NO_RESULT");
                            BLLogger.commonLogger.severe("COULD NOT LOAD PLAYER!!!\nError: " + errorMessage, new Exception(errorMessage));
                            return;
                        }

                        BLLogger.commonLogger.debug("Could not find player! Creating a new player!");
                        BLLogger.commonLogger.debug("uuid: " + uuid + " name: " + name);
                        createPlayer(BLPlayer_legacy.builder().uuid(uuid).name(name).build());
                    }
                }
        );
        return get(uuid);
    }

    @Deprecated
    private void createPlayer(BLPlayer_legacy newPlayer) {
        playersMap.put(newPlayer.getUuid(), newPlayer);

        SQLManager.getSQLHelper().insert("player", newPlayer.getSqlMapAndSetUnchanged())
                .whenComplete((n, ex) -> {
                    if (ex != null) {
                        BLLogger.commonLogger.severe("Could not insert new player!!!\nError: " + ex.getMessage(), ex);
                    }
                });
    }

    @Deprecated
    public BLPlayer_legacy playerJoin(UUID uuid, String name) {
        BLPlayer_legacy blp = loadOrCreatePlayerSync(uuid, name);

        if (blp == null) {

            BLLogger.commonLogger.debug("Could not load or create player!! Loaded: " + playersMap.size());

            return null;
        }
        return blp;
    }

    @Deprecated
    public void playerLeave(UUID uuid) {
        if (isLoaded(uuid)) {
            BLPlayer_legacy blp = getPlayer(uuid);
            if (blp.isChanged()) {
                SQLManager.getSQLHelper_legacy().updateSync("player",
                        new String[]{
                                "uuid = " + SQLData.ofUuid(blp.getUuid())
                        },
                        null, blp.getSqlMapAndSetUnchanged(), new SQLUpdateCallback() {
                            @Override
                            public void success(int rowsAffected) {
                                BLLogger.commonLogger.info("Saved player " + blp.getName());
                            }

                            @Override
                            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                                BLLogger.commonLogger.severe("COULD NOT SAVE PLAYER " + blp.getName() + "!!!", new Exception(errorMessage));
                            }
                        });
            }
        }
    }

    @Deprecated
    public Set<String> getAllCachedNames() {
        return playerNameCache.keySet();
    }

    @Deprecated
    public boolean isLoaded(UUID uuid) {
        return contains(uuid);
    }

    @Deprecated
    public BLPlayer_legacy getPlayer(UUID uuid) {
        if (isLoaded(uuid))
            return get(uuid);
        return null;
    }

    @Deprecated
    public void getOrLoadPlayer(String player, BLPlayerCallback callback) {
        if (playerNameCache.containsKey(player)) {
            getOrLoadPlayer(playerNameCache.get(player), callback);
        } else {
            callback.failure(SQLExceptionType.NO_RESULT, "Could not find player name in cache!");
        }
    }

    @Deprecated
    public synchronized void getOrLoadPlayer(UUID uuid, BLPlayerCallback callback) {
        if (isLoaded(uuid)) {
            callback.success(getPlayer(uuid));
            return;
        }

        SQLManager.getSQLHelper_legacy().select("player",
                null,
                new String[]{
                        "uuid = " + SQLData.ofUuid(uuid)
                },
                null,
                new SQLSelectCallback() {
                    @Override
                    public void success(LinkedList<Map<String, Object>> result) {
                        BLPlayer_legacy blp = BLPlayer_legacy.builderFromSqlData(result.getFirst()).build();
                        playersMap.put(blp.getUuid(), blp);
                        callback.success(blp);
                    }

                    @Override
                    public void failure(SQLExceptionType exceptionType, String errorMessage) {
                        callback.failure(exceptionType, errorMessage);
                    }
                }
        );
    }

    @Deprecated
    public synchronized void saveAndUnloadPlayers(List<UUID> onlinePlayers) {
        for (BLPlayer_legacy blp : playersMap.values()) {
            if (blp.shouldSave()) {
                SQLManager.getSQLHelper_legacy().update("player",
                        new String[]{
                                "uuid = " + SQLData.ofUuid(blp.getUuid())
                        },
                        null, blp.getSqlMapAndSetUnchanged(), new SQLUpdateCallback() {
                            @Override
                            public void success(int rowsAffected) {
                                BLLogger.commonLogger.info("Saved player " + blp.getName());
                            }

                            @Override
                            public void failure(SQLExceptionType exceptionType, String errorMessage) {
                                for (int i = 0; i < 5; i++)
                                    BLLogger.commonLogger.severe("COULD NOT SAVE PLAYER " + blp.getName() + "!!!", new Exception(errorMessage));
                            }
                        });
            }
            if (blp.shouldRemove() && !onlinePlayers.contains(blp.getUuid())) {
                remove(blp.getUuid());
            }
        }
    }

    @Deprecated
    private boolean contains(UUID uuid) {
        for (UUID u : playersMap.keySet()) {
            if (u.equals(uuid)) return true;
        }
        return false;
    }

    @Deprecated
    private void remove(UUID uuid) {
        for (UUID u : playersMap.keySet()) {
            if (u.equals(uuid)) playersMap.remove(u);
        }
    }

    @Deprecated
    private BLPlayer_legacy get(UUID uuid) {
        for (BLPlayer_legacy blp : playersMap.values()) {
            if (blp.getUuid().equals(uuid)) return blp;
        }
        return null;
    }
}
