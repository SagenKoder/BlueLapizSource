/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:10 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.sql;

import net.bluelapiz.common.BLLogger;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SQLTableHelper {

    private static final HashMap<String, String> tableCreators = new HashMap<>();

    private static final ArrayList<String> tablesCreated = new ArrayList<>();

    public void addTable(String tableName, String createStatement) {
        if (tableCreators.containsKey(tableName.toLowerCase()))
            throw new IllegalStateException("Cannot create multiple tables with the same name!\nTable: " + tableName);
        tableCreators.put(tableName.toLowerCase(), createStatement);
    }

    public void createTablesNotExisting() {
        List<String> existingTables = SQLManager.getSQLHelper_legacy().getTables().stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());

        tableCreators.forEach((k, v) -> {
            if (existingTables.contains(k.toLowerCase())) return;

            try {
                SQLManager.getSQLHelper_legacy().updateSync(v.replace("%table", k));
                tablesCreated.add(k);
            } catch (SQLException e) {
                BLLogger.commonLogger.severe("Could not create table!!\nError: " + e.getMessage() + "\nSql: " + v, e);
                e.printStackTrace();
            }
        });
    }
}
