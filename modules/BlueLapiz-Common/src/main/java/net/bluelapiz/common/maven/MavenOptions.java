package net.bluelapiz.common.maven;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@Data
@AllArgsConstructor
public final class MavenOptions {

    private String customRepository;
    private boolean alwaysUpdate;

    public void setCustomRepository(String customRepository) {
        this.customRepository = MavenUrl.fixUrl(customRepository);
    }
}
