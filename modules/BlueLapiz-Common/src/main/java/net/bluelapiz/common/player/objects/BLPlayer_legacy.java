/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player.objects;

import net.bluelapiz.common.punish.PunishReason;
import net.bluelapiz.common.punish.PunishType;
import net.bluelapiz.common.sql.SQLData;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Deprecated
public class BLPlayer_legacy {

    private boolean changed = false;
    private UUID uuid;
    private String name;
    private Rank rank;
    private Rank tempRank;
    private long tempRankUntil;
    private long balance;
    private long coin;
    private boolean vanished;
    private boolean glowing;
    private long firstLogin;
    private PunishType punishType;
    private PunishReason punishReason;
    private int punishNumber;
    private String punishDescription;
    private UUID punishedBy;
    private long punishStart;
    private long punishDuration;
    private BLPlayer_legacy(Builder builder) {
        this.uuid = builder.uuid;
        this.name = builder.name;
        this.rank = builder.rank;
        this.tempRank = builder.tempRank;
        this.tempRankUntil = builder.tempRankUntil;
        this.balance = builder.balance;
        this.coin = builder.coin;
        this.vanished = builder.vanished;
        this.glowing = builder.glowing;
        this.firstLogin = builder.firstLogin;

        this.punishType = builder.punishType;
        this.punishReason = builder.punishReason;
        this.punishNumber = builder.punishNumber;
        this.punishDescription = builder.punishDescription;
        this.punishedBy = builder.punishedBy;
        this.punishStart = builder.punishStart;
        this.punishDuration = builder.punishDuration;
    }

    @Deprecated
    public static Builder builder() {
        return new Builder();
    }

    @Deprecated
    public static UUID stringToUUID(String uuid) {
        if (uuid == null) return null;
        return UUID.fromString(uuid.replaceAll("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5"));
    }

    @Deprecated
    public static long bigIntegerToLong(Object object) {
        return ((BigInteger) object).longValue();
    }

    @Deprecated
    public static Builder builderFromSqlData(Map<String, Object> result) {
        return builder()
                .uuid(stringToUUID(((String) result.get("uuid"))))
                .name((String) result.get("name"))
                .rank(Rank.getRank((Integer) result.get("rank")))
                .tempRank(Rank.getRank((Integer) result.get("temp_rank")))
                .tempRankUntil(bigIntegerToLong(result.get("temp_rank_until")))
                .balance(bigIntegerToLong(result.get("balance")))
                .coin(bigIntegerToLong(result.get("coin")))
                .vanished(((Boolean) result.get("vanish")))
                .glowing(((Boolean) result.get("glow")))
                .firstLogin(bigIntegerToLong(result.get("first_login")))
                .punishType(PunishType.valueOf((String) result.get("punish_type")))
                .punishReason(PunishReason.valueOf((String) result.get("punish_reason")))
                .punishNumber((Integer) result.get("punish_number"))
                .punishDescription((String) result.get("punish_desc"))
                .punishedBy(stringToUUID((String) result.get("punish_by")))
                .punishStart(bigIntegerToLong(result.get("punish_start")))
                .punishDuration(bigIntegerToLong(result.get("punish_duration")));
    }

    @Deprecated
    public UUID getUuid() {
        return uuid;
    }

    @Deprecated
    public String getName() {
        return name;
    }

    @Deprecated
    public void setName(String name) {
        this.name = name;
        changed = true;
    }

    @Deprecated
    public Rank getPermRank() {
        return rank;
    }

    @Deprecated
    public Rank getTempRank() {
        return tempRank;
    }

    @Deprecated
    public void setTempRank(Rank tempRank) {
        this.tempRank = tempRank;
        changed = true;
    }

    @Deprecated
    public long getTempRankUntil() {
        return tempRankUntil;
    }

    @Deprecated
    public void setTempRankUntil(long tempRankUntil) {
        this.tempRankUntil = tempRankUntil;
        changed = true;
    }

    @Deprecated
    public Rank getRank() {
        return Rank.getRank(Math.max(rank.getLevel(),
                (tempRankUntil > System.currentTimeMillis() ? tempRank.getLevel() : Rank.PLAYER.getLevel())));
    }

    @Deprecated
    public void setRank(Rank rank) {
        this.rank = rank;
        changed = true;
    }

    @Deprecated
    public long getBalance() {
        return balance;
    }

    @Deprecated
    public void setBalance(long balance) {
        this.balance = balance;
        changed = true;
    }

    @Deprecated
    public long getCoin() {
        return coin;
    }

    @Deprecated
    public void setCoin(long coin) {
        this.coin = coin;
        changed = true;
    }

    @Deprecated
    public boolean isVanished() {
        return vanished;
    }

    @Deprecated
    public void setVanished(boolean vanished) {
        this.vanished = vanished;
        changed = true;
    }

    @Deprecated
    public boolean isGlowing() {
        return glowing;
    }

    @Deprecated
    public void setGlowing(boolean glowing) {
        this.glowing = glowing;
        changed = true;
    }

    @Deprecated
    public long getFirstLogin() {
        return firstLogin;
    }

    @Deprecated
    public PunishType getPunishType() {
        return punishType;
    }

    @Deprecated
    public void setPunishType(PunishType punishType) {
        this.punishType = punishType;
        changed = true;
    }

    @Deprecated
    public PunishReason getPunishReason() {
        return punishReason;
    }

    @Deprecated
    public void setPunishReason(PunishReason punishReason) {
        this.punishReason = punishReason;
        changed = true;
    }

    @Deprecated
    public int getPunishNumber() {
        return punishNumber;
    }

    @Deprecated
    public void setPunishNumber(int punishNumber) {
        this.punishNumber = punishNumber;
        changed = true;
    }

    @Deprecated
    public String getPunishDescription() {
        return punishDescription;
    }

    @Deprecated
    public void setPunishDescription(String description) {
        this.punishDescription = description;
        changed = true;
    }

    @Deprecated
    public UUID getPunishedBy() {
        return punishedBy;
    }

    @Deprecated
    public void setPunishedBy(UUID punishedBy) {
        this.punishedBy = punishedBy;
        changed = true;
    }

    @Deprecated
    public long getPunishStart() {
        return punishStart;
    }

    @Deprecated
    public void setPunishStart(long punishStart) {
        this.punishStart = punishStart;
        changed = true;
    }

    @Deprecated
    public long getPunishDuration() {
        return punishDuration;
    }

    @Deprecated
    public void setPunishDuration(long punishDuration) {
        this.punishDuration = punishDuration;
        changed = true;
    }

    @Deprecated
    public HashMap<String, SQLData> getSqlMapAndSetUnchanged() {
        HashMap<String, SQLData> sqlMap = new HashMap<String, SQLData>() {{
            put("uuid", SQLData.ofUuid(uuid));
            put("name", SQLData.ofString(name));
            put("rank", SQLData.ofInteger(rank.getLevel()));
            put("temp_rank", SQLData.ofInteger(tempRank.getLevel()));
            put("temp_rank_until", SQLData.ofInteger(tempRankUntil));
            put("balance", SQLData.ofInteger(balance));
            put("coin", SQLData.ofInteger(coin));
            put("vanish", SQLData.ofInteger(vanished ? 1 : 0));
            put("glow", SQLData.ofInteger(glowing ? 1 : 0));
            put("first_login", SQLData.ofInteger(firstLogin));
            put("punish_type", SQLData.ofString(punishType.name()));
            put("punish_reason", SQLData.ofString(punishReason.name()));
            put("punish_number", SQLData.ofInteger(punishNumber));
            put("punish_desc", SQLData.ofString(punishDescription));
            put("punish_by", punishedBy == null ? SQLData.ofNull() : SQLData.ofUuid(punishedBy));
            put("punish_Start", SQLData.ofInteger(punishStart));
            put("punish_duration", SQLData.ofInteger(punishDuration));
        }};
        this.changed = false;
        return sqlMap;
    }

    @Deprecated
    public boolean isChanged() {
        return changed;
    }

    @Deprecated
    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    @Deprecated
    // 10 seconds between last touch and save to not save all the time
    public boolean shouldSave() {
        return changed;
    }

    @Deprecated
    public boolean shouldRemove() {
        return !changed;
    }

    @Deprecated
    public static class Builder {
        private UUID uuid;
        private String name;
        private Rank rank = Rank.PLAYER;
        private Rank tempRank = Rank.PLAYER;
        private long tempRankUntil = 0L;
        private long balance = 0L;
        private long coin = 0L;
        private boolean vanished = false;
        private boolean glowing = false;
        private long firstLogin = System.currentTimeMillis();

        private PunishType punishType = PunishType.NO_PUNISH;
        private PunishReason punishReason = PunishReason.NO_REASON;
        private int punishNumber = 0;
        private String punishDescription = null;
        private UUID punishedBy = null;
        private long punishStart = 0;
        private long punishDuration = 0;

        private boolean setNew = false;

        private Builder() {
        }

        public Builder uuid(UUID uuid) {
            this.uuid = uuid;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder rank(Rank rank) {
            this.rank = rank;
            return this;
        }

        public Builder tempRank(Rank rank) {
            this.tempRank = rank;
            return this;
        }

        public Builder tempRankUntil(long tempRankUntil) {
            this.tempRankUntil = tempRankUntil;
            return this;
        }

        public Builder balance(long balance) {
            this.balance = balance;
            return this;
        }

        public Builder coin(long coin) {
            this.coin = coin;
            return this;
        }

        public Builder vanished(boolean vanished) {
            this.vanished = vanished;
            return this;
        }

        public Builder glowing(boolean glowing) {
            this.glowing = glowing;
            return this;
        }

        public Builder firstLogin(long firstLogin) {
            this.firstLogin = firstLogin;
            return this;
        }

        public Builder punishType(PunishType punishType) {
            this.punishType = punishType;
            return this;
        }

        public Builder punishReason(PunishReason punishReason) {
            this.punishReason = punishReason;
            return this;
        }

        public Builder punishNumber(int punishNumber) {
            this.punishNumber = punishNumber;
            return this;
        }

        public Builder punishDescription(String punishDescription) {
            this.punishDescription = punishDescription;
            return this;
        }

        public Builder punishedBy(UUID punishedBy) {
            this.punishedBy = punishedBy;
            return this;
        }

        public Builder punishStart(long punishStart) {
            this.punishStart = punishStart;
            return this;
        }

        public Builder punishDuration(long punishDuration) {
            this.punishDuration = punishDuration;
            return this;
        }

        public Builder setNew(boolean setNew) {
            this.setNew = setNew;
            return this;
        }

        @Deprecated
        public BLPlayer_legacy build() {
            if (name == null || uuid == null)
                throw new IllegalStateException("Cannot build a BLPlayer_legacy without setting name and uuid!");
            BLPlayer_legacy player = new BLPlayer_legacy(this);
            player.setChanged(setNew);
            return new BLPlayer_legacy(this);
        }
    }
}
