/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:10 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player;

import net.bluelapiz.common.BLLogger;
import net.bluelapiz.common.sql.SQLData;
import net.bluelapiz.common.sql.SQLManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class FriendManager {

    private static FriendManager friendManager;
    ConcurrentHashMap<UUID, LinkedList<UUID>> playerFriendsMap = new ConcurrentHashMap<>();

    private FriendManager() {
        SQLManager.getTableHelper().addTable("friend",
                "CREATE TABLE %table (" +
                        "   player1 CHAR(32) NOT NULL," +
                        "   player2 CHAR(32) NOT NULL" +
                        ");");
        SQLManager.getTableHelper().createTablesNotExisting();
    }

    public static FriendManager get() {
        if (friendManager == null) friendManager = new FriendManager();
        return friendManager;
    }

    public void loadFriendsOf(UUID uuid) {
        String sql = "SELECT player1 AS uuid FROM friend WHERE player2 = " + SQLData.ofUuid(uuid);
        sql += " UNION SELECT player2 AS uuid FROM friend WHERE player1 = " + SQLData.ofUuid(uuid);

        SQLManager.getSQLHelper().select(sql).whenComplete((r, ex) -> {
            if (ex != null) {
                BLLogger.commonLogger.severe("Error loading friends of " + uuid.toString() + "!\nError: " + ex.getMessage(), ex);
                ex.printStackTrace();
            } else {
                LinkedList<UUID> friendMap = new LinkedList<>();
                for (Map<String, Object> row : r) {
                    friendMap.add(SQLData.stringToUuid((String) row.get("uuid")));
                }
                playerFriendsMap.put(uuid, friendMap);
            }
        });
    }

    public boolean isFriends(UUID uuid1, UUID uuid2) {
        return getAllFriendsOf(uuid1).contains(uuid2) || getAllFriendsOf(uuid2).contains(uuid1);
    }

    public void unloadFriends(UUID uuid) {
        playerFriendsMap.remove(uuid);
    }

    public void createFriends(UUID uuid1, UUID uuid2) {
        if (isFriends(uuid1, uuid2)) return;

        getAllFriendsOf(uuid1).add(uuid2);
        getAllFriendsOf(uuid2).add(uuid1);

        SQLManager.getSQLHelper().insert("friend", new HashMap<String, SQLData>() {{
            put("player1", SQLData.ofUuid(uuid1));
            put("player2", SQLData.ofUuid(uuid2));
        }}, true).whenComplete((r, ex) -> {
            if (ex != null) {
                BLLogger.commonLogger.severe("Error trying to insert friends!\nError: " + ex.getMessage(), ex);
                ex.printStackTrace();
            } else {
                BLLogger.commonLogger.debug("Inserted " + r + " into friend!");
            }
        });
    }

    public void deleteFriends(UUID uuid1, UUID uuid2) {

        SQLData u1 = SQLData.ofUuid(uuid1);
        SQLData u2 = SQLData.ofUuid(uuid2);
        SQLData c1 = SQLData.ofKeyword("player1");
        SQLData c2 = SQLData.ofKeyword("player2");

        SQLManager.getSQLHelper().delete("friend", c1.equalsOther(u1).and(c2.equalsOther(u2)).or(c1.equalsOther(u2).and(c2.equalsOther(u1))))
                .whenComplete((r, ex) -> {
                    if (ex != null) {
                        BLLogger.commonLogger.severe("Could not delete friends " + uuid1.toString() + " " + uuid2.toString() + "\nError: " + ex.getMessage(), ex);
                        ex.printStackTrace();
                    } else {
                        BLLogger.commonLogger.debug("Deleted friends! " + uuid1.toString() + " " + uuid2.toString());
                    }
                });
    }

    public List<UUID> getAllFriendsOf(UUID uuid) {
        return playerFriendsMap.getOrDefault(uuid, new LinkedList<>());
    }
}
