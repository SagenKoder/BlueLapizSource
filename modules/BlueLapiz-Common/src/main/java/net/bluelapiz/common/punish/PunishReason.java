/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

import java.util.concurrent.TimeUnit;

public enum PunishReason {

    NO_REASON(false, "YOU HAVE BEEN GOOD <3"),

    ADVERTISING(
            "Advertising is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(14)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    BAD_BEHAVIOR_IN_CHAT("Bad behaviour in chat is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.DAYS.toMillis(3)),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.DAYS.toMillis(30))),

    BAD_BEHAVIOR_IN_GAME("Bad behaviour in game is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    BUG_ABUSE("Bug abuse is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    CROSS_TEAMING("Cross teaming is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(60))),

    GRIEF("Grief is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    HACK("Using hacks is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    IMPERSONATING("Impersonating is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    INAPPROPRIATE_NAME("Inappropriate name is not allowed! Change name and log back in!",
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN)),

    INAPPROPRIATE_SKIN("Inappropriate skin is not allowed! Change skin and log back in!",
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN)),

    MAP_ABUSE("Map abuse is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(1)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30))),

    PUNISHMENT_BYPASS("Bypassing punishment is not allowed!",
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN),
            new PunishTypeDuration(PunishType.PERMBAN)),

    RACISM("Racism or hate-speech is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    REPORT_ABUSE("Abuse of report functions is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.MINUTES.toMillis(15)),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.MINUTES.toMillis(30)),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.MINUTES.toMillis(60))),

    SPAM("Spamming, caps and flaming is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.DAYS.toMillis(1)),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.MUTE, TimeUnit.DAYS.toMillis(30))),

    SPAWN_KILLING("Spawn killing is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(1)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30))),

    TARGETING("Targeting is not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(1)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30))),

    X_RAY("X-ray mods and textures are not allowed!",
            new PunishTypeDuration(PunishType.KICK),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(7)),
            new PunishTypeDuration(PunishType.BAN, TimeUnit.DAYS.toMillis(30)),
            new PunishTypeDuration(PunishType.PERMBAN)),

    CUSTOM(false, "%customMessage");

    private boolean inPunishCommand = true;
    private String message;
    private PunishTypeDuration[] punishTypeDurations;

    PunishReason(String message, PunishTypeDuration... punishTypeDurations) {
        this.message = message;
        this.punishTypeDurations = punishTypeDurations;
    }

    PunishReason(boolean inPunishCommand, String message, PunishTypeDuration... punishTypeDurations) {
        this.inPunishCommand = inPunishCommand;
        this.message = message;
        this.punishTypeDurations = punishTypeDurations;
    }

    public boolean isInPunishCommand() {
        return inPunishCommand;
    }

    public String getMessage() {
        return message;
    }

    public PunishTypeDuration[] getPunishTypeDurations() {
        return punishTypeDurations;
    }

    public PunishTypeDuration getPunishTypeDuration(int number) {
        return punishTypeDurations[Math.min(punishTypeDurations.length - 1, number)];
    }

}
