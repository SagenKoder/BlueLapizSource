/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/26/18 2:40 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.punish;

public enum PunishType {

    NO_PUNISH(0, false, "§9§lBlue§f§lLapiz\n\n§aYou do not have any active punishments!"),
    KICK(1, false, "§9§lBlue§f§lLapiz\n\n§c§lYou were given a warning!\nReason: §f%reason %number"),
    BAN(4, false, "§9§lBlue§f§lLapiz\n\n§c§lYou are temporarily banned from this server!\nBanned since: §f%startTime\n§c§lTime until unban: §f%endTime\n§c§lReason: §f%reason %number"),
    MUTE(2, false, "§9§lBlue§f§lLapiz\n\n§c§lYou are temporarily muted from this server!\nMuted since: §f%startTime\n§c§lTime until unmute: §f%endTime\n§c§lReason: §f%reason %number"),
    PERMMUTE(3, true, "§9§lBlue§f§lLapiz\n\n§c§lYou are permanently muted on this server!\nMuted since: §f%startTime\n§c§lReason: §f%reason %number"),
    PERMBAN(5, true, "§9§lBlue§f§lLapiz\n\n§c§lYou are permanently banned from this server!\nBanned since: §f%startTime\n§c§lReason: §f%reason %number");

    private int priority;
    private boolean permanent;
    private String desciption;

    PunishType(int priority, boolean permanent, String desciption) {
        this.priority = priority;
        this.desciption = desciption;
        this.permanent = permanent;
    }

    public boolean isPermanent() {
        return permanent;
    }

    public String getDesciption() {
        return desciption;
    }

    public int getPriority() {
        return priority;
    }
}
