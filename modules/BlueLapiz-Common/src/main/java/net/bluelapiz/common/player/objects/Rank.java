/******************************************************************************
 * Copyright (C) BlueLapiz.net - All Rights Reserved                          *
 * Unauthorized copying of this file, via any medium is strictly prohibited   *
 * Proprietary and confidential                                               *
 * Last edited 11/27/18 2:10 PM                                               *
 * Written by Alexander Sagen <alexmsagen@gmail.com>                          *
 ******************************************************************************/

package net.bluelapiz.common.player.objects;

import net.bluelapiz.common.BLLogger;

import java.awt.*;

public enum Rank {

    PLAYER(100, "Player", "§f", "§7>", 'f', null),
    VIP(125, "Vip", "§5Vip §7| §5", "§7>§f", '5', null),
    BUILDER(150, "Builder", "§bBuilder §7| §b", "§7>§f", 'b', null),
    MOD_JR(200, "JrMod", "§3Jr Mod §7| §3", "§7>§f", '3', null),
    MOD(300, "Mod", "§9Mod §7| §9", "§7>§f", '9', null),
    MOD_SR(400, "SrMod", "§9Sr Mod §7| §9", "§7>§f", '9', null),
    ADMIN(500, "Admin", "§cAdmin §7| §c", "§7>§f", 'c', null),
    FOUNDER(600, "Founder", "§e§lFounder §7| §e", "§7>§f", 'e', null);

    private int level;
    private String name;
    private String prefix;
    private String suffix;
    private char colorChar;
    private String color;
    private Color discordColor;
    private String headPrefix;
    Rank(int level, String name, String prefix, String suffix, char color, String headPrefix) {
        this.level = level;
        this.name = name;
        this.prefix = prefix;
        this.suffix = suffix;
        this.colorChar = color;
        this.color = "§" + color;
        this.discordColor = getColor(this.color);
        this.headPrefix = (headPrefix == null) ? this.color : headPrefix;
    }

    public static Rank getRank(int id) {
        for (Rank r : values()) {
            if (r.getLevel() == id) {
                return r;
            }
        }
        BLLogger.commonLogger.severe("Could not find rank " + id + " !!!!", new Exception());
        return PLAYER;
    }

    public static Rank getRank(String name) {
        Rank rank = getRankOrNull(name);
        if (rank != null) return rank;
        return PLAYER;
    }

    public static Rank getRankOrNull(String name) {
        for (Rank r : values()) {
            String a = r.getName().toLowerCase().replaceAll("[^a-zA-Z0-9]", "");
            String b = name.toLowerCase().replaceAll("[^a-zA-Z0-9]", "");
            if (a.equalsIgnoreCase(b)) {
                return r;
            }
        }

        return null;
    }

    private static Color getColor(String color) {

        switch (color.toLowerCase().trim()) {
            case "§4":
                return new Color(0xbe0000);
            case "§c":
                return new Color(0xfe3f3f);
            case "§2":
                return new Color(0x00be00);
            case "§a":
                return new Color(0x3ffe3f);
            case "§b":
                return new Color(0x3ffefe);
            case "§3":
                return new Color(0x00bebe);
            case "§1":
                return new Color(0x0000be);
            case "§9":
                return new Color(0x3f3ffe);
            case "§d":
                return new Color(0xfe3ffe);
            case "§5":
                return new Color(0xbe00be);
            case "§f":
                return new Color(0xffffff);
            case "§7":
                return new Color(0xbebebe);
            case "§8":
                return new Color(0x3f3f3f);
            case "§0":
                return new Color(0x000000);
            case "§6":
                return new Color(0xffaa00);
            case "§e":
                return new Color(0xffff55);
        }

        return new Color(0xffffff);
    }

    public int getLevel() {
        return level;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public String getColor() {
        return color;
    }

    public char getColorChar() {
        return colorChar;
    }

    public Color getDiscordColor() {
        return discordColor;
    }

    public String getHeadPrefix() {
        return headPrefix;
    }

    public boolean isAtLeast(Rank rank) {
        return this.level >= rank.getLevel();
    }

}
